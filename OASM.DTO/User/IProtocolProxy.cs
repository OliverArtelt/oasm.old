﻿using System;


namespace comain.User
{
    
    public interface IProtocolProxy
    {
        
        /// <summary>
        /// Protokolleinträge der Benutzeraktivitäten geben
        /// </summary>                                        
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <param name="type">Filter Ereignistyp</param>
        /// <param name="logVon">Filter Eintrag von</param>
        /// <param name="logBis">Filter Eintrag bis</param>
        /// <param name="datVon">Filter Datum Termine/Realisierungen von</param>
        /// <param name="datBis">Filter Datum Termine/Realisierungen bis</param>
        /// <param name="user">Filter Benutzer</param>
        /// <param name="auCode">Filter Auftrag</param>
        Protocol Get(Credentials authinfo, 
                     int type, 
                     DateTime? logVon, DateTime? logBis, DateTime? datVon, DateTime? datBis,
                     String user, String auCode);
         
        /// <summary>
        /// Protokolleinträge löschen
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <param name="von">Zeitraum von</param>
        /// <param name="bis">Zeitraum bis</param>
        /// <returns>Anzahl gelöschter Sätze</returns>
        int DeleteEntries(Credentials authinfo, DateTime von, DateTime bis);             
    }
}

﻿using System;
using System.Runtime.Serialization;


namespace comain.User
{
   
    /// <summary>
    /// identifizierter Benutzer mit seinen Rechten geben
    /// </summary>
    public class GrantedUser
    {
    
        /// <summary>
        /// Id des Bearbeiters in der DB
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// hat Benutzer Administratorzugang?
        /// </summary>
        public bool IsAdmin { get; set; }
        /// <summary>
        /// Ist Benutzer Meisterzugriff des Kunden?
        /// </summary>
        public bool IsClientUser { get; set; }
        /// <summary>
        /// Name des Benutzers für Protokollierung
        /// </summary>
        public String Name { get; set; }
        /// <summary>
        /// zu filternde KST
        /// </summary>
        public String KstFilter { get; set; }
        /// <summary>
        /// darf Benutzer Planungen bearbeiten
        /// </summary>
        public bool MayPlan { get; set; }
        /// <summary>
        /// darf Benutzer Stunden sehen?
        /// </summary>
        public bool SeeHours { get; set; }
        /// <summary>
        /// darf Benutzer Preise sehen?
        /// </summary>
        public bool SeePrices { get; set; }
    }
}

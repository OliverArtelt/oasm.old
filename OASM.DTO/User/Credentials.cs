﻿using System;
using System.Runtime.Serialization;


namespace comain.User
{

    /// <summary>
    /// Authentifizierung beim Service
    /// </summary>
    public class Credentials
    {
    
        /// <summary>
        /// ist Benutzer Meister des Kunden (Client==true) oder Dienstleister?
        /// </summary>
        public bool IsClientUser { get; set; }
        /// <summary>
        /// zu verwendender Benutzername
        /// </summary>
        public String Username { get; set; }
        /// <summary>
        /// zu verwendendes Passwort
        /// </summary>
        public String Password { get; set; }
        /// <summary>
        /// zu verwendender Mandant/Datenbank
        /// </summary>
        public String Client { get; set; }
    }
}

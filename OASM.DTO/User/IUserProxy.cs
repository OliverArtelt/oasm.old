﻿using System;
using System.Collections.Generic;


namespace comain.User
{
   
    public interface IUserProxy
    {

        /// <summary>
        /// Benutzer erstellen/geben
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <returns></returns>
        GrantedUser Create(comain.User.Credentials authinfo);
        /// <summary>
        /// vorhandene Mandanten/Datenbanken geben
        /// </summary>
        /// <returns></returns>
        IEnumerable<String> Clients();
    }
}

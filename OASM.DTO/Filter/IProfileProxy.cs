﻿using System;
using comain.User;
using System.Collections.Generic;


namespace comain.Filter
{
    
    public interface IProfileProxy
    {

        /// <summary>
        /// Aufträge an ein Profil anfügen
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="profilId">Id des Profiles</param>
        /// <param name="auList">Liste der zu verwendenden Auftrag-Id's</param>
        void Add(Credentials authinfo, int profilId, IList<int> auList);
        /// <summary>
        /// Neues Profil anlegen
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="profilname">Name des Profiles</param>
        /// <param name="auList">Liste der zu verwendenden Auftrag-Id's</param>
        /// <returns>neue Profil-Id</returns>
        int Create(Credentials authinfo, String profilname, IList<int> auList);
        /// <summary>
        /// Profil löschen
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="profilId">Id des Profiles</param>
        void Delete(Credentials authinfo, int profilId);
        /// <summary>
        /// Aufträge des Profiles durch neue ersetzen
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="profilId">Id des Profiles</param>
        /// <param name="auList">Liste der zu verwendenden Auftrag-Id's</param>
        void Replace(Credentials authinfo, int profilId, IList<int> auList);
    }
}

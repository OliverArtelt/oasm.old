﻿using System;
using System.Collections.Generic;
using comain.Schedule;


namespace comain.Filter
{
    
    public interface IFacadesProxy
    {

        /// <summary>
        /// Auftragsliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        List<PicklistItem> AuList(comain.User.Credentials authinfo);
        /// <summary>
        /// Bereichsliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        List<PicklistItem> BereichList(comain.User.Credentials authinfo);
        /// <summary>
        /// IH-Objektliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        List<PicklistItem> IHList(comain.User.Credentials authinfo);
        /// <summary>
        /// Kostenstellenliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        List<PicklistItem> KstList(comain.User.Credentials authinfo);
        /// <summary>
        /// Leistungsarten als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        List<PicklistItem> LaList(comain.User.Credentials authinfo);
        /// <summary>
        /// Profilliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        List<PicklistItem> ProfilList(comain.User.Credentials authinfo);
        /// <summary>
        /// Standortliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        List<PicklistItem> PzList(comain.User.Credentials authinfo);
    }
}

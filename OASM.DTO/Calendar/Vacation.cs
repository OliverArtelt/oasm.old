﻿using System;
using System.Runtime.Serialization;


namespace comain.Calendar
{
    
    /// <summary>
    /// Betriebsferien
    /// </summary>
    public class Vacation
    {
        
        /// <summary>
        /// Startzeitpunkt
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// Endzeitpunkt
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// Name der Ferien
        /// </summary>
        public String Name { get; set; }
    }
}

﻿using System;


namespace comain.Calendar
{

    /// <summary>
    /// Bearbeiter modifiziert Exporteinstellungen für zukünftige Sessions
    /// </summary>
    public class BaseExportConfiguration
    {
        
        /// <summary>
        /// Pfad/Url Exportschablone Excel-Jahresplanung
        /// </summary>
        public String WIYearTemplatePath { get; set; }
        /// <summary>
        /// Pfad/Url Exportschablone Excel-Tagesplanung
        /// </summary>
        public String WIWeekTemplatePath { get; set; }
        /// <summary>
        /// erste Ausgabezeile Export Excel-Jahresplanung
        /// </summary>
        public int WIYearTemplateRow { get; set; }
        /// <summary>
        /// erste Ausgabezeile Export Excel-Tagesplanung
        /// </summary>
        public int WIWeekTemplateRow { get; set; }
    }
}

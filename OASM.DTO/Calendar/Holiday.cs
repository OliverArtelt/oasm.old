﻿using System;
using System.Runtime.Serialization;


namespace comain.Calendar
{
    
    /// <summary>
    /// gesetzlicher Feiertag
    /// </summary>
    public class Holiday
    {
        
        /// <summary>
        /// Zeitpunkt
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Name des Feiertags
        /// </summary>
        public String Name { get; set; }
    }
}

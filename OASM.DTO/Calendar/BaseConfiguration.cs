﻿using System;


namespace comain.Calendar
{
    
    /// <summary>
    /// Bearbeiter modifiziert Einstellungen für zukünftige Sessions
    /// </summary>
    public class BaseConfiguration
    {
    
        /// <summary>
        /// Grauzonenberechnungsmodus
        /// </summary>
        public enum UpdateMode { Inactive, FixDate, Dynamic }
        
        /// <summary>
        /// Operativberechnungsmodus ermitteln
        /// </summary>
        public UpdateMode OperType
        {
            get {
               
                if (OperDate.HasValue) return UpdateMode.FixDate;
                if (OperWeeks.HasValue) return UpdateMode.Dynamic;
                return UpdateMode.Inactive;
            }
        }
        
        /// <summary>
        /// Grauzonenberechnungsmodus ermitteln
        /// </summary>
        public UpdateMode GreyType
        {
            get {
               
                if (GreyDate.HasValue) return UpdateMode.FixDate;
                if (GreyWeeks.HasValue) return UpdateMode.Dynamic;
                return UpdateMode.Inactive;
            }
        }

 
        /// <summary>
        /// Jahr Terminplanung
        /// </summary>
        public Int32? PlanJahr { get; set; }
        /// <summary>
        /// Operative Planung, fester Termin
        /// </summary>
        public DateTime? OperDate { get; set; }
        /// <summary>
        /// Operative Planung, Wochen voraus
        /// </summary>        
        public Int32? OperWeeks { get; set; }
        /// <summary>
        /// Grauzonenplanung, fester Termin
        /// </summary>
        public DateTime? GreyDate { get; set; }
        /// <summary>
        /// Grauzonenplanung, Wochen zurück
        /// </summary>        
        public Int32? GreyWeeks { get; set; }
        /// <summary>
        /// Grauzplanungstyp (0: Terminsummen, 1: Termine und realisierte Summen)
        /// </summary>
        public Int32 GreySumType { get; set; }
    }
}

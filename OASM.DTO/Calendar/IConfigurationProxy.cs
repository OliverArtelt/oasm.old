﻿using System;


namespace comain.Calendar
{
    
    public interface IConfigurationProxy
    {
        
        /// <summary>
        /// Aktuell zu verwendende Konfiguration geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        Configuration ReadConfig(comain.User.Credentials authinfo);
        /// <summary>
        /// Konfigurationseinstellungen geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        BaseConfiguration ReadBaseConfig(comain.User.Credentials authinfo);
        /// <summary>
        /// Konfigurationseinstellungen schreiben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="cfg"></param>
        void WriteBaseConfig(comain.User.Credentials authinfo, BaseConfiguration cfg);
        /// <summary>
        /// Exporteinstellungen geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        BaseExportConfiguration ReadBaseExportConfig(comain.User.Credentials authinfo);
        /// <summary>
        /// Exporteinstellungen schreiben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="cfg"></param>
        void WriteBaseExportConfig(comain.User.Credentials authinfo, BaseExportConfiguration cfg);
    }
}

﻿using System;
using System.Collections.Generic;


namespace comain.Calendar
{
    
    public interface IHolidayProxy
    {
        
        /// <summary>
        /// Betriebsferien geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="von">Zeitraum von</param>
        /// <param name="bis">Zeitraum bis</param>
        /// <returns></returns>
        List<Vacation> GetVacationCloseDown(comain.User.Credentials authinfo, DateTime von, DateTime bis);
        /// <summary>
        /// Gesetzliche Feiertage geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="von">Zeitraum von</param>
        /// <param name="bis">Zeitraum bis</param>
        /// <returns></returns>
        List<Holiday> GetHoliday(comain.User.Credentials authinfo, DateTime von, DateTime bis);
    }
}

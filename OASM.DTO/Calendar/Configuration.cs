﻿using System;


namespace comain.Calendar
{
    
    /// <summary>
    /// in der aktuellen Session zu verwendende Konfiguration
    /// </summary>
    public class Configuration
    {
        
        /// <summary>
        /// Ist Grauzonenberechnung eingestellt?
        /// </summary>
        public bool UseGreyArea { get; set; }
        /// <summary>
        /// Jahr Terminplanung
        /// </summary>
        public Int32 PlanYear { get; set; }
        /// <summary>
        /// Operative Planung, fester Termin
        /// </summary>
        public DateTime OperDate { get; set; }
        /// <summary>
        /// Grauzonenplanung, fester Termin
        /// </summary>
        public DateTime GreyDate { get; set; }
        /// <summary>
        /// Grauzplanungstyp (0: Terminsummen, 1: Termine und realisierte Summen)
        /// </summary>
        public Int32 GreySumType { get; set; }        
        /// <summary>
        /// kleinstes planbares Datum
        /// </summary>
        public DateTime MinDate { get; set; }
        /// <summary>
        /// größtes planbares Datum
        /// </summary>
        public DateTime MaxDate { get; set; }
        /// <summary>
        /// Konfiguration als Freitext für Exports
        /// </summary>
        public String AsText { get; set; }
        /// <summary>
        /// Pfad/Url Exportschablone Excel-Jahresplanung
        /// </summary>
        public String WIYearTemplatePath { get; set; }
        /// <summary>
        /// Pfad/Url Exportschablone Excel-Tagesplanung
        /// </summary>
        public String WIWeekTemplatePath { get; set; }
        /// <summary>
        /// erste Ausgabezeile Export Excel-Jahresplanung
        /// </summary>
        public int WIYearTemplateRow { get; set; }
        /// <summary>
        /// erste Ausgabezeile Export Excel-Tagesplanung
        /// </summary>
        public int WIWeekTemplateRow { get; set; }
    }
}

﻿using System;


namespace comain.Schedule
{
    
    public class ModifiedDate
    {
    
        /// <summary>
        /// Bearbeitungsstand des Termines (gelöscht, realisiert, ...)
        /// </summary>
        public EditedType EditedType { get; set; }
        
        /// <summary>
        /// tblautermin.idautermin des zugrundeliegenden Termins
        /// </summary>
        public int PlannedId { get; set; }
        /// <summary>
        /// tblrealzyklus.idrealzyklus der zugrundeliegenden Realisierung
        /// </summary>
        public int ImplementedId { get; set; }
        /// <summary>
        /// tblps.psid des ausführenden Personals
        /// </summary>
        public int PersonalId { get; set; }
        /// <summary>
        /// tbl_auftrag.auid des Auftrages
        /// </summary>
        public int AuftragId { get; set; }

        /// <summary>
        /// geplante/realisierte Menge
        /// </summary>
        public short EditedMenge { get; set; }
        /// <summary>
        /// geplanter Termin
        /// </summary>
        public DateTime PlannedDate { get; set; }
        /// <summary>
        /// frühestmögliches Datum der Realisierung, wenn keine Planung vorliegt
        /// </summary>
        public DateTime FirstDate { get; set; }        
    }
}

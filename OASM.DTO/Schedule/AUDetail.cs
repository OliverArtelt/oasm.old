﻿using System;
using System.Runtime.Serialization;


namespace comain.Schedule
{
    
    public class AUDetail : IEquatable<AUDetail>
    {
    
        /// <summary>
        /// DB-Spalte auid
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// DB-Spalte aucode
        /// </summary>
        public String Code { get; set; } 
        /// <summary>
        /// DB-Spalte aucodeextern
        /// </summary>
        public String CodeExtern { get; set; } 
        /// <summary>
        /// DB-Spalte aukurzb
        /// </summary>
        public String Kurzbeschreibung { get; set; } 
        /// <summary>
        /// DB-Spalte auzyklus
        /// </summary>
        public short Zyklus { get; set; } 
        /// <summary>
        /// kundenspezifische Interpretation der DB-Spalte auzyklus
        /// </summary>
        public String ZyklusAsString { get; set; } 
        /// <summary>
        /// DB-Spalte auprowoche
        /// </summary>
        public float ProWoche { get; set; } 
        /// <summary>
        /// DB-Spalte 
        /// </summary>
        public short Menge { get; set; } 
        /// <summary>
        /// DB-Spalte 
        /// </summary>
        public Decimal Wert { get; set; } 
        /// <summary>
        /// DB-Spalte 
        /// </summary>
        public float Stunden { get; set; }

        
        public bool Equals(AUDetail other)
        {
            return this.Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            
            AUDetail other = obj as AUDetail;
            if (obj == null) return false;
            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}

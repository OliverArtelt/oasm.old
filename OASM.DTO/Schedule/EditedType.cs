﻿using System;


namespace comain.Schedule
{

        /// <summary>
        /// Termintypen abh. vom Bearbeitungsstand
        /// </summary>
        public enum EditedType : byte 
        {

            /// <summary>
            /// Termin ist leer oder Bearbeitung verworfen
            /// </summary>
            Empty = 0,
            /// <summary>
            /// Bereits bestehende Terminplanung oder -realisierung wurde gelöscht
            /// </summary>
            Deleted = 1,
            /// <summary>
            /// Planung: Termin ist geplant
            /// </summary>
            Planned = 2,
            /// <summary>
            /// Realisierung: Termin ist normal realisiert
            /// </summary>
            Common = 3,
            /// <summary>
            /// Realisierung: Termin ist als Sonderleistung realisiert
            /// </summary>
            Special = 4,
            /// <summary>
            /// Planung: Termin ist als Tagesauftrag geplant
            /// </summary>
            Daily = 5
        }
}

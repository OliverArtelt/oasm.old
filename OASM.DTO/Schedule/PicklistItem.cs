﻿using System;
using System.Runtime.Serialization;


namespace comain.Schedule
{

    public class PicklistItem
    {
    
        /// <summary>
        /// Schlüssel der Entitaet
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// erster anzugebender Wert
        /// </summary>
        public String Value1 { get; set; }
        /// <summary>
        /// zweiter Wert: 
        /// auszugebender Text zweite Listbox oder Filterkriterium bei abh. Listboxen
        /// </summary>
        public String Value2 { get; set; }


        public override string ToString()
        {
            return Value1;
        }
    }
}

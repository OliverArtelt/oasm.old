﻿using System;
using System.Runtime.Serialization;


namespace comain.Schedule
{
    
    public class Planung
    {
            
        /// <summary>
        /// tblautermin.idautermin: Id des Termines
        /// </summary>
        public int Id { get; set; }
        
        public int Auftrag { get; set; }
        /// <summary>
        /// tblautermin.attermin
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// tblautermin.atfixed
        /// </summary>
        public bool Fixed { get; set; }
        /// <summary>
        /// tblautermin.atstatus
        /// </summary>
        public short Status { get; set; }
        /// <summary>
        /// tblautermin.atmenge
        /// </summary>
        public short Menge { get; set; }
        /// <summary>
        /// tblautermin.atwert
        /// </summary>
        public decimal Wert { get; set; }
        /// <summary>
        /// tblautermin.atstunden
        /// </summary>
        public float Stunden { get;  set; }
    }
}

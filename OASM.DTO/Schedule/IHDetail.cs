﻿using System;
using System.Runtime.Serialization;


namespace comain.Schedule
{
    
    public class IHDetail : IEquatable<IHDetail>
    {
    
        /// <summary>
        /// DB-Spalte ihid
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// DB-Spalte ihnr
        /// </summary>
        public String Nummer { get; set; } 
        /// <summary>
        /// DB-Spalte ihinventnr
        /// </summary>
        public String InventarNummer { get; set; } 
        /// <summary>
        /// DB-Spalte ihname
        /// </summary>
        public String Name { get; set; } 

        
        public bool Equals(IHDetail other)
        {
            return this.Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            
            IHDetail other = obj as IHDetail;
            if (obj == null) return false;
            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}

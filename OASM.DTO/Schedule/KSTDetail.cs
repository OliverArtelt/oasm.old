﻿using System;
using System.Runtime.Serialization;


namespace comain.Schedule
{
    
    public class KSTDetail : IEquatable<KSTDetail>
    {
    
        /// <summary>
        /// DB-Spalte id_kst
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// DB-Spalte kstnr
        /// </summary>
        public String Nummer { get; set; } 

        
        public bool Equals(KSTDetail other)
        {
            return this.Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            
            KSTDetail other = obj as KSTDetail;
            if (obj == null) return false;
            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace comain.Schedule
{
    
    public class Leistung
    {
       
        public int IhIdBgr      { get; set; }
        public int IhIdMo       { get; set; }
        public int PzId         { get; set; }
        public int KstId        { get; set; }
        public int AuId         { get; set; }
        public int PsId         { get; set; }
        public int BereichId    { get; set; }


        public IList<Planung> Planungen { get; set; }


        public IList<Realisierung> Realisierungen { get; set; }

        
        public bool IstRelevant
        {

            get {

                return Realisierungen.Count + Planungen.Count > 0;     
            }
        }
    }
}

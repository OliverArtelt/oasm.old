﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace comain.Schedule
{
   
    /// <summary>
    /// Schedule als DataTransferObjekt, sich wiederholende Zellenwerte in Fliegengewichte kapseln
    /// </summary>
    public class ScheduleDTO
    {
                                              
        /// <summary>
        /// Tuple mit den Ids
        /// </summary>                                                                 
        public List<Leistung> Leistungen { get; set; }
        /// <summary>
        /// Fliegengewicht-Lookup Aufträge
        /// </summary>
        public Dictionary<int, AUDetail> AuftragInfo { get; set; }
        /// <summary>
        /// Fliegengewicht-Lookup Bereich/Auftraggeber
        /// </summary>
        public Dictionary<int, BereichDetail> BereichInfo { get; set; }
        /// <summary>
        /// Fliegengewicht-Lookup IH-Objekte
        /// </summary>
        public Dictionary<int, IHDetail> IHObjektInfo { get; set; }
        /// <summary>
        /// Fliegengewicht-Lookup Kostenstellen
        /// </summary>
        public Dictionary<int, KSTDetail> KostenstellenInfo { get; set; }
        /// <summary>
        /// Fliegengewicht-Lookup Personal
        /// </summary>
        public Dictionary<int, PSDetail> PersonalInfo { get; set; }
        /// <summary>
        /// Fliegengewicht-Lookup Standorte
        /// </summary>
        public Dictionary<int, PZDetail> StandortInfo { get; set; }    
    }
}

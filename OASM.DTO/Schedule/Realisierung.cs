﻿using System;
using System.Runtime.Serialization;


namespace comain.Schedule
{
    
    public class Realisierung : IComparable<Realisierung>, IEquatable<Realisierung>
    {
            
        /// <summary>
        /// tblrealzyklus.id_realzyklus
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// tblrealzyklus.key_au
        /// </summary>
        public int Auftrag { get; set; }
        /// <summary>
        /// Realisierungsdatum als Integer kodiert, um es in der Terminliste zuordnen zu können
        /// (besitzt diesen int als Zugriffsschlüssel)
        /// </summary>
        public int Key { get; set; }
        /// <summary>
        /// tblrealzyklus.aurealzyklus
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// tblrealzyklus.rztyp != 0 => Sonderleistung
        /// </summary>
        public bool Sonder { get; set; }
        /// <summary>
        /// tblrealzyklus.rzmenge
        /// </summary>
        public short Menge { get; set; }
        /// <summary>
        /// tblrealzyklus.rzwert
        /// </summary>
        public Decimal Wert { get; set; }
        /// <summary>
        /// tblrealzyklus.rzstunden
        /// </summary>
        public float Stunden { get;  set; }

        
        /// <summary>
        /// Vergleicher für IList.Sort(); nach Aufträgen und Realisierungen sortieren
        /// </summary>
        /// <param name="other">zu vergleichende Realisierung</param>
        /// <returns>Sortierstatus</returns>
        public int CompareTo(Realisierung other)
        {
            if (this.Auftrag < other.Auftrag) return -1;
            if (this.Auftrag == other.Auftrag && this.Id < other.Id) return -1;
            if (this.Auftrag > other.Auftrag) return 1;
            if (this.Auftrag == other.Auftrag && this.Id > other.Id) return 1;
            return 0;
        }

        /// <summary>
        /// Vergleicher für IList.Find(); nach Realisierungen suchen
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(Realisierung other)
        {
            return this.Id == other.Id;
        }
    }
}

﻿using System;
using System.Runtime.Serialization;


namespace comain.Schedule
{
    
    public class PSDetail : IEquatable<PSDetail>
    {
    
        /// <summary>
        /// DB-Spalte psid
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// DB-Spalte psnr
        /// </summary>
        public String Nummer { get; set; } 
        /// <summary>
        /// DB-Spalten psname + psvorname
        /// </summary>
        public String Name { get; set; } 

        
        public bool Equals(PSDetail other)
        {
            return this.Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            
            PSDetail other = obj as PSDetail;
            if (obj == null) return false;
            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}

﻿using System;
using System.Runtime.Serialization;


namespace comain.Schedule
{
    
    public class PZDetail : IEquatable<PZDetail>
    {
    
        /// <summary>
        /// DB-Spalte pzid
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// DB-Spalte pzkey
        /// </summary>
        public String Key { get; set; } 
        /// <summary>
        /// DB-Spalte pznr
        /// </summary>
        public String Nummer { get; set; } 
        /// <summary>
        /// DB-Spalte pzname
        /// </summary>
        public String Name { get; set; } 

        
        public bool Equals(PZDetail other)
        {
            return this.Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            
            PZDetail other = obj as PZDetail;
            if (obj == null) return false;
            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}

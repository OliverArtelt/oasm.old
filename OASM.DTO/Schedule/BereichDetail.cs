﻿using System;
using System.Runtime.Serialization;


namespace comain.Schedule
{
    
    public class BereichDetail : IEquatable<BereichDetail>
    {
    
        /// <summary>
        /// DB-Spalte id_auftgb
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// DB-Spalte auauftraggb
        /// </summary>
        public String Name { get; set; } 

        
        public bool Equals(BereichDetail other)
        {
            return this.Id == other.Id;
        }

        public override bool Equals(object obj)
        {
            
            BereichDetail other = obj as BereichDetail;
            if (obj == null) return false;
            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}

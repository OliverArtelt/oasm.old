﻿using System;
using System.Collections.Generic;


namespace comain.Schedule
{
    
    public interface IPlanProxy
    {
        
        /// <summary>
        /// Schedule geben
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <param name="config">Filtereinstellungen</param>
        /// <returns></returns>
        ScheduleDTO GetPlan(comain.User.Credentials authinfo, Dictionary<String, List<String>> config);
        /// <summary>
        /// Schedule aktualisieren
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <param name="modified">geänderte Plandaten</param>
        void UpdatePlan(comain.User.Credentials authinfo, IEnumerable<ModifiedDate> modified);
    }
}

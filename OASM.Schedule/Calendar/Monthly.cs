﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace comain.Calendar
{
    
    public class Monthly : ICalendarStrategy
    {
        
        int myYear;
        List<int> list;
        int? currentKey;

        
        public Monthly(int year)
        {
        
            myYear = year;
            list = new List<int>(12);
            for (int i = 1; i <= 12; i++) list.Add(myYear * 100 + i);
            
            if (DateTime.Now.Year != myYear) currentKey = null;
            else                             currentKey = CalculateCalendarKey(DateTime.Now);
        }
        
        
        public DateTime DateVon
        {
            get { return new DateTime(myYear, 1, 1); }
        }


        public DateTime DateBis
        {
            get { return new DateTime(myYear, 12, 31); }
        }


        public IList<int> CalendarKeys
        {
            get { return list; }
        }


        public int CalculateCalendarKey(DateTime date)
        {
            return date.Year * 100 + date.Month;
        }


        public DateTime CalculateFirstDate(int key)
        {
            return new DateTime(key / 100, key % 100, 1);
        }

 
        public DateTime CalculateLastDate(int key)
        {
            return CalculateFirstDate(key).AddMonths(1).AddDays(-1);    
        }


        public string FormatKeyShort(int key)
        {
            return (key % 100).ToString("D02");
        }


        public string FormatKeyLong(int key)
        {
            return "Monat " + (key % 100).ToString("D02") + "." + (key / 100).ToString("D04");
        }


        public IDictionary<int, string> Holidays
        {
            get { return new Dictionary<int, string>(); }
        }

        public int? CurrentKey
        {
            get { return currentKey; }
        }

        public bool HasGreaterKeys
        {
            get { return false; }
        }

        public string FormatGreaterKey(int key)
        {
            return String.Empty;
        }
    }
}

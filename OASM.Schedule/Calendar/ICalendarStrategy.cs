﻿using System;
using System.Collections.Generic;


namespace comain.Calendar
{
    
    
    /// <summary>
    /// Zeitraum des Schedules ermitteln
    /// </summary>
    public interface ICalendarStrategy
    {
        
        /// <summary>
        /// Startpunkt Jahresplan
        /// </summary>
        /// <returns>Erster Tag des Jahresplanes</returns>
        DateTime DateVon { get; }
        /// <summary>
        /// Endpunkt Jahresplan
        /// </summary>
        /// <returns>Letzter Tag des Jahresplanes</returns>
        DateTime DateBis { get; }
        /// <summary>
        /// Liste der Terminslots
        /// </summary>
        /// <returns>Liste der Terminslots, sortiert</returns>
        IList<int> CalendarKeys { get; }
        /// <summary>
        /// Funktion zum Berechnen des Terminslotschlüssels in seiner Auftragsliste
        /// </summary>
        /// <param name="date">zu indizierendes Datum</param>
        /// <returns>Schlüssel</returns>
        int CalculateCalendarKey(DateTime date);
        /// <summary>
        /// Ersten Zeitpunkt, den der Schlüssel repräsentiert, ausgeben.
        /// </summary>
        /// <remarks>
        /// Wichtig für Jahresplanung:
        /// erfolgt in KW, Termine werden als Datum gespeichert
        /// => Montag dieser Woche übergeben
        /// </remarks>
        /// <param name="key">Terminslotschlüssel</param>
        /// <returns>Erstes verfügbare Datum dieses Terminslotschlüssels</returns>
        DateTime CalculateFirstDate(int key);
        /// <summary>
        /// Letzten Zeitpunkt, den der Schlüssel repräsentiert, ausgeben.
        /// </summary>
        /// <param name="key">Terminslotschlüssel</param>
        /// <returns>Letztes verfügbare Datum dieses Terminslotschlüssels</returns>
        DateTime CalculateLastDate(int key);
        /// <summary>
        /// Schlüssel benutzerfreundlich ausgeben
        /// </summary>
        /// <param name="key">anzuzeigender Kalenderschlüssel</param>
        /// <returns>Formatierte Ausgabe</returns>
        String FormatKeyShort(int key);
        /// <summary>
        /// Schlüssel benutzerfreundlich ausgeben (ausführlich für Berichte)
        /// </summary>
        /// <param name="key">anzuzeigender Kalenderschlüssel</param>
        /// <returns>Formatierte Ausgabe</returns>
        String FormatKeyLong(int key);
        /// <summary>
        /// Freie Arbeitstage speziell kennzeichnen (Terminslotschlüssel, Bezeichnung)
        /// </summary>
        IDictionary<int, String> Holidays { get; }
        /// <summary>
        /// Schlüssel des heutigen Datums geben oder null wenn kein solcher Schlüssel existiert
        /// </summary>
        int? CurrentKey { get; }
        /// <summary>
        /// besitzen Kalenderkeys eine übergeordnete Einteilung (z.B. Quartal I - IV)?
        /// </summary>
        bool HasGreaterKeys { get; }
        /// <summary>
        /// Übergeordneten Schlüssel geben oder Leerstring wenn keiner existiert/unpassend 
        /// </summary>
        /// <param name="key">anzuzeigender Kalenderschlüssel</param>
        /// <returns>Formatierte Ausgabe</returns>
        String FormatGreaterKey(int key);
    }
}

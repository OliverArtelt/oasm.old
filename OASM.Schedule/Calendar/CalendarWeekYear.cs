﻿using System;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using comain.User;


namespace comain.Calendar
{
    
    /// <summary>
    /// Jahresplanung Schule Süd: Jahr geht von KW 1 bis KW 52/53
    /// </summary>
    public class CalendarWeekYear : ICalendarStrategy
    {
        
        List<int> list;
        SortedList<int, String> holidays;
        CultureInfo ci;
        int? currentKey;

        
        /// <summary>
        /// Jahresplanung Schule Süd: Jahr geht von KW 1 bis KW 52/53
        /// </summary>
        /// <param name="year">zu planendes Jahr</param>
        public CalendarWeekYear(int year, IUser user, CultureInfo ci_)
        {
            
            ci = ci_;
            
            DateVon = new DateTime(year, 1, 1);

            //zum Montag hangeln
            while (ci.Calendar.GetDayOfWeek(DateVon) != DayOfWeek.Monday) DateVon = DateVon.AddDays(-1);
            
            //Überhang berücksichtigen
            if (DateVon.WeekOfYear(ci) > 50) DateVon = DateVon.AddDays(7);

            DateBis = new DateTime(year, 12, 31);

            //zum Sonntag hangeln
            while (ci.Calendar.GetDayOfWeek(DateBis) != DayOfWeek.Sunday) DateBis = DateBis.AddDays(1);
            
            //Überhang berücksichtigen
            if (DateBis.WeekOfYear(ci) < 50) DateBis = DateBis.AddDays(-7);
            
            //Terminslotschlüssel
            CalculateKeys(year);

            //Betriebsferien
            holidays = new SortedList<int,string>();
            NonWorkingPeriod h = new NonWorkingPeriod(user, DateVon, DateBis);
            
            foreach (var entry in h.GetVacationCloseDown()) {

                for (DateTime t = entry.StartDate; t < entry.EndDate; t = t.AddDays(7)) {
                
                    holidays[CalculateCalendarKey(t)] = entry.Name;    
                }
                
                holidays[CalculateCalendarKey(entry.EndDate)] = entry.Name;    
            }            
 
            if (DateTime.Today < DateVon || DateTime.Today > DateBis) currentKey = null;
            else                                                      currentKey = CalculateCalendarKey(DateTime.Today);
       }
        
        /// <summary>
        /// Startzeitpunkt übergeben
        /// </summary>
        public DateTime DateVon { get; private set; }   
        /// <summary>
        /// Endzeitpunkt übergeben
        /// </summary>
        public DateTime DateBis { get; private set; }   

        /// <summary>
        /// Liste der Terminslotschlüssel berechnen
        /// </summary>
        /// <remarks>
        /// z.B. 200801, 200802, ... => KW 01 Jahr 2008, KW 02 Jahr 2008 ... 
        /// </remarks>
        private void CalculateKeys(int year) 
        {
        
            int v = DateVon.WeekWithYear(ci);    
            int b = DateBis.WeekWithYear(ci);
            
            list = new List<int>(b - v + 1);
            //KW immer von 1 bis 52/53 => einfach iterieren
            for (int i = v; i <= b; ++i) list.Add(i);
        }

        /// <summary>
        /// Terminslotschlüssel eines Datums in der Form yyyyww berechnen (Jahr + KW)
        /// </summary>
        /// <param name="date">Ausgangsdatum</param>
        /// <returns>zu berechnender Schlüssel</returns>
        public int CalculateCalendarKey(DateTime date) 
        {
            return date.WeekWithYear(ci);
        } 

        /// <summary>
        /// Zeitschlüsselliste übergeben
        /// </summary>
        public IList<int> CalendarKeys
        {
            get { return list; }
        }
        
        /// <summary>
        /// Stringrepräsentation für Terminslotschlüssel übergeben (hier nur KW aus Platzgründen anzeigen)
        /// </summary>
        /// <param name="key">zu formattierender Schlüssel</param>
        /// <returns>Schlüssel im Anzeigeformat (hier nur KW aus Platzgründen anzeigen)</returns>
        public String FormatKeyShort(int key)
        {
            return (key % 100).ToString("D02");
        }

        /// <summary>
        /// Stringrepräsentation für Zeitschlüssel übergeben (ausführlich für Berichte)
        /// </summary>
        /// <param name="key">zu formattierender Schlüssel</param>
        /// <returns>Schlüssel im Anzeigeformat</returns>
        public String FormatKeyLong(int key)
        {
            
            String s = "KW " + (key % 100).ToString("D02");
            
            DateTime t1 = DateTimeExtended.FirstDateOfCalendarWeek(key / 100, key % 100, ci);
            if (t1 < DateVon) t1 = DateVon;
            DateTime t2 = t1.AddDays(6);
            if (t2 > DateBis) t2 = DateBis;
            
            s += t1.ToString(": dd.MM-");
            s += t2.ToString("dd.MM.yy");            
           
            return s; 
        }

        /// <summary>
        /// Ersten Zeitpunkt, den der Schlüssel repräsentiert, ausgeben
        /// </summary>
        /// <param name="key">Zeitraumschlüssel</param>
        /// <returns>repräsentierendes Datum</returns>
        public DateTime CalculateFirstDate(int key)
        {
            return DateTimeExtended.FirstDateOfCalendarWeek(key / 100, key % 100, ci);    
        }
 
        public DateTime CalculateLastDate(int key)
        {
            return DateTimeExtended.FirstDateOfCalendarWeek(key / 100, key % 100, ci).AddDays(6);    
        }

        /// <summary>
        /// Betriebsferien ermitteln
        /// </summary>
        public IDictionary<int, string> Holidays
        {
            get { return holidays; }
        }
        
        public int? CurrentKey
        {
            get { return currentKey; }
        }

        public bool HasGreaterKeys
        {
            get { return false; }
        }

        public string FormatGreaterKey(int key)
        {
            return String.Empty;
        }
    }
}

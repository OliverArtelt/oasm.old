﻿using System;
using System.Collections.Generic;
using comain.User;


namespace comain.Calendar
{
    
    /// <summary>
    /// Betriebsferien etc ermitteln
    /// </summary>
    class NonWorkingPeriod
    {
        
        /// <summary>
        /// Startdatum Betrachtungszeitraum
        /// </summary>
        readonly DateTime von;    
        /// <summary>
        /// Enddatum Betrachtungszeitraum
        /// </summary>
        readonly DateTime bis; 
        
        IUser myUser;
           
    
        /// <summary>
        /// Betriebsferien etc ermitteln
        /// </summary>
        /// <param name="v">Startdatum</param>
        /// <param name="b">Enddatum</param>
        public NonWorkingPeriod(IUser user, DateTime v, DateTime b)
        {
        
            von = v;
            bis = b;
            myUser = user;
        }
        
        
        /// <summary>
        /// Betriebsferien ermitteln
        /// </summary>
        /// <returns>Betriebsferien</returns>
        public IList<Vacation> GetVacationCloseDown()
        {
        
            //var proxy = new HolidayProxy();
            return null;
        }
        
        
        /// <summary>
        /// Feiertage ermitteln
        /// </summary>
        /// <returns>Feiertage</returns>
        public IList<Holiday> GetHoliday()
        {
        
            //var proxy = new HolidayProxy();
            return null;
        }
    }
}

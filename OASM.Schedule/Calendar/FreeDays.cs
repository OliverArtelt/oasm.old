﻿using System;
using System.Data;
using System.Globalization;
using System.Collections.Generic;
using comain.User;


namespace comain.Calendar
{
    
    /// <summary>
    /// Freier Zeitraum, tagesgenaue Planung
    /// </summary>
    public class FreeDays : ICalendarStrategy
    {
        
        List<int> list;
        SortedList<int, String> holidays;
        CultureInfo ci;
        int? currentKey;


        /// <summary>
        /// Freier Zeitraum, tagesgenaue Planung der letzten 7 Tage
        /// </summary>
        public FreeDays(IUser user, CultureInfo ci_) : 
            this(DateTime.Today.AddDays(-7), DateTime.Today, user, ci_) {}
        
        /// <summary>
        /// Freier Zeitraum, tagesgenaue Planung von einem Zeitpunkt bis heute
        /// </summary>
        /// <param name="von">Starttag, Endtag ist heute</param>
        public FreeDays(DateTime von, IUser user, CultureInfo ci_) : 
            this(von, DateTime.Today, user, ci_) {}
        
        /// <summary>
        /// Freier Zeitraum, tagesgenaue Planung 
        /// </summary>
        /// <param name="von">Starttag</param>
        /// <param name="bis">Endtag</param>
        public FreeDays(DateTime von, DateTime bis, IUser user, CultureInfo ci_)
        {            
            
            DateVon = von.Date;
            DateBis = bis.Date;
            ci = ci_;
            
            if (DateVon > DateBis) throw new ApplicationException("Das Ende des Zeitraumes darf nicht älter als dessen Start sein.");
            
            //Terminslotschlüssel
            CalculateKeys();
            
            //Freie Tage
            holidays = new SortedList<int,string>();
            for (DateTime t = DateVon; t <= DateBis; t = t.AddDays(1))             
                if (t.DayOfWeek == DayOfWeek.Saturday || t.DayOfWeek == DayOfWeek.Sunday) 
                    holidays[CalculateCalendarKey(t)] = "Wochenende";
                
            //Feiertage
            NonWorkingPeriod h = new NonWorkingPeriod(user, DateVon, DateBis);
            
            foreach (var entry in h.GetHoliday())
                holidays[CalculateCalendarKey(entry.Date)] = entry.Name;                

            //Betriebsferien
            foreach (var entry in h.GetVacationCloseDown()) {

                for (DateTime t = entry.StartDate; t <= entry.EndDate; t = t.AddDays(1)) 
                    holidays[CalculateCalendarKey(t)] = entry.Name;    
            } 
            
            if (DateTime.Today < DateVon || DateTime.Today > DateBis) currentKey = null;
            else                                                      currentKey = CalculateCalendarKey(DateTime.Today);
                       
        }

        /// <summary>
        /// Startzeitpunkt übergeben
        /// </summary>
        public DateTime DateVon { get; private set; } 
        /// <summary>
        /// Endzeitpunkt übergeben
        /// </summary>
        public DateTime DateBis { get; private set; } 

        /// <summary>
        /// Liste der Terminslotschlüssel berechnen
        /// </summary>
        /// <remarks>
        /// z.B. 20080106, 20080107, ... => 06.01.2008, 07.01.2008 ... 
        /// </remarks>
        private void CalculateKeys()
        {
            
            DateTime t = DateVon;
            list = new List<int>();
            
            while (t <= DateBis) {
            
                //einfach für jeden Tag seinen Terminslotschlüssel berechnen und eintragen
                list.Add(CalculateCalendarKey(t));
                t = t.AddDays(1);
            }
        }

        /// <summary>
        /// Terminslotschlüssel eines Datums in der Form yyyymmdd berechnen (Jahr + Monat + Tag)
        /// </summary>
        /// <param name="date">Ausgangsdatum</param>
        /// <returns>zu berechnender Schlüssel</returns>
        public int CalculateCalendarKey(DateTime date) 
        {
            return date.Year * 10000 + date.Month * 100 + date.Day;
        }

        /// <summary>
        /// Zeitschlüsselliste übergeben
        /// </summary>
        public IList<int> CalendarKeys
        {
            get { return list; }
        }
        
        /// <summary>
        /// Stringrepräsentation für Terminslotschlüssel übergeben 
        /// </summary>
        /// <remarks>
        /// z.B. "Fr. 26.03.", Patz ist vorhanden, da normalerweise nur 7 Tage angezeigt werden
        /// </remarks>
        /// <param name="key">zu formattierender Schlüssel</param>
        /// <returns>Schlüssel im Anzeigeformat</returns>
        public String FormatKeyShort(int key)
        {
            //Datum aus Zeitschlüssel wieder zusammenbauen, um Wochentag zu erhalten
            DateTime dat = new DateTime(key / 10000, key / 100 % 100, key % 100);
            return dat.ToString("ddd dd.MM.", ci);
        }

        /// <summary>
        /// Stringrepräsentation für Zeitschlüssel übergeben (ausführlich für Berichte)
        /// </summary>
        /// <param name="key">zu formattierender Schlüssel</param>
        /// <returns>Schlüssel im Anzeigeformat</returns>
        public string FormatKeyLong(int key)
        {
            //Datum aus Zeitschlüssel wieder zusammenbauen, um Wochentag zu erhalten
            DateTime d = new DateTime(key / 10000, key / 100 % 100, key % 100);
            return d.ToString("dd.MM.yyyy ddd.", ci);          
        }

        /// <summary>
        /// Ersten Zeitpunkt, den der Schlüssel repräsentiert, ausgeben
        /// </summary>
        /// <param name="key">Zeitraumschlüssel</param>
        /// <returns>repräsentierendes Datum</returns>
        public DateTime CalculateFirstDate(int key)
        {
            return new DateTime(key / 10000, key / 100 % 100, key % 100);
        }

        public DateTime CalculateLastDate(int key)
        {
            return CalculateFirstDate(key);
        }

        /// <summary>
        /// Feiertage/Wochenenden ermitteln
        /// </summary>
        public IDictionary<int, string> Holidays
        {
            get { return holidays; }
        }

        public int? CurrentKey
        {
            get { return currentKey; }
        }

        public bool HasGreaterKeys
        {
            get { return true; }
        }

        public string FormatGreaterKey(int key)
        {
	      
            DateTime dt = CalculateFirstDate(key);
            if (dt.DayOfWeek != DayOfWeek.Monday) return String.Empty;

            return dt.WeekOfYear(ci).ToString();
        }
    }
}

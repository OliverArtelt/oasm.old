﻿using System;


namespace comain.Statistics
{

    /// <summary>
    /// Eintrag in Gesamtstatistik des Schedules
    /// </summary>
    public class TotalEntry
    {

        public enum EntryType
        {

            Default = 0,
            DeltaPositive,
            DeltaNegative,
            DeltaUnspecified
        }
    

        public String       Name        { get; set; }
                                                    
        public String       Period      { get; set; }
                                                    
        public String       Value       { get; set; }
                                                    
        public EntryType    Type        { get; set; }
    }
}

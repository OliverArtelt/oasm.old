﻿using System;
using System.Collections.Generic;
using System.Linq;
using comain.Calendar;
using comain.Schedule;
using comain.User;


namespace comain.Statistics
{
    
    /// <summary>
    /// Statistiken mit Grauzonenberechnung
    /// </summary>
    public class GreyStats : IStatistics
    {
      
        Dictionary<int, Decimal> implemented;   //Wochen R
        Dictionary<int, Decimal> greySum;       //Wochen GS
        Dictionary<int, Decimal> remains;       //Wochenrestplanung 
        Dictionary<int, float>   hours;         //Wochenstunden
        Dictionary<int, Decimal> special;       //Wochensonderleistungen
        Decimal auImpl = 0;                     //R Auftrag
        float  auHours = 0;                     //Gesamtstunden Auftrag
        Decimal totalGP = 0;                    //Grauzone alle Terminplanungen komplett / Mode 2: nur die nicht relaisiert sind
        Decimal totalGR = 0;                    //Grauzone alle Realiserungen komplett
        Decimal auRemains = 0;                  //Restplanung Auftrag
        Decimal auSpec = 0;                     //Sonderleistungen Auftrag
        Decimal auGS = 0;                       //Grauzonensumme Auftrag
        
        
        ICalendarStrategy calendar;
        DateTime greyDate;
        DateTime operDate;
        int greyKey;
        int operKey;
        bool greySumType;
        bool seePrices;
        bool seeHours;
        bool seeGray;
        int deltaVon;
        int deltaBis;

        
        /// <summary>
        /// Statistiken mit Grauzonenberechnung
        /// </summary>
        /// <param name="gk">Key Grauzonenbeginn</param>
        /// <param name="ok">Key Beginn operative Planung</param>
        /// <param name="gs">Summierungsverhalten in der Grauzone</param>
        internal GreyStats(ICalendarStrategy cal, DateTime gd, DateTime od, bool gs, bool showprices, bool showhours, bool showgray, int deltaVon, int deltaBis)
        {
        
            greyKey = cal.CalculateCalendarKey(gd);
            operKey = cal.CalculateCalendarKey(od);
            greyDate = gd;
            operDate = od;
            greySumType = gs;   
            seePrices = showprices;
            seeHours = showhours;
            seeGray = showgray;
            calendar = cal;
            this.deltaVon = deltaVon;
            this.deltaBis = deltaBis;
            implemented = new Dictionary<int,Decimal>(cal.CalendarKeys.Count);
            greySum = new Dictionary<int,Decimal>(cal.CalendarKeys.Count);
            remains = new Dictionary<int,Decimal>(cal.CalendarKeys.Count);
            special = new Dictionary<int,Decimal>(cal.CalendarKeys.Count);
            hours   = new Dictionary<int,float>(cal.CalendarKeys.Count);
            DeltaPlanned = 0;
            DeltaPlannedNotImplemented = 0;
            DeltaImplementedNotPlanned = 0;
            DeltaDifference = 0;

            //Kalenderwochen vorbelegen
            foreach (var d in cal.CalendarKeys) {
            
                implemented.Add(d, 0);
                greySum.Add(d, 0);
                remains.Add(d, 0);
                special.Add(d, 0);
                hours.Add(d, 0);
            }
        }
        
        
        public void PresentAuftrag(comain.Schedule.Auftrag order)
        {

            auImpl = 0;         
            auHours = 0;        
            auRemains = 0;
            auGS = 0;
            auSpec = 0;
            
            order.SetMengenDifferenz(deltaVon, deltaBis);


            foreach (KeyValuePair<int, Termin> pair in order.Dates) {
            
                Termin d = pair.Value;
                
                //1. Implemented (R): alle Realisierungen rzmenge*rzwert            
                if (d.Key < greyKey) {
                
                    if (d.ImplementedId > 0 && d.EditedType == EditedType.Empty
                        && d.ImplementedSpecial == false || d.EditedType == EditedType.Common) {

                        implemented[d.Key] += d.ImplementedValue;
                        auImpl += d.ImplementedValue;
                    }
                }
                
                //2. Gesamtstunden
                auHours += d.PlannedHours; 
                hours[d.Key] += d.PlannedHours;

                //3. GS
                if (d.Key >= greyKey && d.Key < operKey) {
                
                    if (greySumType) {
                    
                        Decimal gs = d.ImplementedValue;
                        totalGR += gs;                       
                        
                        if (d.ImplementedId == 0 && d.EditedType != EditedType.Special && d.EditedType != EditedType.Common) {
                        
                            gs += d.PlannedValue;
                            totalGP += d.PlannedValue;
                        }    
                            
                        auGS += gs;
                        greySum[d.Key] += gs;
                    
                    } else {
                    
                        //Sonderfall: neu eingegebene Sonderleistungen dürfen Planung nicht überlagern
                        if (d.PlannedId > 0 && d.EditedType != EditedType.Deleted 
                            || d.EditedType == EditedType.Daily || d.EditedType == EditedType.Planned) {

                            Decimal gs = d.PlannedValue;

                            greySum[d.Key] += gs;
                            auGS += gs;
                            totalGP += gs;
                        }
                    }
                }
                
                //4.Restplanung
                if (d.Key >= operKey) {
                
                    //Sonderfall: neu eingegebene Sonderleistungen dürfen Planung nicht überlagern
                    if (d.PlannedId > 0 && d.EditedType != EditedType.Deleted 
                        || d.EditedType == EditedType.Daily || d.EditedType == EditedType.Planned) {

                        remains[d.Key] += d.PlannedValue;
                        auRemains += d.PlannedValue;
                    }
                }
                
                //5.Sonderleistungen
                if (d.ImplementedSpecial == true || d.EditedType == EditedType.Special) {
                    
                    auSpec += d.ImplementedValue;
                    special[d.Key] += d.ImplementedValue;
                }
            }

            DeltaPlanned += order.DeltaPlanned(deltaVon, deltaBis);
            DeltaPlannedNotImplemented += order.DeltaPlannedNotImplemented(deltaVon, deltaBis);
            DeltaImplementedNotPlanned += order.DeltaImplementedNotPlanned(deltaVon, deltaBis);
            DeltaDifference += order.DeltaRestsumme(deltaVon, deltaBis);
        }

        public IList<TotalEntry> GetTotal()
        {
        
            List<TotalEntry> list = new List<TotalEntry>();            
            String s;
            TotalEntry pos;
            
         
            if (seePrices) {
            
                if (seeGray) {
                
                    //Planungen in der Zukunft haben keine Realisierungen
                    if (calendar.DateVon <= greyDate) {
                
                        s = String.Format("{0} - {1}", calendar.DateVon.ToShortDateString(), greyDate.ToShortDateString());
                        pos = new TotalEntry { Name = "Realisierung", Period = s, Value = TotalImplemented.ToString("C2") };        
                        list.Add(pos);
                    }
                    
                    s = String.Format("{0} - {1}", greyDate.ToShortDateString(), operDate.ToShortDateString());
                    pos = new TotalEntry { Name = "Grauzonenplanung", Period = s, Value = totalGP.ToString("C2") };        
                    list.Add(pos);

                    if (greySumType) {
                    
                        s = String.Format("{0} - {1}", greyDate.ToShortDateString(), operDate.ToShortDateString());
                        pos = new TotalEntry { Name = "Grauzonenrealisierung", Period = s, Value = totalGR.ToString("C2") };        
                        list.Add(pos);
                    }
                }
                
                //Planungen in der Zukunft haben keine Realisierungen
                if (calendar.DateVon <= greyDate) {
                
                    s = String.Format("{0} - {1}", calendar.DateVon.ToShortDateString(), operDate.ToShortDateString());
                    pos = new TotalEntry { Name = "Verbrauchtes Budget", Period = s, Value = TotalBudgetUsed.ToString("C2") };       
                    list.Add(pos);
                }
                
                //Planungen in der Vergangenheit haben keine Reste
                if (operDate <= calendar.DateBis) {
                
                    s = String.Format("{0} - {1}", operDate.ToShortDateString(), calendar.DateBis.ToShortDateString());
                    pos = new TotalEntry { Name = "Restplanung", Period = s, Value = TotalRemains.ToString("C2") };       
                    list.Add(pos);
                }
                
                s = String.Format("{0} - {1}", calendar.DateVon.ToShortDateString(), calendar.DateBis.ToShortDateString());
                pos = new TotalEntry { Name = "Gesamtbudget", Period = s, Value = TotalBudgetTotal.ToString("C2") };       
                list.Add(pos);
            }
              
            if (seeHours) {
            
                s = String.Format("{0} - {1}", calendar.DateVon.ToShortDateString(), calendar.DateBis.ToShortDateString());
                pos = new TotalEntry { Name = "Gesamtstunden", Period = s, Value = TotalHoursTotal.ToString("N2") + " h" };        
                list.Add(pos);
            }

            if (seePrices) {
            
                s = String.Format("{0} - {1}", calendar.DateVon.ToShortDateString(), calendar.DateBis.ToShortDateString());
                pos = new TotalEntry { Name = "Sonderleistungen", Period = s, Value = TotalSpecial.ToString("C2") };       
                list.Add(pos);
            
                String deltaPeriod = String.Format("{0} - {1}", calendar.CalculateFirstDate(deltaVon).ToShortDateString(), calendar.CalculateLastDate(deltaBis).ToShortDateString());

                pos = new TotalEntry { Name = "Δ Geplant", Period = deltaPeriod, Value = DeltaPlanned.ToString("C2"), Type = TotalEntry.EntryType.DeltaUnspecified };       
                list.Add(pos);
            
                pos = new TotalEntry { Name = "Δ Geplant, nicht realisiert", Period = deltaPeriod, Value = DeltaPlannedNotImplemented.ToString("C2"), Type = TotalEntry.EntryType.DeltaNegative };       
                list.Add(pos);
            
                pos = new TotalEntry { Name = "Δ Realisiert, nicht geplant", Period = deltaPeriod, Value = DeltaImplementedNotPlanned.ToString("C2"), Type = TotalEntry.EntryType.DeltaPositive };       
                list.Add(pos);
            
                pos = new TotalEntry { Name = "Δ Restsumme", Period = deltaPeriod, Value = DeltaDifference.ToString("C2"), Type = TotalEntry.EntryType.DeltaUnspecified };       
                list.Add(pos);
            }
       
            return list;        
        }


        public Decimal CurrentBudgetUsed
        {
            get { return auImpl + auGS; }
        }

        public Decimal CurrentBudgetTotal
        {
            get { return CurrentBudgetUsed + auRemains; }
        }

        public float CurrentHoursTotal
        {
            get { return auHours; }
        }

        public Decimal TotalImplemented
        {
            get { return implemented.Values.Sum(); }
        }

        public Decimal TotalGreyPlanned
        {
            get { return totalGP; }
        }

        public Decimal TotalGreyImplemented
        {
            get { return totalGR; }
        }

        public Decimal TotalRemains
        {
            get { return remains.Values.Sum(); }
        }

        public Decimal TotalBudgetUsed
        {
            get { return implemented.Values.Sum() + totalGR + totalGP; }
        }

        public Decimal TotalBudgetTotal
        {
            get { return TotalBudgetUsed + TotalRemains; }
        }

        public Decimal WeekImplemented(int weekKey)
        {
            return implemented[weekKey];
        }

        public Decimal WeekGreySum(int weekKey)
        {
            return greySum[weekKey];
        }

        public Decimal WeekRemains(int weekKey)
        {
            return remains[weekKey];
        }

        public Decimal WeekBudgetUsed(int weekKey)
        {
            return implemented[weekKey] + greySum[weekKey];
        }

        public Decimal WeekBudgetTotal(int weekKey)
        {
            return WeekBudgetUsed(weekKey) + WeekRemains(weekKey);
        }

        public Decimal CurrentSpecial
        {
            get { return auSpec; }
        }

        public float TotalHoursTotal
        {
            get { return hours.Values.Sum(); }
        }

        public Decimal TotalSpecial
        {
            get { return special.Values.Sum(); }
        }

        public Decimal WeekSpecial(int weekKey)
        {
            return special[weekKey];
        }

        public float WeekHoursTotal(int weekKey)
        {
            return hours[weekKey];
        }


        public decimal DeltaPlanned { get; private set; }
        public decimal DeltaPlannedNotImplemented { get; private set; }
        public decimal DeltaImplementedNotPlanned { get; private set; }
        public decimal DeltaDifference { get; private set; }
    }
}

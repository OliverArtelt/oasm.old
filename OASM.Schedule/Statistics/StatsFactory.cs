﻿using comain.Calendar;
using comain.User;


namespace comain.Statistics
{
    
    public static class StatsFactory
    {

        /// <summary>
        /// Statistikobjekt für Summen etc geben
        /// </summary>
        /// <param name="cal">aktueller Kalender der Planung</param>
        /// <param name="user"></param>
        /// <param name="cfg"></param>
        /// <param name="showPrices">Preise berechnen?</param>
        /// <param name="showHours">Stunden berechnen?</param>
        /// <param name="realAreaVon">Betrachtungszeitraum Start</param>
        /// <param name="realAreaBis">Betrachtungszeitraum Ende</param>
        /// <returns>Statistikobjekt</returns>
        public static IStatistics CreateStatsObject(ICalendarStrategy cal, IUser user, Configuration cfg, 
                                                    bool showPrices, bool showHours, 
                                                    int deltaVon, int deltaBis)
        {

            IStatistics stats;
            bool prices = showPrices && user.SeePrices;
            bool hours  = showHours && user.SeeHours;
            bool gray   = user.MayAdminister;

            if (cfg.UseGreyArea) stats = new GreyStats(cal, cfg.GreyDate, cfg.OperDate, cfg.GreySumType == 1, prices, hours, gray, deltaVon, deltaBis);
            else                 stats = new ClassicStats(cal, cfg.OperDate, prices, hours, deltaVon, deltaBis);

            return stats;
        }
    }
}

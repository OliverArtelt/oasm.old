﻿using System;
using System.Collections.Generic;
using System.Linq;
using comain.Calendar;
using comain.Schedule;
using comain.User;


namespace comain.Statistics
{
    
    /// <summary>
    /// Klassische Statistikberechnung
    /// </summary>
    public class ClassicStats : IStatistics
    {
        
        
        Dictionary<int, Decimal> implemented;    //Wochen  R
        Dictionary<int, Decimal> remains;        //Wochenrestplanung 
        Dictionary<int, float>  hours;          //Wochenstunden
        Dictionary<int, Decimal> special;        //Wochensonderleistungen
        Decimal auImpl = 0;                      //R Auftrag
        float  auHours = 0;                     //Gesamtstunden Auftrag
        Decimal auRemains = 0;                   //Restplanung Auftrag
        Decimal auSpec = 0;                      //Sonderleistungen Auftrag
        DateTime remainDate;                    //Restplanung Datum
        ICalendarStrategy calendar;
        bool seePrices;
        bool seeHours;
        int deltaVon;
        int deltaBis;

        
        /// <summary>
        /// Klassische Statistikberechnung
        /// </summary>
        /// <param name="cal">Kalender, dessen Keys zur Berechnung verwendet werden (aus Schedule)</param>
        /// <param name="rem">Datum Restplanung (für Totalstatsanzeige)</param>
        internal ClassicStats(ICalendarStrategy cal, DateTime rem, bool showprices, bool showhours, int deltaVon, int deltaBis)
        {
        
            implemented = new Dictionary<int,Decimal>(cal.CalendarKeys.Count);
            remains = new Dictionary<int,Decimal>(cal.CalendarKeys.Count);
            special = new Dictionary<int,Decimal>(cal.CalendarKeys.Count);
            hours = new Dictionary<int,float>(cal.CalendarKeys.Count);
            remainDate = rem;
            calendar = cal;
            seePrices = showprices;
            seeHours = showhours;
            this.deltaVon = deltaVon;
            this.deltaBis = deltaBis;
            DeltaPlanned = 0;
            DeltaPlannedNotImplemented = 0;
            DeltaImplementedNotPlanned = 0;
            DeltaDifference = 0;

            //Kalenderwochen vorbelegen
            foreach (var d in cal.CalendarKeys) {
            
                implemented.Add(d, 0);
                remains.Add(d, 0);
                special.Add(d, 0);
                hours.Add(d, 0);
            }
        }
          
        
        public void PresentAuftrag(comain.Schedule.Auftrag order)
        {
            
            auImpl = 0;         
            auHours = 0;        
            auRemains = 0;      
            auSpec = 0;      
            int planKey = 0;    //größter Key Realisierung (ab nächsten beginnt Planung)
            
            order.SetMengenDifferenz(deltaVon, deltaBis);


            foreach (KeyValuePair<int, Termin> var in order.Dates) {
            
                Termin d = var.Value;
                
                //1. Implemented (R): alle Realisierungen rzmenge*rzwert            
                if (d.ImplementedId > 0 && d.EditedType == EditedType.Empty
                    && d.ImplementedSpecial == false || d.EditedType == EditedType.Common) {
                    
                    //letzte Realisierung als Schranke zur Planung merken
                    if (d.Key > planKey) planKey = d.Key;
                
                    implemented[d.Key] += d.ImplementedValue;
                    auImpl += d.ImplementedValue;
                
                    //2. Gesamtstunden
                    auHours += d.ImplementedHours;
                    hours[d.Key] += d.ImplementedHours;
                } 
                
                //Sonderleistungen
                if (d.ImplementedSpecial == true || d.EditedType == EditedType.Special) {
                    
                    auSpec += d.ImplementedValue;
                    special[d.Key] += d.ImplementedValue;
                }
            }
            
            //3. Restplanung ermitteln
            foreach (KeyValuePair<int, Termin> var in order.Dates) {

                //erst ab letzter Realisierung
                if (var.Key <= planKey) continue;

                Termin d = var.Value;
                
                //Sonderfall: neu eingegebene Sonderleistungen dürfen Planung nicht überlagern
                if (d.PlannedId > 0 && d.EditedType != EditedType.Deleted 
                    || d.EditedType == EditedType.Daily || d.EditedType == EditedType.Planned) {
                    
                    remains[d.Key] += d.PlannedValue;
                    auRemains += d.PlannedValue;
                    
                    //2. Gesamtstunden
                    auHours += d.PlannedHours;                
                    hours[d.Key] += d.PlannedHours;
                }
            }

            DeltaPlanned += order.DeltaPlanned(deltaVon, deltaBis);
            DeltaPlannedNotImplemented += order.DeltaPlannedNotImplemented(deltaVon, deltaBis);
            DeltaImplementedNotPlanned += order.DeltaImplementedNotPlanned(deltaVon, deltaBis);
            DeltaDifference += order.DeltaRestsumme(deltaVon, deltaBis);
        }
        
        public IList<TotalEntry> GetTotal()
        {
        
            List<TotalEntry> list = new List<TotalEntry>(2);            
            String s;
            TotalEntry pos;
            
            if (seePrices) {
            
                s = String.Format("{0} - {1}", remainDate.ToShortDateString(), calendar.DateBis.ToShortDateString());
                pos = new TotalEntry { Name = "Restplanung", Period = s, Value = TotalRemains.ToString("C2") };        
                list.Add(pos);

                s = String.Format("{0} - {1}", calendar.DateVon.ToShortDateString(), calendar.DateBis.ToShortDateString());
                pos = new TotalEntry { Name = "Gesamtsumme", Period = s, Value = TotalBudgetTotal.ToString("C2") };        
                list.Add(pos);
            }
            
            if (seeHours) {
            
                s = String.Format("{0} - {1}", calendar.DateVon.ToShortDateString(), calendar.DateBis.ToShortDateString());
                pos = new TotalEntry { Name = "Gesamtstunden", Period = s, Value = TotalHoursTotal.ToString("N2") + " h" };        
                list.Add(pos);
            }
            
            if (seePrices) {
            
                s = String.Format("{0} - {1}", calendar.DateVon.ToShortDateString(), calendar.DateBis.ToShortDateString());
                pos = new TotalEntry { Name = "Sonderleistungen", Period = s, Value = TotalSpecial.ToString("C2") };       
                list.Add(pos);
            
                String deltaPeriod = String.Format("{0} - {1}", calendar.CalculateFirstDate(deltaVon).ToShortDateString(), calendar.CalculateLastDate(deltaBis).ToShortDateString());

                pos = new TotalEntry { Name = "Δ Geplant", Period = deltaPeriod, Value = DeltaPlanned.ToString("C2"), Type = TotalEntry.EntryType.DeltaUnspecified };       
                list.Add(pos);
            
                pos = new TotalEntry { Name = "Δ Geplant, nicht realisiert", Period = deltaPeriod, Value = DeltaPlannedNotImplemented.ToString("C2"), Type = TotalEntry.EntryType.DeltaNegative };       
                list.Add(pos);
            
                pos = new TotalEntry { Name = "Δ Realisiert, nicht geplant", Period = deltaPeriod, Value = DeltaImplementedNotPlanned.ToString("C2"), Type = TotalEntry.EntryType.DeltaPositive };       
                list.Add(pos);
            
                pos = new TotalEntry { Name = "Δ Restsumme", Period = deltaPeriod, Value = DeltaDifference.ToString("C2"), Type = TotalEntry.EntryType.DeltaUnspecified };       
                list.Add(pos);
            }
            
            return list;        
        }

        public Decimal CurrentBudgetTotal
        {
            get { return auImpl + auRemains; }
        }

        public float CurrentHoursTotal
        {
            get { return auHours; }
        }

        public Decimal CurrentBudgetUsed
        {
            get { return auImpl; }
        }

        public Decimal TotalImplemented
        {
            get { return implemented.Values.Sum(); }
        }

        public Decimal TotalRemains
        {
            get { return remains.Values.Sum(); }
        }

        public Decimal TotalBudgetTotal
        {
            get { return TotalImplemented + TotalRemains; }
        }

        public Decimal WeekImplemented(int weekKey)
        {
            return implemented[weekKey];
        }

        public Decimal WeekRemains(int weekKey)
        {
            return remains[weekKey];
        }

        public Decimal WeekBudgetTotal(int weekKey)
        {
            return implemented[weekKey] + remains[weekKey];
        }

        public Decimal CurrentSpecial
        {
            get { return auSpec; }
        }

        public float TotalHoursTotal
        {
            get { return hours.Values.Sum(); }
        }

        public Decimal TotalSpecial
        {
            get { return special.Values.Sum(); }
        }

        public Decimal WeekSpecial(int weekKey)
        {
            return special[weekKey];
        }

        public float WeekHoursTotal(int weekKey)
        {
            return hours[weekKey];
        }
        
#region N O T   S U P P O R T E D

        public Decimal TotalGreyPlanned
        {
            get { return 0; }
        }

        public Decimal TotalGreyImplemented
        {
            get { return 0; }
        }

        public Decimal TotalBudgetUsed
        {
            get { return 0; }
        }
 
        public Decimal WeekGreySum(int weekKey)
        {
            return 0;
        }

        public Decimal WeekBudgetUsed(int weekKey)
        {
            return 0;
        }

#endregion


        public decimal DeltaPlanned { get; private set; }
        public decimal DeltaPlannedNotImplemented { get; private set; }
        public decimal DeltaImplementedNotPlanned { get; private set; }
        public decimal DeltaDifference { get; private set; }
    }
}

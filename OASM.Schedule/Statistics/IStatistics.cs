﻿using System;
using System.Collections.Generic;
using comain.Schedule;


namespace comain.Statistics
{
    
    /// <summary>
    /// Preis- und Stundenberechnung, Summen ausgeben
    /// </summary>         
    public interface IStatistics
    {
                     
        /// <summary>
        /// ein einzelnen Auftrag präsentieren, dessen Werte in den Statistiken berücksichtigt werden sollen
        /// </summary>
        /// <param name="order">präsentierter Auftrag</param>
        void PresentAuftrag(Auftrag order);
        /// <summary>
        /// Gesamtstatistiken Schedule 
        /// </summary>                         
        IList<TotalEntry> GetTotal();
        /// <summary>
        /// verbrauchtes Budget des zuletzt präsentierten Auftrages
        /// </summary>
        Decimal CurrentBudgetUsed { get; }
        /// <summary>
        /// Gesamtbudget des zuletzt präsentierten Auftrages
        /// </summary>
        Decimal CurrentBudgetTotal { get; }
        /// <summary>
        /// Gesamtstunden des zuletzt präsentierten Auftrages
        /// </summary>
        float CurrentHoursTotal { get; }
        /// <summary>
        /// Wert Sonderleistungen des zuletzt präsentierten Auftrages
        /// </summary>
        Decimal CurrentSpecial { get; }
        /// <summary>
        /// Realisierung R über alles
        /// </summary>
        Decimal TotalImplemented { get; }
        /// <summary>
        /// nichtrealiserte Termine in Grauzone GP über alles
        /// </summary>
        Decimal TotalGreyPlanned { get; }
        /// <summary>
        /// Realisierungen in Grauzone GR über alles
        /// </summary>
        Decimal TotalGreyImplemented { get; }
        /// <summary>
        /// Restplanung über alles
        /// </summary>
        Decimal TotalRemains { get; }
        /// <summary>
        /// verbrauchtes Budget über alles
        /// </summary>
        Decimal TotalBudgetUsed { get; }
        /// <summary>
        /// Gesamtbudget über alles
        /// </summary>
        Decimal TotalBudgetTotal { get; }
        /// <summary>
        /// Gesamtstunden über alles
        /// </summary>
        float TotalHoursTotal { get; }
        /// <summary>
        /// Wert Sonderleistungen über alles
        /// </summary>
        Decimal TotalSpecial { get; }
        /// <summary>
        /// Realisierung R über Kalenderwoche
        /// </summary>
        Decimal WeekImplemented(int weekKey);
        /// <summary>
        /// Grauzonensumme über Kalenderwoche
        /// </summary>
        Decimal WeekGreySum(int weekKey);
        /// <summary>
        /// Restplanung über Kalenderwoche
        /// </summary>
        Decimal WeekRemains(int weekKey);
        /// <summary>
        /// verbrauchtes Budget über Kalenderwoche
        /// </summary>
        Decimal WeekBudgetUsed(int weekKey);
        /// <summary>
        /// Gesamtbudget über Kalenderwoche
        /// </summary>
        Decimal WeekBudgetTotal(int weekKey);
        /// <summary>
        /// Wert Sonderleistungen über Kalenderwoche
        /// </summary>
        Decimal WeekSpecial(int weekKey);
        /// <summary>
        /// Gesamtstunden über Kalenderwoche
        /// </summary>
        float WeekHoursTotal(int weekKey);
        /// <summary>
        /// Betrachtungszeitraum: Was ist geplante Menge?
        /// </summary>
        Decimal DeltaPlanned { get; }
        /// <summary>
        /// Betrachtungszeitraum: Was war geplant, wurde nicht realisiert??
        /// </summary>
        Decimal DeltaPlannedNotImplemented { get; }
        /// <summary>
        /// Betrachtungszeitraum: Was wurde realisiert, war aber nicht geplant?
        /// </summary>
        Decimal DeltaImplementedNotPlanned { get; }
        /// <summary>
        /// Betrachtungszeitraum: Daraus resultierend Restsumme (Was war geplant - Rot + Grün)!
        /// </summary>
        Decimal DeltaDifference { get; }
    }
}

﻿using System.Collections.Generic;


namespace comain.Schedule
{

    /// <summary>
    /// Alle Aufträge an/abwählen, von Undo geschützte Aufträge berücksichtigen
    /// </summary>
    public class SelectAllOrders : IScheduleVisitor
    {

        /// <summary>
        /// zu setzender Status
        /// </summary>
        bool newState;
        /// <summary>
        /// Aufträge, die nicht berücksichtigt werden sollen
        /// Liste muss sortiert sein
        /// </summary>
        List<int> protectedOrders;
        
        /// <summary>
        /// Alle Aufträge an/abwählen, von Undo geschützte Aufträge berücksichtigen
        /// </summary>
        /// <param name="state">zu setzender Status</param>
        /// <param name="prtcd">Liste von Aufträgen, die verschont werden sollen, Liste muss sortiert sein.</param>
        public SelectAllOrders(bool state, List<int> prtcd)
        {
        
            newState = state;
            protectedOrders = prtcd;
        }

        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
        }

        /// <summary>
        /// Auftrag-Selektstatus setzen
        /// </summary>
        /// <param name="obj">zu manipulierender </param>
        public void Visit(Auftrag obj)
        {
            
            //Sollen einfach alle gesetzt werden (Redo)?
            if (protectedOrders == null) {
            
                obj.Selected = newState;
                return;
            }
        
            //Undo: nur die, die damals noch nicht diesen Status hatten, wieder zurücksetzen
            int index = protectedOrders.BinarySearch(obj.Id);
            if (index < 0 || index >= protectedOrders.Count) obj.Selected = newState;
        }

        public void Visit(Termin obj)
        {
        }
    }
}

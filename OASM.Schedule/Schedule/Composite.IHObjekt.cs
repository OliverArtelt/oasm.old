﻿using System;
using System.Collections.Generic;


namespace comain.Schedule
{


    /// <summary>
    /// IHObjektknoten des Schedules
    /// </summary>
    [Serializable]
    public class IHObjekt : IScheduleComposite
    {
        
        /// <summary>
        /// tblih.ihid
        /// </summary>
        int id;
        /// <summary>
        /// tblih.ihname
        /// </summary>
        String name;
        /// <summary>
        /// tblih.ihinventnr - Inventarnummer
        /// </summary>
        String invent;
        /// <summary>
        /// Liste dem IHObjekt zugeordneten Aufträge
        /// </summary>
        List<Auftrag> myAUs;
        /// <summary>
        /// Planeigenschaften für Termninberechnung referenzieren
        /// </summary>
        Traits myTraits;

        /// <summary>
        /// Elternknoten geben
        /// </summary>
        public IScheduleComposite Parent { get; private set; }
       
        public Standort Standort { get { return (Standort)Parent; } }
       
        /// <summary>
        /// Zeilennummer des IHObjektes im Grid geben
        /// </summary>
        public int Position { get; set; }
        /// <summary>
        /// IHO-Nummer tblih.ihnr 
        /// </summary>
        public String Nummer { get; private set; }
        /// <summary>
        /// IHO-Bezeichnung geben
        /// </summary>
        public String Name 
        { 
            get {
             
                String s = Nummer + " - " + name;
                if (myTraits.Projection[ScheduleParts.ShowIHInvent] && invent.Length > 0) s += " [Inventar " + invent + "]";
                
                return s; 
            } 
        }
        
        /// <summary>
        /// Main-IHO-Nummer und Bezeichnung geben, leer wenn IHO keine Baugruppe ist
        /// </summary>
        public String MainName { get; private set; }
        /// <summary>
        /// true: IHO ist Baugruppe, Haupt-IHO steht in MainName
        /// </summary>
        public bool IstBaugruppe { get; private set; }
        
        
        /// <summary>
        /// Planeigenschaften geben
        /// </summary>
        public Traits Traits { get { return myTraits; } }
        
        /// <summary>
        /// IHObjektknoten des Schedules
        /// </summary>
        /// <param name="id_">tblih.ihid</param>
        /// <param name="nr_">tblih.ihnr</param>
        /// <param name="name_">tblih.ihname</param>
        /// <param name="invent_">tblih.ihinventnr - Inventarnummer</param>
        /// <param name="parent_">Elternknoten</param>
        public IHObjekt(int id_, String nr_, String name_, String invent_, Standort parent_)
        {
            id       = id_;
            Nummer   = nr_;
            name     = name_;
            invent   = invent_;
            myAUs    = new List<Auftrag>();
            Parent   = parent_;
            myTraits = ((Root)Parent.Parent.Parent).Traits;
            
            MainName  = String.Empty;
            IstBaugruppe = false;
        }
        
        /// <summary>
        /// IHObjektknoten des Schedules
        /// </summary>
        /// <param name="id_">tblih.ihid</param>
        /// <param name="nr_">tblih.ihnr</param>
        /// <param name="name_">tblih.ihname</param>
        /// <param name="invent_">tblih.ihinventnr - Inventarnummer</param>
        /// <param name="nr_mo_">Hauptobjekt tblih.ihnr</param>
        /// <param name="name_mo_">Hauptobjekt tblih.ihname</param>
        /// <param name="invent_mo_">Hauptobjekt tblih.ihinventnr</param>
        /// <param name="parent_">Elternknoten</param>
        public IHObjekt(int id_, String nr_, String name_, String invent_, 
                        String nr_mo_, String name_mo_, String invent_mo_, Standort parent_)
        
            : this(id_, nr_, name_, invent_, parent_)              
                        
        {
            
            if (nr_mo_ == nr_) return;
            
            MainName = nr_mo_ + " - " + name_mo_;
            IstBaugruppe = true;
        }
        
        /// <summary>
        /// Kindknoten anfügen
        /// </summary>
        /// <param name="item">Auftrag als Kindknoten</param>
        public void Add(Auftrag item)
        {
            myAUs.Add(item);
        }
        
        /// <summary>
        /// Besucherschnittstelle implementieren
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        public void Accept(IScheduleVisitor v)
        {
            v.Visit(this);
            foreach (Auftrag au in myAUs) au.Accept(v);
        }

        /// <summary>
        /// Degenerierte Besucherschnittstelle implementieren, Besucher nur einmal rückrufen
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        /// <param name="row">Position im Control als Zuständigkeitskriterium</param>
        public void AcceptOnce(IScheduleVisitor v, int row)
        {
        
            //Standort ist direkt gemeint
            if (row == Position || row == Position + 1) {
            
                v.Visit(this);
            
            } else {           
            
                Auftrag currAU = null;
                
                //in Auftragsteilbäume nach dem zuständigen Auftrag suchen
                foreach (Auftrag au in myAUs) {
                
                    if (au.Position > row) {
                    
                        currAU.AcceptOnce(v, row);
                        return;
                    }
                    
                    currAU = au;
                }

                currAU.AcceptOnce(v, row);
            }            
        }

        /// <summary>
        /// Auftragsanzahl ermitteln
        /// </summary>
        /// <returns>Anzahl Aufträge im Teilbaum</returns>
        public int CountOrders()
        {
            return myAUs.Count;
        }

        public override string ToString()
        {
            return Nummer;
        }
    }
}

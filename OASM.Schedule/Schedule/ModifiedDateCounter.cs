﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using comain.Calendar;


namespace comain.Schedule
{
    
    /// <summary>
    /// Geänderte Termine in SQL-Anweisungen umwandeln, 
    /// Builder-Director verwendet Besucherschnittstelle des Schedules
    /// </summary>
    public class ModifiedDateCounter : IScheduleVisitor
    {
        
        int modified = 0;
        
        /// <summary>
        /// Anzahl veränderter Termine
        /// </summary>
        public int ChangesCount { get { return modified; } }

        
        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
        }

        public void Visit(Auftrag obj)
        {
        }

        /// <summary>
        /// Geänderte Termine in DB-Befehlsobjekte wandeln
        /// </summary>
        /// <param name="obj">zu betrachtendes Terminobjekt</param>
        public void Visit(Termin obj)
        {
        
            if (obj.EditedType == EditedType.Empty) return;
            modified++;
        }
    }
}

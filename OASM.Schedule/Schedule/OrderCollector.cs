﻿using System.Collections.Generic;


namespace comain.Schedule
{

    /// <summary>
    /// Aufträge mit einem bestimmten Status ermitteln, 
    /// wird z.B. für die Undo-Funktion benötigt
    /// </summary>
    public class OrderCollector : IScheduleVisitor
    {

        /// <summary>
        /// Neuer Status, den alle Aufträge danach haben sollen
        /// </summary>
        bool newState;
        /// <summary>
        /// Aufträge, die bereits diesen zu ändernden Status besitzen, 
        /// dürfen durch das Undo nicht angefaßt werden
        /// </summary>
        List<int> myOrders; 
        
        /// <summary>
        /// Aufträge mit diesem zu ändernden Status geben
        /// </summary>
        public List<int> CollectedOrders { get { return myOrders;} }
        
        /// <summary>
        /// Aufträge mit einem bestimmten Status ermitteln, 
        /// wird für die undo-Funktion benötigt
        /// </summary>
        /// <param name="state">Neuer, zu setzender Status</param>
        public OrderCollector(bool state)
        {
            newState = state;
            myOrders = new List<int>();
        }
        

        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
        }

        /// <summary>
        /// Wenn Auftrag gesuchten Status besitzt, diesen anfügen
        /// </summary>
        /// <param name="obj">zu testender Auftrag</param>
        public void Visit(Auftrag obj)
        {
            if (obj.Selected == newState) myOrders.Add(obj.Id);
        }

        public void Visit(Termin obj)
        {
        }
    }
}

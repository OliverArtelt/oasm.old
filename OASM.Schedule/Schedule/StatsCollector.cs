﻿using System.Collections.Generic;
using comain.Calendar;
using comain.Statistics;
using comain.User;


namespace comain.Schedule
{
    
    /// <summary>
    /// Statistiken einsammeln
    /// </summary>
    public class StatsCollector : IScheduleVisitor
    {
       
       
        public IStatistics CollectedStats { get; private set; }

        
        /// <summary>
        /// Statistiken einsammeln
        /// </summary>
        /// <param name="traits">Eigenschaften aktueller Plan</param>
        /// <param name="realStatsVon">Realisierungsstatistik Beginn Zeitraum</param>
        /// <param name="realStatsBis">Realisierungsstatistik Ende Zeitraum</param>
        public StatsCollector(Traits traits, int deltaVon, int deltaBis)        
        {

            bool prices = traits.Projection[ScheduleParts.ShowPrice];
            bool hours = traits.Projection[ScheduleParts.ShowHours];
            CollectedStats = StatsFactory.CreateStatsObject(traits.Calendar, traits.User, traits.OASMSettings, prices, hours, deltaVon, deltaBis); 
        }
        
        /// <summary>
        /// Gesamtstatistik durchreichen
        /// </summary>
        /// <returns>Gesamtstatistik Planung</returns>
        public IList<TotalEntry> GetTotalStats() { return CollectedStats.GetTotal(); }
        

        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
        }

        public void Visit(Auftrag obj)
        {
        
            CollectedStats.PresentAuftrag(obj);
            obj.HoursTotal = CollectedStats.CurrentHoursTotal;
            obj.BudgetTotal = CollectedStats.CurrentBudgetTotal;
            obj.BudgetUsed = CollectedStats.CurrentBudgetUsed;            
        }

        public void Visit(Termin obj)
        {
        }
    }
}

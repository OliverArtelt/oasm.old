﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using comain.Calendar;


namespace comain.Schedule
{
    
    /// <summary>
    /// Geänderte Termine in SQL-Anweisungen umwandeln, 
    /// Builder-Director verwendet Besucherschnittstelle des Schedules
    /// </summary>
    public class ModifiedDateList : IScheduleVisitor
    {
        
        ICalendarStrategy myCalendar;
        
        
        /// <summary>
        /// Liste mit veränderten Terminen
        /// </summary>
        public List<ModifiedDate> Modified { get; private set; }

        /// <summary>
        /// Anzahl veränderter Termine
        /// </summary>
        public int ChangesCount { get { return Modified.Count; } }

        
        /// <summary>
        /// Geänderte Termine in SQL-Anweisungen umwandeln
        /// </summary>
        /// <param name="bld_">DbCommand-Liste-Erbauer</param>
        public ModifiedDateList(ICalendarStrategy cal)
        {
        
            myCalendar = cal;
            Modified = new List<ModifiedDate>();
        }

        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
        }

        public void Visit(Auftrag obj)
        {
        }

        /// <summary>
        /// Geänderte Termine in DB-Befehlsobjekte wandeln
        /// </summary>
        /// <param name="obj">zu betrachtendes Terminobjekt</param>
        public void Visit(Termin obj)
        {
        
            if (obj.EditedType == EditedType.Empty) return;
            
            Modified.Add( new ModifiedDate { EditedType = obj.EditedType, 
                                             PlannedId = obj.PlannedId, ImplementedId = obj.ImplementedId,
                                             PersonalId = ((Bearbeiter)(obj.Parent.Parent.Parent.Parent)).Id,
                                             AuftragId = ((Auftrag)obj.Parent).Id, 
                                             EditedMenge = obj.EditedMenge,
                                             PlannedDate = obj.PlannedDate,
                                             FirstDate = myCalendar.CalculateFirstDate(obj.Key) });
        }
    }
}

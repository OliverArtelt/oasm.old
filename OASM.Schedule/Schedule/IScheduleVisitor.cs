﻿using System;


namespace comain.Schedule
{
    
    /// <summary>
    /// Besucherschnittstelle des Schedules, 
    /// Methoden des Schedules aus Übersichtlichkeits- und Entkopplungsgründen extern definieren
    /// </summary>
    public interface IScheduleVisitor
    {
    
        /// <summary>
        /// Operation auf Baumwurzel implentieren
        /// </summary>
        /// <param name="obj">Baumwurzel</param>
        void Visit(Root obj);
        /// <summary>
        /// Operation auf Bearbeiter implentieren
        /// </summary>
        /// <param name="obj">Bearbeiter</param>
        void Visit(Bearbeiter obj);
        /// <summary>
        /// Operation auf Standort implentieren
        /// </summary>
        /// <param name="obj">Standort</param>
        void Visit(Standort obj);
        /// <summary>
        /// Operation auf IHObjekt implentieren
        /// </summary>
        /// <param name="obj">IHObjekt</param>
        void Visit(IHObjekt obj);
        /// <summary>
        /// Operation auf Auftrag implentieren
        /// </summary>
        /// <param name="obj">Auftrag</param>
        void Visit(Auftrag obj);
        /// <summary>
        /// Operation auf Termin implentieren
        /// </summary>
        /// <param name="obj">Termin</param>
        void Visit(Termin obj);
    }
}

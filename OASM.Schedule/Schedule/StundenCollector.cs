﻿using System;
using System.Collections.Generic;
using System.Linq;
using comain.Calendar;
using comain.Schedule.Reports;
using comain.Statistics;
using comain.User;


namespace comain.Schedule
{
    
    /// <summary>
    /// Aufträge mit Stunden im Zeitraum einsammeln, immer in KW
    /// </summary>
    public class StundenCollector : IScheduleVisitor
    {
      
        private readonly Dictionary<String, StundenReportRow> samples;
        private readonly Traits traits;
      
       
        public IList<StundenReportRow> GetResult()
        {

            return samples.Values.OrderBy(p => p.Standort)
                                 .ThenBy(p => p.Leistungsart)
                                 .ThenBy(p => p.WeekKey)
                                 .ToList();
        }

       
        public StundenCollector(Traits traits)        
        {
            
            this.traits = traits;
            samples = new Dictionary<String, StundenReportRow>();
        }
        

        public void Visit(Root obj)
        {
        }


        public void Visit(Bearbeiter obj)
        {
        }


        public void Visit(Standort obj)
        {
        }


        public void Visit(IHObjekt obj)
        {
        }


        public void Visit(Auftrag obj)
        {
        }


        public void Visit(Termin obj)
        {

            if (obj.Stunden == 0) return;

            DateTime date = obj.ImplementedId > 0? obj.ImplementedDate: obj.PlannedDate;
            date = date.MondayOf(traits.CI);
            if (traits.Calendar.DateVon > date) date = traits.Calendar.DateVon; 
            int key = date.WeekWithYear(traits.CI);

            String kw = String.Format(@"{0:D2} / {1:D4}", key % 100, key / 100);
            String et = date.ToShortDateString();
            var entry = new StundenReportRow(obj, key, kw, et);

            if (samples.ContainsKey(entry.ToString())) samples[entry.ToString()].Stunden += entry.Stunden;
            else samples.Add(entry.ToString(), entry);
        }
    }
}

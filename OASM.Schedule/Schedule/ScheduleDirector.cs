﻿using System.Data.Common;
using System.Data;
using comain.Calendar;


namespace comain.Schedule
{
    
    /// <summary>
    /// Planerstellung dirigieren
    /// </summary>
    public class ScheduleDirector
    {
        
        private IScheduleBuilder builder;
        
        
        /// <summary>
        /// ctor.: Planersteller übergeben
        /// </summary>
        /// <param name="bld">Planersteller</param>
        public ScheduleDirector(IScheduleBuilder bld)
        {
            builder = bld;
        }
            

        /// <summary>
        /// Plan konstruieren
        /// </summary>
        /// <remarks>
        /// Es wird davon ausgegegangen, dass die Daten sortiert sind.
        /// Direktor wandelt Tabellenstruktur im Baumstruktur um.
        /// </remarks>
        /// <returns>Erstellter Plan</returns>
        public Root Construct(ScheduleDTO dto)
        {
            
            Build(dto);
            return builder.Result();
        }
        
        
        private void Build(ScheduleDTO dto)
        {
        
            int psid = -1;  //neuer Teilbaum mit anderen Bearbeiter?
            int pzid = -1;  //neuer Teilbaum mit anderen Standort?
            int ihid = -1;  //neuer Teilbaum mit anderen IH-Objekt?

            foreach (var row in dto.Leistungen) {
            
                //anderer Bearbeiter? => neuer Teilbaum
                int psid_tmp = 0;
                if (row.PsId != 0) psid_tmp = row.PsId;
                if (psid_tmp != psid) {
                
                    if (row.PsId == 0) builder.BuildBearbeiter(new PSDetail());
                    else builder.BuildBearbeiter(dto.PersonalInfo[row.PsId]);   
                    psid = psid_tmp;
                    pzid = -1;
                    ihid = -1;
                }
                        
                //anderer Standort? => neuer Teilbaum
                int pzid_tmp = row.PzId;
                if (pzid_tmp != pzid) {
                
                    builder.BuildStandort(dto.StandortInfo[row.PzId]);   
                    pzid = pzid_tmp;
                    ihid = -1;
                }
                
                //anderes IH-Objekt? => neuer Teilbaum
                int ihid_tmp = row.IhIdBgr;
                if (ihid_tmp != ihid) {
                
                    builder.BuildIHObjekt(dto.IHObjektInfo[row.IhIdMo], dto.IHObjektInfo[row.IhIdBgr]);   
                    ihid = ihid_tmp;
                }
                
                builder.BuildAuftrag(dto.AuftragInfo[row.AuId], dto.KostenstellenInfo[row.KstId]);   
                foreach (var at in row.Planungen) builder.BuildTermin(at);
                foreach (var rz in row.Realisierungen) builder.BuildRealisierung(rz);
            }
        }
    }
}

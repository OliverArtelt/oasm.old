﻿using System;
using System.Data.Common;


namespace comain.Schedule
{
    
    /// <summary>
    /// Terminplaner-Schnittstelle zum Erstellen der Datenstruktur
    /// </summary>
    public interface IScheduleBuilder
    {
    
        /// <summary>
        /// aktueller Bearbeiter-Teilbaum erstellen (an dem Standorte angegliedert werden)
        /// </summary>
        void BuildBearbeiter(PSDetail ps);
        /// <summary>
        /// aktueller Standort-Teilbaum erstellen (an dem IH-Objekte angegliedert werden)
        /// </summary>
        void BuildStandort(PZDetail pz);
        /// <summary>
        /// aktueller IHObjekt-Teilbaum erstellen (an dem Aufträge angegliedert werden)
        /// </summary>
        void BuildIHObjekt(IHDetail mo, IHDetail bg);
        /// <summary>
        /// aktueller Auftrag-Teilbaum erstellen (an dem Termine eingetragen werden)
        /// </summary>
        void BuildAuftrag(AUDetail au, KSTDetail kst);
        /// <summary>
        /// Geplante Termine in Auftragsterminliste eintragen
        /// </summary>
        void BuildTermin(Planung at);
        /// <summary>
        /// Realisierungen in Auftragsterminliste eintragen
        /// </summary>
        void BuildRealisierung(Realisierung rz);
        /// <summary>
        /// Ergebnis ausliefern
        /// </summary>
        /// <returns>Plan als Baum-Datenstruktur</returns>
        Root Result();
    }
}
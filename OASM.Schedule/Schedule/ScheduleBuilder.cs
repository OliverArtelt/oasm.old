﻿using System;
using System.Data.Common;


namespace comain.Schedule
{    
    
    /// <summary>
    /// Jahresplan erstellen
    /// </summary>
    public class ScheduleBuilder : IScheduleBuilder
    {
    
        /// <summary>
        /// zu erstellendes Objekt
        /// </summary>
        Root myBuilding;
        /// <summary>
        /// aktueller Bearbeiter-Teilbaum (an dem Standorte angegliedert werden)
        /// </summary>
        Bearbeiter currentPS;
        /// <summary>
        /// aktueller Standort-Teilbaum (an dem IH-Objekte angegliedert werden)
        /// </summary>
        Standort currentPZ;
        /// <summary>
        /// aktueller IHObjekt-Teilbaum (an dem Termine eingetragen werden)
        /// </summary>
        IHObjekt currentIHO;
        /// <summary>
        /// Geplante Termine in Auftragsterminliste eintragen
        /// </summary>
        Auftrag currentAU;
        
        
        /// <summary>
        /// Baumstruktur einrichten
        /// </summary>
        /// <param name="traits">individuelle Berichtseinstellungen des Benutzers (mit/ohne Zusatzinfo usw.)</param>
        public ScheduleBuilder(Traits traits)
        {
            
            myBuilding = new Root(traits);
        }
        
        /// <summary>
        /// Neuer Bearbeiter-Teilbaum erstellen
        /// </summary>
        /// <param name="r">zu verarbeitende Daten in der aktuellen Zeile des Cursors</param>
        public void BuildBearbeiter(PSDetail ps)
        {
            
            currentPS = new Bearbeiter(ps.Id, ps.Nummer, ps.Name, myBuilding);
 	        myBuilding.Add(currentPS);
        }
        
        /// <summary>
        /// Neuer Standort-Teilbaum erstellen
        /// </summary>
        /// <param name="r">zu verarbeitende Daten in der aktuellen Zeile des Cursors</param>
        public void BuildStandort(PZDetail pz)
        {
            
            currentPZ = new Standort(pz.Id, pz.Nummer, pz.Key, pz.Name, currentPS);
 	        currentPS.Add(currentPZ);
        }
        
        /// <summary>
        /// Neuer IH-Objekt-Teilbaum erstellen
        /// </summary>
        /// <param name="r">zu verarbeitende Daten in der aktuellen Zeile des Cursors</param>
        public void BuildIHObjekt(IHDetail mo, IHDetail bg)
        {
            
            if (myBuilding.Traits.Projection[ScheduleParts.ShowBG] && mo.Id != bg.Id) {
            
                currentIHO = new IHObjekt(bg.Id, bg.Nummer, bg.Name, bg.InventarNummer, mo.Nummer, mo.Name, mo.InventarNummer, currentPZ);

            } else {
            
                currentIHO = new IHObjekt(mo.Id, mo.Nummer, mo.Name, mo.InventarNummer, currentPZ);
 	        }
 	        
 	        currentPZ.Add(currentIHO);
        }
        
        /// <summary>
        /// Neuer Auftrag-Teilbaum erstellen
        /// </summary>
        /// <param name="r">zu verarbeitende Daten in der aktuellen Zeile des Cursors</param>
        public void BuildAuftrag(AUDetail au, KSTDetail kst)
        {
            
            currentAU = new Auftrag(au.Id, au.Code, au.Kurzbeschreibung, au.ProWoche > 0, au.ZyklusAsString, 
                                    kst.Nummer, au.Menge, au.Wert, au.Stunden, currentIHO);
 	        currentIHO.Add(currentAU);
        }
        
        /// <summary>
        /// Geplante Termine in Auftragsterminliste eintragen
        /// </summary>
        /// <param name="r">zu verarbeitende Daten in der aktuellen Zeile des Cursors</param>
        public void BuildTermin(Planung at)
        {

            int key = myBuilding.Traits.Calendar.CalculateCalendarKey(at.Date);
            currentAU.EmbossPlannedDate(key, at.Id, at.Date, at.Menge, at.Wert, at.Stunden);
        }


        public void BuildRealisierung(Realisierung rz)
        {

            int key = myBuilding.Traits.Calendar.CalculateCalendarKey(rz.Date);
            currentAU.EmbossImplementedDate(key, rz.Id, rz.Date, rz.Sonder, rz.Menge, rz.Wert, rz.Stunden);
        }
        
        /// <summary>
        /// Ergebnis ausliefern
        /// </summary>
        /// <returns>fertiggestellter Plan</returns>
        public Root Result()
        {
 	        return myBuilding;
        }
    }
}

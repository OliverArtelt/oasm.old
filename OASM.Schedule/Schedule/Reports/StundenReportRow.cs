﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace comain.Schedule.Reports
{
    
    public class StundenReportRow
    {

        public StundenReportRow(Termin model, int weekkey, String kw, String ersterTag)
        {
        
            WeekKey = weekkey;
            Stunden = model.Stunden;
            Leistungsart = model.Auftrag.Leistungsart;
            Standort = model.Auftrag.IHObjekt.Standort.Name;
            Kalenderwoche = kw;
            ErsterTag = ersterTag;
        }


        public String   Standort        { get; private set; }
        public String   Leistungsart    { get; private set; }
        public int      WeekKey         { get; private set; }
        public String   Kalenderwoche   { get; private set; }
        public String   ErsterTag       { get; private set; }
       
        public float    Stunden         { get; set; }


        public override string ToString()
        {
            return String.Format("{0}-{1}-{2}", WeekKey, Leistungsart, Standort); 
        }
    }
}

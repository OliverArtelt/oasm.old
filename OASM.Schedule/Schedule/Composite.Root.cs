﻿using System.Collections.Generic;
using System;


namespace comain.Schedule
{

    /// <summary>
    /// Wurzelknotentyp des Schedules, globale Eigenschaften speichern
    /// </summary>
    [Serializable]
    public class Root : IScheduleComposite
    {
    
        /// <summary>
        /// Liste Kinderknoten
        /// </summary>
        private List<Bearbeiter> Users { get; set; }
        /// <summary>
        /// Position im darstellenden Control
        /// </summary>
        public int Position { get { return 0; } set {} }
        /// <summary>
        /// Scheduleeigenschaften
        /// </summary>
        public Traits Traits { get; private set; }
        /// <summary>
        /// Elternknoten ausgeben
        /// </summary>
        public IScheduleComposite Parent { get { return null;} }

        /// <summary>
        /// Wurzelknotentyp des Schedules erstellen
        /// </summary>
        /// <param name="traits">Scheduleeigenschaften</param>
        public Root(Traits traits)
        {
            Traits = traits;
            Users = new List<Bearbeiter>();
        }
        
        /// <summary>
        /// Kindknoten anfügen
        /// </summary>
        /// <param name="item">Bearbeiterobjekt als Kindknoten</param>
        public void Add(Bearbeiter item)
        {
            Users.Add(item);
        }
        
        /// <summary>
        /// Besucherschnittstelle implementieren
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        public void Accept(IScheduleVisitor v)
        {
            v.Visit(this);
            foreach (Bearbeiter user in Users) user.Accept(v);
        }

        /// <summary>
        /// Degenerierte Besucherschnittstelle implementieren, Besucher nur einmal rückrufen
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        /// <param name="row">Position im Control als Zuständigkeitskriterium</param>
        public void AcceptOnce(IScheduleVisitor v, int row)
        {
            
            if (row < 0) return;
            Bearbeiter currUser = null;
            
            //in Bearbeiterteilbäumen nach dem zuständigen Bearbeiter suchen
            foreach (Bearbeiter user in Users) {
            
                if (user.Position > row) {
                
                    currUser.AcceptOnce(v, row);
                    return;
                }
                
                currUser = user;
            }

            currUser.AcceptOnce(v, row);
        }

        /// <summary>
        /// Auftragsanzahl ermitteln
        /// </summary>
        /// <returns>Anzahl Aufträge im Teilbaum</returns>
        public int CountOrders()
        {
            int i = 0;
            foreach (Bearbeiter user in Users) i += user.CountOrders();
            return i;
        }
    }
}

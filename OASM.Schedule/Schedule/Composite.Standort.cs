﻿using System;
using System.Collections.Generic;


namespace comain.Schedule
{

    /// <summary>
    /// Standortknoten des Schedules
    /// </summary>
    [Serializable]
    public class Standort : IScheduleComposite
    {

        /// <summary>
        /// tblpz.id_pz
        /// </summary>
        int id;
        /// <summary>
        /// tblpz.pznr
        /// </summary>
        String nr;
        /// <summary>
        /// tblpz.pzkey
        /// </summary>
        String key;
        /// <summary>
        /// tblpz.pzname
        /// </summary>
        String name;
        /// <summary>
        /// Liste dem Standort zugeordneten IHObjekte
        /// </summary>
        List<IHObjekt> myIHOs;
        
        /// <summary>
        /// Elternknoten geben
        /// </summary>
        public IScheduleComposite Parent { get; private set;}
       
        public Bearbeiter Bearbeiter { get { return (Bearbeiter)Parent; } }
       
        /// <summary>
        /// Adresse des Standortes im Widget, im Grid beispielsweise Zeilennummer
        /// </summary>
        public int Position { get; set; }
        /// <summary>
        /// Name des Standortes geben
        /// </summary>
        public String Name { get { return String.Format("[{0}] {1} - {2}", key, nr, name); } }
        
        /// <summary>
        /// Standortknoten des Schedules
        /// </summary>
        /// <param name="id_">tblpz.id_pz</param>
        /// <param name="nr_">tblpz.pznr</param>
        /// <param name="name_">tblpz.pzname</param>
        /// <param name="parent_">Elternknoten</param>
        public Standort(int id_, String nr_, String key_, String name_, Bearbeiter parent_)
        {
            id       = id_;
            nr       = nr_;
            key      = key_;
            name     = name_;
            myIHOs   = new List<IHObjekt>();
            Parent   = parent_;
        }
        
        /// <summary>
        /// Kindknoten anfügen
        /// </summary>
        /// <param name="item">IHObjekt als Kindknoten</param>
        public void Add(IHObjekt item)
        {
            myIHOs.Add(item);
        }
        
        /// <summary>
        /// Besucherschnittstelle implementieren
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        public void Accept(IScheduleVisitor v)
        {
            v.Visit(this);
            foreach (IHObjekt ih in myIHOs) ih.Accept(v);
        }

        /// <summary>
        /// Degenerierte Besucherschnittstelle implementieren, Besucher nur einmal rückrufen
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        /// <param name="row">Position im Control als Zuständigkeitskriterium</param>
        public void AcceptOnce(IScheduleVisitor v, int row)
        {
            
            //Standort ist direkt gemeint
            if (row == Position) {
                                    
                v.Visit(this);
                
            } else {
            
                IHObjekt currIHO = null;
                
                //in IHO-Teilbäumen nach dem zuständigen IHO suchen
                foreach (IHObjekt ih in myIHOs) {
                
                    if (ih.Position > row) {
                    
                        currIHO.AcceptOnce(v, row);
                        return;
                    }
                    
                    currIHO = ih;
                }

                currIHO.AcceptOnce(v, row);
            }
        }

        /// <summary>
        /// Auftragsanzahl ermitteln
        /// </summary>
        /// <returns>Anzahl Aufträge im Teilbaum</returns>
        public int CountOrders()
        {
            int i = 0;
            foreach (IHObjekt ih in myIHOs) i += ih.CountOrders();
            return i;
        }


        public override string ToString()
        {
            return Name;
        }
    }
}

﻿using System;


namespace comain.Schedule
{

    /// <summary>
    /// Knotenschnittstelle des Schedules
    /// </summary>
    public interface IScheduleComposite
    {
    
        /// <summary>
        /// Double dispatch-Schnittstelle für Besucher
        /// </summary>
        /// <param name="v">rückzurufender Besucher</param>
        void Accept(IScheduleVisitor v);
        /// <summary>
        /// Double dispatch-Schnittstelle
        /// </summary>
        /// <param name="v">rückzurufender Kettenknoten</param>
        /// <param name="rowIndex">auslösende Controlposition (wenn Kettenknoten für mehr als ein Controlelement veantwortlich ist)</param>
        void AcceptOnce(IScheduleVisitor v, int rowIndex);
        /// <summary>
        /// Elternknoten übergeben
        /// </summary>
        IScheduleComposite Parent { get; }
        /// <summary>
        /// Position im darstellenden Control zur Identifikation (z.B. Zeilennummer im Datagridview)
        /// </summary>
        int Position { get; set;}
        /// <summary>
        /// Anzahl vorhandener Auftragskoten für Progressbar ermitteln
        /// </summary>
        /// <returns></returns>
        int CountOrders();
    }
}

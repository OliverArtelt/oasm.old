﻿using System;
using System.Collections.Generic;


namespace comain.Schedule
{

    
    /// <summary>
    /// Bearbeiterknoten des Schedules
    /// </summary>
    [Serializable]
    public class Bearbeiter : IScheduleComposite
    {
        
        /// <summary>
        /// tblps.psnr
        /// </summary>
        String nr;
        /// <summary>
        /// tblps.psnachname,vorname
        /// </summary>
        String name;
        /// <summary>
        /// Liste dem Bearbeiter zugeordneten Standorte
        /// </summary>
        List<Standort> myPZs;    

        /// <summary>
        /// tblps.psid
        /// </summary>
        public int Id { get; private set;}
        /// <summary>
        /// Elterknoten (hier Planwurzel) geben
        /// </summary>
        public IScheduleComposite Parent { get; private set;}
       
        public Root Root { get { return (Root)Parent; } }
       
        /// <summary>
        /// Adresse des Bearbeiters im Widget, im Grid beispielsweise Zeilennummer
        /// </summary>
        public int Position { get; set; }
        /// <summary>
        /// Name des Bearbeiters geben
        /// </summary>
        public String Name { get { return (String.IsNullOrEmpty(name))? "<nicht zugeordnet>": name; } }


        /// <summary>
        /// Bearbeiterknoten des Schedules
        /// </summary>
        /// <param name="id_">tblps.psid</param>
        /// <param name="nr_">tblps.psnr</param>
        /// <param name="name_">tblps.psnachname,vorname</param>
        /// <param name="parent_">Elterknoten (hier Planwurzel)</param>
        public Bearbeiter(int id_, String nr_, String name_, Root parent_)
        {
            Id       = id_;
            nr       = nr_;
            name     = name_;
            myPZs    = new List<Standort>();
            Parent   = parent_;
        }
         
        /// <summary>
        /// Kindknoten anfügen
        /// </summary>
        /// <param name="item">Standort als Kindknoten</param>
        public void Add(Standort item)
        {
            myPZs.Add(item);
        }
        
        /// <summary>
        /// Besucherschnittstelle implementieren
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        public void Accept(IScheduleVisitor v)
        {
            v.Visit(this);
            foreach (Standort pz in myPZs) pz.Accept(v);
        }

        /// <summary>
        /// Degenerierte Besucherschnittstelle implementieren, Besucher nur einmal rückrufen
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        /// <param name="row">Position im Control als Zuständigkeitskriterium</param>
        public void AcceptOnce(IScheduleVisitor v, int row)
        {
            
            //Bearbeiter ist direkt gemeint
            if (row == Position) {
            
                v.Visit(this);
                
            } else {
             
                Standort currPZ = null;
                
                //in Standortteilbäumen nach dem zuständigen Standort suchen
                foreach (Standort pz in myPZs) {
                
                    if (pz.Position > row) {
                    
                        currPZ.AcceptOnce(v, row);
                        return;
                    }
                    
                    currPZ = pz;
                }

                currPZ.AcceptOnce(v, row);
            }    
        }

        /// <summary>
        /// Auftragsanzahl ermitteln
        /// </summary>
        /// <returns>Anzahl Aufträge im Teilbaum</returns>
        public int CountOrders()
        {
            int i = 0;
            foreach (Standort pz in myPZs) i += pz.CountOrders();
            return i;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

﻿using System;
using comain.User;


namespace comain.Schedule
{
    
    /// <summary>
    /// dynamische Spalten des Schedules verwalten (Datagridview, Excelsheet)
    /// </summary>
    public class GridColumns
    {
    
        Traits myTraits;
 
        private const int FirstDynamicColumn = 5;

 
#region I N D I C I E S
 
        
        /// <summary>
        /// Schedule hat eine Checkbox, womit Aufträge gewählt werden können
        /// </summary>
        public int CheckIndex     { get; private set;}
        /// <summary>
        /// Schedule hat eine Auftragscodespalte
        /// </summary>
        public int AuCodeIndex    { get; private set;}
        /// <summary>
        /// Schedule hat eine Spalte für die Auftragskurzbeschreibung
        /// </summary>
        public int AuKurzBIndex   { get; private set;}
        /// <summary>
        /// Schedule hat eine Spalte für den Turnus
        /// </summary>
        public int TurnusIndex    { get; private set;}
        /// <summary>
        /// Schedule hat eine Spalte für die Anzahl
        /// </summary>
        public int AnzahlIndex    { get; private set;}
        /// <summary>
        /// Schedule hat eine Spalte für die Differenz zwischen realisierter und geplanter Menge
        /// </summary>
        public int DAnzahlIndex   { get; private set;}
        /// <summary>
        /// Schedule hat eine Spalte für die Terminleiste
        /// </summary>
        public int DateIndex      { get; private set;}
        /// <summary>
        /// Hat Schedule eine Spalte für den Einzelpreis? (-1 == nicht vorhanden)
        /// </summary>
        public int EPreisIndex    { get; private set;}
        /// <summary>
        /// Hat Schedule eine Spalte für den Gesamtpreis? (-1 == nicht vorhanden)
        /// </summary>
        public int GPreisIndex    { get; private set;}
        /// <summary>
        /// Hat Schedule eine Spalte für den Differenzpreis? (-1 == nicht vorhanden)
        /// </summary>
        public int DPreisIndex    { get; private set;}
        /// <summary>
        /// Hat Schedule eine Spalte für die Einzelstunden? (-1 == nicht vorhanden)
        /// </summary>
        public int EStundenIndex  { get; private set;}
        /// <summary>
        /// Hat Schedule eine Spalte für die Gesamtstunden? (-1 == nicht vorhanden)
        /// </summary>
        public int GStundenIndex  { get; private set;}
        /// <summary>
        /// Hat Schedule eine Spalte für die Differenzstunden? (-1 == nicht vorhanden)
        /// </summary>
        public int DStundenIndex  { get; private set;}
        /// <summary>
        /// Hat Schedule eine Spalte für die Kostenstelle? (-1 == nicht vorhanden)
        /// </summary>
        public int KSTIndex       { get; private set;}
        /// <summary>
        /// Hat Schedule eine Spalte für den Budgetverbrauch? (-1 == nicht vorhanden)
        /// </summary>
        public int VerbrauchIndex { get; private set;}
        
        /// <summary>
        /// neue Gridzeile geben
        /// </summary>
        public object[] Row { get; private set; }
        /// <summary>
        /// Anzahl Spalten geben
        /// </summary>
        public int Count { get { return Row.Length; } }


#endregion

#region C O L U M N S 
       
        
        public object CheckColumn { get { return Row[CheckIndex];} set { Row[CheckIndex] = value;}}
        
        public object AuCodeColumn { get { return Row[AuCodeIndex];} set { Row[AuCodeIndex] = value;}}
        
        public object AuKurzBColumn { get { return Row[AuKurzBIndex];} set { Row[AuKurzBIndex] = value;}}
        
        public object TurnusColumn { get { return Row[TurnusIndex];} set { Row[TurnusIndex] = value;}}
        
        public object AnzahlColumn { get { return Row[AnzahlIndex];} set { Row[AnzahlIndex] = value;}}
        
        public object DAnzahlColumn { get { return Row[DAnzahlIndex];} set { Row[DAnzahlIndex] = value;}}
        
        public object DateColumn { get { return Row[DateIndex];} set { Row[DateIndex] = value;}}
        
        public object EPreisColumn 
        { 
            
            get {
            
                if (EPreisIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Einzelpreisspalte.");
                return Row[EPreisIndex];
            }
            
            set {
            
                if (EPreisIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Einzelpreisspalte.");
                Row[EPreisIndex] = value;
            }
        }
        
        public object GPreisColumn 
        {
         
            get {
            
                if (GPreisIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Gesamtpreisspalte.");
                return Row[GPreisIndex];
            }
         
            set {
            
                if (GPreisIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Gesamtpreisspalte.");
                Row[GPreisIndex] = value;
            }
        }
        
        public object DPreisColumn 
        {
         
            get {
            
                if (DPreisIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Differenzpreisspalte.");
                return Row[DPreisIndex];
            }
         
            set {
            
                if (DPreisIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Differenzpreisspalte.");
                Row[DPreisIndex] = value;
            }
        }

        public object EStundenColumn 
        {
         
            get {
            
                if (EStundenIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Einzelstundenspalte.");
                return Row[EStundenIndex];
            }
         
            set {
            
                if (EStundenIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Einzelstundenspalte.");
                Row[EStundenIndex] = value;
            }
        }
        
        public object GStundenColumn 
        {
         
            get {
            
                if (GStundenIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Gesamtstundenspalte.");
                return Row[GStundenIndex];
            }
         
            set {
            
                if (GStundenIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Gesamtstundenspalte.");
                Row[GStundenIndex] = value;
            }
        }
        
        public object DStundenColumn 
        {
         
            get {
            
                if (DStundenIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Differenzstundenspalte.");
                return Row[DStundenIndex];
            }
         
            set {
            
                if (DStundenIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Differenzstundenspalte.");
                Row[DStundenIndex] = value;
            }
        }
        
        public object VerbrauchColumn 
        {
         
            get {
            
                if (VerbrauchIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Spalte für das verbrauchte Budget.");
                return Row[VerbrauchIndex];
            }
         
            set {
            
                if (VerbrauchIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Spalte für das verbrauchte Budget.");
                Row[VerbrauchIndex] = value;
            }
        }
        
        public object KSTColumn 
        {
         
            get {
            
                if (KSTIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Kostenstellenspalte.");
                return Row[KSTIndex];
            }
         
            set {
            
                if (KSTIndex == -1) throw new ArgumentException("Dieser Plan besitzt keine Kostenstellenspalte.");
                Row[KSTIndex] = value;
            }
        }
    
#endregion

#region C O N S T R U C T I O N 

    
        /// <summary>
        /// dynamische Spalten des Schedules im Datagridview verwalten
        /// </summary>
        public GridColumns(Traits traits) 
        { 
        
            myTraits = traits; 
            
            CheckIndex     =  0;
            AuCodeIndex    =  1;
            AuKurzBIndex   =  2;
            TurnusIndex    =  3;
            AnzahlIndex    =  4;
            DateIndex      = -1; 
            EPreisIndex    = -1; 
            GPreisIndex    = -1;
            EStundenIndex  = -1;
            GStundenIndex  = -1;
            KSTIndex       = -1;
            VerbrauchIndex = -1;
            DAnzahlIndex   = -1;
            DPreisIndex    = -1; 
            DStundenIndex  = -1;
            

            int clm = GridColumns.FirstDynamicColumn;   //Auftragscheck,Au-Code,Au-Kurzbeschreibung,Turnus,Anzahl,Terminleiste

            if (myTraits.Projection[ScheduleParts.ShowKST])   { KSTIndex = clm++; }
            if (myTraits.Projection[ScheduleParts.ShowHours]) { EStundenIndex = clm++; }
            if (myTraits.Projection[ScheduleParts.ShowPrice]) { EPreisIndex = clm++; }

            DAnzahlIndex = clm++;

            if (myTraits.Projection[ScheduleParts.ShowHours]) { DStundenIndex = clm++; }
            if (myTraits.Projection[ScheduleParts.ShowPrice]) { DPreisIndex = clm++; }

            DateIndex = clm++;

            if (myTraits.Projection[ScheduleParts.ShowHours]) { GStundenIndex = clm++; }
            if (myTraits.Projection[ScheduleParts.ShowPrice] && myTraits.User is ClientUser) { VerbrauchIndex = clm++; }
            if (myTraits.Projection[ScheduleParts.ShowPrice]) { GPreisIndex = clm++; }

            Row = new object[clm]; 
        }
        

#endregion
        
    }
}

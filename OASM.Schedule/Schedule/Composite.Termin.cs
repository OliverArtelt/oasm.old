﻿using System;
using System.Text;
using System.ComponentModel;
using comain.User;


namespace comain.Schedule
{


    /// <summary>
    /// Terminknoten des Schedules
    /// </summary>
    [Serializable]
    public class Termin : IScheduleComposite
    {

 
#region P R O P E R T I E S
 
        
        /// <summary>
        /// Elternknoten 
        /// </summary>
        public IScheduleComposite  Parent { get; private set; }
       
        public Auftrag Auftrag { get { return (Auftrag)Parent; } }
       
        /// <summary>
        /// Terminslotschlüssel 
        /// </summary>
        public int Key { get; private set; }
        /// <summary>
        /// Zeile im Grid, das vom dazugehörigen Auftrag belegt wird, geben
        /// </summary>
        public int Position { get { return Parent.Position; } set {} }
        /// <summary>
        /// tblautermin.idautermin 
        /// </summary>
        public int PlannedId { get; private set; } 
        /// <summary>
        /// geplanter Terminzeitpunkt 
        /// </summary>
        public DateTime PlannedDate { get; private set; }
        /// <summary>
        /// tblrealzyklus.id_realzyklus 
        /// </summary>
        public int ImplementedId { get; private set; }
        /// <summary>
        /// Realisierungszeitpunkt
        /// </summary>
        public DateTime ImplementedDate { get; private set; }
        /// <summary>
        /// als Sonderleistung realisiert?
        /// </summary>
        public bool ImplementedSpecial { get; private set; }
        /// <summary>
        /// geplante Einzelstunden 
        /// </summary>
        public float Stunden { get; private set; }
        /// <summary>
        /// geplanter Einzelwert 
        /// </summary>
        public Decimal Wert { get; private set; }
        /// <summary>
        /// bearbeiten Termintyp 
        /// </summary>
        public EditedType EditedType { get; private set; }
        /// <summary>
        /// bearbeitete Menge 
        /// </summary>
        public short EditedMenge { get; private set; }
        /// <summary>
        /// geplante Menge 
        /// </summary>
        public short PlannedMenge { get; private set; }
        /// <summary>
        /// realisierte Menge 
        /// </summary>
        public short ImplementedMenge { get; private set; }
        /// <summary>
        /// Geplante Gesamtsumme (Tagesaufträge in Wochenansicht)
        /// </summary>
        public Decimal PlannedSum { get; private set; }
        /// <summary>
        /// Realisierte Gesamtsumme (Tagesaufträge in Wochenansicht)
        /// </summary>
        public Decimal ImplementedSum { get; private set; }
        /// <summary>
        /// Geplante Gesamtstunden (Tagesaufträge in Wochenansicht)
        /// </summary>
        public float PlannedHours { get; private set; }
        /// <summary>
        /// Realisierte Gesamtstunden (Tagesaufträge in Wochenansicht)
        /// </summary>
        public float ImplementedHours { get; private set; }
        
        /// <summary>
        /// geplanter Betrag für Statistiken, berücksichtigen, dass Termin in dieser Sitzung bearbeitetet werden konnte
        /// </summary>
        public Decimal PlannedValue 
        {
            get {
            
                if (EditedType == EditedType.Planned || EditedType == EditedType.Daily) 
                     return Wert * ((EditedMenge > 0)? EditedMenge: 1);
                else if (PlannedId != 0 && EditedType != EditedType.Deleted) 
                     return PlannedSum;
                else return 0;
            }
        }
        /// <summary>                                             
        /// realisierten Betrag für Statistiken, berücksichtigen, dass Termin in dieser Sitzung bearbeitetet werden konnte
        /// </summary>
        public Decimal ImplementedValue 
        {
            get {
            
                if (EditedType == EditedType.Common || EditedType == EditedType.Special) 
                     return Wert * ((EditedMenge > 0)? EditedMenge: 1);
                else if (ImplementedId != 0 && EditedType != EditedType.Deleted) 
                     return ImplementedSum;
                else return 0;
            }
        }

#endregion

        
        /// <summary>
        /// Terminknoten des Schedules
        /// </summary>
        /// <param name="parent_">Elternknoten</param>
        /// <param name="key_">Terminslotschlüssel</param>
        public Termin(Auftrag parent_, int key_)
        {
            
            PlannedId           = 0;
            PlannedMenge        = 0;
            Wert                = 0;
            PlannedSum          = 0;
            ImplementedSum      = 0;
            PlannedHours        = 0F;
            ImplementedHours    = 0F;
            Stunden             = 0F;
            ImplementedId       = 0;
            ImplementedSpecial  = false;
            ImplementedMenge    = 0;
            EditedType          = EditedType.Empty;
            EditedMenge         = 0;
            Parent              = parent_;
            Key                 = key_;
        }
        
        
        /// <summary>
        /// Terminmarkierung erstellen (z.Z. Zeichen + Ziffer - Kombination)
        /// </summary>
        /// <returns>Terminmarkierung</returns>
        public override String ToString()
        {
        
            //Termin/Realisierung wurde vom Bearbeiter gelöscht => leer darstellen 
            if (EditedType == EditedType.Deleted) return "";
            
            //normal realisiert
            if (EditedType == EditedType.Common) {

                if (EditedMenge > 1) return "o" + EditedMenge.ToString();
                return "o";
            }
        
            //als Sonderleistung realisiert
            if (EditedType == EditedType.Special) {

                if (EditedMenge > 1) return "S" + EditedMenge.ToString();
                return "S";
            }
            
            //als Tagesauftrag realisiert
            //#1101
            //if (EditedType == EditedType.Daily) {

            //    if (EditedMenge > 1) return "T" + EditedMenge.ToString();
            //    return "T";
            //}
            
            //geplant
            if (EditedType == EditedType.Planned) {

                if (EditedMenge > 1) return "x" + EditedMenge.ToString();
                return "x";
            }
            
            //#1101 Tagesaufträge in der Wochenplanung nicht mehr als 'T' markieren
            if (((Auftrag)Parent).IsDaily && 
                ((Auftrag)Parent).Traits.Type != ScheduleType.Week && 
                (ImplementedId > 0 || PlannedId > 0)) return "T";
            
            
            //nocht nicht bearbeitet, wurde aber früher schon realisiert
            if (ImplementedId > 0) {
            
                String s = (ImplementedSpecial)? "S": "o";
                
                if (ImplementedMenge > 1) return s + ImplementedMenge.ToString();
                return s;
            }
            
            //nocht nicht bearbeitet aber geplant
            if (PlannedId > 0) {
            
                String s = "x";

                if (PlannedMenge > 1) return s + PlannedMenge.ToString();
                return s;
            }
            
            //leerer Terminslot
            return "";
        }

        /// <summary>
        /// Termindaten verständlich ausgeben
        /// </summary>
        /// <returns>Termindaten als Tooltiptext</returns>
        public String Explain()
        {
        
            
            IUser user = ((Auftrag)Parent).Traits.User;
            StringBuilder bld = new StringBuilder();
            Auftrag au = (Auftrag)Parent;
            
            
            if (PlannedId != 0) {
                        
                
                bld.Append("Geplant ");
                if (!au.IsDaily || au.Traits.Type == ScheduleType.Week) bld.Append(PlannedDate.ToShortDateString());
                bld.Append(Environment.NewLine);
            
                
                if (user.SeePrices && Wert != 0) {   
                
                    bld.Append(PlannedMenge).Append(" x ");
                    
                    if (PlannedMenge * Wert == PlannedSum) bld.Append(Wert.ToString("C"));
                    else                                   bld.Append("diverse");

                    bld.Append(" = ").Append(PlannedSum.ToString("C")).Append(Environment.NewLine);
                }
                
                
                if (user.SeeHours && Stunden != 0) {
                    
                    bld.Append(PlannedMenge).Append(" x ");
                    
                    if (PlannedMenge * Stunden == PlannedHours) bld.Append(Stunden).Append(" h");
                    else                                        bld.Append("diverse");

                    bld.Append(" = ").Append(PlannedHours).Append(" h").Append(Environment.NewLine);
                }
                
                
                bld.Append(Environment.NewLine);   
            }
            
            
            if (ImplementedId != 0) {
            
                
                if (ImplementedSpecial) bld.Append("Sonderleistung ");
                else                    bld.Append("Realisiert ");
                if (!au.IsDaily || au.Traits.Type == ScheduleType.Week) bld.Append(ImplementedDate.ToShortDateString());
                bld.Append(Environment.NewLine);
                
                
                if (user.SeePrices && Wert != 0) {   
                
                    bld.Append(ImplementedMenge).Append(" x ");
                    
                    if (ImplementedMenge * Wert == ImplementedSum) bld.Append(Wert.ToString("C"));
                    else                                           bld.Append("diverse");

                    bld.Append(" = ").Append(ImplementedSum.ToString("C")).Append(Environment.NewLine);
                }
                
                
                if (user.SeeHours && Stunden != 0) {
                
                    bld.Append(ImplementedMenge).Append(" x ");
                    
                    if (ImplementedMenge * Stunden == ImplementedHours) bld.Append(Stunden).Append(" h");
                    else                                                bld.Append("diverse");

                    bld.Append(" = ").Append(ImplementedHours).Append(" h").Append(Environment.NewLine);
                }
                
                
                bld.Append(Environment.NewLine);    
            }
            
            
            if (EditedType != EditedType.Empty) {
            
                String[] types = { "", "Gelöscht", "Geplant", "Realisiert", "Sonderleistung", "Realisiert" };
            
                bld.Append("Bearbeitet: ").Append(types[(int)EditedType]).Append(Environment.NewLine);
                
                if (EditedType != EditedType.Deleted) {
                
                    if (user.SeePrices && Wert != 0)    
                        
                        bld.Append(EditedMenge).Append(" x ").Append(Wert.ToString("C")).Append(" = ")
                           .Append((EditedMenge * Wert).ToString("C")).Append(Environment.NewLine);
                    
                    if (user.SeeHours && Stunden != 0) 
                    
                        bld.Append(EditedMenge).Append(" x ").Append(Stunden).Append(" h").Append(" = ")
                           .Append(EditedMenge * Stunden).Append(" h").Append(Environment.NewLine);
                }
            }
        
            return bld.ToString().Trim(new char[] { '\n', '\r' });
        }
        
        /// <summary>
        /// Planung des Termines setzen
        /// </summary>
        /// <param name="id">tblautermin.idautermin</param>
        /// <param name="dat_">geplanter Terminzeitpunkt</param>
        /// <param name="menge_">geplante Menge</param>
        /// <param name="wert_">geplanter Wert</param>
        /// <param name="stunden_">geplante Stunden</param>
        public void SetPlanned(int id, DateTime dat_, short menge_, Decimal wert_, float stunden_)
        {
            PlannedId = id;
            PlannedDate = dat_;
            PlannedMenge += menge_;
            PlannedSum += menge_ * wert_;
            PlannedHours += menge_ * stunden_;
            Wert = wert_;
            Stunden = (float)Math.Round(stunden_, 2);
        }
        
        /// <summary>
        /// Realisierung des Termines setzen
        /// </summary>
        /// <param name="id">tblrealzyklus.id_realzyklus</param>
        /// <param name="date_">realiserter Terminzeitpunkt</param>
        /// <param name="spec_">als Sonderleistung realisert?</param>
        /// <param name="menge_">realisierte Menge</param>
        public void SetImplemented(int id, DateTime date_, bool spec_, short menge_, Decimal wert_, float stunden_)
        {
            ImplementedId = id;
            ImplementedDate = date_;
            ImplementedSpecial = spec_;
            ImplementedMenge += menge_;
            ImplementedSum += menge_ * wert_;
            ImplementedHours += menge_ * stunden_;
            Wert = wert_;
            Stunden = (float)Math.Round(stunden_, 2);
        }
        
        /// <summary>
        /// Termin bearbeiten
        /// </summary>
        /// <remarks>
        /// Löschen hat hier zwei Bedeutungen:
        /// 1. Termin wurde in dieser Sitzung bearbeitet und Löschen angefordert => diese Bearbeitung entfernen
        /// 2. Termin wurde in dieser Sitzung nicht bearbeitet oder Bearbeitung bereits wieder entfernt 
        ///    => drunter liegenden Typ (Planung, Realisiserung) entfernen
        /// </remarks>
        /// <param name="type">bearbeiteter Termintyp</param>
        /// <param name="menge">bearbeitete Menge</param>
        public void SetEdited(EditedType type, short menge)
        {   
        
            if (type == EditedType.Deleted) {
            
                if (EditedType != EditedType.Empty) EditedType = EditedType.Empty;
                else if (PlannedId != 0 || ImplementedId != 0) EditedType = EditedType.Deleted;
                
                EditedMenge = menge;
            
            } else {
            
                EditedType  = type;
                EditedMenge = menge;
                
                if (EditedType == EditedType.Planned || EditedType == EditedType.Common || EditedType == EditedType.Daily || EditedType == EditedType.Special) {
                
                    Wert = ((Auftrag)Parent).EPreis;
                    Stunden = ((Auftrag)Parent).EStunden;
                }
            }
        }


        public int MengenDifferenz
        {

            get {

                switch (EditedType) {

                    case EditedType.Planned: return ImplementedMenge - EditedMenge;
                    case EditedType.Common: return EditedMenge - PlannedMenge;                     
                    case EditedType.Deleted: return 0;
                    //case EditedType.Special: return EditedMenge - PlannedMenge;                     
                    default: return ImplementedMenge - PlannedMenge;
                }
            }
        }


        public decimal PreisDifferenz { get { return MengenDifferenz * ((Auftrag)Parent).EPreis; } }


        public float StundenDifferenz { get { return MengenDifferenz * ((Auftrag)Parent).EStunden; } }


        public decimal DeltaPlan
        {

            get {

                decimal editedSum = EditedMenge * ((Auftrag)Parent).EPreis;

                switch (EditedType) {

                    case EditedType.Planned: return editedSum;
                    case EditedType.Deleted: return 0;
                    default:                 return PlannedSum;
                }
            }
        }


        public decimal DeltaPreisDifferenz
        {

            get {

                decimal editedSum = EditedMenge * ((Auftrag)Parent).EPreis;

                switch (EditedType) {

                    case EditedType.Planned: return editedSum - ImplementedSum;
                    case EditedType.Common:  return PlannedSum - editedSum;                     
                    case EditedType.Deleted: return 0;
                    default:                 return PlannedSum - ImplementedSum;
                }
            }
        }

        /// <summary>
        /// Besucherschnittstelle implementieren
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        public void Accept(IScheduleVisitor v)
        {
            v.Visit(this);
        }

        /// <summary>
        /// Degenerierte Besucherschnittstelle implementieren, Besucher nur einmal rückrufen
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        /// <param name="row">Position im Control als Zuständigkeitskriterium</param>
        public void AcceptOnce(IScheduleVisitor v, int row)
        {
        }

        /// <summary>
        /// Auftragsanzahl ermitteln
        /// </summary>
        /// <returns>Anzahl Aufträge im Teilbaum</returns>
        public int CountOrders()
        {
            return 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using comain.Calendar;
using comain.User;


namespace comain.Schedule
{

    /// <summary>
    /// individuelle Planeigenschaften kapseln
    /// </summary>
    [Serializable]
    public class Traits
    {
                
        /// <summary>
        /// Anzahl Terminspalten geben
        /// </summary>
        public int Count { get { return Calendar.CalendarKeys.Count; }}
        /// <summary>
        /// Planungstyp geben
        /// </summary>
        public ScheduleType Type { get; private set; }
        /// <summary>
        /// Kalenderobjekt geben
        /// </summary>
        public ICalendarStrategy Calendar { get; private set; }
        /// <summary>
        /// Allgemeine OASM-Konfiguration, Grauzone etc.
        /// </summary>
        public Configuration OASMSettings { get; private set; }
        /// <summary>
        /// Benutzer
        /// </summary>
        public IUser User { get; private set; }
        /// <summary>
        /// Anzuzeigende Elemente/Modifikationen/Teile des Planes
        /// </summary>
        public BitVector32 Projection { get; private set; }
        /// <summary>
        /// Filterkriterien für SQLBuilder
        /// </summary>
        public Dictionary<String, List<String>> FilterValues { get; private set; }
        /// <summary>
        /// Filterkriterien für Berichtskopf
        /// </summary>
        public String FilterDescription { get; private set; }
        /// <summary>
        /// Name Schedule lang
        /// </summary>
        public String Name { get; private set; }
        /// <summary>
        /// Name Schedule kurz (Dateiname etc)
        /// </summary>
        public String ShortName { get; private set; }
        /// <summary>
        /// Name verwendeter Mandant/Datenbank
        /// </summary>
        public String Client { get; private set; }
        /// <summary>
        /// Name verwendeter Mandant/Datenbank
        /// </summary>
        public GridColumns Columns { get; private set; }
        /// <summary>
        /// Index der Terminleistenspalte
        /// </summary>
        public int DateColumn { get { return Columns.DateIndex; } }
        /// <summary>
        /// Formatter für Datumsangaben
        /// </summary>
        public CultureInfo CI { get; private set; }


        /// <summary>
        /// individuelle Planeigenschaften kapseln
        /// </summary>
        /// <param name="type">Planungstyp</param>
        /// <param name="cfg">OASM-Konfigurationsobjekt (Grauzone etc)</param>
        /// <param name="user">Angemeldeter Benutzer (Rechte)</param>
        /// <param name="ci">verwendete Kultur</param>
        /// <param name="year">Jahr der Jahresplanung (wenn keine Wochenplanung)</param>
        /// <param name="filterValues">Filterkriterien</param>
        /// <param name="filterDesc">Filterbeschreibung</param>
        /// <param name="client">Mandant (verwendete Datenbank)</param>
        public Traits(ScheduleType type, Configuration cfg, IUser user, int year, 
                      Dictionary<String, List<String>> filterValues, String filterDesc) 
        {
            
            FilterValues = filterValues;
            FilterDescription = filterDesc;            
            Type = type;
            OASMSettings = cfg;            
            User = user;
            Client = user.AuthenticateInfo.Client;            
            CI = new CultureInfo("de-DE");
            
            int v = 0;
            IList<String> l = filterValues["Proj"];
            if (l.Contains("ShowPrice"))    v += ScheduleParts.ShowPrice;
            if (l.Contains("ShowHours"))    v += ScheduleParts.ShowHours;
            if (l.Contains("ShowPZ"))       v += ScheduleParts.ShowPZ;
            if (l.Contains("ShowIH"))       v += ScheduleParts.ShowIH;
            if (l.Contains("ShowBG"))       v += ScheduleParts.ShowBG;
            if (l.Contains("ShowIHInvent")) v += ScheduleParts.ShowIHInvent;
            if (l.Contains("ShowKST"))      v += ScheduleParts.ShowKST;
            if (l.Contains("ShowCWYear"))   v += ScheduleParts.ShowCWYear;
            if (l.Contains("ShowBedarf"))   v += ScheduleParts.ShowBedarf;
            if (l.Contains("ShowRelevant")) v += ScheduleParts.ShowRelevant;
            Projection = new BitVector32(v); 


            if (Type == ScheduleType.Week) {
            
                DateTime von = DateTime.Parse(FilterValues["Period"][0]);
                DateTime bis = DateTime.Parse(FilterValues["Period"][1]);
                Calendar = new FreeDays(von, bis, user, CI);
            
            } else {
            
                if (Projection[ScheduleParts.ShowCWYear]) Calendar = new CalendarWeekYear(year, user, CI); 
                else                                      Calendar = new DayYear(year, user, CI);
            }
            

            switch (Type) {
            
            case ScheduleType.Week:
            
                Name = String.Format("Wochenplanung {0} - {1}", Calendar.DateVon.ToString("d"),
                                                                Calendar.DateBis.ToString("d"));                       
                ShortName = String.Format("Wochen {0} - {1}", Calendar.DateVon.ToString("d"),
                                                              Calendar.DateBis.ToString("d"));                       
                break;
            
            case ScheduleType.YearCurrent:
            
                Name = "Operative Planung " + year;
                ShortName = "Oper. Plan " + year; 
                break;
            
            case ScheduleType.YearNew:
            
                Name = "Strategische Planung " + year;
                ShortName = "Strat. Plan " + year; 
                break;
            
            case ScheduleType.YearOld:
            
                Name = "Alte Planung " + year;
                ShortName = "Alter Plan " + year; 
                break;
            }
            
            //Zeitfilter aktualisieren
            List<String> tl = new List<string>(2);
            tl.Add(Calendar.DateVon.ToShortDateString());
            tl.Add(Calendar.DateBis.ToShortDateString());
            FilterValues["Period"] = tl;

            Columns = new GridColumns(this);
        }
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;


namespace comain.Schedule
{


    /// <summary>
    /// Auftragsknoten des Schedules
    /// </summary>
    [Serializable]
    public class Auftrag : IScheduleComposite
    {
        
        /// <summary>
        /// Verbrauchtes Budget Auftrag (=> Gesamtpreisspalte)
        /// </summary>
        public Decimal BudgetUsed { get; set; }
        /// <summary>
        /// Gesamtbudget Auftrag (=> Gesamtpreisspalte)
        /// </summary>
        public Decimal BudgetTotal { get; set; }
        /// <summary>
        /// Gesamtstunden Auftrag (=> Gesamtstundenspalte)
        /// </summary>
        public float HoursTotal { get; set; }
        
        /// <summary>
        /// Elternknoten geben
        /// </summary>
        public IScheduleComposite Parent { get; private set; }
       
        public IHObjekt IHObjekt { get { return (IHObjekt)Parent; } }
       
        /// <summary>
        /// Planeigenschaften für Terminberechnung
        /// </summary>
        public Traits Traits { get; private set; }
        /// <summary>
        /// Auftrags-Id (tbl_auftrag.auid)
        /// </summary>
        public int Id { get; private set; }
        /// <summary>
        /// Adresse des Objektes im Widget, im Grid beispielsweise Zeilennummer
        /// </summary>
        public int Position { get; set; }
        /// <summary>
        /// Auftrag für Profilspeicherung markiert?
        /// </summary>
        public bool Selected { get; set; }
        /// <summary>
        /// Terminslots des Auftrages
        /// </summary>
        public SortedList<int, Termin> Dates { get; private set; }
        /// <summary>
        /// Auftragscode geben
        /// </summary>
        public String Nummer { get; private set; }
        /// <summary>
        /// Kurzbeschreibung des Auftrages geben
        /// </summary>
        public String Name { get; private set; }
        /// <summary>
        /// Terminzyklus geben
        /// </summary>
        public String Turnus { get; private set; }
        /// <summary>
        /// Kostenstellenbezeichnung geben
        /// </summary>
        public String Kst { get; private set; }
        /// <summary>
        /// ist Auftrag Tagesauftrag?
        /// </summary>
        public bool IsDaily { get; private set; }
        /// <summary>
        /// Einzelstunden geben
        /// </summary>
        public float EStunden { get; private set; }
        /// <summary>
        /// Auftragswert Einzelpreis geben
        /// </summary>
        public Decimal EPreis { get; private set; }
        /// <summary>
        /// geplante, zu realisierende Menge
        /// </summary>
        public int Menge { get; private set; }        
        /// <summary>
        /// Differenz zwischen geplanter und realisierter Menge eines Zeitraumes
        /// </summary>
        /// <remarks>
        /// 0: korrekt implementiert oder nichts zu tun und nichts passiert
        /// größer 0: überrealisiert (zu oft gearbeitet)
        /// kleiner 0: unterrealisiert (zu wenig gearbeitet)
        /// </remarks>
        public int MengenDifferenz { get; private set; }        
        /// <summary>
        /// Differenz zwischen geplante und realisierte Kosten eines Zeitraumes
        /// </summary>
        /// <remarks>
        /// 0: korrekt implementiert oder nichts zu tun und nichts passiert
        /// größer 0: überrealisiert (zu oft gearbeitet)
        /// kleiner 0: unterrealisiert (zu wenig gearbeitet)
        /// </remarks>
        public Decimal PreisDifferenz { get; private set; }        
        /// <summary>
        /// Differenz zwischen geplante und realisierte Stunden eines Zeitraumes
        /// </summary>
        /// <remarks>
        /// 0: korrekt implementiert oder nichts zu tun und nichts passiert
        /// größer 0: überrealisiert (zu oft gearbeitet)
        /// kleiner 0: unterrealisiert (zu wenig gearbeitet)
        /// </remarks>
        public float StundenDifferenz { get; private set; }        


        public String Leistungsart { get { return String.IsNullOrEmpty(Nummer)? String.Empty: Nummer.Substring(0, 1); } }


        /// <summary>
        /// Auftragsknoten des Schedules
        /// </summary>
        /// <param name="auid_">tbl_auftrag.auid</param>
        /// <param name="aucode_">tbl_auftrag.aucode</param>
        /// <param name="aukurzb_">tbl_auftrag.aukurzb - Kurzbeschreibung des Auftrages</param>
        /// <param name="daily_">Tagesauftrag?</param>
        /// <param name="turnus_">Terminzyklus (Anzahl per Woche/alle n Wochen)</param>
        /// <param name="kst_">Kostenstellenbezeichnung</param>
        /// <param name="menge_">Auftragsmenge</param>
        /// <param name="wert_">Auftragswert</param>
        /// <param name="stunden_">Auftragsstunden</param>
        /// <param name="parent_">Elternknoten</param>
        public Auftrag(int auid_, String aucode_, String aukurzb_, bool daily_, String turnus_, String kst_, 
                       int menge_, Decimal wert_, float stunden_, IHObjekt parent_)
        {
            Id       = auid_;
            Nummer   = aucode_;
            Name     = aukurzb_;
            IsDaily  = daily_;
            Turnus   = turnus_;
            Kst      = kst_;
            Menge    = menge_;
            EPreis   = wert_;
            EStunden = stunden_;
            Selected = false;
            Parent   = parent_;
            Traits   = ((Root)Parent.Parent.Parent.Parent).Traits;
            Dates    = new SortedList<int, Termin>(Traits.Count);
            
            //für jeden Terminslot leeren Termin anlegen, damit er bearbeitet oder eingefügt werden kann
            foreach (int i in Traits.Calendar.CalendarKeys) Dates.Add(i, new Termin(this, i));
        }
        
        /// <summary>
        /// Besucherschnittstelle implementieren
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        public void Accept(IScheduleVisitor v)
        {
            v.Visit(this);
            foreach (KeyValuePair<int, Termin> pair in Dates) pair.Value.Accept(v);
        }

        /// <summary>
        /// Degenerierte Besucherschnittstelle implementieren, Besucher nur einmal rückrufen
        /// </summary>
        /// <param name="v">Rückzurufender Besucher</param>
        /// <param name="row">Position im Control als Zuständigkeitskriterium</param>
        public void AcceptOnce(IScheduleVisitor v, int row)
        {
            v.Visit(this);
        }

        /// <summary>
        /// Auftragsanzahl ermitteln
        /// </summary>
        /// <returns>Anzahl Aufträge im Teilbaum</returns>
        public int CountOrders()
        {
            return 1;
        }

        /// <summary>
        /// Differenz zwischen geplanter und realisierten Mengen ermitteln
        /// </summary>
        /// <param name="vonKey">Start Kalenderschlüssel, null => ab Kalenderstart</param>
        /// <param name="bisKey">Ende Kalenderschlüssel, null => aktueller Schlüssel/Kalenderende bei vergangenen Schedules</param>
        public void SetMengenDifferenz(int vonKey, int bisKey)
        {

            MengenDifferenz = 0;
            PreisDifferenz = 0;
            StundenDifferenz = 0;

            foreach (var dt in Dates.Values.Where(p => p.Key >= vonKey && p.Key <= bisKey)) {

                MengenDifferenz  += dt.MengenDifferenz;
                PreisDifferenz   += dt.PreisDifferenz;
                StundenDifferenz += dt.StundenDifferenz;
            }
        }

        /// <summary>
        /// Delta-Berechnung: Was war geplant?
        /// </summary>
        /// <param name="vonKey">Betrachtungszeitraum (Delta) Start</param>
        /// <param name="bisKey">Betrachtungszeitraum (Delta) End</param>
        public decimal DeltaPlanned(int vonKey, int bisKey)
        {

            decimal sum = 0;

            foreach (var p in Dates.Values.Where(p => p.Key >= vonKey && p.Key <= bisKey)) {
             
                sum += p.DeltaPlan;
            }

            return sum;
        }

        /// <summary>
        /// Delta-Berechnung: Was war geplant, wurde nicht realisiert?
        /// </summary>
        /// <param name="vonKey">Betrachtungszeitraum (Delta) Start</param>
        /// <param name="bisKey">Betrachtungszeitraum (Delta) End</param>
        public decimal DeltaPlannedNotImplemented(int vonKey, int bisKey)
        {

            decimal sum = 0;

            foreach (var p in Dates.Values.Where(p => p.Key >= vonKey && p.Key <= bisKey)) {
             
                if (p.DeltaPreisDifferenz > 0) sum += p.DeltaPreisDifferenz;
            }

            return sum;
        }

        /// <summary>
        /// Was wurde realisiert, war aber nicht geplant?
        /// </summary>
        /// <param name="vonKey">Betrachtungszeitraum (Delta) Start</param>
        /// <param name="bisKey">Betrachtungszeitraum (Delta) End</param>
        public decimal DeltaImplementedNotPlanned(int vonKey, int bisKey)
        {

            decimal sum = 0;

            foreach (var p in Dates.Values.Where(p => p.Key >= vonKey && p.Key <= bisKey)) {
             
                if (p.DeltaPreisDifferenz < 0) sum += Math.Abs(p.DeltaPreisDifferenz);
            }

            return sum;
        }

        /// <summary>
        /// Delta-Berechnung: Daraus resultierende Restsumme
        /// </summary>
        /// <param name="vonKey">Betrachtungszeitraum (Delta) Start</param>
        /// <param name="bisKey">Betrachtungszeitraum (Delta) End</param>
        public decimal DeltaRestsumme(int vonKey, int bisKey)
        {
            
            return DeltaPlanned(vonKey, bisKey) - DeltaPlannedNotImplemented(vonKey, bisKey) + DeltaImplementedNotPlanned(vonKey, bisKey);
        }   

        /// <summary>
        /// bereits geplanten Termin eintragen
        /// </summary>
        /// <param name="key">Terminslotschlüssel</param>
        /// <param name="id">tblautermin.idautermin</param>
        /// <param name="dat_">Terminzeitpunkt</param>
        /// <param name="menge_">geplante Menge</param>
        /// <param name="wert_">geplanter Wert</param>
        /// <param name="stunden_">geplante Stunden</param>
        internal void EmbossPlannedDate(int key, int id, DateTime dat_, short menge_, Decimal wert_, float stunden_)
        {
            
            Dates[key].SetPlanned(id, dat_, menge_, wert_, stunden_);
        }
        
        /// <summary>
        /// bereits realisierten Termin eintragen
        /// </summary>
        /// <param name="key">Terminslotschlüssel</param>
        /// <param name="id">tblrealzyklus.id_realzyklus</param>
        /// <param name="date_">Realiserungszeitpunkt</param>
        /// <param name="spec_">als Sonderleistung realisiert?</param>
        /// <param name="menge_">realisierte Menge</param>
        internal void EmbossImplementedDate(int key, int id, DateTime date_, bool spec_, short menge_, Decimal wert_, float stunden_)
        {

            Dates[key].SetImplemented(id, date_, spec_, menge_, wert_, stunden_);
        }
        
        /// <summary>
        /// Termin bearbeiten
        /// </summary>
        /// <param name="key">Terminslotschlüssel</param>
        /// <param name="type">bearbeiteter Termintyp</param>
        /// <param name="menge">bearbeitete Menge</param>
        internal void EmbossEditedDate(int key, EditedType type, short menge)
        {
            Dates[key].SetEdited(type, menge);
        }

        
        public override string ToString()
        {
            return Nummer;
        }
    }
}

﻿using System;


namespace comain.Schedule
{
    
    /// <summary>
    /// Auftrag im Schedule suchen
    /// </summary>
    public class FindOrder : IScheduleVisitor
    {
    
        String myAuCode;
        int foundPosition = -1;
        int lastHeader = -1;
        
        public int Position { get { return foundPosition; } } 
        public int Header { get { return (foundPosition == -1)? -1: lastHeader; } } 
    
    
        /// <summary>
        /// Auftrag im Schedule suchen
        /// </summary>
        /// <param name="auCode">Auftragscode</param>
        public FindOrder(String auCode)
        {
            myAuCode = auCode;  
        }
        
        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
            if (foundPosition == -1) lastHeader = obj.Position + 1;    
        }

        public void Visit(Auftrag obj)
        {
            if (obj.Nummer == myAuCode) foundPosition = obj.Position;
        }

        public void Visit(Termin obj)
        {
        }
    }
}

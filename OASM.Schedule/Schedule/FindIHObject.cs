﻿using System;


namespace comain.Schedule
{
    
    
    public class FindIHObject : IScheduleVisitor
    {
    
        String myIhCode;
        int foundPosition = -1;
        
        public int Position { get { return foundPosition; } } 
    
    
        public FindIHObject(String ihCode)
        {
            myIhCode = ihCode;  
        }
        
        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
            if (obj.Nummer == myIhCode) foundPosition = obj.Position;
        }

        public void Visit(Auftrag obj)
        {
        }

        public void Visit(Termin obj)
        {
        }
    }
}

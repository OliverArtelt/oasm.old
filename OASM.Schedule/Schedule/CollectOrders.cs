﻿using System;
using System.Collections.Generic;


namespace comain.Schedule
{

    public class CollectOrders : IScheduleVisitor
    {
        
        List<String> myItems = new List<string>();
        public List<String> Items { get { myItems.Sort(); return myItems; } }

        
        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
        }

        public void Visit(Auftrag obj)
        {
            myItems.Add(obj.Nummer);    
        }

        public void Visit(Termin obj)
        {
        }
    }
}

﻿using System;


namespace comain.Schedule
{

    /// <summary>
    /// Zusatzteile des Planes
    /// </summary>
    public struct ScheduleParts
    {
        
        /// <summary>
        /// Preise anzeigen
        /// </summary>
        public static int ShowPrice = 1;
        /// <summary>
        /// geplante Stunden anzeigen
        /// </summary>
        public static int ShowHours = 2;
        /// <summary>
        /// Standortgruppierungen anzeigen
        /// </summary>
        public static int ShowPZ = 4;
        /// <summary>
        /// IH-Objektgruppierung anzeigen
        /// </summary>
        public static int ShowIH = 8;
        /// <summary>
        /// Baugruppen anzeigen
        /// </summary>
        public static int ShowBG = 16;
        /// <summary>
        /// Inventarnummer anzeigen
        /// </summary>
        public static int ShowIHInvent = 32;
        /// <summary>
        /// Kostenstellen anzeigen
        /// </summary>
        public static int ShowKST = 64;
        /// <summary>
        /// externe Auftragsnummer des Kunden anzeigen
        /// </summary>
        public static int ShowPosition = 128;
        /// <summary>
        /// Jahresplanung als Kalenderjahr (Schule Süd)
        /// </summary>
        public static int ShowCWYear = 256;
        /// <summary>
        /// Bedarfspositionen mit ausgeben (Aufträge ohne festen Termin)
        /// </summary>
        public static int ShowBedarf = 512;
        /// <summary>
        /// nur relevante Aufträge (mit geplanten Terminen im Zeitraum) anzeigen
        /// </summary>
        public static int ShowRelevant = 1024;
    }
   
    
    /// <summary>
    /// Plantypen
    /// </summary>
    public enum ScheduleType 
    { 
        /// <summary>
        /// kein Planungstyp gewählt
        /// </summary>
        None, 
        /// <summary>
        /// Planung vergangenes Jahr, gesperrt
        /// </summary>
        YearOld, 
        /// <summary>
        /// Planung aktuelles Jahr, Realisieren, Grauzonenunterstützung
        /// </summary>
        YearCurrent, 
        /// <summary>
        /// Planung zukünfiges Jahr, nur Planen
        /// </summary>
        YearNew, 
        /// <summary>
        /// Plan tagesgenauer, freier Zeitraum
        /// </summary>
        Week 
    }
}

﻿using System;


namespace comain.User
{

    /// <summary>
    /// Benutzerauthentifizierung, Rechteverwaltung und individuelle Planerstellung
    /// </summary>
    public interface IUser
    {
        
        /// <summary>
        /// Identifikator des Benutzers in der Datenbank
        /// </summary>
        int Id { get; }
        /// <summary>
        /// Name des Benutzers
        /// </summary>
        String Name { get; }
        
        /// <summary>
        /// Anmeldeinformationen für den Dienst geben
        /// </summary>
        Credentials AuthenticateInfo { get; }
        
        /// <summary>
        /// Benutzer darf Termine planen / löschen
        /// </summary>
        bool MayPlan { get; }
        /// <summary>
        /// Benutzer darf Termine realisieren
        /// </summary>
        bool MayImplement { get; }
        /// <summary>
        /// Benutzer darf Anwendung administrieren (Meister bearbeiten, Grauzone einstellen)
        /// </summary>
        bool MayAdminister { get; }
        /// <summary>
        /// Sieht Benutzer Grauzonenplanung (wenn sie denn eingeschaltet ist)
        /// </summary>
        bool MayUseGreyArea { get; }
        /// <summary>
        /// Benutzer darf Stundenplanung sehen
        /// </summary>
        bool SeeHours { get; }
        /// <summary>
        /// Benutzer darf Preise sehen
        /// </summary>
        bool SeePrices { get; }
    }
}

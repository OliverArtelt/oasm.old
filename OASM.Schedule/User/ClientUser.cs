﻿using System;


namespace comain.User
{
    
    /// <summary>
    /// Meisterzugang : Mitarbeiter des Kunden des Dienstleisters, die Leistungen beauftragen (Auftraggeber / Bereich)
    /// </summary>
    public class ClientUser : IUser
    {

        int id;
        String name;
        String kstFilter;
        bool mayPlan;
        bool seeHours;
        bool seePrices;
        Credentials myCredentials;
        

#region  C O N S T R U C T I O N

        /// <summary>
        /// Meister einrichten
        /// </summary>
        /// <param name="user">vom Service erhaltende Identifikation</param>
        public ClientUser(GrantedUser user, Credentials authinfo)
        {
        
            id = user.Id;
            name = user.Name;
            kstFilter = user.KstFilter;
            mayPlan = user.MayPlan;
            seeHours = user.SeeHours;
            seePrices = user.SeePrices;
            myCredentials = authinfo;
        }        

#endregion

#region I M P L E M E N T I O N 

        /// <summary>
        /// Identifikator des Meisters in der Datenbank (adept.idadept)
        /// </summary>
        public int Id { get { return id; }}
        /// <summary>
        /// Name des Meisters
        /// </summary>
        public string Name { get { return name; }}


        public bool MayPlan
        {
            get { return mayPlan; }
        }

        public bool MayImplement
        {
            get { return false; }
        }

        public bool MayAdminister
        {
            get { return false; }
        }

        public bool MayUseGreyArea
        {
            get { return true; }
        }

        public bool SeeHours
        {
            get { return seeHours; }
        }

        public bool SeePrices
        {
            get { return seePrices; }
        }

        public Credentials AuthenticateInfo
        {
            get { return myCredentials; }
        }
        
#endregion


    } 
}

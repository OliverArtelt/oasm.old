﻿using System;


namespace comain.User
{
   
    /// <summary>
    /// Mitarbeiter (Bearbeiter /  Administratoren) des Dienstleisters, die Leistungen bearbeiten (Auftragnehmer)
    /// </summary>           
    public class ServiceUser : IUser
    {


        int id;
        String name;
        bool isAdmin = false;
        Credentials myCredentials;
        
        
#region  C O N S T R U C T I O N
         
         
        /// <summary>
        /// Dienstleisterzugang: Zugangsdaten verifizieren und Administrator einrichten
        /// </summary>
        /// <param name="authinfo">Login-Informationen</param>
        /// <param name="logEvent">soll erfolgreicher Login protokolliert werden?</param>
        public ServiceUser(GrantedUser user, Credentials authinfo)
        {
            
            id = user.Id;
            name = user.Name;
            isAdmin = user.IsAdmin;
            myCredentials = authinfo;
        }

#endregion

#region I M P L E M E N T I O N 

        /// <summary>
        /// Identifikator des Administrators in der Datenbank (tblps.psid)
        /// </summary>
        public int Id { get { return id; }}
        /// <summary>
        /// Name des Administrators
        /// </summary>
        public string Name { get { return name; }}

        
        public bool MayPlan
        {
            get { return isAdmin; }
        }

        public bool MayImplement
        {
            get { return true; }
        }

        public bool MayAdminister
        {
            get { return isAdmin; }
        }

        public bool MayUseGreyArea
        {
            get { return isAdmin; }
        }

        public bool SeeHours
        {
            get { return true; }
        }

        public bool SeePrices
        {
            get { return isAdmin; }
        }

        public Credentials AuthenticateInfo
        {
            get { return myCredentials; }
        }
        
#endregion


    } 
}

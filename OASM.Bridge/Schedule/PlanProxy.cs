﻿using System;
using System.Linq;
using System.Collections.Generic;
using comain.User;
using comain.Adapter;
using comain.Connect;


namespace comain.Schedule
{
    
    public class PlanProxy: IPlanProxy
    {

        public ScheduleDTO GetPlan(User.Credentials authinfo, Dictionary<string, List<string>> config)
        {
                
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            ScheduleBuilder bld = new ScheduleBuilder(authinfo.Client, user, config);
            return bld.Schedule;
        }

        public void UpdatePlan(User.Credentials authinfo, IEnumerable<ModifiedDate> modified)
        {
                
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            SqlDmlBuilder bld = new SqlDmlBuilder(user.Name, user.Id, user.IsClientUser);
                
            bld.InsertBegin();
            SqlDmlDirector dir = new SqlDmlDirector(bld);
            dir.Create(modified);
            bld.InsertCommit();

            BatchExecuter exec = new BatchExecuter(authinfo.Client);
            exec.Execute(dir.CommandList);                
        }
    }
}

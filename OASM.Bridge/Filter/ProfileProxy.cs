﻿using System;
using System.Linq;
using comain.User;
using System.Collections.Generic;
using comain.Adapter;


namespace comain.Filter
{
    
    public class ProfileProxy : IProfileProxy
    {

        public void Add(Credentials authinfo, int profilId, IList<int> auList)
        {

            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            ProfileAdapter adp = new ProfileAdapter(authinfo.Client, user);
            adp.Add(profilId, auList);
        }

        public int Create(Credentials authinfo, string profilname, IList<int> auList)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            ProfileAdapter adp = new ProfileAdapter(authinfo.Client, user);
            return adp.Create(profilname, auList);
        }

        public void Delete(Credentials authinfo, int profilId)
        {
        
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            ProfileAdapter adp = new ProfileAdapter(authinfo.Client, user);
            adp.Delete(profilId);
        }

        public void Replace(Credentials authinfo, int profilId, IList<int> auList)
        {
        
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            ProfileAdapter adp = new ProfileAdapter(authinfo.Client, user);
            adp.Replace(profilId, auList);
        }
    }
}

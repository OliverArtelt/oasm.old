﻿using System;
using System.Collections.Generic;
using comain.Schedule;
using comain.User;
using comain.Adapter;


namespace comain.Filter
{
    
    public class FacadesProxy : IFacadesProxy
    {

        public List<PicklistItem> AuList(User.Credentials authinfo)
        {
                
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
            return adp.AuList();
        }

        public List<PicklistItem> BereichList(User.Credentials authinfo)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
            return adp.BereichList();
        }

        public List<PicklistItem> IHList(User.Credentials authinfo)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
            return adp.IHList();
        }

        public List<PicklistItem> KstList(User.Credentials authinfo)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
            return adp.KstList();
        }

        public List<PicklistItem> LaList(User.Credentials authinfo)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
            return adp.LaList();
        }

        public List<PicklistItem> ProfilList(User.Credentials authinfo)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
            return adp.ProfilList();
        }

        public List<PicklistItem> PzList(User.Credentials authinfo)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
            return adp.PzList();
        }
    }
}

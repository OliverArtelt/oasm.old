﻿using System;
using comain.Adapter;
using System.Security.Authentication;


namespace comain.User
{
    
    public class MasterProxy : IMasterProxy
    {

        public Master Get(Credentials authinfo)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");
                
            MasterAdapter adp = new MasterAdapter();
            adp.Load(authinfo.Client);
            return adp.Dataset;
        }

        public void Set(Credentials authinfo, Master data)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");

            MasterAdapter adp = new MasterAdapter(data);
            adp.Save(authinfo.Client, user);
        }
    }
}

﻿using System;
using comain.Adapter;
using System.Security.Authentication;


namespace comain.User
{
    
    public class ProtocolProxy : IProtocolProxy
    {

        public Protocol Get(Credentials authinfo, int type, DateTime? logVon, DateTime? logBis, DateTime? datVon, DateTime? datBis, string ps, string auCode)
        {

            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst benötigt Administratorenrechte.");
                
            UserProtocolFactory f = new UserProtocolFactory(type, logVon, logBis, datVon, datBis, ps, auCode);
            return f.GetProtocol(authinfo.Client);
        }

        public int DeleteEntries(Credentials authinfo, DateTime von, DateTime bis)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst benötigt Administratorenrechte.");
                
            return UserProtocolFactory.DeleteEntries(authinfo.Client, von, bis);
        }
    }
}

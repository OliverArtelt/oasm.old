﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using comain.Adapter;
using comain.Connect;


namespace comain.User
{
   
    public class UserProxy : IUserProxy
    {
        
        public GrantedUser Create(Credentials authinfo)
        {
            
            GrantedUser u = GrantedUserFactory.CreateUser(authinfo);
            return u;
        }

        public IEnumerable<string> Clients()
        {

            ConnectionList cl = null;
            String path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            
            using (XmlReader rdr = XmlReader.Create(path + Path.DirectorySeparatorChar + "db.config")) {
                
                cl = new ConnectionList(rdr);
                return cl.Names;
            }
        }
    }
}

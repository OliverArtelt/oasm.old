﻿using System;
using System.Collections.Generic;
using comain.User;
using comain.Adapter;


namespace comain.Calendar
{
    
    public class HolidayProxy : IHolidayProxy
    {

        public List<Vacation> GetVacationCloseDown(User.Credentials authinfo, DateTime von, DateTime bis)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            HolidayAdapter adp = new HolidayAdapter(authinfo.Client);
            return adp.GetVacationCloseDown(von, bis);
        }

        public List<Holiday> GetHoliday(User.Credentials authinfo, DateTime von, DateTime bis)
        {
            
            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            HolidayAdapter adp = new HolidayAdapter(authinfo.Client);
            return adp.GetHoliday(von, bis);;
        }
    }
}

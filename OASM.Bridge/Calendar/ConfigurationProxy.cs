﻿using System;
using comain.User;
using comain.Adapter;
using System.Security.Authentication;


namespace comain.Calendar
{
    
    public class ConfigurationProxy : IConfigurationProxy
    {
        
        public Configuration ReadConfig(User.Credentials authinfo)
        {

            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                
            using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                return mapper.Read();
            }
        }

       
        public BaseConfiguration ReadBaseConfig(User.Credentials authinfo)
        {

            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                
            using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                return mapper.ReadBase();
            }
        }

        
        public void WriteBaseConfig(User.Credentials authinfo, BaseConfiguration cfg)
        {

            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");
                
            using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                mapper.WriteBase(cfg, user);
            }
        }

        
        public BaseExportConfiguration ReadBaseExportConfig(User.Credentials authinfo)
        {

            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                
            using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                return mapper.ReadExport();
            }
        }

        
        public void WriteBaseExportConfig(User.Credentials authinfo, BaseExportConfiguration cfg)
        {

            GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
            if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");
               
            using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                mapper.WriteExport(cfg, user);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using comain.Calendar.HolidayService;


namespace comain.Calendar
{
    
    public class HolidayProxy : IHolidayProxy
    {
        
        public List<Vacation> GetVacationCloseDown(User.Credentials authinfo, DateTime von, DateTime bis)
        {

            HolidayClient proxy = null;
            List<Vacation> list;

            try {
                        
                proxy = new HolidayClient();
                list = proxy.GetVacationCloseDown(authinfo, von, bis);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }

        public List<Holiday> GetHoliday(User.Credentials authinfo, DateTime von, DateTime bis)
        {

            HolidayClient proxy = null;
            List<Holiday> list;

            try {
                        
                proxy = new HolidayClient();
                list = proxy.GetHoliday(authinfo, von, bis);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }
    }
}

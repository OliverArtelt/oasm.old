﻿using System;
using comain.Calendar.ConfigurationService;


namespace comain.Calendar
{
    
    public class ConfigurationProxy : IConfigurationProxy
    {
        
        public Configuration ReadConfig(User.Credentials authinfo)
        {

            ConfigurationClient proxy = null;
            Configuration cfg;

            try {
                        
                proxy = new ConfigurationClient();
                cfg = proxy.ReadConfig(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return cfg;
        }

        public BaseConfiguration ReadBaseConfig(User.Credentials authinfo)
        {

            ConfigurationClient proxy = null;
            BaseConfiguration cfg;

            try {
                        
                proxy = new ConfigurationClient();
                cfg = proxy.ReadBaseConfig(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return cfg;
        }

        public void WriteBaseConfig(User.Credentials authinfo, BaseConfiguration cfg)
        {

            ConfigurationClient proxy = null;

            try {
                        
                proxy = new ConfigurationClient();
                proxy.WriteBaseConfig(authinfo, cfg);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }
        }

        public BaseExportConfiguration ReadBaseExportConfig(User.Credentials authinfo)
        {

            ConfigurationClient proxy = null;
            BaseExportConfiguration cfg;

            try {
                        
                proxy = new ConfigurationClient();
                cfg = proxy.ReadBaseExportConfig(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return cfg;
        }

        public void WriteBaseExportConfig(User.Credentials authinfo, BaseExportConfiguration cfg)
        {

            ConfigurationClient proxy = null;

            try {
                        
                proxy = new ConfigurationClient();
                proxy.WriteBaseExportConfig(authinfo, cfg);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using comain.Schedule.PlanService;


namespace comain.Schedule
{
    
    public class PlanProxy: IPlanProxy
    {
        
        public ScheduleDTO GetPlan(User.Credentials authinfo, Dictionary<string, List<string>> config)
        {

            PlanClient proxy = null;
            ScheduleDTO data;

            try {
                        
                proxy = new PlanClient();
                data = proxy.GetPlan(authinfo, config);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return data;
        }

        public void UpdatePlan(User.Credentials authinfo, IEnumerable<ModifiedDate> modified)
        {

            PlanClient proxy = null;

            try {
                        
                proxy = new PlanClient();
                proxy.UpdatePlan(authinfo, modified.ToList());
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }
        }
    }
}

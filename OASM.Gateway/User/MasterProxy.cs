﻿using System;
using comain.User.MasterService;


namespace comain.User
{
    
    public class MasterProxy : IMasterProxy
    {
        
        public Master Get(Credentials authinfo)
        {

            MasterClient proxy = null;
            Master data;

            try {
                        
                proxy = new MasterClient();
                data = proxy.Get(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return data;
        }

        
        public void Set(Credentials authinfo, Master data)
        {

            MasterClient proxy = null;

            try {
                        
                proxy = new MasterClient();
                proxy.Set(authinfo, data);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }
        }
    }
}

﻿using System;
using comain.User.ProtocolService;


namespace comain.User
{
    
    public class ProtocolProxy : IProtocolProxy
    {
        
        public Protocol Get(Credentials authinfo, int type, DateTime? logVon, DateTime? logBis, 
                            DateTime? datVon, DateTime? datBis, string user, string auCode)
        {

            UserProtocolClient proxy = null;
            Protocol log;

            try {
                        
                proxy = new UserProtocolClient();
                log = proxy.Get(authinfo, type, logVon, logBis, datVon, datBis, user, auCode);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return log;
        }

        
        public int DeleteEntries(Credentials authinfo, DateTime von, DateTime bis)
        {

            UserProtocolClient proxy = null;
            int result;

            try {
                        
                proxy = new UserProtocolClient();
                result = proxy.DeleteEntries(authinfo, von, bis);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return result;
        }
    }
}

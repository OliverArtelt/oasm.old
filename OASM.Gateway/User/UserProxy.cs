﻿using System;
using comain.User.UserService;
using System.Collections.Generic;


namespace comain.User
{
   
    public class UserProxy : IUserProxy
    {
        
        public GrantedUser Create(Credentials authinfo)
        {

            UserFactoryClient proxy = null;
            GrantedUser user;

            try {
                        
                proxy = new UserFactoryClient();
                user = proxy.Create(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return user;
        }

        public IEnumerable<string> Clients()
        {

            UserFactoryClient proxy = null;
            IEnumerable<string> list;

            try {
                        
                proxy = new UserFactoryClient();
                list = proxy.Clients();
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }
    }
}

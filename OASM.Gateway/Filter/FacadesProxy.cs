﻿using System;
using System.Collections.Generic;
using comain.Schedule;
using comain.Filter.FacadeService;


namespace comain.Filter
{
    
    public class FacadesProxy : IFacadesProxy
    {

        public List<PicklistItem> AuList(comain.User.Credentials authinfo)
        {

            FacadesClient proxy = null;
            List<PicklistItem> list;

            try {
                        
                proxy = new FacadesClient();
                list = proxy.AuList(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }


        public List<PicklistItem> BereichList(comain.User.Credentials authinfo)
        {

            FacadesClient proxy = null;
            List<PicklistItem> list;

            try {
                        
                proxy = new FacadesClient();
                list = proxy.BereichList(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }


        public List<PicklistItem> IHList(comain.User.Credentials authinfo)
        {

            FacadesClient proxy = null;
            List<PicklistItem> list;

            try {
                        
                proxy = new FacadesClient();
                list = proxy.IHList(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }


        public List<PicklistItem> KstList(comain.User.Credentials authinfo)
        {

            FacadesClient proxy = null;
            List<PicklistItem> list;

            try {
                        
                proxy = new FacadesClient();
                list = proxy.KstList(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }


        public List<PicklistItem> LaList(comain.User.Credentials authinfo)
        {

            FacadesClient proxy = null;
            List<PicklistItem> list;

            try {
                        
                proxy = new FacadesClient();
                list = proxy.LaList(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }


        public List<PicklistItem> ProfilList(comain.User.Credentials authinfo)
        {

            FacadesClient proxy = null;
            List<PicklistItem> list;

            try {
                        
                proxy = new FacadesClient();
                list = proxy.ProfilList(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }


        public List<PicklistItem> PzList(comain.User.Credentials authinfo)
        {

            FacadesClient proxy = null;
            List<PicklistItem> list;

            try {
                        
                proxy = new FacadesClient();
                list = proxy.PzList(authinfo);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return list;
        }
    }
}

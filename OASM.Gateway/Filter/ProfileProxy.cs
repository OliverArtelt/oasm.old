﻿using System;
using System.Linq;
using comain.User;
using System.Collections.Generic;
using comain.Filter.ProfileService;


namespace comain.Filter
{
    
    public class ProfileProxy : IProfileProxy
    {

        public void Add(Credentials authinfo, int profilId, IList<int> auList)
        {

            ProfileClient proxy = null;

            try {
                        
                proxy = new ProfileClient();
                proxy.Add(authinfo, profilId, auList.ToList());
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }
        }

        
        public int Create(Credentials authinfo, string profilname, IList<int> auList)
        {

            ProfileClient proxy = null;
            int result;

            try {
                        
                proxy = new ProfileClient();
                result = proxy.Create(authinfo, profilname, auList.ToList());
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }

            return result;
        }

        
        public void Delete(Credentials authinfo, int profilId)
        {

            ProfileClient proxy = null;

            try {
                        
                proxy = new ProfileClient();
                proxy.Delete(authinfo, profilId);
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }
        }

        
        public void Replace(Credentials authinfo, int profilId, IList<int> auList)
        {

            ProfileClient proxy = null;

            try {
                        
                proxy = new ProfileClient();
                proxy.Replace(authinfo, profilId, auList.ToList());
                proxy.Close();
            }
            catch (Exception) { 
            
                if (proxy != null) proxy.Abort();
                throw;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using comain.Adapter;
using comain.Calendar;
using comain.User;


namespace comain.Service.Calendar
{
    
    [ServiceBehavior(Namespace = "http://oasm.cubeoffice.de")]
    public class HolidaySvc : IHoliday
    {

        
        public List<Vacation> GetVacationCloseDown(comain.User.Credentials authinfo, DateTime von, DateTime bis)
        {
                   
            try {
                
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                HolidayAdapter adp = new HolidayAdapter(authinfo.Client);
                return adp.GetVacationCloseDown(von, bis);
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        
        public List<Holiday> GetHoliday(comain.User.Credentials authinfo, DateTime von, DateTime bis)
        {

                   
            try {
                
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                HolidayAdapter adp = new HolidayAdapter(authinfo.Client);
                return adp.GetHoliday(von, bis);;
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using comain.Calendar;
using System.Security.Authentication;


namespace comain.Service.Calendar
{
    
    /// <summary>
    /// Freie Arbeitstage des Zeitraumes geben damit diese entsprechend gekennzeichnet werden können
    /// </summary>
    [ServiceContract(Namespace = "http://oasm.cubeoffice.de")]
    public interface IHoliday
    {
        
        /// <summary>
        /// Betriebsferien geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="von">Zeitraum von</param>
        /// <param name="bis">Zeitraum bis</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        List<Vacation> GetVacationCloseDown(comain.User.Credentials authinfo, DateTime von, DateTime bis);
        
        /// <summary>
        /// Gesetzliche Feiertage geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="von">Zeitraum von</param>
        /// <param name="bis">Zeitraum bis</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        List<Holiday> GetHoliday(comain.User.Credentials authinfo, DateTime von, DateTime bis);
    }
}

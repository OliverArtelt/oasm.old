﻿using System;
using System.Security.Authentication;
using System.ServiceModel;
using comain.Calendar;


namespace comain.Service.Calendar
{

    /// <summary>
    /// Konfigurations-DTO geben/aktualisieren
    /// </summary>
    [ServiceContract(Namespace = "http://oasm.cubeoffice.de")]
    public interface IConfiguration
    {
        
        /// <summary>
        /// Aktuell zu verwendende Konfiguration geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        Configuration ReadConfig(comain.User.Credentials authinfo);
        
        /// <summary>
        /// Konfigurationseinstellungen geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        BaseConfiguration ReadBaseConfig(comain.User.Credentials authinfo);
        
        /// <summary>
        /// Konfigurationseinstellungen schreiben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="cfg"></param>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        void WriteBaseConfig(comain.User.Credentials authinfo, BaseConfiguration cfg);
        
        /// <summary>
        /// Exporteinstellungen geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        BaseExportConfiguration ReadBaseExportConfig(comain.User.Credentials authinfo);
        
        /// <summary>
        /// Exporteinstellungen schreiben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="cfg"></param>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        void WriteBaseExportConfig(comain.User.Credentials authinfo, BaseExportConfiguration cfg);
    }
}

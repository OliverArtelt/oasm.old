﻿using System;
using System.Security.Authentication;
using System.ServiceModel;
using comain.Adapter;
using comain.Calendar;
using comain.User;


namespace comain.Service.Calendar
{

    [ServiceBehavior(Namespace = "http://oasm.cubeoffice.de")]
    public class ConfigurationSvc : IConfiguration
    {

        public Configuration ReadConfig(Credentials authinfo)
        {
            
            try {
        
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                
                using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                    return mapper.Read();
                }
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        
        public BaseConfiguration ReadBaseConfig(Credentials authinfo)
        {
            
            try {
        
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                
                using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                    return mapper.ReadBase();
                }
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        
        public void WriteBaseConfig(Credentials authinfo, BaseConfiguration cfg)
        {
        
            try {
        
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");
                
                using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                    mapper.WriteBase(cfg, user);
                }
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        
        public BaseExportConfiguration ReadBaseExportConfig(Credentials authinfo)
        {
            
            try {
        
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                
                using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                    return mapper.ReadExport();
                }
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        
        public void WriteBaseExportConfig(Credentials authinfo, BaseExportConfiguration cfg)
        {
        
            try {
        
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");
               
                using (ConfigAdapter mapper = new ConfigAdapter(authinfo.Client)) {
                
                    mapper.WriteExport(cfg, user);
                }
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }
    }
}

﻿using System;
using comain.Adapter;
using comain.User;
using System.Security.Authentication;
using System.ServiceModel;


namespace comain.Service.User
{
   
    [ServiceBehavior(Namespace = "http://oasm.cubeoffice.de")]
    public class MasterSvc : IMaster
    {

        public Master Get(Credentials authinfo)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");
                
                MasterAdapter adp = new MasterAdapter();
                adp.Load(authinfo.Client);
                return adp.Dataset;
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        public void Set(Credentials authinfo, comain.User.Master data)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");

                MasterAdapter adp = new MasterAdapter(data);
                adp.Save(authinfo.Client, user);
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }
    }
}

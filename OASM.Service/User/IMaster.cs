﻿using System.ServiceModel;
using comain.User;
using System;
using System.Security.Authentication;


namespace comain.Service.User
{

    [ServiceContract(Namespace = "http://oasm.cubeoffice.de")]
    public interface IMaster
    {
        
        /// <summary>
        /// Ado.Net-Dataset mit Meisterzugängen geben
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <returns>Dataset</returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        Master Get(Credentials authinfo);
        
        /// <summary>
        /// (Verändertes) Ado.Net-Dataset mit Meisterzugängen speichern und aktualisiert zurückgeben
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <param name="data">zu speicherndes Dataset</param>
        /// <returns>Dataset mit aktualisierten DB-Defaults</returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        void Set(Credentials authinfo, Master data);
    }
}

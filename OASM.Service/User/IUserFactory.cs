﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using comain.User;
using System.Security.Authentication;


namespace comain.Service.User
{

    [ServiceContract(Namespace = "http://oasm.cubeoffice.de")]
    public interface IUserFactory
    {

        /// <summary>
        /// Benutzer erstellen/geben
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        GrantedUser Create(comain.User.Credentials authinfo);

        /// <summary>
        /// vorhandene Mandanten/Datenbanken geben
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        IEnumerable<String> Clients();
    }
}

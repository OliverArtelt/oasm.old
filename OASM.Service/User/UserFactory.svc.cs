﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Xml;
using comain.Adapter;
using comain.Connect;
using comain.User;


namespace comain.Service.User
{

    [ServiceBehavior(Namespace = "http://oasm.cubeoffice.de")]
    public class UserFactorySvc : IUserFactory
    {

        public GrantedUser Create(comain.User.Credentials authinfo)
        {
            
            try {
            
                GrantedUser u = GrantedUserFactory.CreateUser(authinfo);
                return u;
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }


        public IEnumerable<String> Clients()
        {

            ConnectionList cl = null;
            String path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            
            try {
            
                using (XmlReader rdr = XmlReader.Create(path + "db.config")) {
                
                    cl = new ConnectionList(rdr);
                    return cl.Names;
                }
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }
    }
}

﻿using System;
using System.Security.Authentication;
using System.ServiceModel;
using comain.Adapter;
using comain.User;


namespace comain.Service.User
{

    [ServiceBehavior(Namespace = "http://oasm.cubeoffice.de")]
    public class UserProtocolSvc : IUserProtocol
    {

        public Protocol Get(comain.User.Credentials authinfo, 
                            int type, 
                            DateTime? logVon, DateTime? logBis, DateTime? datVon, DateTime? datBis, 
                            string ps, string auCode)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");
                
                UserProtocolFactory f = new UserProtocolFactory(type, logVon, logBis, datVon, datBis, ps, auCode);
                return f.GetProtocol(authinfo.Client);
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }


        public int DeleteEntries(Credentials authinfo, DateTime von, DateTime bis)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                if (!user.IsAdmin) throw new AuthenticationException("Dieser Dienst verlangt Administratorenrechte.");
                
                return UserProtocolFactory.DeleteEntries(authinfo.Client, von, bis);
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }
    }
}

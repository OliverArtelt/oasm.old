﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using comain.Adapter;
using comain.User;


namespace comain.Service.Filter
{

    [ServiceBehavior(Namespace = "http://oasm.cubeoffice.de")]
    public class ProfileSvc : IProfile
    {

        public int Create(comain.User.Credentials authinfo, string profilname, IList<int> auList)
        {
            
            try {
        
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                ProfileAdapter adp = new ProfileAdapter(authinfo.Client, user);
                return adp.Create(profilname, auList);
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        
        public void Add(comain.User.Credentials authinfo, int profilId, IList<int> auList)
        {
            
            try {
        
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                ProfileAdapter adp = new ProfileAdapter(authinfo.Client, user);
                adp.Add(profilId, auList);
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        
        public void Delete(comain.User.Credentials authinfo, int profilId)
        {
            
            try {
        
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                ProfileAdapter adp = new ProfileAdapter(authinfo.Client, user);
                adp.Delete(profilId);
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        
        public void Replace(comain.User.Credentials authinfo, int profilId, IList<int> auList)
        {
            
            try {
        
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                ProfileAdapter adp = new ProfileAdapter(authinfo.Client, user);
                adp.Replace(profilId, auList);
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }
    }
}

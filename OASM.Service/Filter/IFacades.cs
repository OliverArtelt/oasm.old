﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using comain.Schedule;
using comain.User;
using System.Security.Authentication;


namespace comain.Service.Filter
{

    [ServiceContract(Namespace = "http://oasm.cubeoffice.de")]
    public interface IFacades
    {
        
        /// <summary>
        /// Auftragsliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        List<PicklistItem> AuList(comain.User.Credentials authinfo);
        
        /// <summary>
        /// Bereichsliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        List<PicklistItem> BereichList(comain.User.Credentials authinfo);
        
        /// <summary>
        /// IH-Objektliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        List<PicklistItem> IHList(comain.User.Credentials authinfo);
        
        /// <summary>
        /// Kostenstellenliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        List<PicklistItem> KstList(comain.User.Credentials authinfo);
        
        /// <summary>
        /// Leistungsarten als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        List<PicklistItem> LaList(comain.User.Credentials authinfo);

        /// <summary>
        /// Profilliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        List<PicklistItem> ProfilList(comain.User.Credentials authinfo);

        /// <summary>
        /// Standortliste als Filtermöglichkeit geben
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        List<PicklistItem> PzList(comain.User.Credentials authinfo);
    }
}

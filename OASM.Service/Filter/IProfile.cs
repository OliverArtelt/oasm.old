﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using comain.User;
using System.Security.Authentication;


namespace comain.Service.Filter
{

    /// <summary>
    /// Profilverwaltung des Benutzers
    /// </summary>
    [ServiceContract(Namespace = "http://oasm.cubeoffice.de")]
    public interface IProfile
    {
        
        /// <summary>
        /// Aufträge an ein Profil anfügen
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="profilId">Id des Profiles</param>
        /// <param name="auList">Liste der zu verwendenden Auftrag-Id's</param>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        void Add(Credentials authinfo, int profilId, IList<int> auList);
        
        /// <summary>
        /// Neues Profil anlegen
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="profilname">Name des Profiles</param>
        /// <param name="auList">Liste der zu verwendenden Auftrag-Id's</param>
        /// <returns>neue Profil-Id</returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        int Create(Credentials authinfo, String profilname, IList<int> auList);
        
        /// <summary>
        /// Profil löschen
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="profilId">Id des Profiles</param>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        void Delete(Credentials authinfo, int profilId);
        
        /// <summary>
        /// Aufträge des Profiles durch neue ersetzen
        /// </summary>
        /// <param name="authinfo">Benutzerauthentifizierung</param>
        /// <param name="profilId">Id des Profiles</param>
        /// <param name="auList">Liste der zu verwendenden Auftrag-Id's</param>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        void Replace(Credentials authinfo, int profilId, IList<int> auList);
    }
}

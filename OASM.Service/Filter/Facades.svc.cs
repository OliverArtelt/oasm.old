﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using comain.Adapter;
using comain.Schedule;
using comain.User;


namespace comain.Service.Filter
{

    [ServiceBehavior(Namespace = "http://oasm.cubeoffice.de")]
    public class FacadeSvc : IFacades
    {
        
        public List<PicklistItem> AuList(Credentials authinfo)
        {            
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
                return adp.AuList();
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        public List<PicklistItem> BereichList(Credentials authinfo)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
                return adp.BereichList();
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        public List<PicklistItem> IHList(Credentials authinfo)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
                return adp.IHList();
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        public List<PicklistItem> KstList(Credentials authinfo)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
                return adp.KstList();
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        public List<PicklistItem> LaList(Credentials authinfo)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
                return adp.LaList();
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        public List<PicklistItem> ProfilList(Credentials authinfo)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
                return adp.ProfilList();
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }

        public List<PicklistItem> PzList(Credentials authinfo)
        {
        
            try {
            
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                FilterAdapter adp = new FilterAdapter(user, authinfo.Client);
                return adp.PzList();
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }
    }
}

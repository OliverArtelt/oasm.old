﻿using System;
using System.Security.Authentication;
using System.ServiceModel;
using System.Diagnostics;


namespace comain.Service
{
    
    /// <summary>
    /// Fehler protokollieren und in SOAP-Fault wandeln
    /// </summary>
    public static class ThrowHelper
    {
            
        /// <summary>
        /// Ausnahme protokollieren und in SOAP-Fault wandeln
        /// </summary>
        /// <param name="x">zu maskierende Ausnahme</param>
        /// <returns>Soap-Fault</returns>
        public static FaultException CreateSoapFault(Exception x)
        {
        
            try {
            
                //in das Maschinenlog schreiben
                EventLog log = new EventLog("Application");
                log.Source = "OASM";
                log.WriteEntry(x.ToString(), EventLogEntryType.Error); 
            }
            catch {}
            
            //neue Ausnahmen generieren damit der Stacktrace aus Sicherheitsgründen nicht übertragen wird
            if (x is AuthenticationException) { 
            
                return new FaultException<AuthenticationException>(new AuthenticationException(), x.Message);
            
            } else if (x is ApplicationException) { 
            
                return new FaultException<ApplicationException>(new ApplicationException(), x.Message);
            }

            return new FaultException<ApplicationException>(new ApplicationException(), "Ein interner Fehler ist aufgetreten.");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.ServiceModel;
using comain.Schedule;
using System.Security.Authentication;


namespace comain.Service.Schedule
{

    [ServiceContract(Namespace = "http://oasm.cubeoffice.de")]
    public interface IPlan
    {
        
        /// <summary>
        /// Schedule geben
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <param name="config">Filtereinstellungen</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        ScheduleDTO GetPlan(comain.User.Credentials authinfo, Dictionary<String, List<String>> config);
        
        /// <summary>
        /// Schedule aktualisieren
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        /// <param name="modified">geänderte Plandaten</param>
        [OperationContract]
        [FaultContract(typeof(AuthenticationException))]
        [FaultContract(typeof(ApplicationException))]
        void UpdatePlan(comain.User.Credentials authinfo, IEnumerable<ModifiedDate> modified);
    }
}

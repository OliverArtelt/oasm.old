﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using comain.Adapter;
using comain.Connect;
using comain.Schedule;
using comain.User;


namespace comain.Service.Schedule
{

    [ServiceBehavior(Namespace = "http://oasm.cubeoffice.de")]
    public class PlanSvc : IPlan
    {

        public ScheduleDTO GetPlan(comain.User.Credentials authinfo, Dictionary<string, List<string>> config)
        {
            
            try {
                    
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                ScheduleBuilder bld = new ScheduleBuilder(authinfo.Client, user, config);
                return bld.Schedule;
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }


        public void UpdatePlan(Credentials authinfo, IEnumerable<ModifiedDate> modified)
        {
            
            try {
                    
                GrantedUser user = GrantedUserFactory.InitializeUser(authinfo);
                SqlDmlBuilder bld = new SqlDmlBuilder(user.Name, user.Id, user.IsClientUser);            
                
                bld.InsertBegin();
                SqlDmlDirector dir = new SqlDmlDirector(bld);
                dir.Create(modified);
                bld.InsertCommit();

                BatchExecuter exec = new BatchExecuter(authinfo.Client);
                exec.Execute(dir.CommandList);                
            }
            catch (Exception x) { 
            
                throw ThrowHelper.CreateSoapFault(x);
            }
        }
    }
}

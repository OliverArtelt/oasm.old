﻿using System;


namespace CO.OpenXML
{
    
    public static class DateTimeExtended
    {
    
        /// <summary>
        /// Zeit nach Universal Time konvertieren und als String geben
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static String ToOpenXMLString(this DateTime dt)
        {
        
            return dt.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'");
        }
    }
}

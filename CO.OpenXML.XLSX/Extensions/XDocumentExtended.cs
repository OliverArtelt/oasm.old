﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace CO.OpenXML
{
    
    public static class XDocumentExtended
    {

        public static String ToStringWithDeclaration(this XDocument doc)
        {
            
            if (doc == null) throw new ArgumentNullException("doc");

            var builder = new StringBuilder();
           
            using (TextWriter writer = new StringWriter(builder)) {
                
                doc.Save(writer);
            }

            return builder.ToString();
        }
    }
}

﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;


namespace CO.OpenXML
{
   
    public static class StringExtended
    {
    
        public static String EscapeXMLValue(this String value)
        {
            
            String s1 = value.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
            //String s2 = new String(s1.Where(ch => XmlConvert.IsXmlChar(ch)).ToArray());

            return s1;
        }
    }
}

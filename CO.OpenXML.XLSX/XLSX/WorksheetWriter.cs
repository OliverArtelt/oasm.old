﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using CO.OpenXML.XLSX.ExcelStyles;


namespace CO.OpenXML.XLSX
{
    
    /// <summary>
    /// Eine Tabelle beschreiben
    /// </summary>
    public class WorksheetWriter
    {
    
        /// <summary>
        /// Sheet-Index im Workbook (ab 1 beginnend)
        /// </summary>
        int myId;
        /// <summary>
        /// im Dokument vorhandene Dateien
        /// </summary>
        FileList myFiles;
        /// <summary>
        /// Liste der anzupassenden Zeilenhöhen
        /// </summary>
        Dictionary<int, float> myRowHeights;
        /// <summary>
        /// Liste aller Spaltenbreiten wenn angegeben
        /// </summary>
        List<ColumnStyle> myColumnWidths;
        /// <summary>
        /// beschriebene Tabellenzellen
        /// </summary>
        Dictionary<CellAddress, CellValue> myCells;
        /// <summary>
        /// erste zu verwendende Ausgabezeile
        /// </summary>
        int myFirstOutputRow;
        /// <summary>
        /// globales Wörterbuch
        /// </summary>
        StringPoolEncoder myDictionary;
        /// <summary>
        /// globales Stilverzeichnis
        /// </summary>
        StylePool myStyles;
        /// <summary>
        /// von der Ausgabe ausgeschlossene Spalten
        /// </summary>
        List<int> mySuppressedCols;
        /// <summary>
        /// Ausgangsdokument als XML-String
        /// </summary>
        //String sheetAsXml; 
        /// <summary>
        /// größte verwendete (auszugebende) Zeile
        /// </summary>
        int maxrow = 0;
        /// <summary>
        /// größte verwendete Spalte (Zeilen müssen einen verwendeten Zellenbereich angeben)
        /// </summary>
        int maxcol = 0;
    

        /// <summary>
        /// Formatierobjekte um kulturunabhängige Zellinhalte lesen zu können (Dezimalpunkt statt Komma)
        /// </summary>
        CultureInfo invariantCulture = new CultureInfo("");

    
        /// <summary>
        /// größte verwendete (auszugebende) Zeile
        /// </summary>
        public int MaxRow { get { return maxrow + myFirstOutputRow - 1; } }
        /// <summary>
        /// größte verwendete Spalte (Zeilen müssen einen verwendeten Zellenbereich angeben)
        /// </summary>
        public int MaxColumn { get { return maxcol; } }
    
        
        /// <summary>
        /// einzelne Tabelle schreiben und in das Archiv einfügen
        /// </summary>
        /// <param name="sheet">Ausgangs-/Rohsheet als XML-Dokument</param>
        /// <param name="sp">zu aktualisierender Stringpool</param>
        /// <param name="styles">zu aktualisierender Stylepool</param>
        /// <param name="fstRow">erste zu verwendende Ausgabezeile</param>
        internal WorksheetWriter(int id, FileList files, StringPoolEncoder sp, StylePool styles, int fstRow) 
                   : this(id, files, sp, styles, fstRow, null) {}
        
        /// <summary>
        /// einzelne Tabelle schreiben und in das Archiv einfügen
        /// </summary>
        /// <param name="sheet">Ausgangs-/Rohsheet als XML-Dokument</param>
        /// <param name="sp">zu aktualisierender Stringpool</param>
        /// <param name="styles">zu aktualisierender Stylepool</param>
        /// <param name="fstRow">erste zu verwendende Ausgabezeile</param>
        /// <param name="supCols">von der Ausgabe ausgeschlossene Spalten</param>
        internal WorksheetWriter(int id, FileList files, StringPoolEncoder sp, StylePool styles, int fstRow, List<int> supCols)
        {
        
            if (fstRow < 1 || fstRow > 1048576) throw new IndexOutOfRangeException("Unzulässige Excel-Zeilennummer");

            myFirstOutputRow = fstRow;
            myDictionary = sp;
            myStyles = styles;
            mySuppressedCols = supCols;
            myCells = new Dictionary<CellAddress,CellValue>();
            myRowHeights = new Dictionary<int,float>();
            myFiles = files;
            myId = id;
        }

        /// <summary>
        /// Individuelle Zeilenhöhe eintragen
        /// </summary>
        /// <param name="row"></param>
        /// <param name="height"></param>
        public void SetRowHeight(int row, float height) 
        { 
            myRowHeights[row] = height;
        }

        /// <summary>
        /// Spaltenbreiten ersetzen, es werden alle Spalten durch diese Liste ersetzt
        /// </summary>
        /// <param name="list">neue Liste</param>
        public void SetColumnWidth(IEnumerable<ColumnStyle> list) 
        { 
        
            myColumnWidths = list.ToList();
            myColumnWidths.Sort();
        }

        /// <summary>
        /// Tabellenbezeichnung im Tabellenregister unten eintragen
        /// </summary>
        /// <param name="name">Neuer Name</param>
        public void SetName(String name)
        {
            
            using (TextReader rdr = new StringReader(myFiles.GetString("xl/workbook.xml"))) {
            
                XDocument doc = XDocument.Load(rdr);
                XNamespace ns = doc.Root.Name.Namespace;
                XElement sheet = doc.Descendants(ns + "sheets")
                                    .Descendants(ns + "sheet")
                                    .Where(p => p.Attribute("sheetId").Value == myId.ToString())
                                    .First();
                sheet.Attribute("name").Value = name;
                
                String content = String.Format("{0}{1}{2}", doc.Declaration, Environment.NewLine, 
                                               doc.ToString(SaveOptions.None));
                myFiles.SetString("xl/workbook.xml", content);
            }
        }
        
        /// <summary>
        /// Tabelle in das Verzeichnis zurückschreiben
        /// </summary>
        public void Save()
        {
            myFiles.SetString(Filename, WriteSheet());
        }
        

#region C E L L   W R I T I N G

        
        public void SetValue(String val, int row, int col)                { SetValue(val, new CellAddress(row, col), new Style()); } 
        public void SetValue(String val, int row, int col, Style style)   { SetValue(val, new CellAddress(row, col), style); } 
        public void SetValue(String val, String address)                  { SetValue(val, new CellAddress(address), new Style()); } 
        public void SetValue(String val, String address, Style style)     { SetValue(val, new CellAddress(address), style); } 
        public void SetValue(String val, CellAddress cell)                { SetValue(val, cell, new Style()); } 

        public void SetValue(Decimal val, int row, int col)               { SetValue(val, new CellAddress(row, col), new Style()); } 
        public void SetValue(Decimal val, int row, int col, Style style)  { SetValue(val, new CellAddress(row, col), style); } 
        public void SetValue(Decimal val, String address)                 { SetValue(val, new CellAddress(address), new Style()); } 
        public void SetValue(Decimal val, String address, Style style)    { SetValue(val, new CellAddress(address), style); } 
        public void SetValue(Decimal val, CellAddress cell)               { SetValue(val, cell, new Style()); } 
        
        public void SetValue(DateTime val, int row, int col)              { SetValue(val, new CellAddress(row, col), new Style()); } 
        public void SetValue(DateTime val, int row, int col, Style style) { SetValue(val, new CellAddress(row, col), style); } 
        public void SetValue(DateTime val, String address)                { SetValue(val, new CellAddress(address), new Style()); } 
        public void SetValue(DateTime val, String address, Style style)   { SetValue(val, new CellAddress(address), style); } 
        public void SetValue(DateTime val, CellAddress cell)              { SetValue(val, cell, new Style()); } 
        
        
        public void SetValue(String val, CellAddress cell, Style style) 
        {
        
            SetMaximum(cell);            
            myCells[cell] = new CellValue { Value = myDictionary[val].ToString(), IsPooled = true, StyleIndex = myStyles[style] };   
        }
        
        
        public void SetValue(Decimal val, CellAddress cell, Style style)
        {
        
            SetMaximum(cell);
            myCells[cell] = new CellValue { Value = val.ToString(invariantCulture), IsPooled = false, StyleIndex = myStyles[style] };   
        }
        
        
        public void SetValue(DateTime val, CellAddress cell, Style style)
        {
        
            SetMaximum(cell);
            double v = val.ToOADate();
            myCells[cell] = new CellValue { Value = v.ToString(), IsPooled = false, StyleIndex = myStyles[style] };   
        }
        

        private void SetMaximum(CellAddress addr)
        {
        
            if (addr.Column > maxcol) maxcol = addr.Column;
            if (addr.Row > maxrow) maxrow = addr.Row;
        }
        

#endregion

        
        /// <summary>
        /// Pfad dieser Tabelle im Dateiverzeichnis geben
        /// </summary>
        private String Filename { get { return String.Format("xl/worksheets/sheet{0}.xml", myId); } }
        
        /// <summary>
        /// Exceltabelle als XML-Dokument geben
        /// </summary>
        /// <returns></returns>
        private string WriteSheet()
        {
        
            using (TextReader rdr = new StringReader(myFiles.GetString(Filename))) {
            
                XDocument doc = XDocument.Load(rdr);
                XNamespace ns = doc.Root.Name.Namespace;
                XElement data = doc.Descendants(ns + "sheetData").First();
                
                //Dokumenttyp "Wörterbuch" registrieren
                XElement types = doc.Element(ns + "Types");

                //ungenutzte Spalten entfernen
                ReplaceColumns(doc);

                //alter Inhalt neu zu beschreibender Zeilen entfernen um Mißinterpretationen vorzubeugen
                var rows = (from r in data.Descendants(ns + "row")
                            where Int32.Parse(r.Attribute("r").Value) >= myFirstOutputRow
                            select r).ToList();
                foreach (var node in rows) node.Remove();

                //Zeilen haben eine MaxSpan-Angabe
                String spans = String.Format("1:{0}", MaxColumn);
                
                List<CellAddress> writtenCells = myCells.Keys.ToList();

                foreach (var row in (from c in writtenCells select c.Row).OrderBy(p => p).Distinct()) {
                
                    XElement xrow = new XElement(ns + "row");
                    data.Add(xrow);
                    xrow.Add(new XAttribute("r", (row + myFirstOutputRow - 1).ToString()));
                    xrow.Add(new XAttribute("spans", spans));
                    if (myRowHeights.ContainsKey(row)) {
                    
                        xrow.Add(new XAttribute("customHeight", "1"));
                        xrow.Add(new XAttribute("ht", myRowHeights[row].ToString(invariantCulture)));
                    } 
                
                    foreach (var cell in writtenCells.Where(p => p.Row == row).OrderBy(p => p.Column)) {
                    
                        XElement xcell = new XElement(ns + "c");
                        xrow.Add(xcell);
                        
                        //Zelladressen anpassen/verschieben wenn Ausgabezeile > 1
                        CellAddress outCell = (myFirstOutputRow == 1)? cell: new CellAddress(cell.Row + myFirstOutputRow - 1, cell.Column);
                        
                        xcell.Add(new XAttribute("r", outCell));
                        CellValue val = myCells[cell];
                        if (val.IsPooled) xcell.Add(new XAttribute("t", "s"));
                        if (val.StyleIndex > 0) xcell.Add(new XAttribute("s", val.StyleIndex));
                        xcell.Add(new XElement(ns + "v", val.Value));                        
                    } 
                }                     
    
                String content = String.Format("{0}{1}{2}", doc.Declaration, Environment.NewLine, 
                                               doc.ToString(SaveOptions.DisableFormatting));
                return content;
            }
        }

        
        private void ReplaceColumns(XDocument doc)
        {
        
            if (myColumnWidths == null) return;
            
            XNamespace ns = doc.Root.Name.Namespace;            
            XElement parent = doc.Descendants(ns + "cols").FirstOrDefault();
            if (parent != null) {
            
                parent.RemoveAll();
            
            } else {
            
                parent = new XElement(ns + "cols");
                XElement sheetFormatPr = doc.Descendants(ns + "sheetFormatPr").First();
                sheetFormatPr.AddAfterSelf(parent);
            }
            
            foreach (var entry in myColumnWidths) {
            
                parent.Add(
                    new XElement(ns + "col",
                        new XAttribute("min", entry.Min),
                        new XAttribute("max", entry.Max),
                        new XAttribute("width", entry.Width),
                        new XAttribute("bestFit", "1"),
                        new XAttribute("customWidth", "1")
                    ) 
                );
            }    
        }
        
        
        private void RemoveColumns(XDocument doc)
        {
            
            if (mySuppressedCols == null) return;
            
            XNamespace ns = doc.Root.Name.Namespace;            
            List<XElement> colGroups = (from c in doc.Descendants(ns + "cols").Descendants(ns + "col")
                                        orderby c.Attribute("min").Value descending
                                        select c).ToList();
        
            foreach (var removedCol in mySuppressedCols.OrderByDescending(p => p)) {
            
                RemoveColumn(removedCol, colGroups);
            }  
        }              

        
        private void RemoveColumn(int column, List<XElement> groups)
        {
            
            foreach (var gr in groups) {
            
                int max = Int32.Parse(gr.Attribute("max").Value);
                int min = Int32.Parse(gr.Attribute("min").Value);
                
                //dahinter => don't care
                if (max < column) return;
                
                //genau eine Spalte
                if (max == column && min == column) {
                
                    groups.Remove(gr);
                    gr.Remove();
                    return;   
                }
            
                if (max >= column) gr.Attribute("max").Value = (max - 1).ToString();
                if (min > column)  gr.Attribute("min").Value = (min - 1).ToString();
            }
        }              
    }
}

﻿using System;
using System.Xml;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    /// <summary>
    /// Ausrichtungstypen in Zellen
    /// </summary>
    public enum AlignTypes
    {
        None,
        Top,
        Center,
        Bottom,
        Left,
        Right
    }


    /// <summary>
    /// Ausrichtungsobjekt
    /// </summary>
    public class Alignment : ICellStyleType, IEquatable<Alignment>
    {
        

        AlignTypes  horizontal;
        AlignTypes  vertical;
        int         textRotation;
        bool        wrapText;
        

        public Alignment() : this(AlignTypes.None, AlignTypes.None, false, 0) {}

        public Alignment(bool w) : this(AlignTypes.None, AlignTypes.None, w, 0) {}

        public Alignment(int r) : this(AlignTypes.None, AlignTypes.None, false, r) {}

        public Alignment(AlignTypes h) : this(h, AlignTypes.None, false, 0) {}

        public Alignment(AlignTypes h, bool w) : this(h, AlignTypes.None, w, 0) {}

        public Alignment(AlignTypes h, AlignTypes v) : this(h, v, false, 0) {}

        public Alignment(AlignTypes h, AlignTypes v, int r) : this(h, v, false, r) {}

        public Alignment(AlignTypes h, AlignTypes v, bool w) : this(h, v, w, 0) {}

        public Alignment(AlignTypes h, AlignTypes v, bool w, int r)
        {
        
            horizontal = h;
            vertical = v;
            wrapText = w;
            SetRotation(r); 
        }


        public bool IsDefault()
        {
            return horizontal == AlignTypes.None && vertical == AlignTypes.None && wrapText == false && textRotation == 0;
        }

        
        public void WriteXML(XElement parent)
        {
            throw new NotImplementedException();
        }

        
        public bool Equals(Alignment other)
        {
            return this.horizontal == other.horizontal && 
                   this.vertical == other.vertical &&
                   this.wrapText == other.wrapText &&
                   this.textRotation == other.textRotation;
        }

 
        public void AddAlignmentToXFNode(XElement parent)
        {
        
            //Standardausrichtung => kein Subknoten einfügen
            if (IsDefault()) return;

            XNamespace ns = parent.Name.Namespace;
            XElement alignment = new XElement(ns + "alignment");
            parent.Add(alignment);
            
            if (horizontal != AlignTypes.None) alignment.Add(new XAttribute("horizontal", XMLAlignName(horizontal)));
            if (vertical   != AlignTypes.None) alignment.Add(new XAttribute("vertical", XMLAlignName(vertical)));
            if (textRotation != 0)             alignment.Add(new XAttribute("textRotation", textRotation));
            if (wrapText)                      alignment.Add(new XAttribute("wrapText", "1"));
        } 
        
        
        private void SetRotation(int r) 
        {

            if (r < -90 || r > 90) throw new InvalidOperationException("Die gedrehte Darstellung des Zellinhaltes muss zwischen -90° und +90° liegen.");
            textRotation = r;
        }


        private String XMLAlignName(AlignTypes t) 
        {

            if (t == AlignTypes.Top)    return "top";
            if (t == AlignTypes.Center) return "center";
            if (t == AlignTypes.Bottom) return "bottom";
            if (t == AlignTypes.Left)   return "left";
            if (t == AlignTypes.Right)  return "right";
            return String.Empty;
        }
    }
}

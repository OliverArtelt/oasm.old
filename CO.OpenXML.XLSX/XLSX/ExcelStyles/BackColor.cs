﻿using System;
using System.Drawing;
using System.Xml;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    /// <summary>
    /// Füllobjekt
    /// </summary>
    public class BackColor : ICellStyleType, IEquatable<BackColor>
    {
        
        public Color color;
        
        
        public BackColor() : this(Color.Black) {}

        public BackColor(Color c)
        {
            color = c;        
        }


        public bool IsDefault()
        {
            return color == Color.Black;
        }

        
        public void WriteXML(XElement parent)
        {
         
            XNamespace ns = parent.Name.Namespace;
            
            XElement fill = new XElement(ns + "fill",
                new XElement(ns + "patternFill",
                    new XAttribute("patternType", "solid"),
                    new XElement(ns + "fgColor", 
                        new XAttribute("rgb", color.ToRgbHexString())),
                    new XElement(ns + "bgColor", 
                        new XAttribute("indexed", "64")
                    )
                )
            );
            
            parent.Add(fill);    
        }

        
        public bool Equals(BackColor other)
        {
            return this.color == other.color;
        }
    }
}

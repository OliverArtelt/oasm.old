﻿using System;
using System.Xml;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    /// <summary>
    /// Zellrahmentypen
    /// </summary>
    public enum BorderType
    {
        None,  
        Thin,
        Thick, 
        Double
    }

    /// <summary>
    /// Zellrahmenzonen für Zellen mit unterschiedlichen Rahmentypen an den Kanten
    /// </summary>
    public enum BorderZone
    {
        Top,
        Right,
        Bottom,
        Left
    }

    /// <summary>
    /// Rahmenobjekt
    /// </summary>
    public class Border : ICellStyleType, IEquatable<Border>
    {

        BorderType[] border;
        

        public Border() : this(BorderType.None) {}
        
        public Border(BorderType t) 
        { 
        
            border = new BorderType[4];
        
            border[(int)BorderZone.Top] = t;
            border[(int)BorderZone.Right] = t;
            border[(int)BorderZone.Bottom] = t;
            border[(int)BorderZone.Left] = t;
        }

        public Border(BorderType t, BorderType r, BorderType b, BorderType l)
        { 
            
            border = new BorderType[4];

            border[(int)BorderZone.Top] = t; 
            border[(int)BorderZone.Right] = r; 
            border[(int)BorderZone.Bottom] = b; 
            border[(int)BorderZone.Left] = l;
        }


        public bool IsDefault()
        {
            return border[(int)BorderZone.Top] == BorderType.None && border[(int)BorderZone.Right] == BorderType.None && 
                   border[(int)BorderZone.Bottom] == BorderType.None && border[(int)BorderZone.Left] == BorderType.None;
        }

        
        public void WriteXML(XElement parent)
        {
         
            XNamespace ns = parent.Name.Namespace;
            
            XElement b = new XElement(ns + "border");
            parent.Add(b);
            XElement x;
            
            x = new XElement(ns + "left");
            b.Add(x);
            if (border[(int)BorderZone.Left] != BorderType.None) {
         
                x.Add(new XAttribute("style", XMLStyleName(BorderZone.Left)));
                x.Add(new XElement(ns + "color", 
                        new XAttribute("indexed", 64)));
            }
            
            x = new XElement(ns + "right");
            b.Add(x);
            if (border[(int)BorderZone.Right] != BorderType.None) {
         
                x.Add(new XAttribute("style", XMLStyleName(BorderZone.Right)));
                x.Add(new XElement(ns + "color", 
                        new XAttribute("indexed", 64)));
            }
            
            x = new XElement(ns + "top");
            b.Add(x);
            if (border[(int)BorderZone.Top] != BorderType.None) {
         
                x.Add(new XAttribute("style", XMLStyleName(BorderZone.Top)));
                x.Add(new XElement(ns + "color", 
                        new XAttribute("indexed", 64)));
            }
            
            x = new XElement(ns + "bottom");
            b.Add(x);
            if (border[(int)BorderZone.Bottom] != BorderType.None) {
         
                x.Add(new XAttribute("style", XMLStyleName(BorderZone.Bottom)));
                x.Add(new XElement(ns + "color", 
                        new XAttribute("indexed", 64)));
            }
                
            b.Add(new XElement(ns + "diagonal"));
        }

        
        public bool Equals(Border other)
        {
            return this.border[(int)BorderZone.Top]    == other.border[(int)BorderZone.Top]    && 
                   this.border[(int)BorderZone.Right]  == other.border[(int)BorderZone.Right]  &&
                   this.border[(int)BorderZone.Bottom] == other.border[(int)BorderZone.Bottom] && 
                   this.border[(int)BorderZone.Left]   == other.border[(int)BorderZone.Left];
        }

        
        private String XMLStyleName(BorderZone z) 
        {

            if (border[(int)z] == BorderType.Thin)   return "thin";
            if (border[(int)z] == BorderType.Thick)  return "thick";
            if (border[(int)z] == BorderType.Double) return "double";
        
            return String.Empty;
        }
    }
}

﻿using System;
using System.Drawing;
using System.Linq;
using System.Xml;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    /// <summary>
    /// Fontstilobjekt
    /// </summary>
    public class FontStyle : ICellStyleType, IEquatable<FontStyle>
    {
        
        public float size;
        public Color color;
        public bool  bold;
        public bool  italic;
        
                
        public FontStyle()                         : this(0, Color.Black, false, false) {}                
                
        public FontStyle(float s)                  : this(s, Color.Black, false, false) {}               
                
        public FontStyle(float s, Color c)         : this(s, c, false, false) {}                
                
        public FontStyle(float s, Color c, bool b) : this(s, c, b, false) {}                
                
        public FontStyle(float s, Color c, bool b, bool i)
        {
       
            size = s; 
            color = c; 
            bold = b; 
            italic = i; 
        }
                
                
        public bool IsDefault()
        {
            
            return size == 0F && color == Color.Black && !bold && !italic;
        }

        
        public void WriteXML(XElement parent)
        {

            XNamespace ns = parent.Name.Namespace;
            int    stdSize = 10;
            String stdName = "Arial";
            
            XElement defaultNode = parent.Descendants(ns + "font").FirstOrDefault();
            if (defaultNode != null) {
            
                stdSize = Int32.Parse(defaultNode.Descendants(ns + "sz").Attributes("val").First().Value);
                stdName = defaultNode.Descendants(ns + "name").Attributes("val").First().Value;
            }

            //Knoten mit definierten Attributen anlegen
            XElement font = new XElement(ns + "font",
                                new XElement(ns + "sz",
                                    new XAttribute("val", (size == 0)? stdSize: size)),
                                new XElement(ns + "name",
                                    new XAttribute("val", stdName)));
            parent.Add(font);
            
            if (color != Color.Black) {

                font.Add(new XElement(ns + "color",
                            new XAttribute("rgb", color.ToRgbHexString())));
            }

            if (bold)   font.Add(new XElement(ns + "b"));
            if (italic) font.Add(new XElement(ns + "i"));
        }

        
        public bool Equals(FontStyle other)
        {
            return this.size == other.size && this.color == other.color && 
                   this.bold == other.bold && this.italic == other.italic;
        }
    }
}

﻿using System;


namespace CO.OpenXML.XLSX.ExcelStyles
{
    
    public class Style
    {

        /// <summary>
        /// Fontobjekt
        /// </summary>
        public FontStyle Font { get; set; }
        /// <summary>
        /// Formattierungsobjekt
        /// </summary>
        public NumberFormat Format { get; set; }
        /// <summary>
        /// Rahmenobjekt
        /// </summary>
        public Border Border { get; set; }
        /// <summary>
        /// Farbobjekt
        /// </summary>
        public BackColor Color { get; set; }
        /// <summary>
        /// Ausrichtungsobjekt
        /// </summary>
        public Alignment Align { get; set; }

    }
}

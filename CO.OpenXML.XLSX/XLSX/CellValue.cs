﻿using System;


namespace CO.OpenXML.XLSX
{
    
    /// <summary>
    /// (gesetzter) Wert einer Zelle
    /// </summary>
    class CellValue
    {
    
        /// <summary>
        /// der Wert der Zelle
        /// </summary>
        public String Value { get; set; }
        /// <summary>
        /// wenn String ein Fliegengewicht im Wörterbuch ist, dann ist Value der Index im Wörterbuch
        /// </summary>
        public bool IsPooled { get; set; } 
        /// <summary>
        /// Index im Stilverzeichnis
        /// </summary>
        public int StyleIndex { get; set; }


        public override string ToString()
        {
            return Value;
        }
    }
}

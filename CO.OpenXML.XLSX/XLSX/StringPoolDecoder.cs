﻿using System;
using System.Collections.Generic;


namespace CO.OpenXML.XLSX
{
    
    /// <summary>
    /// Strings können in Excel-Dokumenten in einem Wörterbuch (Fliegengewichte) vorliegen
    /// </summary>
    public class StringPoolDecoder : StringPoolBase
    {

        List<String> dictionary;

        
        /// <summary>
        /// Wörterbuch aus Dokument lesen
        /// </summary>
        /// <param name="src">shared_strings-XML-Dokument</param>
        public StringPoolDecoder(String src) 
        { 
        
            dictionary = new List<string>();
            ParseMap(src); 
        }
                         
        /// <summary>
        /// auf Strings über ihren Index zugreifen
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public String this[int index] { get { return dictionary[index]; } }
    

        protected override void Add(String value)
        {
            dictionary.Add(value);
        }
    }
}

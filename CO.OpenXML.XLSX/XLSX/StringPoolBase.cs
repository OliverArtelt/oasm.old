﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;


namespace CO.OpenXML.XLSX 
{
    
    public abstract class StringPoolBase 
    {
        
        
        protected abstract void Add(String value);
        
        
        /// <summary>
        /// sharedString-XMLFile ins Wörterbuch lesen
        /// </summary>
        /// <param name="src">bereits im Wörterbuch vorhandene Einträge (Dokument aus Vorlage generieren)</param>
        /// <returns>Anzahl vorhandener Einträge</returns>
        protected int ParseMap(String src)
        {
        
            using (TextReader rdr = new StringReader(src)) {
            
                XDocument doc = XDocument.Load(rdr);
                XNamespace ns = doc.Root.Name.Namespace;
                
                var items = from item in doc.Descendants(ns + "sst").Descendants(ns + "si")
                            select item;
                        
                foreach (var entry in items) {
            
                    XElement t = entry.Element("t");
                    
                    if (t != null) {
                    
                        //ist Einfachknoten
                        if (t.NodeType == XmlNodeType.Element) Add(t.Value);
                    
                    } else {
                    
                        String s = String.Empty;
                    
                        //ist Komplexknoten (String in der Zelle hat wechselnde Formatierungen)
                        foreach (var piece in entry.DescendantNodes()) {
                        
                            if (piece.NodeType == XmlNodeType.Text) 
                                s += piece;
                        }
                        
                        Add(s);
                    }                    
                }              

                XElement sst = doc.Element(ns + "sst");
                if (sst == null) return 0;
                XAttribute count = sst.Attribute("count");
                if (count == null) return 0;
                
                return Int32.Parse(count.Value);
            }
        }
    }
}

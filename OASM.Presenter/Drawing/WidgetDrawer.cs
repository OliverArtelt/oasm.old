﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;
using comain.Schedule;
using comain.User;
using comain.Workspace;


namespace comain.Drawing
{
    
    /// <summary>
    /// Selbstzuzeichnende DataGridView-Elemente des Schedules,
    /// </summary>
    public class DataGridViewDrawer : IScheduleVisitor
    {
        
        /// <summary>
        /// konstanten String mit Abmessungen geben
        /// </summary>
        class MeasuredString
        {
        
            public String Text { get; private set; }
            public Font   Font { get; private set; }
            public SizeF  Size { get; private set; }

            public override string ToString()
            {
                return Text;
            }
        
            public int Width { get { return Size.ToSize().Width; } }
        
            public int Height { get { return Size.ToSize().Height; } }
        
            public MeasuredString(String text, Font font, Graphics graphics)
            {
            
                Text = text;
                Font = font;
                Size = graphics.MeasureString(Text, font);
            }
        }

        /// <summary>
        /// das zu zeichnende Datagridview
        /// </summary>
        DataGridView myControl;
        /// <summary>
        /// Ereignisargumente zum Zeichnen
        /// </summary>
        DataGridViewCellPaintingEventArgs myEvent;
        
        const int TextSpan = 6;
        
        MeasuredString psString;
        MeasuredString pzString;
        MeasuredString ihMainString;
        MeasuredString ihSubString;
       
       
        /// <summary>
        /// der feststehende linke Teil des Grids in Pixel
        /// </summary>
        int FixedWidth 
        {
            get {
            
                //return myControl.Columns[0].Width + myControl.Columns[1].Width + myControl.Columns[2].Width;
                return 150;
            }
        }

        
        /// <summary>
        /// Selbstzuzeichnende DataGridView-Elemente des Schedules, Elemente übergeben
        /// </summary>
        /// <param name="sender">das zu zeichnende Datagridview</param>
        /// <param name="e">Ereignisargumente zum Zeichnen</param>
        public DataGridViewDrawer(object sender, DataGridViewCellPaintingEventArgs e)
        {
        
            myEvent = e;
            myControl = sender as DataGridView;
            
            if (myControl == null) throw new ApplicationException("Interner Fehler: Ungültiges Steuerelement");

            psString     = new MeasuredString("Aufträge für:",   FontFactory.Head1Caption, myEvent.Graphics);
            pzString     = new MeasuredString("Standort:",       FontFactory.Head2Caption, myEvent.Graphics);
            ihMainString = new MeasuredString("IH-Hauptobjekt:", FontFactory.Head2Caption, myEvent.Graphics);
            ihSubString  = new MeasuredString("IH-Objekt:",      FontFactory.Head2Caption, myEvent.Graphics);
        }
        
        /// <summary>
        /// Das Wurzelobjekt wird nicht dargestellt
        /// </summary>
        /// <param name="obj">Wurzelobjekt des Schedules</param>
        public void Visit(Root obj) {}
        
        /// <summary>
        /// Bearbeiterzeile zeichnen
        /// </summary>
        /// <param name="obj">darzustellender Bearbeiter</param>
        public void Visit(Bearbeiter obj)
        {
            
            myEvent.Graphics.TextRenderingHint = TextRenderingHint.SingleBitPerPixelGridFit;

            //Hintergrund der Zeichenfläche ausfüllen
            myEvent.Graphics.FillRectangle(BrushFactory.HeadBearbeiter, myEvent.CellBounds);

            //Begrenzung unten zeichnen
            myEvent.Graphics.DrawLine(PenFactory.Grid, new Point(myEvent.CellBounds.Left, myEvent.CellBounds.Bottom - 1), 
                                                       new Point(myEvent.CellBounds.Right - 1, myEvent.CellBounds.Bottom - 1)); 
            
            if (!Session.Current.IsScrolledHorizontally) {

                //Text schreiben, in Caption (reguläre Schrift) und Inhalt (fette Schrift) trennen
                myEvent.Graphics.DrawString(psString.Text, psString.Font, Brushes.Black,  FixedWidth - psString.Width, 
                                            myEvent.CellBounds.Top + (myEvent.CellBounds.Height - psString.Height) / 2);
                myEvent.Graphics.DrawString(obj.Name, FontFactory.Head1Value, Brushes.Black, FixedWidth + TextSpan, 
                                            myEvent.CellBounds.Top + (myEvent.CellBounds.Height - psString.Height) / 2);
            }
            
            //Zeichnen der anderen Teile (Checkbox, Spaltengrenzen) unterdrücken
            myEvent.Handled = true;
        }
        
        /// <summary>
        /// Standortzeile zeichnen
        /// </summary>
        /// <param name="obj">darzustellender Standort</param>
        public void Visit(Standort obj)
        {

            myEvent.Graphics.TextRenderingHint = TextRenderingHint.SingleBitPerPixelGridFit;

            //Hintergrund der Zeichenfläche ausfüllen (Checkbox überzeichnen)
            myEvent.Graphics.FillRectangle(BrushFactory.HeadStandort, myEvent.CellBounds);
            
            //Begrenzung unten zeichnen
            myEvent.Graphics.DrawLine(PenFactory.Grid, new Point(myEvent.CellBounds.Left, myEvent.CellBounds.Bottom - 1), 
                                                       new Point(myEvent.CellBounds.Right - 1, myEvent.CellBounds.Bottom - 1)); 
            
            if (!Session.Current.IsScrolledHorizontally) {

                //Text schreiben, in Caption (reguläre Schrift) und Inhalt (fette Schrift) trennen
                myEvent.Graphics.DrawString(pzString.Text, pzString.Font, BrushFactory.Text, FixedWidth - pzString.Width, 
                                            myEvent.CellBounds.Top + (myEvent.CellBounds.Height - pzString.Height) / 2);
                myEvent.Graphics.DrawString(obj.Name, FontFactory.Head2Value, BrushFactory.Text, FixedWidth + TextSpan, 
                                            myEvent.CellBounds.Top + (myEvent.CellBounds.Height - pzString.Height) / 2);
            }
            
            //Zeichnen der anderen Teile (Checkbox, Spaltengrenzen) unterdrücken
            myEvent.Handled = true;
        }
        
        /// <summary>
        /// IH-Objekt- und Auftragskopfzeile zeichnen
        /// </summary>
        /// <param name="obj">darzustellendes IH-Objekt</param>
        public void Visit(IHObjekt obj)
        {
    
            //IH-Objekt
            if (obj.Position == myEvent.RowIndex) {
                               
                myEvent.Graphics.TextRenderingHint = TextRenderingHint.SingleBitPerPixelGridFit;
    
                //Hintergrund der Zeichenfläche ausfüllen (Checkbox überzeichnen)
                myEvent.Graphics.FillRectangle(BrushFactory.HeadIHObjekt, myEvent.CellBounds);
                
                //Begrenzung unten zeichnen
                myEvent.Graphics.DrawLine(PenFactory.Grid, new Point(myEvent.CellBounds.Left, myEvent.CellBounds.Bottom - 1), 
                                                           new Point(myEvent.CellBounds.Right - 1, myEvent.CellBounds.Bottom - 1)); 
                
                if (!Session.Current.IsScrolledHorizontally) {
                                       
                    //Text schreiben, in Caption (reguläre Schrift) und Inhalt (fette Schrift) trennen
                    float y = myEvent.CellBounds.Top + (myEvent.CellBounds.Height - ihMainString.Height) / 2;
                     
                    if (obj.IstBaugruppe) {
                        
                        myEvent.Graphics.DrawString(ihMainString.Text, ihMainString.Font, BrushFactory.Text, 
                                                    FixedWidth - ihMainString.Width, y - 10);
                        myEvent.Graphics.DrawString(obj.MainName, FontFactory.Head2Value, BrushFactory.Text, 
                                                    FixedWidth, y - 10);
                    }
                            
                    myEvent.Graphics.DrawString(ihSubString.Text, ihSubString.Font, BrushFactory.Text, 
                                                FixedWidth - ihSubString.Width, y + 10);
                    myEvent.Graphics.DrawString(obj.Name, FontFactory.Head2Value, BrushFactory.Text, 
                                                FixedWidth, y + 10);
                }
                
                //Zeichnen der anderen Teile (Checkbox, Spaltengrenzen) unterdrücken
                myEvent.Handled = true;
            
             
            } else {  //Auftragskopf:  obj.Position + 1 == myEvent.RowIndex     
             
                myEvent.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                
                //Checkbox ausblenden                    
                if (myEvent.ColumnIndex == 0) {
                
                    //Zeichenfläche der Checkbox ermitteln
                    Rectangle r = new Rectangle(myEvent.CellBounds.Left, myEvent.CellBounds.Top, myEvent.CellBounds.Width, myEvent.CellBounds.Height);
                    
                    //Hintergrund der Zeichenfläche ausfüllen (Checkbox überzeichnen)
                    myEvent.Graphics.FillRectangle(GetTypeBrush(obj.Traits), r);
                    
                    //Begrenzung unten/rechts zeichnen
                    myEvent.Graphics.DrawLine(PenFactory.Grid, new Point(myEvent.CellBounds.Left, myEvent.CellBounds.Bottom - 1), 
                                                               new Point(myEvent.CellBounds.Right - 1, myEvent.CellBounds.Bottom - 1)); 
                
                    //Zeichnen der Checkbox unterdrücken
                    myEvent.Handled = true;
                    return;
                }
                
                //Kalenderleiste darstellen
                if (myEvent.ColumnIndex == obj.Traits.DateColumn) {
                
                    //Zeichenfläche der Kalenderleiste ermitteln
                    Rectangle r = new Rectangle(myEvent.CellBounds.Left, myEvent.CellBounds.Top, myEvent.CellBounds.Width, myEvent.CellBounds.Height);
                    
                    //Hintergrund der Zeichenfläche ausfüllen
                    myEvent.Graphics.FillRectangle(GetTypeBrush(obj.Traits), r);
                    
                    //Kalenderleiste zeichnen
                    DrawCalendar(obj);
                    DrawCellBorder();
                    myEvent.Handled = true;
                }
            }        
        }
        
        /// <summary>
        /// Auftrag und Termine zeichnen
        /// </summary>
        /// <param name="obj">darzustellender Auftrag</param>
        public void Visit(Auftrag obj)
        {

            myEvent.Graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            var clms = obj.Traits.Columns;

            //Terminleistenspalte zeichnen
            if (myEvent.ColumnIndex == clms.DateIndex) {
            
                int operVon = 0;
                int editVon = obj.Traits.Calendar.CalculateCalendarKey(obj.Traits.OASMSettings.MinDate);
                int editBis = obj.Traits.Calendar.CalculateCalendarKey(obj.Traits.OASMSettings.MaxDate);
                
                //Administratoren nicht weiter begrenzen
                if (!obj.Traits.User.MayAdminister) {
                
                    //Meister sind durch Grauzone beschränkt
                    if (obj.Traits.User is ClientUser) {
                    
                        operVon = obj.Traits.Calendar.CalculateCalendarKey(obj.Traits.OASMSettings.OperDate) - 1;
                    }
                    
                    editVon = obj.Traits.Calendar.CalculateCalendarKey(obj.Traits.OASMSettings.GreyDate);
                }

                DrawDates(obj, editVon, editBis, operVon);
                DrawCellBorder();
                myEvent.Handled = true;
            }

            //Differenz Anzahl zeichnen
            if (myEvent.ColumnIndex == clms.DAnzahlIndex) {

                String text = obj.MengenDifferenz > 0 ? "+" + obj.MengenDifferenz.ToString(): obj.MengenDifferenz.ToString();
                DrawDifferenz(obj, FontFactory.StrongText, text, true);
            }

            //Differenz Preise zeichnen
            if (myEvent.ColumnIndex == clms.DPreisIndex) {

                String text = obj.MengenDifferenz > 0 ? "+" + obj.PreisDifferenz.ToString("C"): obj.PreisDifferenz.ToString("C");
                DrawDifferenz(obj, FontFactory.StandardText, text, false);
            }

            //Differenz Stunden zeichnen
            if (myEvent.ColumnIndex == clms.DStundenIndex) {

                String text = obj.MengenDifferenz > 0 ? "+" + obj.StundenDifferenz.ToString("N2"): obj.StundenDifferenz.ToString("N2");
                DrawDifferenz(obj, FontFactory.StandardText, text, false);
            }
        }

        /// <summary>
        /// Termine werden vom Auftrag mitgezeichnet
        /// </summary>
        /// <param name="obj">darzustellender Termin</param>
        public void Visit(Termin obj) {}


#region I N T E R N A L S


        private void DrawDifferenz(Auftrag obj, Font f, String text, bool centered)
        {
               
            var backgroundBrush = obj.Position == Session.Current.CurrentPosition? BrushFactory.DateActiveSelected: BrushFactory.DateActive;
            var foregroundBrush = BrushFactory.EqualImplemented;
            if (obj.MengenDifferenz < 0) foregroundBrush = BrushFactory.TooLessImplemented;
            if (obj.MengenDifferenz > 0) foregroundBrush = BrushFactory.TooMuchImplemented;

            myEvent.Graphics.FillRectangle(backgroundBrush, myEvent.CellBounds.X, myEvent.CellBounds.Y, myEvent.CellBounds.Width, myEvent.CellBounds.Height);
            DrawCellBorder();
           
            var p1 = new Point(myEvent.CellBounds.X - 1, myEvent.CellBounds.Top);
            if (centered) DrawCentered(text, foregroundBrush, f, p1, myEvent.CellBounds.Width, myEvent.CellBounds.Height);
            else DrawRightAligned(text, foregroundBrush, f, p1, myEvent.CellBounds.Width, myEvent.CellBounds.Height);

            myEvent.Handled = true;
        }


        /// <summary>
        /// Begrenzung unten/rechts zeichnen
        /// </summary>
        private void DrawCellBorder()
        {

            var r = myEvent.CellBounds;

            myEvent.Graphics.DrawLine(PenFactory.Grid, new Point(r.Left, r.Bottom - 1), new Point(r.Right - 1, r.Bottom - 1)); 
            myEvent.Graphics.DrawLine(PenFactory.Grid, new Point(r.Right - 1, r.Top), new Point(r.Right - 1, r.Bottom - 1)); 
        }


        /// <summary>
        /// Einzelnen Zeitschlüssel zeichnen
        /// </summary>
        private void DrawCentered(String text, Brush b, Font f, Point pos, int width, int height)
        {
        
            if (text == "") return;
            SizeF s = myEvent.Graphics.MeasureString(text, f);
            
            float offsX = (width - s.Width) / 2F;
            if (offsX < 0) offsX = 0;
            float offsY = (height - s.Height) / 2F;
            if (offsY < 0) offsY = 0;
            
            myEvent.Graphics.DrawString(text, f, b, pos.X + offsX + 1, pos.Y + offsY); 
        }


        /// <summary>
        /// Einzelnen Zeitschlüssel zeichnen
        /// </summary>
        private void DrawRightAligned(String text, Brush b, Font f, Point pos, int width, int height)
        {
        
            if (text == "") return;
            SizeF s = myEvent.Graphics.MeasureString(text, f);
            
            float offsX = width - s.Width - 3;
            float offsY = (height - s.Height) / 2F;
            if (offsY < 0) offsY = 0;
            
            myEvent.Graphics.DrawString(text, f, b, pos.X + offsX + 1, pos.Y + offsY); 
        }

        
        /// <summary>
        /// Einzelne Terminkennzeichnung zeichnen
        /// </summary>
        /// <param name="dat">zu zeichnender Termin</param>
        /// <param name="g">Zeichenoberfläche</param>
        /// <param name="pos">Verfügbare Startzeichenposition</param>
        /// <param name="width">Verfügbare Breite (für Ausrichtung)</param>
        /// <param name="height">Verfügbare Höhe (für Ausrichtung)</param>
        private void DrawTag(Termin dat, Point pos, int width, int height)
        {
        
            //zu zeichndene Markierung vom Terminobjekt lesen (z.Z. Stringkodierung)
            String text = dat.ToString();

            //Zeichenumfang für Ausrichtung ermitteln
            if (String.IsNullOrEmpty(text)) return;            
            SizeF s = myEvent.Graphics.MeasureString(text, FontFactory.DateTag);
            
            //Startzeichenposition via Ausrichtung aushandeln
            float offsX = (width - s.Width) / 2F;
            float offsY = (height - s.Height) / 2F;
            if (offsY < 0) offsY = 0;

            //Zeichnen, Farbe vom Terminobjekt lesen
            if (offsX < -1) myEvent.Graphics.DrawString("...", FontFactory.DateTag, GetTagBrush(dat), pos.X, pos.Y + offsY);
            else            myEvent.Graphics.DrawString(text, FontFactory.DateTag, GetTagBrush(dat), pos.X + offsX, pos.Y + offsY); 
        }
        
        /// <summary>
        /// Zeitschlüsselleiste zeichnen
        /// </summary>
        /// <param name="iho">verantwortliches IH-Objekt</param>
        void DrawCalendar(IHObjekt iho)
        {
            
            //Endpunkte für Zellgrenzlinie
            Point p1 = new Point(myEvent.CellBounds.X - 1, myEvent.CellBounds.Top);
            Point p2 = new Point(myEvent.CellBounds.X - 1, myEvent.CellBounds.Bottom - 1);
            
            //aktuelle Breite einer Zelle (Anwender kann Spalte dynamisch verbreitern)
            int len = myEvent.CellBounds.Width / iho.Traits.Count;
            
            //für jeden Zeitschlüssel    
            foreach (int cal in iho.Traits.Calendar.CalendarKeys) {
             
                //aktuellen, d.h. heutigen Slot kennzeichnen
                //if (iho.Traits.Calendar.CurrentKey.HasValue && iho.Traits.Calendar.CurrentKey.Value == cal) {
                
                //    DrawMarkedCalendarSlot(BrushFactory.CurrentKey, p1.X, len);
                
                //} else 
                if (cal >= Session.Current.DeltaVon && cal <= Session.Current.DeltaBis) {

                    DrawMarkedCalendarSlot(BrushFactory.DeltaKeys, p1.X, len);
                }

                DrawCentered(iho.Traits.Calendar.FormatKeyShort(cal), BrushFactory.Text, FontFactory.DateHead, p1, len, myEvent.CellBounds.Height);

                //nur rechte Begrenzungslinie des Termines zeichnen
                p1.X += len;
                p2.X  = p1.X;
                myEvent.Graphics.DrawLine(PenFactory.Grid, p1, p2);
            }
        }


        void DrawMarkedCalendarSlot(Brush b, int x, int len)
        {

            Rectangle r = new Rectangle(x + 1, myEvent.CellBounds.Top, len - 1, myEvent.CellBounds.Height);
            myEvent.Graphics.FillRectangle(b, r);
        }

        
        /// <summary>
        /// Terminkette zeichnen
        /// </summary>
        /// <param name="au">Auftrag der Termine</param>
        /// <param name="greyStart">Start Grauzone als Key bei Adminzugriff und entsprechend aktivierter Planung</param>
        /// <param name="planStart">Start operative Planung als Key bei entsprechend aktivierter Planung</param>
        /// <param name="editStart">Start Terminplanung</param>
        /// <param name="editClose">Ende Terminplanung</param>
        void DrawDates(Auftrag au, int editVon, int editBis, int operVon)
        {
                
            //Endpunkte für Zellgrenzlinie
            Point p1 = new Point(myEvent.CellBounds.X - 1, myEvent.CellBounds.Top);
            Point p2 = new Point(myEvent.CellBounds.X - 1, myEvent.CellBounds.Bottom - 1);
                
            //aktuelle Breite einer Zelle (Anwender kann Spalte dynamisch verbreitern)
            int len = myEvent.CellBounds.Width / au.Traits.Count;
            

            foreach (KeyValuePair<int, Termin> pair in au.Dates) {
                
                //Hintergrunddarstellung aushandeln
                Brush br;
                bool isHoliday = au.Traits.Calendar.Holidays.ContainsKey(pair.Key);
                bool isActive = au.Position == Session.Current.CurrentPosition;
                                               
                if (au.IsDaily && au.Traits.Type != ScheduleType.Week) br = BrushFactory.DateWeekOnly;
                else if (au.Traits.Type == ScheduleType.YearOld) br = BrushFactory.DateProtected;
                else if (pair.Key < editVon || pair.Key > editBis) br = BrushFactory.DateProtected;
                else if (operVon != 0 && operVon != editVon && pair.Key <= operVon) br = BrushFactory.DateGreyarea;
                else if (isHoliday && isActive) br = BrushFactory.DateHolidaySelected; 
                else if (isHoliday) br = BrushFactory.DateHoliday; 
                else if (isActive) br = BrushFactory.DateActiveSelected;
                else br = BrushFactory.DateActive;
                
                //Hintergrund zeichnen    
                myEvent.Graphics.FillRectangle(br, p1.X + 1, p1.Y, len, myEvent.CellBounds.Height);

                //Terminkennzeichnung darstellen 
                DrawTag(pair.Value, p1, len, myEvent.CellBounds.Height);
                
                //nur rechte Begrenzungslinie des Termines zeichnen
                p1.X += len;
                p2.X  = p1.X;
                myEvent.Graphics.DrawLine(PenFactory.Grid, p1, p2);
            }
            
            //Überhang zeichnen
            myEvent.Graphics.FillRectangle(BrushFactory.Inactive, p1.X + 1, p1.Y, myEvent.CellBounds.Right - p1.X - 2, myEvent.CellBounds.Height);
        }        
        
        /// <summary>
        /// Terminmarkierungsfarbe aushandeln
        /// </summary>
        /// <param name="dat">Termin</param>
        /// <returns>Pinsel für Terminmarkierung</returns>
        Brush GetTagBrush(Termin dat)
        {
         
            if (dat.EditedType == EditedType.Special) return BrushFactory.SpecialText;
            if (dat.EditedType != EditedType.Empty && dat.EditedType != EditedType.Deleted) return BrushFactory.CommonText;
            if (dat.ImplementedId > 0) return BrushFactory.ImplementedText;
            if (dat.PlannedId > 0) return BrushFactory.PlannedText;
            return BrushFactory.Text;
        }
        
        
        Brush GetTypeBrush(Traits myTraits)
        {
        
            switch (myTraits.Type) {
            
            case ScheduleType.YearOld:
            
                return BrushFactory.TypeYearOld;
            
            case ScheduleType.YearCurrent:
            
                return BrushFactory.TypeYearCurrent;
            
            case ScheduleType.YearNew:
            
                return BrushFactory.TypeYearNew;
            
            case ScheduleType.Week:
            
                return BrushFactory.TypeWeek;
            
            default:
            
                return BrushFactory.DateActive;
            } 
        }
        
        
#endregion        

    }
}

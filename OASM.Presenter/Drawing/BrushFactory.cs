﻿using System.Drawing;
using System.Drawing.Drawing2D;


namespace comain.Drawing
{

    /// <summary>
    /// Häufig benutzte Pinsel bereitstellen
    /// </summary>
    static class BrushFactory
    {
    
        static BrushFactory()
        {
        
            Debug = new SolidBrush(Color.Red);
            Text = new SolidBrush(Color.Black);
            Inactive = new SolidBrush(Color.Silver);
        
            DateActive = new SolidBrush(CommonBackground);
            DateActiveSelected = new SolidBrush(ActiveBackground);
            DateWeekOnly = new HatchBrush(HatchStyle.DarkDownwardDiagonal, Color.SandyBrown, Color.AntiqueWhite);
            DateHoliday = new HatchBrush(HatchStyle.DiagonalCross, Color.PowderBlue, Color.White);
            DateHolidaySelected = new HatchBrush(HatchStyle.DiagonalCross, Color.PowderBlue, Color.Gold);
            DateGreyarea = new HatchBrush(HatchStyle.LightVertical, Color.LightGray, Color.WhiteSmoke);
            DateProtected = new HatchBrush(HatchStyle.DarkDownwardDiagonal, Color.SandyBrown, Color.AntiqueWhite);

            HeadBearbeiter = new SolidBrush(Color.LightGray);
            HeadStandort = new SolidBrush(Color.Gainsboro);
            HeadIHObjekt = new SolidBrush(Color.WhiteSmoke);
            
            TypeYearOld = new SolidBrush(Color.LightBlue);
            TypeYearCurrent = new SolidBrush(Color.PaleTurquoise);
            TypeYearNew = new SolidBrush(Color.LightGreen);
            TypeWeek = new SolidBrush(Color.Thistle);
            
            CurrentKey = new SolidBrush(Color.Gold);
            DeltaKeys = new SolidBrush(Color.Orange);

            SpecialText = new SolidBrush(Color.Green);
            CommonText = new SolidBrush(Color.Black);
            ImplementedText = new SolidBrush(Color.FromArgb(192, 0, 0));
            PlannedText = new SolidBrush(Color.FromArgb(0, 0, 192));
            TooLessImplemented = new SolidBrush(Color.Crimson);
            TooMuchImplemented = new SolidBrush(Color.ForestGreen);
            EqualImplemented = new SolidBrush(Color.DimGray);
        }
        
        /// <summary>
        /// Farbe für inaktiven Hintergrund
        /// </summary>
        public static Color CommonBackground { get { return Color.White; } }
        /// <summary>
        /// Farbe Hintergrund der aktiven Zeile
        /// </summary>
        public static Color ActiveBackground { get { return Color.Gold; } }
        /// <summary>
        /// Pinsel zur Fehlersuche
        /// </summary>
        public static Brush Debug { get; private set; }
        /// <summary>
        /// Pinsel für allgemeinen Text
        /// </summary>
        public static Brush Text { get; private set; }
        /// <summary>
        /// Pinsel für normalen Hintergrund
        /// </summary>
        public static Brush DateActive { get; private set; }
        /// <summary>
        /// Pinsel für normalen Hintergrund der aktiven Zeile
        /// </summary>
        public static Brush DateActiveSelected { get; private set; }
        /// <summary>
        /// Pinsel für inaktive Bereiche
        /// </summary>
        public static Brush Inactive { get; private set; }
        /// <summary>
        /// Pinsel für Tagesaufträge im Jahresplan
        /// </summary>        
        public static Brush DateWeekOnly { get; private set; }
        /// <summary>
        /// Pinsel zum Zeichnen von Wochenenden, Feiertage und Betriebsferien
        /// </summary>
        public static Brush DateHoliday { get; private set; }
        /// <summary>
        /// Pinsel zum Zeichnen von Wochenenden, aktiven Zeile
        /// </summary>
        public static Brush DateHolidaySelected { get; private set; }
        /// <summary>
        /// Pinsel zum Zeichnen der Grauzone
        /// </summary>        
        public static Brush DateGreyarea { get; private set; }
        /// <summary>
        /// Pinsel zum Zeichnen erledigter, unveränderlicher Perioden
        /// </summary>
        public static Brush DateProtected { get; private set; }
        /// <summary>
        /// Pinsel für Hintergrund Kopfzeile Bearbeiter
        /// </summary>
        public static Brush HeadBearbeiter { get; private set; }
        /// <summary>
        /// Pinsel für Hintergrund Kopfzeile Standort
        /// </summary>
        public static Brush HeadStandort { get; private set; }
        /// <summary>
        /// Pinsel für Hintergrund Kopfzeile IH-Objekt
        /// </summary>
        public static Brush HeadIHObjekt { get; private set; }
        /// <summary>
        /// Pinsel für Scheduletyp Jahresplan Alt
        /// </summary>
        public static Brush TypeYearOld { get; private set; }
        /// <summary>
        /// Pinsel für Scheduletyp Jahresplan Aktuell
        /// </summary>
        public static Brush TypeYearCurrent { get; private set; }
        /// <summary>
        /// Pinsel für Scheduletyp Jahresplan Zukunft
        /// </summary>
        public static Brush TypeYearNew { get; private set; }
        /// <summary>
        /// Pinsel für Scheduletyp Wochenplan
        /// </summary>
        public static Brush TypeWeek { get; private set; }
        /// <summary>
        /// Pinsel für aktuellen Terminslot
        /// </summary>
        public static Brush CurrentKey { get; private set; }
        /// <summary>
        /// Pinsel für aktuellen Terminslot
        /// </summary>
        public static Brush DeltaKeys { get; private set; }
        /// <summary>
        /// Pinsel für Sonderleistungen
        /// </summary>
        public static Brush SpecialText { get; private set; }
        /// <summary>
        /// Pinsel für Standardtext
        /// </summary>
        public static Brush CommonText { get; private set; }
        /// <summary>
        /// Pinsel für Realisierungen
        /// </summary>
        public static Brush ImplementedText { get; private set; }
        /// <summary>
        /// Pinsel für Planungen
        /// </summary>
        public static Brush PlannedText { get; private set; }
        /// <summary>
        /// Pinsel für Differenzanzeige von Überrealisierungen
        /// </summary>
        public static Brush TooMuchImplemented { get; private set; }
        /// <summary>
        /// Pinsel für Differenzanzeige von Unterrealisierungen
        /// </summary>
        public static Brush TooLessImplemented { get; private set; }
        /// <summary>
        /// Pinsel für Differenzanzeige von korrekten Realisierungen
        /// </summary>
        public static Brush EqualImplemented { get; private set; }
    }
}

﻿using System.Drawing;


namespace comain.Drawing
{

    /// <summary>
    /// Häufig benutzte Stifte bereitstellen
    /// </summary>
    static class PenFactory
    {
    
        static PenFactory()
        {
        
            Debug = new Pen(Color.Red);
            Grid = new Pen(Color.Gray);
            Group = new Pen(Color.Black, 2);        
        }
        
        /// <summary>
        /// Pinsel zur Fehlersuche
        /// </summary>
        public static Pen Debug { get; private set; }
         /// <summary>
        /// Rahmenpinsel
        /// </summary>
        public static Pen Grid { get; private set; }
        /// <summary>
        /// Pinsel für Gruppenkopfzeilentrenner
        /// </summary>
        public static Pen Group { get; private set; }

    }
}

﻿using System.Drawing;


namespace comain.Drawing
{

    /// <summary>
    /// Häufig benutzte Schriften bereitstellen
    /// </summary>
    static class FontFactory
    {
    
        static FontFactory()
        {
        
            Head1Caption = new Font("Tahoma", 10);
            Head1Value = new Font("Tahoma", 10, FontStyle.Bold);
            Head2Caption = new Font("Tahoma", 9);
            Head2Value = new Font("Tahoma", 9, FontStyle.Bold);
            DateHead = new Font("Tahoma", 8);
            DateTag = new Font("Tahoma", 8, FontStyle.Bold);
            StandardText = new Font("Tahoma", 8);
            StrongText = new Font("Tahoma", 8, FontStyle.Bold);
        }
        
        /// <summary>
        /// Schrift für Überschriftsbezeichnung
        /// </summary>
        public static Font Head1Caption { get; private set; }
        /// <summary>
        /// Schrift für Überschriftsobjekt
        /// </summary>
        public static Font Head1Value { get; private set; }
        /// <summary>
        /// Schrift für Überschriftsbezeichnung
        /// </summary>
        public static Font Head2Caption { get; private set; }
        /// <summary>
        /// Schrift für Überschriftsobjekt
        /// </summary>
        public static Font Head2Value { get; private set; }
        /// <summary>
        /// Schrift für Überschriftsbezeichnung
        /// </summary>
        public static Font DateHead { get; private set; }
        /// <summary>
        /// Schrift für Überschriftsobjekt
        /// </summary>
        public static Font DateTag { get; private set; }
        /// <summary>
        /// Schrift für Überschriftsbezeichnung
        /// </summary>
        public static Font StandardText { get; private set; }
        /// <summary>
        /// Schrift für Überschriftsobjekt
        /// </summary>
        public static Font StrongText { get; private set; }

    }
}

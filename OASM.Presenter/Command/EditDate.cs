﻿using comain.Schedule;
using comain.Forms;


namespace comain.Command
{
    
    /// <summary>
    /// Befehl Termin bearbeiten
    /// </summary>
    public class EditDateCommand : ICommand
    {
    
        /// <summary>
        /// Control, welches den Termin darstellt (für Refresh-Methode)
        /// </summary>
        IMainFormUpdater myForm;
        /// <summary>
        /// Zu bearbeitender Termin
        /// </summary>
        Termin myDate;
        /// <summary>
        /// neuer Termintyp
        /// </summary>
        EditedType newType;
        /// <summary>
        /// Ausgangstermintyp
        /// </summary>
        EditedType oldType;
        /// <summary>
        /// neue Menge
        /// </summary>
        short newMenge;
        /// <summary>
        /// Ausgangsmenge
        /// </summary>
        short oldMenge;
        
        /// <summary>
        /// Befehl Termin bearbeiten
        /// </summary>
        /// <param name="ctrl">Control, welches den Termin darstellt (für Refresh-Methode)</param>
        /// <param name="dat">Zu bearbeitender Termin</param>
        /// <param name="type">neuer, bearbeiteter Termintyp</param>
        /// <param name="menge">neue, bearbeitete Menge</param>
        public EditDateCommand(IMainFormUpdater frm, Termin dat, EditedType type, short menge)
        {
            
            myForm   = frm;
            myDate   = dat;
             
            newType  = type;
            newMenge = menge;
            oldType  = dat.EditedType;
            oldMenge = dat.EditedMenge;
        }

        /// <summary>
        /// Befehl ausführen und in Historie eintragen
        /// </summary>
        public void Execute(CommandHistory hist)
        {
            if (oldType == newType && oldMenge == newMenge) return;
            if (newType == EditedType.Deleted && (oldType == EditedType.Deleted || oldType == EditedType.Empty &&
                                                   myDate.PlannedId == 0 && myDate.ImplementedId == 0)) return;
            Redo();
            if (hist != null) hist.Add(this);
        }

        /// <summary>
        /// Befehl rückgängig machen
        /// </summary>
        public void Undo()
        {
            myDate.SetEdited(oldType, oldMenge);
            myForm.UpdateStatistics();            
            myForm.Invalidate(myDate.Parent.Position);
        }

        /// <summary>
        /// Befehl erneut ausführen
        /// </summary>
        public void Redo()
        {
            myDate.SetEdited(newType, newMenge);
            myForm.UpdateStatistics();
            myForm.Invalidate(myDate.Parent.Position);
        }

        /// <summary>
        /// Befehl kann rückgängig gemacht werden
        /// </summary>
        public bool HasUndo
        {
            get { return true; }
        }


        public bool IsScheduleModificating
        {
            get { return true; }
        }
    }
}

﻿namespace comain.Command
{
	
    /// <summary>		
    /// Befehlsschnittstelle, alle Befehle, die in einer Historie gespeichert werden können, 		
    /// müssen diese Schnittstelle implementieren		
    /// </summary>		
    public interface ICommand 		
    {
		
        /// <summary>		
        /// Befehl (evtl. erneut) ausführen		
        /// </summary> 		
        /// <param name="hist">Historie, in der Befehl einzutragen ist (darf null sein)</param> 		
        void Execute(CommandHistory hist);		
        /// <summary>		
        /// Befehl rückgängig machen		
        /// </summary> 		
        void Undo(); 		
        /// <summary>
        /// Befehl erneut ausführen 		
        /// </summary> 		
        void Redo();	
        /// <summary>
        /// Kann Befehl rückgängig gemacht werden?
        /// </summary>
        bool HasUndo { get; }
        /// <summary>
        /// Verändert Befehl die Planung? (Sicherheitsabfrage beim Verwerfen der Planung)
        /// </summary>
        bool IsScheduleModificating { get; }
    }
}
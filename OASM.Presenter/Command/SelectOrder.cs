﻿using System.Windows.Forms;
using comain.Schedule;


namespace comain.Command
{
    
    /// <summary>
    /// Befehl Auftrag auswählen (Checkbox (ab-)wählen
    /// </summary>
    class SelectOrderCommand : ICommand
    {

        /// <summary>
        /// Control, welches den Termin darstellt (für Refresh-Methode)
        /// </summary>
        DataGridView myControl;
        /// <summary>
        /// Zu bearbeitender Termin
        /// </summary>
        Auftrag myOrder;

        
        public SelectOrderCommand(DataGridView ctrl, Auftrag au)
        {
            myControl = ctrl;
            myOrder   = au;
        }

        public void Execute(CommandHistory hist)
        {
            Redo();
            if (hist != null) hist.Add(this);
        }

        /// <summary>
        /// Befehl rückgängig machen
        /// </summary>
        public void Undo()
        {
            myOrder.Selected = !myOrder.Selected;
            myControl.InvalidateRow(myOrder.Position);            
        }

        /// <summary>
        /// Befehl erneut ausführen
        /// </summary>
        public void Redo()
        {
            myOrder.Selected = !myOrder.Selected;
            myControl.InvalidateRow(myOrder.Position);            
        }

        /// <summary>
        /// Befehl kann rückgängig gemacht werden
        /// </summary>
        public bool HasUndo
        {
            get { return true; }
        }


        public bool IsScheduleModificating
        {
            get { return false; }
        }
    }
}

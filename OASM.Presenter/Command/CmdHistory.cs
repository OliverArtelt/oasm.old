﻿using System.Collections.Generic;
using System.Linq;


namespace comain.Command
{ 
    
    /// <summary>
    /// Rückgängig-Mechanismus für Befehle des Benutzers (Realisierungen, Planungen)
    /// </summary>
    public class CommandHistory
    {
    
        /// <summary>
        /// Stapel der ausgeführten (rückgängig machbaren) Befehle
        /// </summary>
        Stack<ICommand> undo;
        /// <summary>
        /// Stapel der rückgängig gemachten, wiederholbaren Befehle
        /// </summary>
        Stack<ICommand> redo;
                
        /// <summary>
        /// Konstruktor: Stapel einrichten 
        /// </summary>
        public CommandHistory()
        {
        
            undo = new Stack<ICommand>();
            redo = new Stack<ICommand>();
        }
        
        /// <summary>
        /// neuer Befehl in die Historie einfügen
        /// </summary>
        /// <param name="cmd">neuer Befehl</param>
        public void Add(ICommand cmd)
        {
        
            if (cmd.HasUndo) {
            
                //alle Befehle, die rückgängig gemacht, aber nicht erneut ausgeführt wurden, sind nun verwaist
                redo.Clear();
                undo.Push(cmd);
            
            } else {
            
                //Befehl unterstützt Mechanismus nicht: History broken
                undo.Clear();
                redo.Clear();
            }
        }
        
        /// <summary>
        /// letzten Befehl rückgängig machen
        /// </summary>
        public void Prev()
        {
            //Historie leer?
            if (!HasPrev) return;
        
            ICommand cmd = undo.Pop();
            cmd.Undo();
            redo.Push(cmd);
        }
        
        /// <summary>
        /// nächsten Befehl wiederherstellen
        /// </summary>
        public void Next()
        {
            //Batch leer?
            if (!HasNext) return;

            ICommand cmd = redo.Pop();
            cmd.Redo();
            undo.Push(cmd);
        }
        
        /// <summary>
        /// Weitere Befehle auf dem Undo-Stapel?
        /// </summary>
        public bool HasPrev
        {
            get { return undo.Count > 0; }
        }

        /// <summary>
        /// Weitere Befehle auf dem Redo-Stapel?
        /// </summary>
        public bool HasNext
        {
            get { return redo.Count > 0; }
        }

        /// <summary>
        /// Prüfen ob Benutzer Planung verändert hat (Bestätigungsmeldung anzeigen)
        /// </summary>
        /// <returns></returns>
        public bool HasModifications()
        {

            return undo.Any(p => p.IsScheduleModificating);
        }
    }
}

﻿using comain.User;
using comain.Schedule;
using comain.Workspace;


namespace comain.Command
{
    
    /// <summary>
    /// Erlaubte Aktionen des Benutzers aushandeln
    /// </summary>
    public class CommonAction : IUserAction
    {
    
        ScheduleType myType = ScheduleType.None;
        
        /// <summary>
        /// Erlaubte Benutzeraktionen ermitteln. Verwendet allgemeine Eigenschaften des Benutzers und Planes.
        /// </summary>
        /// <param name="user">Eingeloggter Benutzer</param>
        /// <param name="type">Plantyp</param>
        public CommonAction(ScheduleType type)
        {
            myType = type;
        }

        public bool PlanType
        {
            get { return myType != ScheduleType.YearOld && (Session.Current.User.MayPlan || Session.Current.User.MayAdminister); }
        }

        public bool ImplementType
        {
            get { return myType != ScheduleType.YearOld && (Session.Current.User.MayImplement || Session.Current.User.MayAdminister); }
        }

        public bool DailyType
        {
            get { return myType == ScheduleType.Week && (Session.Current.User.MayPlan || Session.Current.User.MayAdminister); }
        }

        public bool SpecialType
        {
            get { return myType != ScheduleType.YearOld && (Session.Current.User.MayImplement || Session.Current.User.MayAdminister); }
        }

        public bool DeleteType
        {
            get { return myType != ScheduleType.YearOld && (Session.Current.User.MayPlan || Session.Current.User.MayImplement || Session.Current.User.MayAdminister); }
        }

        public bool AmountType
        {
            get { return myType != ScheduleType.YearOld && (Session.Current.User.MayPlan || Session.Current.User.MayImplement || Session.Current.User.MayAdminister); }
        }
    }
}

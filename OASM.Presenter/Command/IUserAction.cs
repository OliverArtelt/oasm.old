﻿

namespace comain.Command
{
    
    /// <summary>
    /// Erlaubte Aktionen des Benutzers aushandeln
    /// </summary>
    public interface IUserAction
    {
        
        /// <summary>
        /// Benutzer kann planen
        /// </summary>
        bool PlanType { get; }
        /// <summary>
        /// Benutzer kann realisieren
        /// </summary>
        bool ImplementType { get; }
        /// <summary>
        /// Benutzer kann Tagesaufträge planen
        /// </summary>
        bool DailyType { get; }
        /// <summary>
        /// Benutzer kann Sonderleistungen realisieren
        /// </summary>
        bool SpecialType { get; }
        /// <summary>
        /// Benutzer kann Planungen/Realisierungen löschen
        /// </summary>
        bool DeleteType { get; }
        /// <summary>
        /// Anzahllistbox erlaubt
        /// </summary>
        bool AmountType { get; }
    }
}

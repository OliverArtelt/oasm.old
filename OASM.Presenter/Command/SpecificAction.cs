﻿using comain.User;
using comain.Schedule;


namespace comain.Command
{
    
    /// <summary>
    /// Erlaubte Aktionen des Benutzers aushandeln
    /// </summary>
    public class SpecificAction : IUserAction
    {
    
        Schedule.Traits myTraits = null;
        Termin myDate = null;
        
        /// <summary>
        /// Erlaubte Benutzeraktionen ermitteln. Verwendet spezifische Eigenschaften des Termines.
        /// </summary>
        /// <param name="traits">Planeigenschaften</param>
        /// <param name="key">Terminslotschlüssel</param>
        public SpecificAction(Schedule.Traits traits, Termin date)
        {
            myTraits = traits;
            myDate = date;
        }

        public bool PlanType
        {
            get { return MayPlan(); }
        }

        public bool ImplementType
        {
            get { return MayImplement(); }
        }

        public bool DailyType
        {
            get { return MayPlan() && myTraits.Type == ScheduleType.Week; }
        }

        public bool SpecialType
        {
            get { return MayImplement(); }
        }

        public bool DeleteType
        {
            get { return MayDelete(); }
        }

        public bool AmountType
        {
            get { return MayPlan() || MayImplement(); }
        }


#region P R I V A T E
        
        private bool MayPlan()
        {
        
            int myKey = myDate.Key;
        
            //alter Plan nur zum Lesen
            if (myTraits.Type == ScheduleType.YearOld) return false;
            
            //Admin darf alles.
            if (myTraits.User.MayAdminister) return true;
            
            //Bearbeiter ohne Planung dürfen nicht.
            if (!myTraits.User.MayPlan) return false;
            
            //nicht vor Betrachtungszeitraum
            if (myTraits.Calendar.CalculateCalendarKey(myTraits.OASMSettings.MinDate) > myKey) return false;
            
            //nicht nach Betrachtungszeitraum
            if (myTraits.Calendar.CalculateCalendarKey(myTraits.OASMSettings.MaxDate) < myKey) return false;
            
            //Meister ab Operative Planung
            if (myTraits.User is ClientUser) {
            
                if (myTraits.Calendar.CalculateCalendarKey(myTraits.OASMSettings.OperDate) > myKey) return false;
            }
            
            //Auftragnehmer ab Grauzone
            if (myTraits.User is ServiceUser) {
            
                if (myTraits.Calendar.CalculateCalendarKey(myTraits.OASMSettings.GreyDate) > myKey) return false;
            }
            
            return true; 
        }
        
        private bool MayImplement()
        {
        
            int myKey = myDate.Key;
        
            //alter Plan nur zum Lesen
            if (myTraits.Type == ScheduleType.YearOld) return false;
            
            //Admin darf alles.
            if (myTraits.User.MayAdminister) return true;

            //Bearbeiter ohne Realisierung dürfen nicht.
            if (!myTraits.User.MayImplement) return false;

            //nur aktuelles Jahr
            //abgeklemmt für Jahreswechsel
            //if (myTraits.Calendar.CalculateFirstDate(myKey).Year != DateTime.Now.Year) return false;
            
            //Meister ab Operative Planung
            if (myTraits.User is ClientUser) {
            
                if (myTraits.Calendar.CalculateCalendarKey(myTraits.OASMSettings.OperDate) > myKey) return false;
            }
            
            //Auftragnehmer ab Grauzone
            if (myTraits.User is ServiceUser) {
            
                if (myTraits.Calendar.CalculateCalendarKey(myTraits.OASMSettings.GreyDate) > myKey) return false;
            }
            
            return true; 
        }

        private bool MayDelete()
        {

            //alter Plan nur zum Lesen
            if (myTraits.Type == ScheduleType.YearOld) return false;
            
            //Admin darf alles.
            if (myTraits.User.MayAdminister) return true;

            //eigene Bearbeitungen widerrufen
            if (myDate.EditedType != EditedType.Empty && myDate.EditedType != EditedType.Deleted) return true;
            
            //Meister dürfen geplante, aber noch nicht realisierte Termine entfernen (Kosten sparen)
            if (MayPlan() && myDate.ImplementedId == 0 && myDate.PlannedId != 0) return true;
            
            return false;
        }

#endregion

    }
}

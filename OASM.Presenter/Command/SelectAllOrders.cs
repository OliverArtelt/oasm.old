﻿using System.Windows.Forms;
using comain.Schedule;
using System.Collections.Generic;
using comain.Workspace;


namespace comain.Command
{
    
    /// <summary>
    /// Alle Aufträge für Profil an/abwählen
    /// </summary>
    public class SelectAllOrdersCommand : ICommand
    {
    
        /// <summary>
        /// Der Baum, an dem gearbeitet werden soll
        /// </summary>
        Root myTree;
        /// <summary>
        /// Das DataGridView, welches die Aufträge hält
        /// </summary>
        Control myControl;
        /// <summary>
        /// alle Aufträge ermitteln, die diesen Status bereits besitzen 
        /// (dürfen vom Undo nicht angefaßt werden)
        /// </summary>
        List<int> myOrders;

        
        /// <summary>
        /// Alle Aufträge für Profil an/abwählen
        /// </summary>
        /// <remarks>
        /// Aufträge sollen bei Undo wieder in ihren Ausgangsstatus zurückgesetzt werden, 
        /// deshalb muß die Liste zu ändernder Aufträge angefertigt und gespeichert werden
        /// </remarks>
        /// <param name="tree">Schedulecomposite,dessen Aufträge manipuliert werden sollen</param>
        /// <param name="ctrl">Das DataGridView, welches die Aufträge hält</param>
        public SelectAllOrdersCommand(Root tree, Control ctrl)
        {
            
            myControl = ctrl;
            myTree    = tree;
            
            //alle umzuwandelnden Aufträge ermitteln (damit sie bei Undo wieder korrekt dargestellt werden)
            OrderCollector col = new OrderCollector(!Session.Current.OrdersSelectedState);
            tree.Accept(col);
            myOrders = col.CollectedOrders;
            myOrders.Sort();
        }
        
        /// <summary>
        /// Befehl aktivieren
        /// </summary>
        /// <param name="hist">Befehlsliste, in die der Befehl gespeichert werden soll</param>
        public void Execute(CommandHistory hist)
        {                                                            

            Redo();
            if (hist != null) hist.Add(this);
        }

        /// <summary>
        /// Befehl rückgängig machen
        /// </summary>
        public void Undo()
        {
            
            Session.Current.OrdersSelectedState = !Session.Current.OrdersSelectedState;
            SelectAllOrders s = new SelectAllOrders(Session.Current.OrdersSelectedState, myOrders);
            myTree.Accept(s);
            myControl.Invalidate();
        }

        /// <summary>
        /// Befehl (erneut) ausführen
        /// </summary>
        public void Redo()
        {
            
            Session.Current.OrdersSelectedState = !Session.Current.OrdersSelectedState;
            SelectAllOrders s = new SelectAllOrders(Session.Current.OrdersSelectedState, null);
            myTree.Accept(s);
            myControl.Invalidate();
        }

        /// <summary>
        /// Befehl kann rückgängig gemacht werden
        /// </summary>
        public bool HasUndo
        {
            get { return true; }
        }


        public bool IsScheduleModificating
        {
            get { return false; }
        }
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using comain.User;
using comain.Schedule;
using comain.Workspace;


namespace comain.Filter
{
   
    /// <summary>
    /// IH-Objektfilter
    /// </summary>
    public class IHFilter : IFilterFacade
    {

        /// <summary>
        /// Combolistbox der IH-Objektliste
        /// </summary>
        ComboBox ihBox;
        /// <summary>
        /// Combolistbox der IH-Objektbaugruppenliste
        /// </summary>
        ComboBox ihSubBox;
        /// <summary>
        /// Combolistbox der IH-Objektinventarliste
        /// </summary>
        ComboBox ihInventBox;
        /// <summary>
        /// Daten der IH-Objekte
        /// </summary>
        List<PicklistItem> picklist;


        /// <summary>
        /// IH-Objektfilter
        /// </summary>
        /// <param name="ih">Combolistbox der IH-Objektliste</param>
        /// <param name="ihSub">Combolistbox der IH-Objektbaugruppenliste</param>
        /// <param name="ihInvent">Combolistbox der IH-Objektinventarliste</param>
        public IHFilter(ComboBox ih, ComboBox ihSub, ComboBox ihInvent)
        {
            
            ihBox       = ih;
            ihSubBox    = ihSub;
            ihInventBox = ihInvent;
            picklist = null;
        }

        /// <summary>
        /// IH-Objektlisten füllen
        /// </summary>
        /// <param name="cnc">Datenbankverbindung</param>
        public void BindValues(PicklistType wtb)
        {
        
            try {

                if (picklist == null) {

                    var proxy = new FacadesProxy();
                    picklist = proxy.IHList(Session.Current.User.AuthenticateInfo);
                }

                if (wtb == PicklistType.IH) {
                
                    ihBox.DataSource = picklist.Where(p => IsMainIH(p.Value1)).ToList();   
                    ihBox.DisplayMember = "Value1";
                    ihBox.ValueMember = "Id";
                }
                
                if (wtb == PicklistType.IHSub) {
                
                    if (ihBox.SelectedIndex == -1) {
                    
                        ihSubBox.DataSource = picklist.Where(p => !IsMainIH(p.Value1)).ToList();
                           
                    } else {
                     
                        ihSubBox.DataSource = picklist.Where(p => p.Value1.StartsWith(SelectedMainIHNr) &&
                                                                  !IsMainIH(p.Value1)).ToList(); 
                    }  
                    ihSubBox.DisplayMember = "Value1";
                    ihSubBox.ValueMember = "Id";
                }
                
                if (wtb == PicklistType.IHInvent) {
                
                    if (ihBox.SelectedIndex == -1) {
                    
                        ihInventBox.DataSource = picklist.Where(p => !String.IsNullOrEmpty(p.Value2)).OrderBy(p => p.Value2).ToList();   
                    
                    } else {
                    
                        ihInventBox.DataSource = picklist.Where(p => p.Value1.StartsWith(SelectedMainIHNr) &&
                                                                !String.IsNullOrEmpty(p.Value2)).OrderBy(p => p.Value2).ToList();
                    }                                                                    
                    ihInventBox.DisplayMember = "Value2";
                    ihInventBox.ValueMember = "Id";
                }
                
            }
            catch (Exception x) { new comain.Error(x);}
        }

        /// <summary>
        /// gewähltes IHO übegeben
        /// </summary>
        /// <remarks>
        /// Es gilt Reihenfolge: 
        /// Wenn Baugruppe gewählt => nehmen
        /// Wenn Inventarnummer gewählt => nehmen
        /// oder IH-Objekt nehmen wenn gewählt
        /// </remarks>
        /// <returns>gewähltes IHO ("" wenn nichts gewählt wurde)</returns>
        public List<String> Values 
        { 
        
            get {
            
                List<String> list = new List<string>();
                PicklistItem item = null;
                String filter = String.Empty;                
            
                if (ihSubBox.SelectedItem != null) {
                
                    item = ihSubBox.SelectedItem as PicklistItem; 
                
                } else if (ihInventBox.SelectedItem != null) {
                
                    item = ihInventBox.SelectedItem as PicklistItem; 
                
                } else if (ihBox.SelectedItem != null) {
                
                    filter = "MAIN ";
                    item = ihBox.SelectedItem as PicklistItem; 
                
                } else {
                
                    return list;           
                }
                
                if (item != null) {
                
                    filter += item.Id;
                    list.Add(filter);
                }
                
                return list;
            }
        }

        /// <summary>
        /// IHO-Listen zurücksetzen
        /// </summary>
        public void ResetValue()
        {
            ihBox.SelectedIndex = -1;
            ihSubBox.SelectedIndex = -1;
            ihInventBox.SelectedIndex = -1;
        }


        public string FilterDesc
        {
            
            get { 
            
                if (ihSubBox.SelectedValue != null) return "IH-Objekt " + ihSubBox.Text;
                if (ihInventBox.SelectedValue != null) return "Inventarnummer " + ihInventBox.Text;
                if (ihBox.SelectedValue != null) return "IH-Objekt " + ihBox.Text;

                return "";
            }
        }
        
        
        public void SaveValue() {}
        
        
        private String SelectedMainIHNr
        {
        
            get {
            
                if (ihBox.SelectedIndex == -1) return String.Empty;
            
                PicklistItem item = (PicklistItem)ihBox.SelectedItem;
                return GetIHNr(item.Value1);
            }
        }
        
        
        private static String GetIHNr(String value)
        {
        
            int i = value.IndexOf(' ');
            if (i == -1) return value;
        
            return value.Substring(0, i);
        }
        
        
        private static bool IsMainIH(String value)
        {
        
            String s = GetIHNr(value);
            int c = s.Count(p => p == '-');
            return c == 3;
        }
    }
}

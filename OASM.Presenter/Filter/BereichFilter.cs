﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using comain.User;
using comain.Schedule;
using comain.Workspace;


namespace comain.Filter
{


    /// <summary>
    /// Filter Auftragnehmer
    /// </summary>
    public class BereichFilter : IFilterFacade
    {

        /// <summary>
        /// Combolistbox der Auftragnehmer/Bereich
        /// </summary>
        ComboBox box;
        /// <summary>
        /// Sind Daten geladen?
        /// </summary>
        List<PicklistItem> picklist;


        /// <summary>
        /// Filter Auftragnehmer/Bereich
        /// </summary>
        /// <param name="b">Combolistbox der Auftragnehmer/Bereich</param>
        /// <param name="t">Datencontainer</param>
        public BereichFilter(ComboBox b)
        {
            box = b;
            picklist = null;
        }
                
        /// <summary>
        /// Auftragnehmer/Bereich-Datencontainer beladen
        /// </summary>
        /// <param name="cnc">Datenbankverbindung</param>
        public void BindValues(PicklistType wtb)
        {
        
            if (picklist != null) return;
            
            try {

                var proxy = new FacadesProxy();
                picklist = proxy.BereichList(Session.Current.User.AuthenticateInfo);
                box.DataSource = picklist;
                box.DisplayMember = "Value1";
                box.ValueMember = "Id";
            }
            catch (Exception x) { 
            
                new comain.Error(x);
            }
        }

        /// <summary>
        /// Auftragnehmer/Bereich übergeben ("" bei keiner Wahl)
        /// </summary>
        public List<String> Values
        {
            
            get {
                    
                List<String> list = new List<String>();
                
                if (box.SelectedValue != null) list.Add(box.SelectedValue.ToString());
                return list;
            }
        }

        /// <summary>
        /// Bereichsliste zurücksetzen
        /// </summary>
        public void ResetValue()
        {
            box.SelectedIndex = -1;
        }


        public string FilterDesc
        {
            get { 
            
                if (box.SelectedIndex == -1) return "";
                return "Bereich " + box.Text; 
            }
        }
        
        
        public void SaveValue() {}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using comain.Schedule;
using comain.User;
using comain.Workspace;


namespace comain.Filter
{


    /// <summary>
    /// Filter Leistungsarten
    /// </summary>
    public class LAFilter : IFilterFacade
    {

        CheckedListBox box;
        List<PicklistItem> picklist;
        

        /// <summary>
        /// Filter Leistungsarten
        /// </summary>
        /// <param name="b">Combolistbox der LA-Liste</param>
        /// <param name="t">zu befüllender Datencontainer</param>
        public LAFilter(CheckedListBox b)
        {

            box = b;
            picklist = null;
        }
        
        /// <summary>
        /// LA-Datencontainer befüllen
        /// </summary>
        /// <param name="cnc">Datenbankverbindung</param>
        public void BindValues(PicklistType wtb)
        {
        
            if (picklist != null) return;
        
            try {

                var proxy = new FacadesProxy();
                picklist = proxy.LaList(Session.Current.User.AuthenticateInfo);
                box.Items.Clear();
                box.Items.AddRange(picklist.ToArray());
                box.DisplayMember = "Value1";
                box.ValueMember = "Id";
            }
            catch (Exception x) { 
            
                new comain.Error(x);
            }
        }

        /// <summary>
        /// gewählte Leistungsart übergeben
        /// </summary>
        public List<String> Values
        {
            
            get {
                    
                List<String> list = new List<String>();
                
                foreach (var it in box.CheckedItems) 
                    list.Add(((PicklistItem)it).Id.ToString());
                
                return list;
            }
        }

        /// <summary>
        /// Leistungsartenliste zurücksetzen
        /// </summary>
        public void ResetValue()
        {
            
            for (int i = 0; i < box.Items.Count; i++) box.SetItemChecked(i, false);
        }


        public string FilterDesc
        {
            
            get { 
            
                if (box.CheckedItems.Count == 0) return "";
                var bld = new StringBuilder(); 
                foreach (var it in box.CheckedItems) bld.Append(((PicklistItem)it).Value1).Append(",");
                bld.Length--; 
                return "Leistungsart " + bld.ToString();
            }
        }
        
        
        public void SaveValue() {}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using comain.User;
using comain.Schedule;
using comain.Workspace;


namespace comain.Filter
{


    /// <summary>
    /// Standortfilter
    /// </summary>
    public class PZFilter : IFilterFacade
    {
        
        ComboBox boxVon;
        ComboBox boxBis;
        List<PicklistItem> picklist;

         
        /// <summary>
        /// Standortfilter
        /// </summary>
        /// <param name="b">Combolistbox der Standortliste</param>
        /// <param name="t">zu befüllender Datencontainer</param>
        public PZFilter(ComboBox boxVon, ComboBox boxBis)
        {
            
            this.boxVon = boxVon;
            this.boxBis = boxBis;
            picklist = null;
        }
        
        /// <summary>
        /// Standortliste füllen
        /// </summary>
        public void BindValues(PicklistType wtb)
        {
        
            if (picklist != null) return;
            
            try {

                var proxy = new FacadesProxy();
                picklist = proxy.PzList(Session.Current.User.AuthenticateInfo);
                
                boxVon.BindingContext = new BindingContext(); 
                boxVon.DataSource = picklist;
                boxVon.DisplayMember = "Value1";
                boxVon.ValueMember = "Value2";

                boxBis.BindingContext = new BindingContext(); 
                boxBis.DataSource = picklist;
                boxBis.DisplayMember = "Value1";
                boxBis.ValueMember = "Value2";
            }
            catch (Exception x) { 
            
                new comain.Error(x);
            }
        }

        /// <summary>
        /// gewählten Standort geben ("" wenn keiner gewählt ist)
        /// </summary>
        public List<String> Values
        {
            
            get {

                if (boxVon.SelectedIndex == -1 && boxBis.SelectedIndex == -1 || 
                    picklist == null || picklist.Count == 0) return new List<String>();
                String von = boxVon.SelectedIndex == -1 ? picklist[0].Value2: boxVon.SelectedValue.ToString();
                String bis = boxBis.SelectedIndex == -1 ? picklist[picklist.Count - 1].Value2: boxBis.SelectedValue.ToString();
                
                return new List<String> { von, bis }; 
            }
        }

        /// <summary>
        /// Standort-Combobox zurücksetzen
        /// </summary>
        public void ResetValue()
        {
            boxVon.SelectedIndex = -1;
            boxBis.SelectedIndex = -1;
        }


        public string FilterDesc
        {

            get { 
            
                if (boxVon.SelectedIndex == -1 && boxBis.SelectedIndex == -1 || 
                    picklist == null || picklist.Count == 0) return String.Empty;
                String von = boxVon.SelectedIndex == -1 ? picklist[0].Value1: boxVon.Text;
                String bis = boxBis.SelectedIndex == -1 ? picklist[picklist.Count - 1].Value1: boxBis.Text;
                if (von == bis) return String.Format("Standort {0}", von);
                return String.Format("Standort {0} - {1}", von, bis);
            }
        }
        
        
        public void SaveValue() {}
    }
}

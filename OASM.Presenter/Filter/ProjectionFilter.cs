﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using comain.User;
using comain.Workspace;


namespace comain.Filter
{


    /// <summary>
    /// Planoptionen 
    /// </summary>
    public class ProjectionFilter : IFilterFacade
    {

        /// <summary>
        /// Options-Auswahlliste
        /// </summary>
        CheckedListBox myBox;

        /// <summary>
        /// Planoptionen
        /// </summary>
        /// <param name="b">Options-Auswahlliste</param>
        public ProjectionFilter(CheckedListBox b)
        {
            
            myBox = b;

            myBox.Items.Clear();
            
            if (Session.Current.User.SeePrices) myBox.Items.Add("Preise");
            if (Session.Current.User.SeeHours) myBox.Items.Add("Stunden");
            myBox.Items.Add("Standort");
            myBox.Items.Add("Maschine");
            myBox.Items.Add("Baugruppe");
            myBox.Items.Add("Inventar-Nr.");
            myBox.Items.Add("Kostenstelle");
            myBox.Items.Add("KW 1 - 52/53");
            myBox.Items.Add("Bedarfspos.");
            myBox.Items.Add("Nur relevante");
            if (Session.Current.User.SeeHours) myBox.Items.Add("Nur mit Stunden");
        }


        public void BindValues(PicklistType wtb)
        {
        }
        
        /// <summary>
        /// Wert übergeben
        /// </summary>
        public List<String> Values 
        { 
        
            get { 
            
                int idx = 0;
                List<String> list = new List<string>();

                if (Session.Current.User.SeePrices && myBox.GetItemChecked(idx++)) list.Add("ShowPrice");
                if (Session.Current.User.SeeHours && myBox.GetItemChecked(idx++))  list.Add("ShowHours");
                if (myBox.GetItemChecked(idx++)) list.Add("ShowPZ");
                if (myBox.GetItemChecked(idx++)) list.Add("ShowIH");
                if (myBox.GetItemChecked(idx++)) list.Add("ShowBG");
                if (myBox.GetItemChecked(idx++)) list.Add("ShowIHInvent");
                if (myBox.GetItemChecked(idx++)) list.Add("ShowKST");
                if (myBox.GetItemChecked(idx++)) list.Add("ShowCWYear");
                if (myBox.GetItemChecked(idx++)) list.Add("ShowBedarf");
                if (myBox.GetItemChecked(idx++)) list.Add("ShowRelevant");
                if (myBox.GetItemChecked(idx++)) list.Add("ShowWithHoursOnly");

                return list; 
            } 
        } 

        /// <summary>
        /// Auszuwählende Optionen aus Benutzereinstellungen laden
        /// </summary>
        public void ResetValue()
        {
        
            int idx = 0;
        
            if (Session.Current.User.SeePrices) myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowPrice);
            if (Session.Current.User.SeeHours)  myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowHours);
            myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowPZ);
            myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowIH);
            myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowBG);
            myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowIHInvent);
            myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowKST);
            myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowCWYear);
            myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowBedarf);
            myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowRelevant);
            if (Session.Current.User.SeeHours) myBox.SetItemChecked(idx++, comain.Properties.Settings.Default.ShowWithHoursOnly);
        }
            
        /// <summary>
        /// Ausgewählte Optionen in Benutzereinstellungen speichern
        /// </summary>        
        public void SaveValue()
        {
        
            int idx = 0;
        
            if (Session.Current.User.SeePrices) comain.Properties.Settings.Default.ShowPrice = myBox.GetItemChecked(idx++);
            if (Session.Current.User.SeeHours)  comain.Properties.Settings.Default.ShowHours = myBox.GetItemChecked(idx++);
            comain.Properties.Settings.Default.ShowPZ       = myBox.GetItemChecked(idx++);
            comain.Properties.Settings.Default.ShowIH       = myBox.GetItemChecked(idx++);
            comain.Properties.Settings.Default.ShowBG       = myBox.GetItemChecked(idx++);
            comain.Properties.Settings.Default.ShowIHInvent = myBox.GetItemChecked(idx++);
            comain.Properties.Settings.Default.ShowKST      = myBox.GetItemChecked(idx++);
            comain.Properties.Settings.Default.ShowCWYear   = myBox.GetItemChecked(idx++);
            comain.Properties.Settings.Default.ShowBedarf   = myBox.GetItemChecked(idx++);
            comain.Properties.Settings.Default.ShowRelevant = myBox.GetItemChecked(idx++);
            if (Session.Current.User.SeeHours) comain.Properties.Settings.Default.ShowWithHoursOnly = myBox.GetItemChecked(idx++);
            
            comain.Properties.Settings.Default.Save();
        }


        public string FilterDesc
        {
            get { return ""; }
        }
    }
}

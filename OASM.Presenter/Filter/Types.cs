﻿using System;


namespace comain.Filter
{
    
    public enum PicklistType
    {
    
        Unspecified,
        PZVon,
        PZBis,
        IH,
        IHSub,
        IHInvent,
        KST,
        LA,
        AU,
        Bereich,
        Profil,
        Sort,
        Proj
    }
}

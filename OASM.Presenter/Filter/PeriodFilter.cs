﻿using System;
using CO.Forms;
using System.Collections.Generic;
using comain.Workspace;


namespace comain.Filter
{


    /// <summary>
    /// Zeitraumbezug für Wochenplan
    /// </summary>
    public class PeriodFilter : IFilterFacade
    {

        /// <summary>
        /// Kalendercontrol Zeit von
        /// </summary>
        DTPWithCalWeek von;
        /// <summary>
        /// Kalendercontrol Zeit bis
        /// </summary>
        DTPWithCalWeek bis;


        /// <summary>
        /// Zeitraumbezug für Wochenplan
        /// </summary>
        /// <param name="v">Kalendercontrol Zeit von</param>
        /// <param name="b">Kalendercontrol Zeit bis</param>
        public PeriodFilter(DTPWithCalWeek v, DTPWithCalWeek b)
        {
            
            von = v;
            bis = b;
            
            ResetValue();
        }
        
        /// <summary>
        /// Daten laden == konstant
        /// </summary>
        /// <param name="cnc"></param>
        public void BindValues(PicklistType wtb)
        {
        }

        /// <summary>
        /// gewählten Wert in SQL-BETWEEN-Expression
        /// </summary>
        public List<String> Values 
        { 
            
            get { 
            
                List<String> list = new List<string>();   
                
                list.Add(((DateTime)von.Value).ToShortDateString());
                list.Add(((DateTime)bis.Value).ToShortDateString());

                return list;
            }
        }
        
        /// <summary>
        /// Werte zurücksetzen: letzte 7 Tage
        /// </summary>
        public void  ResetValue()
        {
            von.Value = DateTime.Today.AddDays(-7);
            bis.Value = DateTime.Today;
        }
        
        
        public string FilterDesc
        {
            
            get { 
            
                if (von.Visible && bis.Visible) return von.NullableValue.Value.ToShortDateString() + " - " + bis.NullableValue.Value.ToShortDateString();
                return String.Empty;
            }
        }
        
        
        public void SaveValue() {}
    }
}

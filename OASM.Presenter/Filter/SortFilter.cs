﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;


namespace comain.Filter
{


    /// <summary>
    /// Auswahl Sortiermöglichkeiten
    /// </summary>
    public class SortFilter : IFilterFacade
    {

        /// <summary>
        /// Combolistbox Sortiermethoden
        /// </summary>
        ComboBox box;


        /// <summary>
        /// Auswahl Sortiermöglichkeiten
        /// </summary>
        /// <param name="b">Combolistbox Sortiermethoden</param>
        public SortFilter(ComboBox b)
        {
            box = b;
        }

        /// <summary>
        /// Sortiermöglichkeiten laden == konstant
        /// </summary>
        /// <param name="cnc">Datenbankverbindung</param>
        public void BindValues(PicklistType wtb)
        {               
            ResetValue();
        }

        /// <summary>
        /// gewählte Sortierung als SQL-ORDER-BY Klausel
        /// </summary>
        public List<String> Values
        {
            
            get {
        
                List<String> list = new List<String>();

                switch (box.SelectedIndex) {
                
                case  0: //ORDER BY kstsort,pzkey,i1.ihnr,bereich,aucodeextern,aucode";

                    list.Add("KST");
                    list.Add("PZ");
                    list.Add("IH");
                    list.Add("BEREICH");
                    list.Add("AUCODEEXTERN");
                    list.Add("AUCODE");

                    break;
                        
                case  1: //ORDER BY kstsort,pzkey,i1.ihinventnr,bereich,aucode";

                    list.Add("KST");
                    list.Add("PZ");
                    list.Add("IHINVENT");
                    list.Add("BEREICH");
                    list.Add("AUCODE");

                    break;
                        
                case  2: //ORDER BY pzkey,i1.ihnr,aucode";

                    list.Add("PZ");
                    list.Add("IH");
                    list.Add("AUCODE");

                    break;
                        
                case  3: //ORDER BY i1.ihinventnr,aucode";

                    list.Add("IHINVENT");
                    list.Add("AUCODE");

                    break;
                        
                case  4: //ORDER BY i1.ihnr,aucode";

                    list.Add("IH");
                    list.Add("AUCODE");

                    break;
                        
                case  5: //ORDER BY i1.ihnr DESC,aucode DESC";

                    list.Add("IH DESC");
                    list.Add("AUCODE DESC");

                    break;
                        
                default: //ORDER BY kstsort,pzkey,i1.ihnr,bereich,aucodeextern,aucode";

                    list.Add("KST");
                    list.Add("PZ");
                    list.Add("IH");
                    list.Add("BEREICH");
                    list.Add("AUCODEEXTERN");
                    list.Add("AUCODE");

                    break;                        
                }
                
                return list;
            }
        }

        /// <summary>
        /// Sortierliste zurücksetzen
        /// </summary>
        public void  ResetValue()
        {
            box.SelectedIndex = 0;
        }


        public string FilterDesc
        {
            get {
        
                switch (box.SelectedIndex) {
                
                    case  0: return "Sortiert nach Kostenstelle,Standort,IH-Objekt,Bereich,Externe Auftragsnummer";
                    case  1: return "Sortiert nach Kostenstelle Inventarnummer,Bereich,Auftrag";
                    case  2: return "Sortiert nach Standort,IH-Objekt,Auftrag";
                    case  3: return "Sortiert nach Inventarnummer,Auftrag";
                    case  4: return "Sortiert nach IH-Objekt,Auftrag";
                    case  5: return "Sortiert nach IH-Objekt absteigend,Auftrag absteigend";
                }
                
                return "";                
            }
        }
        
        
        public void SaveValue() {}
    }
}

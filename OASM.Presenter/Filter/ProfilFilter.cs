﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using comain.Schedule;
using comain.Workspace;


namespace comain.Filter
{


    /// <summary>
    /// Profilfilter
    /// </summary>
    public class ProfilFilter : IFilterFacade
    {

        /// <summary>
        /// Combolistbox Profilfilter
        /// </summary>
        ComboBox box1;
        ComboBox box2;
        /// <summary>
        /// Sind Daten geladen?
        /// </summary>
        List<PicklistItem> picklist;

            
        /// <summary>
        /// Profilfilter
        /// </summary>
        /// <remarks>
        /// Benutzer nicht übergeben, da er wechseln kann
        /// </remarks>
        /// <param name="b">Combolistbox Profilfilter</param>
        /// <param name="bEdit">Combolistbox Profil-Bearbeiten-Filter</param>
        public ProfilFilter(ComboBox b, ComboBox bEdit)
        {
            box1 = b;
            box2 = bEdit;
            picklist = null;
        }
        
        /// <summary>
        /// Profildatencontainer laden 
        /// </summary>
        /// <param name="cnc">Datenbankverbindung</param>
        /// <param name="u">Aktueller Benutzer</param>
        public void BindValues(PicklistType wtb)
        {
        
            if (picklist != null) return;
        
            try {
                        
                var proxy = new FacadesProxy();
                picklist = proxy.ProfilList(Session.Current.User.AuthenticateInfo);
                
                box1.DataSource = picklist;
                box1.DisplayMember = "Value1";
                box1.ValueMember = "Id";
                
                box2.DataSource = picklist;
                box2.DisplayMember = "Value1";
                box2.ValueMember = "Id";
            }
            catch (Exception x) { 
            
                new comain.Error(x);
            }
        }

        /// <summary>
        /// Gewähltes Profil als SQL-Substring übergeben 
        /// </summary>
        public List<String> Values
        {
            
            get {
                    
                List<String> list = new List<String>();
                
                if (box1.SelectedValue != null) list.Add(box1.SelectedValue.ToString());                
                return list;
            }
        }

        /// <summary>
        /// Profilfilter zurücksetzen
        /// </summary>
        public void  ResetValue()
        {
            box1.SelectedIndex = -1;
        }


        public string FilterDesc
        {
            get { 
            
                if (box1.SelectedIndex == -1) return String.Empty;
                return "Benutzerprofil " + box1.Text;
            }
        }
        
        
        public void SaveValue() {}
    
        
        public void ClearValues()
        {
        
            picklist = null;
        }
    }
}
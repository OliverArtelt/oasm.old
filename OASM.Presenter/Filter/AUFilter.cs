﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using comain.Schedule;
using comain.Workspace;


namespace comain.Filter
{


    /// <summary>
    /// Auftragsfilter
    /// </summary>
    public class AUFilter : IFilterFacade
    {

        /// <summary>
        /// Combolistbox der Auftragsliste 
        /// </summary>
        ComboBox aubox;
        /// <summary>
        /// Combolistbox der Leistungsarten 
        /// </summary>
        IFilterFacade laFilter;
        /// <summary>
        /// Liste aller Aufträge
        /// </summary>
        List<PicklistItem> picklist; 


        /// <summary>
        /// Auftragsfilter
        /// </summary>
        /// <param name="b">Combolistbox der Auftragsliste</param>
        /// <param name="t">zu befüllender Datencontainer</param>
        public AUFilter(ComboBox aub, IFilterFacade la)
        {
            
            aubox = aub;
            laFilter = la;
            picklist = null;
        }
        
        /// <summary>
        /// Auftrags-Datencontainer befüllen
        /// </summary>
        /// <param name="cnc">Datenbankverbindung</param>
        public void BindValues(PicklistType wtb)
        {
       
            try {
            
                if (picklist == null) {

                    var proxy = new FacadesProxy();
                    picklist = proxy.AuList(Session.Current.User.AuthenticateInfo);
                }
                
                var la = laFilter.Values;
                if (la != null && la.Count > 0) {
                
                    aubox.DataSource = picklist.Where(p => la.Any(q => q == p.Value2)).ToList();
                        
                } else {
                
                    aubox.DataSource = picklist;    
                }    
                
                aubox.DisplayMember = "Value1";
                aubox.ValueMember = "Id";
            }
            catch (Exception x) { new comain.Error(x);}
        }

        /// <summary>
        /// gewählten Auftrag übergeben, "" bei keiner Wahl
        /// </summary>
        public List<String> Values
        {
            
            get {
                    
                List<String> list = new List<String>();
                
                if (aubox.SelectedValue != null) list.Add(aubox.SelectedValue.ToString());
                return list;
            }
        }

        /// <summary>
        /// Auftragsliste zurücksetzen
        /// </summary>
        public void ResetValue()
        {
            aubox.SelectedIndex = -1;
        }


        public string FilterDesc
        {
            get { 
            
                if (aubox.SelectedIndex == -1) return "";
                return "Auftrag " + aubox.Text; 
            }
        }
        
        
        public void SaveValue() {}
    }
}

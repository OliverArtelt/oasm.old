﻿using System;
using System.Collections.Generic;


namespace comain.Filter
{

    /// <summary>
    /// Filterobjekte allgemeingültig verwenden
    /// </summary>
    public interface IFilterFacade
    {             

        /// <summary>
        /// Sicherstellen, das Objekt geladene Daten besitzt
        /// </summary>
        void BindValues(PicklistType whatToBind);
        /// <summary>
        /// gewählten Wert löschen bzw. aus OS-Benutzerprofil lesen
        /// </summary>
        void ResetValue();
        /// <summary>
        /// gewählten Wert in OS-Benutzerprofil speichern
        /// </summary>
        void SaveValue();
        /// <summary>
        /// gewählten Wert für SQLBuilder geben
        /// </summary>
        List<String> Values { get; }
        /// <summary>
        /// gewählten Wert für Berichtskopf geben
        /// </summary>
        String FilterDesc { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using comain.User;
using comain.Schedule;
using comain.Workspace;


namespace comain.Filter
{
    
    
    public class KSTFilter : IFilterFacade
    {

        /// <summary>
        /// Combolistbox der KST-Liste
        /// </summary>
        ComboBox box;
        /// <summary>
        /// Benutzer (dessen KST-Bereich berücksichtigt werden muss)
        /// </summary>
        List<PicklistItem> picklist;

        
        /// <summary>
        /// Kostenstellenfilter
        /// </summary>
        /// <param name="b">Combolistbox der KST-Liste</param>
        /// <param name="t">zu befüllender Datencontainer</param>
        public KSTFilter(ComboBox b)
        {
            
            box = b;
            picklist = null;
        }
        
        /// <summary>
        /// Kostenstellenquelle befüllen, KST-Filter des Meisters berücksichtigen
        /// </summary>
        public void BindValues(PicklistType wtb)
        {
        
            if (picklist != null) return;
            
            try {

                var proxy = new FacadesProxy();
                picklist = proxy.KstList(Session.Current.User.AuthenticateInfo);
                box.DataSource = picklist;
                box.DisplayMember = "Value1";
                box.ValueMember = "Id";
            }
            catch (Exception x) { 
            
                new comain.Error(x);
            }
        }

        /// <summary>
        /// gewählte Kostenstelle geben("" bei keiner Auswahl)
        /// </summary>
        public List<String> Values
        {
            
            get {
                    
                List<String> list = new List<String>();
                
                if (box.SelectedValue != null) list.Add(box.SelectedValue.ToString());
                return list;
            }
        }

        /// <summary>
        /// Kostenstellenliste zurücksetzen
        /// </summary>
        public void  ResetValue()
        {
            box.SelectedIndex = -1;
        }


        public string FilterDesc
        {
            get { 
            
                if (box.SelectedIndex == -1) return "";
                return "Kostenstelle " + box.Text; 
            }
        }
        
        
        public void SaveValue() {}
    }
}

﻿using System;
using System.Collections.Generic;
using comain.Calendar;
using comain.Schedule;
using comain.User;
using comain.Command;


namespace comain.Workspace
{
    
    public sealed class Session
    {
    
        static Session mySession;
        
        /// <summary>
        /// angemeldeter Benutzer
        /// </summary>
        public comain.User.IUser User { get; set; }         
        /// <summary>
        /// nicht aktuelles Kalenderjahr, sondern Jahr gewählter Planung 
        /// </summary>
        public int CurrentYear { get; set; } 
        /// <summary>
        /// temporären Planungsnamen geben, Planungstyp gewählt aber Plan noch nocht erstellt
        /// </summary>
        public String SelectedScheduleName { get; set; } 
        /// <summary>
        /// aktuellen Plan geben
        /// </summary>
        public Root Schedule { get; set; } 
        /// <summary>
        /// ausgewählter Plan, Plan selber muss noch nicht existieren, nur Typ gewählt
        /// </summary>
        public ScheduleType ScheduleType { get; set; } 
        /// <summary>
        /// aktuelle DB/Mandant
        /// </summary>
        public String Database { get { return User.AuthenticateInfo.Client; } }
        /// <summary>
        /// auf Planung ausgeführte/verworfene Befehle
        /// </summary>
        public CommandHistory CommandHistory { get; set; }
        /// <summary>
        /// aktueller Planungstyp im Werkzeugkasten (Planen, Sonderleistungen ...)
        /// </summary>
        public EditedType SelectedEditType { get; set; }
        /// <summary>
        /// aktuelle Planungsanzahl im Werkzeugkasten (Planen, Sonderleistungen ...)
        /// </summary>
        public short SelectedEditAmount { get; set; }
        /// <summary>
        /// erlaubte Aktionen des Benutzers (planen, löschen, implementieren ...)
        /// </summary>
        public IUserAction UserActions { get; set; }
        /// <summary>
        /// Planungskonfiguration
        /// </summary>
        public Configuration OASMSettings { get; set; }
        /// <summary>
        /// Sind alle Aufträge ausgewählt (alle an-/abwählen)
        /// </summary>
        public bool OrdersSelectedState { get; set; }
        /// <summary>
        /// Aktuell gewählte Zeile 
        /// </summary>
        public int CurrentPosition { get; set; }
        /// <summary>
        /// aktuell gewählte (bearbeitete) Zeile
        /// </summary>
        public int ScrollXPosition { get; set; }
        /// <summary>
        /// Beim Scrollen Überschriften nicht zeichnen (Flackern)
        /// </summary>
        public bool IsScrolledHorizontally { get; set; }
        /// <summary>
        /// Betrachtungszeitraum Start
        /// </summary>
        public int DeltaVon { get; set; }
        /// <summary>
        /// Betrachtungszeitraum Ende
        /// </summary>
        public int DeltaBis { get; set; }

    
        static Session() 
        {  
            mySession = new Session(); 
            ResetNewUser();   
        }
        
        /// <summary>
        /// Sessionobjekte geben (aktueller Benutzer, Plan, Konfiguration...)
        /// </summary>
        public static Session Current 
        {
            get {
            
                return mySession;
            }
        }
        
        /// <summary>
        /// neuer Benutzer angemeldet: Sitzung zurücksetzen
        /// </summary>
        public static void ResetNewUser()
        {
                  
            mySession.Schedule = null;
            mySession.OASMSettings = null;
            mySession.ScheduleType = ScheduleType.None;
            mySession.User = null;
            mySession.UserActions = null;
            mySession.CurrentYear = 0;
            mySession.SelectedScheduleName = String.Empty;
            mySession.SelectedEditType = EditedType.Empty;
            mySession.SelectedEditAmount = 1;
            mySession.CurrentPosition = -1;

            ResetNewSchedule();
        }
        
        /// <summary>
        /// neue Planung erzeugt: Sitzung teilweise zurücksetzen
        /// </summary>
        public static void ResetNewSchedule()
        {
        
            mySession.CommandHistory = null;
            mySession.OrdersSelectedState = false;
            mySession.CurrentPosition = -1;
        }
        
        /// <summary>
        /// Anwender am System anmelden
        /// </summary>
        /// <param name="authinfo">Anmeldeinformationen</param>
        public void ReadUser(Credentials authinfo)
        {
        
            User = null;
            var proxy = new UserProxy();
            GrantedUser u = proxy.Create(authinfo);

            if (u.IsClientUser) User = new ClientUser(u, authinfo);
            else                User = new ServiceUser(u, authinfo);
        }
        
        /// <summary>
        /// Konfiguration für die Sitzung auslesen
        /// </summary>
        public void ReadSettings()
        {
        
            OASMSettings = null;
            var proxy = new ConfigurationProxy();
            OASMSettings = proxy.ReadConfig(User.AuthenticateInfo);
        }
        
        /// <summary>
        /// Plan erstellen
        /// </summary>
        /// <param name="filterValues">Filterinformationen des Planes</param>
        /// <param name="filterDescription">Filterbeschreibung für Export etc</param>
        public void CreateSchedule(Dictionary<String, List<String>> filterValues, String filterDescription)
        {
        
            Schedule = null;
            if (Session.Current.ScheduleType != comain.Schedule.ScheduleType.Week) filterValues["Period"].Clear();

            Traits traits = new Traits(Session.Current.ScheduleType, Session.Current.OASMSettings, Session.Current.User, 
                                       Session.Current.CurrentYear, filterValues, filterDescription); 
            
            var proxy = new PlanProxy();
            ScheduleDTO dto = proxy.GetPlan(Session.Current.User.AuthenticateInfo, traits.FilterValues);
            ScheduleDirector dir = new ScheduleDirector(new ScheduleBuilder(traits));
            Schedule = dir.Construct(dto);
        }
        
        /// <summary>
        /// Anzahl geänderter Termine dieser Sitzung zählen
        /// </summary>
        /// <returns></returns>
        public int CountModifications()
        {
        
            ModifiedDateCounter counter = new ModifiedDateCounter();
            Schedule.Accept(counter);
            
            return counter.ChangesCount;
        }
        
        /// <summary>
        /// Planänderungen in Storage zurückschreiben
        /// </summary>
        public void SaveModifications()
        {
        
            ModifiedDateList list = new ModifiedDateList(Schedule.Traits.Calendar);
            Schedule.Accept(list);
        
            var proxy = new PlanProxy();
            proxy.UpdatePlan(Session.Current.User.AuthenticateInfo, list.Modified);
        }
    }
}

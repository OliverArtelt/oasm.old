﻿using System;
using System.Linq;
using System.Collections.Generic;
using comain.User;


namespace comain.Workspace
{
    
    public class DBListFactory
    {
    
        public List<String> List { get; set; }
        
        
        public DBListFactory()
        {
        
            var proxy = new UserProxy();
            List = proxy.Clients().ToList();          
        }
    }
}

﻿using System;
using comain.Calendar;
using System.Windows.Forms;


namespace comain.Workspace
{
    
    public static class BaseConfigAdapter
    {
           
        public static BaseConfiguration LoadBase()
        {
                    
            var proxy = new ConfigurationProxy();
            return proxy.ReadBaseConfig(Session.Current.User.AuthenticateInfo);          
        }
           
           
        public static void SaveBase(BaseConfiguration cfg)
        {
                    
            var proxy = new ConfigurationProxy();
            ValidateBaseConfiguration(cfg);
            proxy.WriteBaseConfig(Session.Current.User.AuthenticateInfo, cfg);          
                
            Session.Current.ReadSettings();
        }
           
           
        public static BaseExportConfiguration LoadExport()
        {
                    
            var proxy = new ConfigurationProxy();
            return proxy.ReadBaseExportConfig(Session.Current.User.AuthenticateInfo);          
        }
          
           
        public static void SaveExport(BaseExportConfiguration cfg)
        {
                    
            var proxy = new ConfigurationProxy();
            proxy.WriteBaseExportConfig(Session.Current.User.AuthenticateInfo, cfg);          

            Session.Current.ReadSettings();
        }
        
        /// <summary>
        /// Konsistenzprüfung
        /// </summary>
        private static void ValidateBaseConfiguration(BaseConfiguration cfg)
        {
           
            if (cfg.OperType == BaseConfiguration.UpdateMode.Inactive) return;
            if (cfg.GreyType == BaseConfiguration.UpdateMode.Inactive) return;
            
            DateTime von = (cfg.GreyType == BaseConfiguration.UpdateMode.FixDate)? cfg.GreyDate.Value: 
                            DateTime.Today.AddDays(-7 * cfg.GreyWeeks.Value).MondayOf(Application.CurrentCulture);
            DateTime bis = (cfg.OperType == BaseConfiguration.UpdateMode.FixDate)? cfg.OperDate.Value: 
                            DateTime.Today.AddDays(7 * cfg.OperWeeks.Value).MondayOf(Application.CurrentCulture);
                            
            if (von > bis) throw new ApplicationException("Der Grauzonentermin muß vor der Operativen Planung liegen.");                


            //Operative Planung
            if (cfg.OperType == BaseConfiguration.UpdateMode.FixDate) {
            
                if (!cfg.OperDate.HasValue) throw new ApplicationException("Kein Datum für Operative Planung angegeben.");
            }
            
            if (cfg.OperType == BaseConfiguration.UpdateMode.Dynamic) {
            
                if (!cfg.OperWeeks.HasValue) throw new ApplicationException("Keine Wochenanzahl für Operative Planung angegeben.");
            }
            
            //Grauzonenplanung
            if (cfg.GreyType == BaseConfiguration.UpdateMode.FixDate) {
            
                if (!cfg.GreyDate.HasValue) throw new ApplicationException("Kein Datum für Grauzonenplanung angegeben.");
            }
            
            if (cfg.GreyType == BaseConfiguration.UpdateMode.Dynamic) {
            
                if (!cfg.GreyWeeks.HasValue) throw new ApplicationException("Keine Wochenanzahl für Grauzonenplanung angegeben.");
            } 
        }
    }
}

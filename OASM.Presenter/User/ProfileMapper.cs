﻿using System;
using System.Collections.Generic;
using comain.Schedule;
using comain.Workspace;
using comain.Filter;


namespace comain.User
{
    
    public class ProfileMapper
    {
    
        List<int> selectedOrders;
        
        
        public ProfileMapper()
        {
        
            OrderCollector c = new OrderCollector(true);
            Session.Current.Schedule.Accept(c);
            
            selectedOrders = c.CollectedOrders;
        }
        
        
        public void Create(String profile)
        {
        
            if (profile == null) throw new ApplicationException("Geben Sie den Namen des Profiles an.");
            
            var proxy = new ProfileProxy();
            proxy.Create(Session.Current.User.AuthenticateInfo, profile, selectedOrders);         
        }
        
        
        public void Add(PicklistItem profile)
        {
        
            if (profile == null) throw new ApplicationException("Wählen Sie ein Profil.");
            
            var proxy = new ProfileProxy();
            proxy.Add(Session.Current.User.AuthenticateInfo, profile.Id, selectedOrders);        
        }
        
        
        public void Delete(PicklistItem profile)
        {
        
            if (profile == null) throw new ApplicationException("Wählen Sie ein Profil.");
            
            var proxy = new ProfileProxy();
            proxy.Delete(Session.Current.User.AuthenticateInfo, profile.Id);       
        }
        
        
        public void Replace(PicklistItem profile)
        {
        
            if (profile == null) throw new ApplicationException("Wählen Sie ein Profil.");
            
            var proxy = new ProfileProxy();
            proxy.Replace(Session.Current.User.AuthenticateInfo, profile.Id, selectedOrders);       
        }
    }
}

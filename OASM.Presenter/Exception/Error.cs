﻿using System;


namespace comain
{
    
    /// <summary>
    /// Fehler protokollieren und anzeigen: anwendungsspezifische Fehlermeldungen
    /// </summary>
    public class Error : CO.ErrorLog 
    {
    
        /// <summary>
        /// Fehler protokollieren und anzeigen
        /// </summary>
        /// <param name="e">erzeugte Ausnahme</param>
        public Error(Exception e) : base(e) {}
        
        /// <summary>
        /// benutzerfreundliche Meldungen erzeugen
        /// </summary>
        /// <returns>ersetzte Meldung</returns>
        protected override String doTranslateMessage()
        {
        
            String Msg = myException.Message;
            
            if (Msg.Contains("Testwurf"))                          return "Fehler generiert zum Testen."; 
            if (Msg.Contains("does not allow nulls"))              return "Das Feld darf nicht leer sein."; 
            if (Msg.Contains("argument was out of the range"))     return "Das Argument besitzt einen ungültigen Wert."; 
            if (Msg.Contains("is constrained to be unique"))       return "Die Werte müssen eindeutig sein."; 
            if (Msg.Contains("is read only"))                      return "Der Wert darf nur gelesen werden."; 
            if (Msg.Contains("is DBNull"))                         return "Der Wert darf nicht leer sein."; 
            if (Msg.Contains("deleting this row will strand"))     return "Der Datensatz wird von anderen Daten benötigt."; 
            if (Msg.Contains("was out of the range"))              return "Dieser Wert befindet sich nicht im gültigen Bereich."; 
            if (Msg.Contains("Connection must be valid and open")) return "Keine Verbindung zur Datenbank möglich."; 
            if (Msg.Contains("Failed to enable constraints"))      return "Die Daten sind ungültig."; 
            if (Msg.Contains("Unable to connect to any"))          return "Keine Verbindung zur Datenbank möglich."; 
            if (Msg.Contains("3D000"))                             return "Die Datenbank ist auf dem Server nicht vorhanden."; 
            if (Msg.Contains("42P01"))                             return "Die Datenbank ist für den Betrieb mit dieser Anwendung nicht eingerichtet."; 
            if (Msg.Contains("establish a connection"))            return "Keine Verbindung zur Datenbank möglich."; 
            if (Msg.Contains("duplicate key violates"))            return "Dieser Wert wird bereits verwendet. Wählen Sie einen anderen."; 
            if (Msg.Contains("nput string was not in a correc"))   return "Der angegebene Wert ist ungültig."; 
            if (Msg.Contains("process cannot access the file "))   return "Die Datei kann nicht geschrieben werden, da sie verwendet wird.";
            if (Msg.Contains("chk_kstrole_range"))                 return "Der KSTBereich \"von\" darf nicht größer als \"bis\" sein.";
            if (Msg.Contains("\"kstvon\" violates not-null"))      return "Der KSTBereich \"von\" muß entweder angegeben oder die gesamte Zeile muß entfernt werden.";
            if (Msg.Contains("interner Fehler ist aufgetreten"))   return "Ein interner Fehler ist aufgetreten. Wenden Sie sich an den Administrator wenn das Problem weiterhin bestehen bleibt.";
             
            return Msg;
        }
    }
}

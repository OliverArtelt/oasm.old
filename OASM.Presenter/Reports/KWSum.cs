﻿using comain.Schedule;
using comain.Workspace;
using comain.Statistics;
using comain.Calendar;


namespace comain.Reports
{

    public partial class KWSum
    {

        public void Fill()
        {

            StatsCollector coll = new StatsCollector(Session.Current.Schedule.Traits,0,0);
            Session.Current.Schedule.Accept(coll);
            IStatistics stats = coll.CollectedStats;
            ICalendarStrategy cal = Session.Current.Schedule.Traits.Calendar;

            int greyVon = 0;
            int greyBis = 0;

            if(Session.Current.OASMSettings.UseGreyArea)
            {

                greyVon = cal.CalculateCalendarKey(Session.Current.OASMSettings.GreyDate);
                greyBis = cal.CalculateCalendarKey(Session.Current.OASMSettings.OperDate);
            }

            foreach(int key in Session.Current.Schedule.Traits.Calendar.CalendarKeys)
            {

                KWSum.WeeksRow row = (KWSum.WeeksRow)Weeks.NewRow();
                row.kw = cal.FormatKeyLong(key);
                row.period = cal.CalculateFirstDate(key).ToShortDateString();
                row.impl = stats.WeekImplemented(key);
                row.gs = stats.WeekGreySum(key);
                row.used = stats.WeekBudgetUsed(key);
                row.remains = stats.WeekRemains(key);
                row.total = stats.WeekBudgetTotal(key);
                row.special = stats.WeekSpecial(key);
                row.grey = key >= greyVon && key < greyBis;

                Weeks.Rows.Add(row);
            }
        }
    }
}

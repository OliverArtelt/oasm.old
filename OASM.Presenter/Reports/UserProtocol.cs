﻿using System;
using comain.User;
using comain.Workspace;


namespace comain.Reports
{
    
    public class UserProtocol
    {
        
        public Protocol DataSet { get; private set; }
        
        
        /// <summary>
        /// Protokollbericht holen
        /// </summary>
        /// <param name="type">Filter Ereignistyp</param>
        /// <param name="logVon">Filter Eintrag von</param>
        /// <param name="logBis">Filter Eintrag bis</param>
        /// <param name="datVon">Filter Datum Termine/Realisierungen von</param>
        /// <param name="datBis">Filter Datum Termine/Realisierungen bis</param>
        /// <param name="user">Filter Benutzer</param>
        /// <param name="auCode">Filter Auftrag</param>
        public UserProtocol(int type, DateTime? logVon, DateTime? logBis, DateTime? datVon, DateTime? datBis,
                            String user, String auCode)
        {
        
            var proxy = new ProtocolProxy();
            DataSet = proxy.Get(Session.Current.User.AuthenticateInfo, type, logVon, logBis, datVon, datBis, user, auCode);   
        }
        
        /// <summary>
        /// Protokolleinträge löschen
        /// </summary>
        /// <param name="von"></param>
        /// <param name="bis"></param>
        /// <returns>Anzahl gelöschter Einträge</returns>
        public static int DeleteEntries(DateTime von, DateTime bis)
        {
        
            var proxy = new ProtocolProxy();
            return proxy.DeleteEntries(Session.Current.User.AuthenticateInfo, von, bis);   
        }
    }
}

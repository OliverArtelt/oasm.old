﻿using System;
using System.Drawing;
using CO.OpenXML.XLSX.ExcelStyles;


namespace comain.Excel
{

    
    internal class WeekStyle : IStyleFactory
    {
        
        Style head      = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(12, Color.Black, true) };
        Style parm      = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(10, Color.Black, true) };
        Style ps        = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(10, Color.Black, true),
                                      Color = new BackColor(Color.Silver), 
                                      Align = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        Style pz        = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(10, Color.Black, true),
                                      Color = new BackColor(Color.Gainsboro), 
                                      Align = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        Style ih        = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(10, Color.Black, true),
                                      Align = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        Style auKopfL   = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8, Color.Black, true),
                                      Border = new Border(BorderType.Thin), Align = new Alignment(AlignTypes.Left),
                                      Color = new BackColor(Color.LightCyan) };
        Style auKopfC   = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8, Color.Black, true),
                                      Border = new Border(BorderType.Thin), Align = new Alignment(AlignTypes.Center),
                                      Color = new BackColor(Color.LightCyan) };
        Style auKopfR   = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8, Color.Black, true),
                                      Border = new Border(BorderType.Thin), Align = new Alignment(AlignTypes.Right),
                                      Color = new BackColor(Color.LightCyan) };
        Style auKopfCW  = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8),
                                      Border = new Border(BorderType.Thin), 
                                      Align = new Alignment(AlignTypes.Center, AlignTypes.Top, 90),
                                      Color = new BackColor(Color.LightCyan) };
        Style auKopfDelta = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8),
                                      Border = new Border(BorderType.Thin), 
                                      Align = new Alignment(AlignTypes.Center, AlignTypes.Top, 90),
                                      Color = new BackColor(Color.Orange) };
        Style auCode    = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8), 
                                      Border = new Border(BorderType.Thin), 
                                      Align = new Alignment(AlignTypes.Left, AlignTypes.Center) };
        Style auKurzB   = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8), 
                                      Border = new Border(BorderType.Thin), 
                                      Align = new Alignment(AlignTypes.Left, true) };
        Style auText    = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8), 
                                      Border = new Border(BorderType.Thin), 
                                      Align = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        Style auNumeric = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8), 
                                      Border = new Border(BorderType.Thin), 
                                      Align = new Alignment(AlignTypes.Right, AlignTypes.Center) };
        Style auMoney   = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8), 
                                      Border = new Border(BorderType.Thin), 
                                      Align = new Alignment(AlignTypes.Right, AlignTypes.Center),
                                      Format = new NumberFormat(Format.Money) };
        Style datum     = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8, Color.Black, true), 
                                      Border = new Border(BorderType.Thin), 
                                      Align = new Alignment(AlignTypes.Center, AlignTypes.Center) };
        Style sumMoney  = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8) };
        Style footText  = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8) };
        Style footValue = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8, Color.Black, true),
                                      Align = new Alignment(AlignTypes.Right) };
        Style grKey     = new Style { Font = new CO.OpenXML.XLSX.ExcelStyles.FontStyle(8) };

        
#region G E T T E R
        
        public Style Head
        {
            get { return head; }
        }

        public Style Params
        {
            get { return parm; }
        }

        public Style Bearbeiter
        {
            get { return ps; }
        }

        public Style Standort
        {
            get { return pz; }
        }

        public Style IHObjekt
        {
            get { return ih; }
        }

        public Style AuKopfL
        {
            get { return auKopfL; }
        }

        public Style AuKopfC
        {
            get { return auKopfC; }
        }

        public Style AuKopfR
        {
            get { return auKopfR; }
        }

        public Style AuKopfCW
        {
            get { return auKopfCW; }
        }

        public Style AuKopfDelta
        {
            get { return auKopfDelta; }
        }

        public Style AuCode
        {
            get { return auCode; }
        }

        public Style AuKurzB
        {
            get { return auKurzB; }
        }

        public Style AuText
        {
            get { return auText; }
        }

        public Style AuNumeric
        {
            get { return auNumeric; }
        }

        public Style AuMoney
        {
            get { return auMoney; }
        }

        public Style Datum
        {
            get { return datum; }
        }

        public Style SumMoney
        {
            get { return sumMoney; }
        }

        public Style FootText
        {
            get { return footText; }
        }

        public Style FootValue
        {
            get { return footValue; }
        }

        public Style GreaterKey
        {
            get { return grKey; }
        }

#endregion

    }
}

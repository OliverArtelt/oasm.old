﻿using System;
using CO.OpenXML.XLSX.ExcelStyles;


namespace comain.Excel
{
    
    interface IStyleFactory
    {

        /// <summary>
        /// Kopfzeile Bericht
        /// </summary>
        Style Head { get; }
        /// <summary>
        /// Berichtsangaben (Periode, Fuß)
        /// </summary>
        Style Params { get; }
        /// <summary>
        /// Personalbezeichnung
        /// </summary>
        Style Bearbeiter { get; }
        /// <summary>
        /// Standortbezeichnung
        /// </summary>
        Style Standort { get; }
        /// <summary>
        /// IHO/Main-IHO-Bezeichnung
        /// </summary>
        Style IHObjekt { get; }
        /// <summary>
        /// Auftragskopfzeile linksbündig
        /// </summary>
        Style AuKopfL { get; }
        /// <summary>
        /// Auftragskopfzeile zentriert
        /// </summary>
        Style AuKopfC { get; }
        /// <summary>
        /// Auftragskopfzeile rechtsbündig
        /// </summary>
        Style AuKopfR { get; }
        /// <summary>
        /// Auftragskopfzeile Kalenderwoche
        /// </summary>
        Style AuKopfCW { get; }
        /// <summary>
        /// Auftragskopfzeile Betrachtungszeitraum
        /// </summary>
        Style AuKopfDelta { get; }
        /// <summary>
        /// Auftragsnummer
        /// </summary>
        Style AuCode { get; }
        /// <summary>
        /// Auftragskurzbeschreibung
        /// </summary>
        Style AuKurzB { get; }
        /// <summary>
        /// Auftragseigenschaft plain text
        /// </summary>
        Style AuText { get; }
        /// <summary>
        /// Auftragseigenschaft als Zahl
        /// </summary>
        Style AuNumeric { get; }
        /// <summary>
        /// Auftragseigenschaft als Währungsbetrag
        /// </summary>
        Style AuMoney { get; }
        /// <summary>
        /// Datum, allgemein
        /// </summary>
        Style Datum { get; }
        /// <summary>
        /// Währungsbetrag Summe, allgemein
        /// </summary>
        Style SumMoney { get; }
        /// <summary>
        /// Fußzeile/Summen allgemeiner Text
        /// </summary>
        Style FootText { get; }
        /// <summary>
        /// Fußzeile Wert (als Text darstellen/selber formattieren)
        /// </summary>
        Style FootValue { get; }
        /// <summary>
        /// Tagesberichte haben KW-Nummer über den Montag
        /// </summary>
        Style GreaterKey { get; }
    }
}

﻿using comain.Schedule;


namespace comain.Excel
{
    
    /// <summary>
    /// Anzahl Zeilen im Schedule zählen
    /// </summary>
    class RowCounter : IScheduleVisitor
    {
        
        /// <summary>
        /// Anzahl Zeilen im Schedule
        /// </summary>
        public int Rows { get; private set; }
        
        
        public void Visit(Root obj)
        {
            Rows = 6;   //Kopfzeilen
        }

        public void Visit(Bearbeiter obj)
        {
            Rows++;
        }

        public void Visit(Standort obj)
        {
            Rows++;
        }

        public void Visit(IHObjekt obj)
        {
            Rows += 2;  //zusätzlich Auftragskopf
        }

        public void Visit(Auftrag obj)
        {
            Rows++;     //Auftrag + Termine auf einer Zeile
        }

        public void Visit(Termin obj)
        {
        }
    }
}

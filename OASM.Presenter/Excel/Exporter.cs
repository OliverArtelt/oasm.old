﻿using System;
using System.Data;
using System.Data.Common;
using CO.OpenXML.XLSX;
using comain.Schedule;
using comain.Workspace;


namespace comain.Excel
{
    
    /// <summary>
    /// Hauptexportklasse für Schedule
    /// </summary>
    public class Exporter
    {
    
        String myPath;
        String myTemplate;
        int myFirstOutputRow;
        IStyleFactory myStyles;
        
        Root myPlan;
        GridColumns myColumns;
        
        
        /// <summary>
        /// Hauptexportklasse
        /// </summary>
        /// <param name="path">zu verwendender Dateipfad</param>
        /// <param name="plan">zu schreibendes Schedule</param>
        public Exporter(String path, Root plan)
        {
        
            myPath = path;
            myPlan = plan;
            myColumns = myPlan.Traits.Columns;
            
            bool isWeekly = plan.Traits.Type == ScheduleType.Week;
            myTemplate = (isWeekly)? Session.Current.OASMSettings.WIWeekTemplatePath: Session.Current.OASMSettings.WIYearTemplatePath;
            myFirstOutputRow = (isWeekly)? Session.Current.OASMSettings.WIWeekTemplateRow: Session.Current.OASMSettings.WIYearTemplateRow;
            if (myFirstOutputRow < 1) myFirstOutputRow = 1;
            
            if (isWeekly) myStyles = new WeekStyle();
            else          myStyles = new YearStyle();
        }
        
        /// <summary>
        /// Excel-Export schreiben
        /// </summary>
        public void Write()
        {
        
            using (WorkbookWriter bw = new WorkbookWriter(myPath, myTemplate, myFirstOutputRow)) {   
           
                WorksheetWriter sw = bw.OpenSheet(1);
                sw.SetName(myPlan.Traits.ShortName);
           
                CellDataWriter cdw = new CellDataWriter(sw, myPlan.Traits, myStyles, Session.Current.DeltaVon, Session.Current.DeltaBis);
                myPlan.Accept(cdw);
                cdw.WriteStatistics();
                
                sw.Save();
                bw.WriteDocument();
            }
        }
    }
}

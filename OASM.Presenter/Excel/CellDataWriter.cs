﻿using System;
using System.Collections.Generic;
using CO.OpenXML.XLSX;
using CO.OpenXML.XLSX.ExcelStyles;
using comain.Calendar;
using comain.Schedule;
using comain.Statistics;


namespace comain.Excel
{
    
    /// <summary>
    /// Excel-Tabelle (Worksheet-Inhalt) schreiben
    /// </summary>
    class CellDataWriter : IScheduleVisitor
    {
        
        private Traits myTraits;
        private GridColumns myColmns;
        private IStatistics myStats;
        private WorksheetWriter myWriter;
        private IStyleFactory myStyles;
        private int currentRow = 1;
        /// <summary>
        /// Überschriften: Hintergrund als "Blocksatz"
        /// </summary>
        private int colCount;
        private String currentMainIHO;
        private int deltaVon;
        private int deltaBis;
 
         
        /// <summary>
        /// Worksheet-Inhalt schreiben
        /// </summary>
        /// <param name="w">Tabellenschreiber</param>
        /// <param name="t">Planeigenschaften</param>
        /// <param name="c">anzuzeigende Spalten</param>
        /// <param name="s">Excel-Zellstilfabrik</param>
        public CellDataWriter(WorksheetWriter w, Traits t, IStyleFactory s, int deltaVon, int deltaBis)
        {
        
            myWriter = w;
            myTraits = t;
            myColmns = t.Columns;
            myStyles = s;
            this.deltaVon = deltaVon;
            this.deltaBis = deltaBis;

            colCount = myColmns.Row.Length - 2 + myTraits.Calendar.CalendarKeys.Count;
            
            bool prices = t.Projection[ScheduleParts.ShowPrice];
            bool hours = t.Projection[ScheduleParts.ShowHours];
            myStats = StatsFactory.CreateStatsObject(t.Calendar, t.User, t.OASMSettings, prices, hours, deltaVon, deltaBis); 
            
            int ndx = 0;
            List<ColumnStyle> list = new List<ColumnStyle>();
            list.Add(new ColumnStyle(++ndx, 12F  * 1.416F));    //AuCode    
            list.Add(new ColumnStyle(++ndx, 30F  * 1.416F));    //AukurzB    
            list.Add(new ColumnStyle(++ndx, 3F   * 1.416F));    //Turnus    
            list.Add(new ColumnStyle(++ndx, 3.5F * 1.416F));    //Anzahl    
            if (myColmns.KSTIndex != -1)                        list.Add(new ColumnStyle(++ndx, 4.5F  * 1.416F));    
            if (myColmns.EStundenIndex != -1)                   list.Add(new ColumnStyle(++ndx, 6F    * 1.416F));    
            if (myColmns.EPreisIndex != -1)                     list.Add(new ColumnStyle(++ndx, 7.67F * 1.416F));
            
            list.Add(new ColumnStyle(++ndx, 3.5F * 1.416F));    //DAnzahl
            if (myColmns.DStundenIndex != -1)                   list.Add(new ColumnStyle(++ndx, 6F    * 1.416F));    
            if (myColmns.DPreisIndex != -1)                     list.Add(new ColumnStyle(++ndx, 7.67F * 1.416F));
            
            foreach (var e in myTraits.Calendar.CalendarKeys)   list.Add(new ColumnStyle(++ndx, 2F    * 1.416F));
            if (myColmns.GStundenIndex != -1)                   list.Add(new ColumnStyle(++ndx, 6F    * 1.416F));    
            if (myColmns.VerbrauchIndex != -1)                  list.Add(new ColumnStyle(++ndx, 9.33F * 1.416F));    
            if (myColmns.GPreisIndex != -1)                     list.Add(new ColumnStyle(++ndx, 9.33F * 1.416F));
            
            w.SetColumnWidth(list);              
        }

        /// <summary>
        /// Plankopf schreiben
        /// </summary>
        /// <param name="obj"></param>
        public void Visit(Root obj)
        {
        
            myWriter.SetValue(myTraits.Client, currentRow++, 2, myStyles.Head);  
            myWriter.SetValue(myTraits.Name, currentRow++, 2, myStyles.Head);    
            
            myWriter.SetValue("Erstellt", currentRow, 1, myStyles.Params);                
            myWriter.SetValue(DateTime.Now.ToString(), currentRow++, 2, myStyles.Params); 
            myWriter.SetValue("Benutzer", currentRow, 1, myStyles.Params);              
            myWriter.SetValue(myTraits.User.Name, currentRow++, 2, myStyles.Params);  
            
            if (!String.IsNullOrEmpty(myTraits.FilterDescription)) {
            
                myWriter.SetValue("Parameter", currentRow, 1, myStyles.Params);      
                myWriter.SetValue(myTraits.FilterDescription, currentRow, 2, myStyles.Params);  
            } 
          
            currentRow += 2;
        }

        /// <summary>
        /// Bearbeiterzeile schreiben
        /// </summary>
        public void Visit(Bearbeiter obj)
        {
          
            myWriter.SetRowHeight(currentRow, 30);
            myWriter.SetValue("Bearbeiter", currentRow, 1, myStyles.Bearbeiter);   
            myWriter.SetValue(obj.Name, currentRow, 2, myStyles.Bearbeiter);
            
            for (int i = 3; i <= colCount; i++) myWriter.SetValue(String.Empty, currentRow, i, myStyles.Bearbeiter);  
            
            currentRow++;
        }

        /// <summary>
        /// Standortzeile schreiben
        /// </summary>
        public void Visit(Standort obj)
        {
          
            myWriter.SetRowHeight(currentRow, 26);
            myWriter.SetValue("Standort", currentRow, 1, myStyles.Standort);     
            myWriter.SetValue(obj.Name, currentRow, 2, myStyles.Standort);     
            
            for (int i = 3; i <= colCount; i++) myWriter.SetValue(String.Empty, currentRow, i, myStyles.Standort);  
            
            currentRow++;
        }

        /// <summary>
        /// IH-Objektzeile und Auftragskopfzeile schreiben
        /// </summary>
        public void Visit(IHObjekt obj)
        {
          
            if (obj.IstBaugruppe && currentMainIHO != obj.MainName) {
            
                myWriter.SetRowHeight(currentRow, 24);
                myWriter.SetValue("IH-Hauptobjekt", currentRow, 1, myStyles.IHObjekt);  
                myWriter.SetValue(obj.MainName, currentRow, 2, myStyles.IHObjekt);   
            
                currentMainIHO = obj.MainName;
                currentRow++;
            }

            myWriter.SetRowHeight(currentRow, 24);
            myWriter.SetValue("IH-Objekt", currentRow, 1, myStyles.IHObjekt);  
            myWriter.SetValue(obj.Name, currentRow, 2, myStyles.IHObjekt);   
            
            currentRow++;

            int currentCol = 1;

            myWriter.SetValue(String.Empty, currentRow, currentCol++, myStyles.AuKopfL);
            myWriter.SetValue("Beschreibung", currentRow, currentCol++, myStyles.AuKopfL);
            myWriter.SetValue("Tur", currentRow, currentCol++, myStyles.AuKopfC);
            myWriter.SetValue("Anz", currentRow, currentCol++, myStyles.AuKopfR);
            if (myColmns.KSTIndex != -1) myWriter.SetValue("KSt", currentRow, currentCol++, myStyles.AuKopfC);
            if (myColmns.EStundenIndex != -1) myWriter.SetValue("E-Std.", currentRow, currentCol++, myStyles.AuKopfR);
            if (myColmns.EPreisIndex != -1) myWriter.SetValue("E-Wert", currentRow, currentCol++, myStyles.AuKopfR);
            
            myWriter.SetValue("Δ Anz", currentRow, currentCol++, myStyles.AuKopfR);
            if (myColmns.DStundenIndex != -1) myWriter.SetValue("Δ Std.", currentRow, currentCol++, myStyles.AuKopfR);
            if (myColmns.DPreisIndex != -1) myWriter.SetValue("Δ Wert", currentRow, currentCol++, myStyles.AuKopfR);
           
            foreach (int key in myTraits.Calendar.CalendarKeys) {

                Style st = (key >= deltaVon && key <= deltaBis)? myStyles.AuKopfDelta: myStyles.AuKopfCW; 
                myWriter.SetValue(myTraits.Calendar.FormatKeyShort(key), currentRow, currentCol, st);
                
                String gk = myTraits.Calendar.FormatGreaterKey(key);
                if (!String.IsNullOrEmpty(gk)) myWriter.SetValue(gk, currentRow - 1, currentCol, myStyles.GreaterKey);

                currentCol++;
            }

            if (myColmns.GStundenIndex != -1) myWriter.SetValue("G-Std.", currentRow, currentCol++, myStyles.AuKopfR);
            if (myColmns.VerbrauchIndex != -1) myWriter.SetValue("Verbrauch", currentRow, currentCol++, myStyles.AuKopfR);
            if (myColmns.GPreisIndex != -1) myWriter.SetValue("G-Wert", currentRow, currentCol++, myStyles.AuKopfR);
            
            currentRow++;
        }

        /// <summary>
        /// Auftragzeile schreiben
        /// </summary>
        public void Visit(Auftrag obj)
        {
           
            myStats.PresentAuftrag(obj);

            int currentCol = 1;

            myWriter.SetValue(obj.Nummer, currentRow, currentCol++, myStyles.AuCode);
            myWriter.SetValue(obj.Name,   currentRow, currentCol++, myStyles.AuKurzB);
            myWriter.SetValue(obj.Turnus, currentRow, currentCol++, myStyles.AuText);
            myWriter.SetValue(obj.Menge,  currentRow, currentCol++, myStyles.AuNumeric);

            if (myColmns.KSTIndex != -1)      myWriter.SetValue(obj.Kst,               currentRow, currentCol++, myStyles.AuText);
            if (myColmns.EStundenIndex != -1) myWriter.SetValue((decimal)obj.EStunden, currentRow, currentCol++, myStyles.AuNumeric);
            if (myColmns.EPreisIndex != -1)   myWriter.SetValue(obj.EPreis,            currentRow, currentCol++, myStyles.AuMoney);
            
            myWriter.SetValue(obj.MengenDifferenz,  currentRow, currentCol++, myStyles.AuNumeric);
            if (myColmns.DStundenIndex != -1) myWriter.SetValue((decimal)obj.StundenDifferenz, currentRow, currentCol++, myStyles.AuNumeric);
            if (myColmns.DPreisIndex != -1)   myWriter.SetValue(obj.PreisDifferenz,            currentRow, currentCol++, myStyles.AuMoney);
            
            foreach (var dat in obj.Dates.Values) myWriter.SetValue(dat.ToString(), currentRow, currentCol++, myStyles.Datum);
            
            if (myColmns.GStundenIndex != -1)  myWriter.SetValue((decimal)myStats.CurrentHoursTotal, currentRow, currentCol++, myStyles.AuNumeric);
            if (myColmns.VerbrauchIndex != -1) myWriter.SetValue(myStats.CurrentBudgetUsed,          currentRow, currentCol++, myStyles.AuMoney);
            if (myColmns.GPreisIndex != -1)    myWriter.SetValue(myStats.CurrentBudgetTotal,         currentRow, currentCol++, myStyles.AuMoney);

            currentRow++;
        }

        public void Visit(Termin obj)
        {
        }
        
        /// <summary>
        /// Fußzeilen schreiben
        /// </summary>
        public void WriteStatistics()
        {
        
            var lines = myStats.GetTotal();
            if (lines.Count == 0) return;

            currentRow++;
            myWriter.SetValue("Summen", currentRow, 1, myStyles.Params);

            currentRow += 2;

            foreach (var entry in lines) {
            
                myWriter.SetValue(entry.Value, currentRow, 1, myStyles.FootValue);
                myWriter.SetValue(entry.Period + "  " + entry.Name, currentRow, 2, myStyles.FootText);
                
                currentRow++;
            } 
        }       
    }
}

﻿using System;
using System.Windows.Forms;
using comain.Schedule;


namespace comain.Forms
{
    
    /// <summary>
    /// Zeilenhöhen anpassen, dass Auftragskurzbeschreibungen gelesen werden können
    /// </summary>
    public class AutoResizer : IScheduleVisitor
    {
        
        /// <summary>
        /// anzupassendes Control
        /// </summary>
        DataGridView myControl;
        
        
        /// <summary>
        /// Zeilenhöhen anpassen, dass Auftragskurzbeschreibungen gelesen werden können
        /// </summary>
        public AutoResizer(object ctrl)
        {
            
            myControl = ctrl as DataGridView;            
            
            if (myControl == null) throw new ApplicationException("Interner Fehler: Ungültiges Steuerelement");        
        }

        
        public void Visit(Root obj) {}

        public void Visit(Bearbeiter obj) {}

        public void Visit(Standort obj) {}

        public void Visit(IHObjekt obj) {}

        /// <summary>
        /// jede Auftragsspalte anpassen lassen
        /// </summary>
        /// <param name="obj">Auftrag im Schedulecomposite</param>
        public void Visit(Auftrag obj)
        {
            
            myControl.AutoResizeRow(obj.Position);
        }

        public void Visit(Termin obj) {}
    }
}

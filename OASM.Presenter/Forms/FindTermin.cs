﻿using System;
using comain.Schedule;
using System.Windows.Forms;
using comain.User;


namespace comain.Forms
{
    
    /// <summary>
    /// Benutzer hat auf Grid geklickt, zugehörigen Termin suchen
    /// </summary>
    public class FindTermin : IScheduleVisitor
    {
        
        /// <summary>
        /// Datagridview, auf dem der Menüklick ausgelöst wurde
        /// </summary>
        DataGridView myControl;
        /// <summary>
        /// Auslösendes Eventargument
        /// </summary>
        DataGridViewCellMouseEventArgs myEvent;
        /// <summary>
        /// gefundener Termin
        /// </summary>
        Termin myDate = null;
        /// <summary>
        /// gefundener Termin
        /// </summary>
        public Termin Termin { get { return myDate; } }
        
        
        /// <summary>
        /// Konstruktor, Elemente übergeben
        /// </summary>
        /// <param name="sender">Datagridview, auf dem der Menüklick ausgelöst wurde</param>
        /// <param name="e">Auslösendes Eventargument</param>
        public FindTermin(object sender, DataGridViewCellMouseEventArgs e)
        {
             
            myControl = sender as DataGridView;
            myEvent   = e;
            
            if (myControl == null) throw new ApplicationException("Interner Fehler: Ungültiges Steuerelement");        
        }
        
        public void Visit(comain.Schedule.Root obj) {}

        public void Visit(comain.Schedule.Bearbeiter obj) {}

        public void Visit(comain.Schedule.Standort obj) {}

        public void Visit(comain.Schedule.IHObjekt obj) {}

        /// <summary>
        /// Rechter Mausklick auf Auftragszeile: Kontextmenü öffnen
        /// </summary>
        /// <param name="obj">angeklickter Auftrag</param>
        public void Visit(comain.Schedule.Auftrag obj)
        {
            
            //Tagesaufträge nicht im Jahresplan bearbeiten
            if (obj.IsDaily && obj.Traits.Type != ScheduleType.Week) return;
            
            //nur Terminzeilen berücksichtigen
            if (obj.Position != myEvent.RowIndex || obj.Traits.DateColumn != myEvent.ColumnIndex) return;

            //Spaltenbreite eines einzelnen Termines
            int singleWidth = myControl.Columns[myEvent.ColumnIndex].Width / obj.Traits.Count;
            //Terminüberhang nicht berücksichtigen
            if (myEvent.X >= singleWidth * obj.Traits.Count) return;
            //Position des Termines in der Terminliste des Auftrages ermitteln
            int pos = myEvent.X / singleWidth;
            
            //Meister dürfen vollendete Zeiträume nicht bearbeiten
            if (obj.Traits.User is ClientUser) {
            
                DateTime? date;
                date = obj.Traits.OASMSettings.OperDate;
                if (date.HasValue && obj.Dates.Keys[pos] < obj.Traits.Calendar.CalculateCalendarKey(date.Value)) return;
                
                date = obj.Traits.OASMSettings.GreyDate;
                if (date.HasValue && obj.Dates.Keys[pos] < obj.Traits.Calendar.CalculateCalendarKey(date.Value)) return;
            }            
            
            myDate = obj.Dates[obj.Dates.Keys[pos]];
        }

        public void Visit(comain.Schedule.Termin obj) {}
    }
}

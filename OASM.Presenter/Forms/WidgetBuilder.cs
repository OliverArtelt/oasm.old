﻿using System;
using System.Windows.Forms;
using System.Drawing;
using comain.Schedule;
using System.Diagnostics;
using comain.Workspace;


namespace comain.Forms
{
    
    /// <summary>
    /// Schedule an ein DataGridView binden
    /// Visitor wird als Builder missbraucht 
    /// </summary>
    /// <remarks>
    /// Dispose() oder 'using' muss verwendet werden, damit Control korrekt dargestellt wird
    /// </remarks>
    public class WidgetBuilder : IScheduleVisitor, IDisposable
    {
    
        /// <summary>
        /// verfügbare Spalten
        /// </summary>
        GridColumns myColumns;
        /// <summary>
        /// zu verwendendes Gridcontrol
        /// </summary>
        DataGridView myControl;
        /// <summary>
        /// Planeigenschaften
        /// </summary>
        Traits myTraits;
        /// <summary>
        /// Verzögerungsanzeige beim Aufbau verwenden?
        /// </summary>
        ProgressBar myProgress;
        /// <summary>
        /// Gridzellformatstile
        /// </summary>
        CellStyleFactory myStyles;
        /// <summary>
        /// Anzahl verarbeiteter Aufträge für die Statusbar
        /// </summary>
        int ordersProcessed;
        

        /// <summary>
        /// Schedule an ein DataGridView binden
        /// </summary>
        /// <param name="ctrl">zu verwendendes Gridcontrol</param>
        /// <param name="prg">Verzögerungsanzeige beim Aufbau</param>
        public WidgetBuilder(DataGridView ctrl, ProgressBar prg = null)
        {
        
            myColumns = Session.Current.Schedule.Traits.Columns;
            myControl = ctrl;
            myProgress = prg;
            myProgress.Value = 0;
            ordersProcessed = 0;
            
            //Zeichnen des Controls während der Bauphase unterdrücken
            myControl.SuspendLayout();
        }        
    
#region F I N A L I Z A T I O N

        /// <summary>
        /// Verzögerungsanzeige zurücksetzen, Control darstellen
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
 	        if (disposing) {
 	        
                myProgress.Value = 0;
                 
                //Control zeichnen
                myControl.ResumeLayout();
            }
        }

        ~WidgetBuilder() 
        {
            Dispose(false);
        }

#endregion

        /// <summary>
        /// Gridspalten abh. der Planeigenschaften einstellen
        /// </summary>
        /// <param name="obj"></param>
        public void Visit(Root obj)
        {

            //Planeinstellungen für Verarbeitung kopieren
            myTraits = obj.Traits;
            myProgress.Maximum = obj.CountOrders();
            myStyles = new CellStyleFactory(obj.Traits.Type);
            
            obj.Position = 0;
            
            //evtl. alten Plan entfernen
            myControl.Rows.Clear();
            myControl.Columns.Clear();

            //Standardspalten
            myControl.Columns.Add(new DataGridViewCheckBoxColumn());
            myControl.Columns[myColumns.CheckIndex].Width = 20;
            myControl.Columns[myColumns.CheckIndex].Frozen = true;
            
            myControl.Columns.Add(new DataGridViewTextBoxColumn());
            myControl.Columns[myColumns.AuCodeIndex].Width = 86;
            myControl.Columns[myColumns.AuCodeIndex].Frozen = true;
            
            myControl.Columns.Add(new DataGridViewTextBoxColumn());
            myControl.Columns[myColumns.AuKurzBIndex].Width = 120;
            myControl.Columns[myColumns.AuKurzBIndex].Frozen = true;
            
            myControl.Columns.Add(new DataGridViewTextBoxColumn());
            myControl.Columns[myColumns.TurnusIndex].Width = 28;
            myControl.Columns[myColumns.TurnusIndex].DefaultCellStyle = myStyles.ZentrierteSpalte;
            
            myControl.Columns.Add(new DataGridViewTextBoxColumn());
            myControl.Columns[myColumns.AnzahlIndex].Width = 28;
            myControl.Columns[myColumns.AnzahlIndex].DefaultCellStyle = myStyles.ZentrierteSpalte;
           
            
            //Kostenstelle?
            if (myColumns.KSTIndex != -1) {
            
                myControl.Columns.Add(new DataGridViewTextBoxColumn());
                myControl.Columns[myColumns.KSTIndex].Width = 60;
                myControl.Columns[myColumns.KSTIndex].DefaultCellStyle = myStyles.ZentrierteSpalte;
            }
            
            //Einzelstunden?
            if (myColumns.EStundenIndex != -1) {
            
                myControl.Columns.Add(new DataGridViewTextBoxColumn());
                myControl.Columns[myColumns.EStundenIndex].Width = 40;
                myControl.Columns[myColumns.EStundenIndex].DefaultCellStyle = myStyles.WerteSpalte;
            }
            
            //Einzelpreise?
            if (myColumns.EPreisIndex != -1) {
            
                myControl.Columns.Add(new DataGridViewTextBoxColumn());
                myControl.Columns[myColumns.EPreisIndex].Width = 60;
                myControl.Columns[myColumns.EPreisIndex].DefaultCellStyle = myStyles.WaehrungsSpalte;
            }

            myControl.Columns.Add(new DataGridViewTextBoxColumn());
            myControl.Columns[myColumns.DAnzahlIndex].Width = 45;
            myControl.Columns[myColumns.DAnzahlIndex].DefaultCellStyle = myStyles.ZentrierteSpalte;

            
            //Differenzstunden?
            if (myColumns.DStundenIndex != -1) {
            
                myControl.Columns.Add(new DataGridViewTextBoxColumn());
                myControl.Columns[myColumns.DStundenIndex].Width = 55;
                myControl.Columns[myColumns.DStundenIndex].DefaultCellStyle = myStyles.WerteSpalte;
            }
            
            //Differenzpreise?
            if (myColumns.DPreisIndex != -1) {
            
                myControl.Columns.Add(new DataGridViewTextBoxColumn());
                myControl.Columns[myColumns.DPreisIndex].Width = 75;
                myControl.Columns[myColumns.DPreisIndex].DefaultCellStyle = myStyles.WaehrungsSpalte;
            }

            
            //Terminspalte
            myControl.Columns.Add(new DataGridViewTextBoxColumn());
            
            //Spaltenbreite ausmessen
            String measure = myTraits.Calendar.FormatKeyShort(myTraits.Calendar.CalendarKeys[0]);
            SizeF box = myControl.CreateGraphics().MeasureString(measure, myControl.DefaultCellStyle.Font);
            
            //Wochen- (freier) Plan: Gesamtbreite begrenzen
            int width = myTraits.Count * (int)(box.Width + 0.5) + 1;
            if (width > 65535) throw new ApplicationException("Der Zeitraum ist zu groß. Verwenden Sie einen kleineren Zeitraum, weniger als 1000 Tage.");
                       
            myControl.Columns[myColumns.DateIndex].Width = width; 
            myControl.Columns[myColumns.DateIndex].MinimumWidth = myControl.Columns[myColumns.DateIndex].Width;
            
            //Gesamtstunden?
            if (myColumns.GStundenIndex != -1) {
            
                myControl.Columns.Add(new DataGridViewTextBoxColumn());
                myControl.Columns[myColumns.GStundenIndex].Width = 55;
                myControl.Columns[myColumns.GStundenIndex].DefaultCellStyle = myStyles.WerteSpalte;
                myControl.Columns[myColumns.GStundenIndex].DefaultCellStyle.Format = "N2";
            }
           
            //Gesamtpreise?
            if (myColumns.VerbrauchIndex != -1) {

                myControl.Columns.Add(new DataGridViewTextBoxColumn());
                myControl.Columns[myColumns.VerbrauchIndex].Width = 75;
                myControl.Columns[myColumns.VerbrauchIndex].DefaultCellStyle = myStyles.WaehrungsSpalte;
                //myControl.Columns[clms.VerbrauchIndex].DefaultCellStyle.Format = "N2";
            }

            if (myColumns.GPreisIndex != -1) {

                myControl.Columns.Add(new DataGridViewTextBoxColumn());
                myControl.Columns[myColumns.GPreisIndex].Width = 75;
                myControl.Columns[myColumns.GPreisIndex].DefaultCellStyle = myStyles.WaehrungsSpalte;
            }
        }
        
        /// <summary>
        /// Bearbeiterzeile einfügen
        /// </summary>
        /// <param name="obj">neuer Bearbeiter</param>
        public void Visit(Bearbeiter obj)
        {

            //Debug.WriteLine(String.Format("{1} RowCount {0}", myControl.RowCount, DateTime.Now.ToLongTimeString()));

            myControl.Rows.Add();
            obj.Position = myControl.RowCount - 1;
            myControl.Rows[obj.Position].Height = 32;
        }
        
        /// <summary>
        /// Standortzeile einfügen
        /// </summary>
        /// <param name="obj">neuer Standort des Bearbeiters</param>
        public void Visit(Standort obj)
        {

            myControl.Rows.Add();
            obj.Position = myControl.RowCount - 1;
            myControl.Rows[obj.Position].Height = 26;
            myControl.Rows[obj.Position].Visible = myTraits.Projection[ScheduleParts.ShowPZ];
        }
        
        /// <summary>
        /// IH-Objektzeile einfügen
        /// </summary>
        /// <param name="obj">neues IH-Objekt des Standorts</param>
        public void Visit(IHObjekt obj)
        {

            //IH-Objekt
            myControl.Rows.Add();
            obj.Position = myControl.RowCount - 1;
            myControl.Rows[obj.Position].Height = 64;
            myControl.Rows[obj.Position].Visible = myTraits.Projection[ScheduleParts.ShowIH];
            

            //Auftragskopf
            myColumns.CheckColumn = false;
            myColumns.AuCodeColumn = "Auftrags-Nr.";
            myColumns.AuKurzBColumn = "Beschreibung";
            myColumns.TurnusColumn = "Tur";
            myColumns.AnzahlColumn = "Anz";
            myColumns.DAnzahlColumn = "Δ Anz";
            myColumns.DateColumn = "";

            if (myColumns.KSTIndex != -1)       myColumns.KSTColumn = "KSt";
            if (myColumns.EStundenIndex != -1)  myColumns.EStundenColumn = "E-Std.";
            if (myColumns.EPreisIndex != -1)    myColumns.EPreisColumn = "E-Wert";
            if (myColumns.GStundenIndex != -1)  myColumns.GStundenColumn = "G-Std.";
            if (myColumns.GPreisIndex != -1)    myColumns.GPreisColumn = "G-Wert";
            if (myColumns.DStundenIndex != -1)  myColumns.DStundenColumn = "Δ Std.";
            if (myColumns.DPreisIndex != -1)    myColumns.DPreisColumn = "Δ Wert";
            if (myColumns.VerbrauchIndex != -1) myColumns.VerbrauchColumn = "Verbrauch";

            myControl.Rows.Add(myColumns.Row);

            myControl.Rows[obj.Position + 1].Height = 18;
            myControl.Rows[obj.Position + 1].DefaultCellStyle = myStyles.AuftragskopfZellen;
        }
        
        /// <summary>
        /// Auftragszeile einfügen
        /// </summary>
        /// <param name="obj">neuer Auftrag des IH-Objektes</param>
        public void Visit(Auftrag obj)
        {

            myColumns.CheckColumn = false;
            myColumns.AuCodeColumn = obj.Nummer;
            myColumns.AuKurzBColumn = obj.Name;
            myColumns.TurnusColumn = obj.Turnus;
            myColumns.AnzahlColumn = obj.Menge;
            myColumns.DAnzahlColumn = "";  //tooltip abklemmen.
            myColumns.DateColumn = "";

            if (myColumns.KSTIndex != -1)       myColumns.KSTColumn = obj.Kst;
            if (myColumns.EStundenIndex != -1)  myColumns.EStundenColumn = obj.EStunden;
            if (myColumns.EPreisIndex != -1)    myColumns.EPreisColumn = obj.EPreis;
            if (myColumns.GStundenIndex != -1)  myColumns.GStundenColumn = obj.HoursTotal;
            if (myColumns.GPreisIndex != -1)    myColumns.GPreisColumn = obj.BudgetTotal;
            if (myColumns.VerbrauchIndex != -1) myColumns.VerbrauchColumn = obj.BudgetUsed;
            if (myColumns.DStundenIndex != -1)  myColumns.DStundenColumn = "";  
            if (myColumns.DPreisIndex != -1)    myColumns.DPreisColumn = "";   

            myControl.Rows.Add(myColumns.Row);

            obj.Position = myControl.RowCount - 1;
            myControl.Rows[obj.Position].Height = 18;
            myControl.Rows[obj.Position].DefaultCellStyle = myStyles.AuftragsZellen;
            myProgress.Value = ordersProcessed++;
        }
        

        public void Visit(Termin obj)
        {
        }
    }
}

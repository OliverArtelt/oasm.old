﻿using System;
using System.Windows.Forms;
using comain.Schedule;


namespace comain.Forms
{
    
    /// <summary>
    /// Zugriff auf Hauptformularmember kapseln
    /// </summary>
    public interface IMainFormUpdater
    {
    
        /// <summary>
        /// Steuerlement welches die Planung visualisiert
        /// </summary>
        Control Grid { get; }
        /// <summary>
        /// Verwaltung Filterfassaden
        /// </summary>
        FilterManager Filter { get; }
        /// <summary>
        /// Anmelden, Fehlerhandler erstellt Caller
        /// </summary>
        void Login(String client, String user, String pass, bool adept);
        /// <summary>
        /// Abmelden, Fehlerhandler erstellt Caller
        /// </summary>
        void Logout();
        /// <summary>
        /// Summendarstellung aktualisieren
        /// </summary>
        void UpdateStatistics();
        /// <summary>
        /// das Control aktualisieren, welches das bearbeitete Item darstellt
        /// </summary>
        /// <param name="psition">interne Kennzeichnung des Items</param>
        void Invalidate(int position);
        /// <summary>
        /// zu einem bestimmten Item springen
        /// </summary>
        /// <param name="position">interne Kennzeichnung des Items</param>
        void ScrollTo(int position);
        /// <summary>
        /// Planung erstellen
        /// </summary>
        void CreateSchedule();
        /// <summary>
        /// geänderte Planung zurückschreiben/sichern
        /// </summary>
        void UpdateSchedule();
        /// <summary>
        /// Scheduletyp einstellen, damit Filterkriterien gewählt werden können
        /// </summary>
        void SetSchedule(ScheduleType type, String name, int year);
        /// <summary>
        /// verfügbare Planungen anzeigen
        /// </summary>
        void UpdateConfiguration();
        /// <summary>
        /// Arbeitsfortschritt anzeigen
        /// </summary>
        /// <param name="percent">0 - 100 %</param>
        void SetWorkProgress(int percent); 
        /// <summary>
        /// Arbeitszustand anzeigen
        /// </summary>
        /// <param name="message"></param>
        void SetWorkState(String message); 
    }
}

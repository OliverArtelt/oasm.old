﻿using System.Windows.Forms;
using System.Drawing;
using comain.Schedule;


namespace comain.Forms
{
   
    /// <summary>
    /// Dieselben Instanzen für Zellformatierungen verwenden (Platzsparendes DataGridView)
    /// </summary>
    /// <remarks>
    /// z.Z. nicht als Singleton implementiert,da unterschiedliche Stile für unterschiedliche 
    /// Scheduletypen möglich sind.
    /// </remarks>
    class CellStyleFactory
    {
    
        /// <summary>
        /// Stilinstanzen erzeugen
        /// </summary>
        /// <param name="type">Scheduletype: Hintergrundfarbe typspezifisch darstellen</param>
        public CellStyleFactory(ScheduleType type)
        {

            Color headCol = Color.White;
            
            //Auftragskopfhintergrundfarbe abh. vom Scheduletyp
            switch (type) {
            
            case ScheduleType.YearNew:
            
                headCol = Color.LightGreen;
                break;
            
            case ScheduleType.YearCurrent:
            
                headCol = Color.PaleTurquoise;
                break;
            
            case ScheduleType.YearOld:
            
                headCol = Color.LightBlue;
                break;
            
            case ScheduleType.Week:
            
                headCol = Color.Thistle;
                break;
            }
            
            //Stil für Auftragskopfzeile        
            AuftragskopfZellen = new DataGridViewCellStyle();   
            AuftragskopfZellen.BackColor = headCol; 
            AuftragskopfZellen.ForeColor = Color.Black;
            AuftragskopfZellen.SelectionBackColor = headCol; 
            AuftragskopfZellen.SelectionForeColor = Color.Black;
        
            //Stil für Auftrags-/Terminzeile
            AuftragsZellen = new DataGridViewCellStyle();   
            AuftragsZellen.BackColor = Color.White;
            AuftragsZellen.ForeColor = Color.Black;
            AuftragsZellen.SelectionBackColor = Color.Gold;
            AuftragsZellen.SelectionForeColor = Color.Black;
            AuftragsZellen.WrapMode = DataGridViewTriState.True;
        
            //Zentrierte Spalte (Turnus, KST, ...)
            ZentrierteSpalte = new DataGridViewCellStyle();   
            ZentrierteSpalte.Alignment = DataGridViewContentAlignment.MiddleCenter;
        
            //Werte-Spalte rechtsbündig (Stunden)
            WerteSpalte = new DataGridViewCellStyle();   
            WerteSpalte.Alignment = DataGridViewContentAlignment.MiddleRight;
        
            //Währungs-Spalte rechtsbündig (Preis)
            WaehrungsSpalte = new DataGridViewCellStyle();   
            WaehrungsSpalte.Alignment = DataGridViewContentAlignment.MiddleRight;
            WaehrungsSpalte.Format = "C2";
        }
        
        /// <summary>
        /// Stil für Auftragskopfzeile
        /// </summary>
        public DataGridViewCellStyle AuftragskopfZellen { get; private set; }
        /// <summary>
        /// Stil für Auftrags-/Terminzeile
        /// </summary>
        public DataGridViewCellStyle AuftragsZellen     { get; private set; }
        /// <summary>
        /// Zentrierte Spalte (Turnus, KST, ...)
        /// </summary>
        public DataGridViewCellStyle ZentrierteSpalte   { get; private set; }
        /// <summary>
        /// Werte-Spalte rechtsbündig (Stunden)
        /// </summary>
        public DataGridViewCellStyle WerteSpalte        { get; private set; }
        /// <summary>
        /// Währungs-Spalte rechtsbündig (Preis)
        /// </summary>
        public DataGridViewCellStyle WaehrungsSpalte    { get; private set; }
    }
}

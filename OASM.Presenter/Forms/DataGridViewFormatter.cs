﻿using System;
using System.Windows.Forms;
using comain.Schedule;
using comain.Workspace;


namespace comain.Forms
{
    
    /// <summary>
    /// Checkbox-Wert erste Spalte ermitteln
    /// </summary>
    public class DataGridViewFormatter : IScheduleVisitor
    {

        /// <summary>
        /// zu zeichnendes Datagridview
        /// </summary>
        DataGridView myControl;
        /// <summary>
        /// Auslösendes Eventargument
        /// </summary>
        DataGridViewCellFormattingEventArgs myEvent;
        /// <summary>
        /// Spaltenindicies
        /// </summary>
        GridColumns myColums;
       
        /// <summary>
        /// Checkbox-Wert erste Spalte ermitteln
        /// </summary>
        /// <param name="ctrl">zu zeichnendes Datagridview</param>
        /// <param name="e">Auslösendes Eventargument</param>
        public DataGridViewFormatter(object ctrl, DataGridViewCellFormattingEventArgs e)
        {
        
            myColums = Session.Current.Schedule.Traits.Columns;
            myControl = ctrl as DataGridView;            
            myEvent = e;
            
            if (myControl == null) throw new ApplicationException("Interner Fehler: Ungültiges Steuerelement");        
        }
        
        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
        }

        /// <summary>
        /// Auftragszeile rendern: Wert der Checkbox übergeben
        /// </summary>
        /// <param name="obj">der Auftrag im Schedule</param>
        public void Visit(Auftrag obj)
        {
        
            if (myEvent.ColumnIndex == 0) {
            
                myEvent.Value = obj.Selected;
                myEvent.FormattingApplied = true;
            }
            
            if (myEvent.ColumnIndex == myColums.VerbrauchIndex) {
            
                myEvent.Value = obj.BudgetUsed.ToString("C2");
                myEvent.FormattingApplied = true;
            }
            
            if (myEvent.ColumnIndex == myColums.GStundenIndex) {
            
                myEvent.Value = obj.HoursTotal.ToString("N2");
                myEvent.FormattingApplied = true;
            }
            
            if (myEvent.ColumnIndex == myColums.GPreisIndex) {
            
                myEvent.Value = obj.BudgetTotal.ToString("C2");
                myEvent.FormattingApplied = true;
            }          
        }

        public void Visit(Termin obj)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using comain.Filter;
using System.Text;


namespace comain.Forms
{
   
    /// <summary>
    /// Filterfassaden verwalten
    /// </summary>
    public class FilterManager
    {
    
    
        
#region F I L T E R   O B J E C T S


        /// <summary>
        /// Standortfilter
        /// </summary>
        PZFilter pzFilter;
        /// <summary>
        /// IH-Objektfilter
        /// </summary>
        IHFilter ihFilter;
        /// <summary>
        /// Auftragsfilter
        /// </summary>
        AUFilter auFilter;
        /// <summary>
        /// Kostenstellenfilter
        /// </summary>
        KSTFilter kstFilter;
        /// <summary>
        /// Filter Leistungsarten
        /// </summary>
        LAFilter laFilter;
        /// <summary>
        /// Filter Auftraggeber/Bereich
        /// </summary>
        BereichFilter bereichFilter;
        /// <summary>
        /// Profilfilter
        /// </summary>
        ProfilFilter profilFilter;
        /// <summary>
        /// Sortierungsmodus
        /// </summary>
        SortFilter sortFilter;
        /// <summary>
        /// Planoptionen
        /// </summary>
        ProjectionFilter projFilter;
        /// <summary>
        /// Zeitbereich für Wochenplanung
        /// </summary>
        PeriodFilter periodFilter;
        
#endregion


        public FilterManager(FilterControls ctrls)
        {
        
            pzFilter = new PZFilter(ctrls.StandortVonListe, ctrls.StandortBisListe);
            ihFilter = new IHFilter(ctrls.IHObjektListe, ctrls.BaugruppenListe, ctrls.InventarListe);
            laFilter = new LAFilter(ctrls.LeistungsartenListe);
            auFilter = new AUFilter(ctrls.AuftragsListe, laFilter);
            kstFilter = new KSTFilter(ctrls.KostenstellenListe);
            bereichFilter = new BereichFilter(ctrls.PersonalListe);
            profilFilter = new ProfilFilter(ctrls.ProfilListe, ctrls.ProfilBearbeitenListe);
            sortFilter = new SortFilter(ctrls.SortierListe);
            projFilter = new ProjectionFilter(ctrls.Projektion);
            periodFilter = new PeriodFilter(ctrls.PeriodeVon, ctrls.PeriodeBis);
            projFilter.BindValues(PicklistType.Proj);
            
            ResetFilter();
        }

        /// <summary>
        /// Filterliste füllen
        /// </summary>
        public void BindValues(PicklistType whatToBind)
        {
        
            switch (whatToBind) {
            
            case PicklistType.IH:
            case PicklistType.IHSub:
            case PicklistType.IHInvent:
            
                ihFilter.BindValues(whatToBind);
                break;
             
            case PicklistType.AU:
            
                auFilter.BindValues(PicklistType.AU); 
                break;
             
            case PicklistType.PZVon:
            
                pzFilter.BindValues(PicklistType.PZVon); 
                break;
             
            case PicklistType.PZBis:
            
                pzFilter.BindValues(PicklistType.PZBis); 
                break;
             
            case PicklistType.KST:
            
                kstFilter.BindValues(PicklistType.KST); 
                break;
             
            case PicklistType.LA:
            
                laFilter.BindValues(PicklistType.LA); 
                break;
             
            case PicklistType.Bereich:
            
                bereichFilter.BindValues(PicklistType.Bereich); 
                break;
             
            case PicklistType.Profil:
            
                profilFilter.BindValues(PicklistType.Profil); 
                break;
            }
        }

        /// <summary>
        /// Filtereinstellungen zurücksetzen / aus Benutzerprofil laden
        /// </summary>
        public void ResetFilter()
        {
        
            pzFilter.ResetValue();
            ihFilter.ResetValue();
            auFilter.ResetValue();
            kstFilter.ResetValue();
            laFilter.ResetValue();
            bereichFilter.ResetValue();
            profilFilter.ResetValue();
            sortFilter.ResetValue();
            projFilter.ResetValue();
            periodFilter.ResetValue();
        }
        
        /// <summary>
        /// Filtereinstellungen in Benutzerprofil speichern
        /// </summary>
        public void SaveFilter()
        {
        
            pzFilter.SaveValue();
            ihFilter.SaveValue();
            auFilter.SaveValue();
            kstFilter.SaveValue();
            laFilter.SaveValue();
            bereichFilter.SaveValue();
            profilFilter.SaveValue();
            sortFilter.SaveValue();
            projFilter.SaveValue();
            periodFilter.SaveValue();
        }
        
        /// <summary>
        /// Filterbeschreibung für Export geben
        /// </summary>
        public String FilterDescription
        {
            
            get {
            
                StringBuilder bld = new StringBuilder();
                
                String s;
                s = periodFilter.FilterDesc;
                if (s.Length > 0) bld.Append(s).Append(", ");
            
                return (bld.Length > 0)? bld.Remove(bld.Length - 2, 2).ToString(): "";
            }
        }
        
        /// <summary>
        /// Filterwerte geben
        /// </summary>
        public Dictionary<String, List<String>> FilterValues
        {
        
            get {
            
                Dictionary<String, List<String>> values = new Dictionary<string, List<string>>();
                
                values.Add("PZ",      pzFilter.Values);
                values.Add("IH",      ihFilter.Values);
                values.Add("AU",      auFilter.Values);
                values.Add("KST",     kstFilter.Values);
                values.Add("LA",      laFilter.Values);
                values.Add("Bereich", bereichFilter.Values);
                values.Add("Profil",  profilFilter.Values);
                values.Add("Sort",    sortFilter.Values);
                values.Add("Proj",    projFilter.Values);
                values.Add("Period",  periodFilter.Values);

                return values;
            }        
        }

        /// <summary>
        /// Profileinstellung rücksetzen
        /// </summary>
        public void ClearProfile()
        {
            profilFilter.ClearValues();
        }
    }
}

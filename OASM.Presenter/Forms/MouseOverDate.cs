﻿using System;
using comain.Schedule;
using System.Windows.Forms;
using comain.User;


namespace comain.Forms
{
    
    /// <summary>
    /// Termin unter Mauszeiger suchen (Zusatzinformationen im Tooltip darstellen)
    /// </summary>
    public class MouseOverDate : IScheduleVisitor
    {
        
        DataGridView myControl;
        int mouseXPos;
        int myRowIndex;
        bool suppressEmptyDates;
        
        
        /// <summary>
        /// gefundener Termin
        /// </summary>
        public Termin Termin { get; private set; }
        
        
        /// <summary>
        /// Konstruktor, Elemente übergeben
        /// </summary>
        /// <param name="sender">Datagridview über dem Maus sich befindet</param>
        /// <param name="rowIndex">RowIndex des Datagridview (entscheiden ob über Terminzeile)</param>
        /// <param name="columnIndex">ColumnIndex des Datagridview (muß Terminspalte sein)</param>
        /// <param name="mouseX">Mouse-X-Position im Control</param>
        /// <param name="suppressEmpty">leere Termine nicht geben</param>
        public MouseOverDate(object sender, int rowIndex, int columnIndex, int mouseX, bool suppressEmpty)
        {
             
            myControl = sender as DataGridView;
            if (myControl == null) throw new ApplicationException("Interner Fehler: Ungültiges Steuerelement"); 
            
            myRowIndex = rowIndex;       
            suppressEmptyDates = suppressEmpty;
            
            mouseXPos = mouseX;
        }
        
        public void Visit(comain.Schedule.Root obj) {}

        public void Visit(comain.Schedule.Bearbeiter obj) {}

        public void Visit(comain.Schedule.Standort obj) {}

        public void Visit(comain.Schedule.IHObjekt obj) {}

        public void Visit(comain.Schedule.Auftrag obj)
        {
            
            //nur Terminzeilen berücksichtigen
            if (obj.Position != myRowIndex) return;
            //Spaltenbreite eines einzelnen Termines
            int singleWidth = myControl.Columns[obj.Traits.DateColumn].Width / obj.Traits.Count;
            //Terminüberhang nicht berücksichtigen
            if (mouseXPos < 0 || mouseXPos >= singleWidth * obj.Traits.Count) return;
            //Position des Termines in der Terminliste des Auftrages ermitteln
            int pos = mouseXPos / singleWidth;
            
            Termin t = obj.Dates[obj.Dates.Keys[pos]];
            if (suppressEmptyDates && t.PlannedId == 0 && t.ImplementedId == 0 && t.EditedType == EditedType.Empty) return;
            
            Termin = t;
        }

        public void Visit(comain.Schedule.Termin obj) {}
        
    }
}

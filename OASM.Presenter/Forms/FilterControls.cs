﻿using System;
using System.Windows.Forms;
using CO.Forms;


namespace comain.Forms
{

    /// <summary>
    /// Steuerelemente für Filterfassaden geben
    /// </summary>
    public class FilterControls
    {
    
        public ComboBox StandortVonListe { get; set; }
        public ComboBox StandortBisListe { get; set; }
        public ComboBox IHObjektListe { get; set; }
        public ComboBox BaugruppenListe { get; set; }
        public ComboBox InventarListe { get; set; }
        public ComboBox AuftragsListe { get; set; }
        public CheckedListBox LeistungsartenListe { get; set; }
        public ComboBox KostenstellenListe { get; set; }
        public ComboBox PersonalListe { get; set; }
        public ComboBox ProfilListe { get; set; }
        public ComboBox ProfilBearbeitenListe { get; set; }
        public ComboBox SortierListe { get; set; }
        public CheckedListBox Projektion { get; set; }
        public DTPWithCalWeek PeriodeVon { get; set; }
        public DTPWithCalWeek PeriodeBis { get; set; }
    }
}

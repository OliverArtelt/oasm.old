﻿using System;
using comain.Command;
using comain.Schedule;
using System.Windows.Forms;


namespace comain.Forms
{
    
    /// <summary>
    /// Auftrag an-/abwählen: Es wurde auf eine Checkbox im Datagridview geklickt 
    /// </summary>
    public class OrderClickProcessor : IScheduleVisitor
    {

        /// <summary>
        /// Datagridview, auf dem der Mausklick ausgelöst wurde
        /// </summary>
        DataGridView myControl;
        /// <summary>
        /// Befehlshistorie, die an den Befehl weitergebgeben wird
        /// </summary>
        CommandHistory history;
        
        /// <summary>
        /// Auftrag auswählen: Es wurde auf eine Checkbox im Datagridview geklickt 
        /// </summary>
        /// <param name="sender">Datagridview, auf dem der Mausklick ausgelöst wurde</param>
        /// <param name="hist">Befehlshistorie, die an den Befehl weitergebgeben wird</param>
        public OrderClickProcessor(object sender, CommandHistory hist)
        {
            
            history   = hist;
            myControl = sender as DataGridView;            

            if (myControl == null) throw new ApplicationException("Interner Fehler: Ungültiges Steuerelement");        
        }
        

        public void Visit(Root obj)
        {
        }

        public void Visit(Bearbeiter obj)
        {
        }

        public void Visit(Standort obj)
        {
        }

        public void Visit(IHObjekt obj)
        {
        }

        /// <summary>
        /// Auftrag, dessen Checkbox geklickt wurde, behandeln
        /// </summary>
        /// <param name="obj"></param>
        public void Visit(Auftrag obj)
        {
            //Befehl aufrufen
            comain.Command.ICommand cmd = new comain.Command.SelectOrderCommand(myControl, obj);
            cmd.Execute(history);
        }

        public void Visit(Termin obj)
        {
        }
    }
}

﻿using System;


namespace comain.Schedule
{
        
    /// <summary>
    /// Knoten der sich an Position befindet, geben
    /// </summary>
    public class NodeAtPosition : IScheduleVisitor
    {
    
        /// <summary>
        /// gefundener Knoten
        /// </summary>
        public IScheduleComposite Node { get; private set; }
        
        
        public NodeAtPosition()
        {
            Node = null;
        }

        public void Visit(Root obj)
        {
            Node = obj;
        }

        public void Visit(Bearbeiter obj)
        {
            Node = obj;
        }

        public void Visit(Standort obj)
        {
            Node = obj;
        }

        public void Visit(IHObjekt obj)
        {
            Node = obj;
        }

        public void Visit(Auftrag obj)
        {
            Node = obj;
        }

        public void Visit(Termin obj)
        {
        }
    }
}

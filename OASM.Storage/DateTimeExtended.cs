﻿using System;
using System.Globalization;


namespace comain
{

    /// <summary>
    /// Zeitberechnungs-Hilfsmethoden
    /// </summary>
    public static class DateTimeExtended
    {
    
        /// <summary>
        /// Kalenderwoche ermitteln, dirty workaround Calendar.GetWeekOfYear-Bug
        /// </summary>
        /// <remarks>
        /// Source: http://www.artiso.com/ProBlog/PermaLink,guid,02aee2cd-15f1-4e6e-9e0f-a56645d1f5c8.aspx
        /// </remarks>
        /// <param name="ci">CultureInfo, dessen Kalender verwendet werden soll</param>
        /// <returns>KW des Datums</returns>
        public static int WeekOfYear(this DateTime t, CultureInfo ci)
        {
        
            //Berechnung in sicheren Bereich verschieben
            if (t.DayOfWeek >= DayOfWeek.Monday && t.DayOfWeek <= DayOfWeek.Wednesday) t = t.AddDays(3);
    
            //Aufruf der vorgesehenen DotNet-Funktion
            System.Globalization.Calendar c = ci.Calendar;

            return c.GetWeekOfYear(t, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        /// <summary>
        /// Kalenderwoche mit Jahr ermitteln, dirty workaround Calendar.GetWeekOfYear-Bug, Monat mit Jahr ausgeben
        /// </summary>
        /// <param name="ci">CultureInfo, dessen Kalender verwendet werden soll</param>
        /// <returns>Jahr/KW des Datums in der Form 200812 (KW 12 des Jahres 2008)</returns>
        public static int WeekWithYear(this DateTime t, CultureInfo ci)
        {       

            //Berechnung in sicheren Bereich verschieben
            if (t.DayOfWeek >= DayOfWeek.Monday && t.DayOfWeek <= DayOfWeek.Wednesday)
                t = t.AddDays(3);
        
            //Über-/Unterlauf des Jahres ermitteln
            System.Globalization.Calendar c = ci.Calendar;
            int week = c.GetWeekOfYear(t, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            //Unterlauf
            if (week > 50 && c.GetMonth(t) == 1) return (c.GetYear(t) - 1) * 100 + week;
            //Überlauf
            if (week < 2 && c.GetMonth(t) == 12) return (c.GetYear(t) + 1) * 100 + week;
            //Normales Format
            return c.GetYear(t) * 100 + week;
        }
        
        /// <summary>
        /// Datum des des Montages einer Kalenderwoche berechnen
        /// </summary>
        /// <param name="year">zu verwendendes Jahr</param>
        /// <param name="week">zu verwendende Kalenderwoche</param>
        /// <returns>Datum des des Montages der Kalenderwoche</returns>
        public static DateTime FirstDateOfCalendarWeek(int year, int week, CultureInfo ci)
        {
        
            DateTime date = new DateTime(year, 1, 1);

            //zum Montag hangeln
            while (ci.Calendar.GetDayOfWeek(date) != DayOfWeek.Monday) date = date.AddDays(-1);

            //Überhang berücksichtigen
            if (date.WeekOfYear(ci) > 50) date = date.AddDays(7);
                
            //date ist jetzt Datum der KW 1 des Jahres
            return date.AddDays(7 * (week - 1));    
        }
        
        /// <summary>
        /// Montag einer Woche ermitteln
        /// </summary>
        /// <param name="date">beliebiger Tag der Woche</param>
        /// <param name="ci">verwendeter Kalender</param>
        /// <returns>Montag der Woche</returns>
        public static DateTime MondayOf(this DateTime date, CultureInfo ci)
        {
            
            while (ci.Calendar.GetDayOfWeek(date) != DayOfWeek.Monday) date = date.AddDays(-1);
            return date;
        }
    }
}

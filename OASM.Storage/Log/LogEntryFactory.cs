﻿using System;
using System.Data;
using System.Data.Common;
using comain.Connect;


namespace comain.Log
{

    /// <summary>
    /// Protokolleintragstypen
    /// </summary>
    public enum EntryType
    {
        Unknown, Login, Download, UploadDate, UploadImpl, PlanConfig, UserConfig   
    }
    
    /// <summary>
    /// Protokolleintrag Upload: Aktualisierungstypen
    /// </summary>
    public enum UploadType
    {
        DateInsert, DateUpdate, DateDelete, ImplInsert, ImplUpdate, ImplDelete, DateFixate
    }
    

    /// <summary>
    /// Befehle für Protokolleinträge erzeugen
    /// </summary>
    public static class LogEntryFactory
    {

        /// <summary>
        /// fehlerhafter Login protkollieren
        /// </summary>
        /// <param name="user">versuchte Login-Bezeichnung</param>
        /// <param name="success">Ablehnungsgrund</param>
        /// <returns>Db-Protokollbefehl</returns>
        public static DbCommand GetLoginEntry(String user, String success)
        {
        
            DbCommand cmd = DbFactory.CreateCommand("INSERT INTO oasm.protocol (username,logtype,arg1) VALUES (:user,1,:arg1)");
            cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("arg1", DbType.String));
            cmd.Parameters[0].Value = user; 
            cmd.Parameters[1].Value = success; 
        
            return cmd;
        }

        /// <summary>
        /// Login protkollieren
        /// </summary>
        /// <param name="user">erkannter Benutzer</param>
        /// <param name="success">Erfolg</param>
        /// <returns>Db-Protokollbefehl</returns>
        public static DbCommand GetLoginEntry(String username, int userid, bool isClientUser, String success)
        {
        
            DbCommand cmd = DbFactory.CreateCommand("INSERT INTO oasm.protocol (username,keyserviceuser,keyclientuser,logtype,arg1) " +
                                                    "VALUES (:user,:susr,:cusr,1,:arg1)");
            cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("susr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("cusr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("arg1", DbType.String));
            cmd.Parameters[0].Value = username; 
            cmd.Parameters[1].Value = GetServiceUser(userid, isClientUser); 
            cmd.Parameters[2].Value = GetClientUser(userid, isClientUser); 
            cmd.Parameters[3].Value = success; 
        
            return cmd;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user">durchführender Benutzer</param>
        /// <param name="planType"></param>
        /// <returns>Db-Protokollbefehl</returns>
        public static DbCommand GetDownloadEntry(String username, int userid, bool isClientUser, String planType)
        {
        
            DbCommand cmd = DbFactory.CreateCommand("INSERT INTO oasm.protocol (username,keyserviceuser,keyclientuser,logtype,arg1) " +
                                                    "VALUES (:user,:susr,:cusr,2,:arg1)");
            cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("susr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("cusr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("arg1", DbType.String));
            cmd.Parameters[0].Value = username; 
            cmd.Parameters[1].Value = GetServiceUser(userid, isClientUser); 
            cmd.Parameters[2].Value = GetClientUser(userid, isClientUser); 
            cmd.Parameters[3].Value = planType; 
        
            return cmd;
        }
        
        
        public static DbCommand GetUploadDateEntry(String username, int userid, bool isClientUser, UploadType type, int id)
        {
        
            String sql = @"INSERT INTO oasm.protocol 
                               (username,keyserviceuser,keyclientuser,logtype,arg1,
                                ataucode,atstatus,atdate,atid,atmenge,atwert,atstunden) 
                           SELECT :user,:susr,:cusr,3,:arg1,
                                aucode,atstatus,attermin,idautermin,atmenge,atwert,atstunden
                           FROM tbl_auftrag JOIN tblautermin ON auid=atkeyauftrag WHERE idautermin=:id";
            DbCommand cmd = DbFactory.CreateCommand(sql);
            cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("susr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("cusr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("arg1", DbType.String));
            cmd.Parameters.Add(DbFactory.CreateParameter("id",   DbType.Int32));
            cmd.Parameters[0].Value = username; 
            cmd.Parameters[1].Value = GetServiceUser(userid, isClientUser); 
            cmd.Parameters[2].Value = GetClientUser(userid, isClientUser); 
            cmd.Parameters[3].Value = GetUploadType(type); 
            cmd.Parameters[4].Value = id; 
        
            return cmd;
        } 
        
        
        public static DbCommand GetUploadDateInsertEntry(String username, int userid, bool isClientUser)
        {
        
            String sql = @"INSERT INTO oasm.protocol 
                               (username,keyserviceuser,keyclientuser,logtype,arg1,
                                ataucode,atstatus,atdate,atid,atmenge,atwert,atstunden) 
                           SELECT :user,:susr,:cusr,3,:arg1,
                                aucode,atstatus,attermin,idautermin,atmenge,atwert,atstunden
                           FROM tbl_auftrag JOIN tblautermin ON auid=atkeyauftrag WHERE idautermin=currval('seq_autermin')";
            DbCommand cmd = DbFactory.CreateCommand(sql);
            cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("susr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("cusr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("arg1", DbType.String));
            cmd.Parameters[0].Value = username; 
            cmd.Parameters[1].Value = GetServiceUser(userid, isClientUser); 
            cmd.Parameters[2].Value = GetClientUser(userid, isClientUser); 
            cmd.Parameters[3].Value = GetUploadType(UploadType.DateInsert); 
        
            return cmd;
        } 
        
        
        public static DbCommand GetUploadImplEntry(String username, int userid, bool isClientUser, UploadType type, int id)
        {
        
            String sql = @"INSERT INTO oasm.protocol 
                               (username,keyserviceuser,keyclientuser,logtype,arg1,
                                ataucode,atstatus,atdate,atid,atmenge,atwert,atstunden) 
                           SELECT :user,:susr,:cusr,4,:arg1,
                                aucode,rztyp,aurealzyklus,id_realzyklus,rzmenge,rzwert,rzstunden
                           FROM tbl_auftrag JOIN tblrealzyklus ON auid=key_au WHERE id_realzyklus=:id";
            DbCommand cmd = DbFactory.CreateCommand(sql);
            cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("susr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("cusr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("arg1", DbType.String));
            cmd.Parameters.Add(DbFactory.CreateParameter("id",   DbType.Int32));
            cmd.Parameters[0].Value = username; 
            cmd.Parameters[1].Value = GetServiceUser(userid, isClientUser); 
            cmd.Parameters[2].Value = GetClientUser(userid, isClientUser); 
            cmd.Parameters[3].Value = GetUploadType(type); 
            cmd.Parameters[4].Value = id; 
        
            return cmd;
        } 
        
        
        public static DbCommand GetUploadImplInsertEntry(String username, int userid, bool isClientUser)
        {
        
            String sql = @"INSERT INTO oasm.protocol 
                               (username,keyserviceuser,keyclientuser,logtype,arg1,
                                ataucode,atstatus,atdate,atid,atmenge,atwert,atstunden) 
                           SELECT :user,:susr,:cusr,4,:arg1,
                                aucode,rztyp,aurealzyklus,id_realzyklus,rzmenge,rzwert,rzstunden
                           FROM tbl_auftrag JOIN tblrealzyklus ON auid=key_au WHERE id_realzyklus=currval('tblrealzyklus_id_realzyklus_seq')";
            DbCommand cmd = DbFactory.CreateCommand(sql);
            cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("susr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("cusr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("arg1", DbType.String));
            cmd.Parameters[0].Value = username; 
            cmd.Parameters[1].Value = GetServiceUser(userid, isClientUser); 
            cmd.Parameters[2].Value = GetClientUser(userid, isClientUser); 
            cmd.Parameters[3].Value = GetUploadType(UploadType.ImplInsert); 
        
            return cmd;
        } 
        
        /// <summary>
        /// Protokollieren Planungskonfiguration bearbeitet
        /// </summary>
        /// <param name="user">durchführender Benutzer</param>
        /// <returns>Db-Protokollbefehl</returns>
        public static DbCommand GetPlanConfigEntry(String username, int userid, bool isClientUser)
        {
        
            DbCommand cmd = DbFactory.CreateCommand("INSERT INTO oasm.protocol (username,keyserviceuser,keyclientuser,logtype,arg1) " +
                                                    "VALUES (:user,:susr,:cusr,5,:arg1)");
            cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("susr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("cusr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("arg1", DbType.String));
            cmd.Parameters[0].Value = username; 
            cmd.Parameters[1].Value = GetServiceUser(userid, isClientUser); 
            cmd.Parameters[2].Value = GetClientUser(userid, isClientUser); 
            cmd.Parameters[3].Value = "Planungskonfiguration bearbeitet"; 
        
            return cmd;
        }
        
        /// <summary>
        /// Protokollieren Meisterzugang bearbeitet
        /// </summary>
        /// <param name="user">durchführender Benutzer</param>
        /// <returns>Db-Protokollbefehl</returns>
        public static DbCommand GetUserConfigEntry(String username, int userid, bool isClientUser)
        {
        
            DbCommand cmd = DbFactory.CreateCommand("INSERT INTO oasm.protocol (username,keyserviceuser,keyclientuser,logtype,arg1) " +
                                                    "VALUES (:user,:susr,:cusr,6,:arg1)");
            cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("susr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("cusr", DbType.Int32)); 
            cmd.Parameters.Add(DbFactory.CreateParameter("arg1", DbType.String));
            cmd.Parameters[0].Value = username; 
            cmd.Parameters[1].Value = GetServiceUser(userid, isClientUser); 
            cmd.Parameters[2].Value = GetClientUser(userid, isClientUser); 
            cmd.Parameters[3].Value = "Meisterzugang bearbeitet"; 
        
            return cmd;
        }

#region I N T E R N A L S

        /// <summary>
        /// Benutzer-Id des Meisters übergeben
        /// </summary>
        /// <param name="user">angemeldeter Benutzer</param>
        /// <returns>oasm.adept.idadept oder 0</returns>
        private static int GetClientUser(int userid, bool isClientUser)
        {
        
            if (!isClientUser) return 0;
            return userid;
        }
        
        /// <summary>
        /// Benutzer-Id des Auftragnehmers übergeben
        /// </summary>
        /// <param name="user">angemeldeter Benutzer</param>
        /// <returns>tblps.psid oder 0</returns>
        private static int GetServiceUser(int userid, bool isClientUser)
        {
        
            if (isClientUser) return 0;
            return userid;
        }
        
        /// <summary>
        /// Upload-Type lesbar protokollieren
        /// </summary>
        /// <param name="type">Upload-Type</param>
        /// <returns>lesbarer Text</returns>
        private static String GetUploadType(UploadType type)
        {
        
            switch (type) {
            
                case UploadType.DateDelete: return "Termin gelöscht";
                case UploadType.DateFixate: return "Termin fixiert";
                case UploadType.DateInsert: return "Geplant";
                case UploadType.DateUpdate: return "Termin aktualisiert";
                case UploadType.ImplDelete: return "Realisierung gelöscht";
                case UploadType.ImplInsert: return "Realisiert";
                case UploadType.ImplUpdate: return "Realisierung aktualisiert";
            }
            
            return "";
        }
        
#endregion
        
    }
}

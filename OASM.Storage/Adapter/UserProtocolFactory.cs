﻿using System;
using System.Data.Common;
using System.Text;
using comain.Connect;
using comain.User;


namespace comain.Adapter 
{
     
    /// <summary>
    /// Protokolleinträge der Benutzeraktivitäten geben
    /// </summary>                                        
    public class UserProtocolFactory 
    {
         
        String mySql; 
        String myFilter; 
        String myPeriod; 
         
               
        /// <param name="type">Filter Ereignistyp</param>
        /// <param name="logVon">Filter Eintrag von</param>
        /// <param name="logBis">Filter Eintrag bis</param>
        /// <param name="datVon">Filter Datum Termine/Realisierungen von</param>
        /// <param name="datBis">Filter Datum Termine/Realisierungen bis</param>
        /// <param name="user">Filter Benutzer</param>
        /// <param name="auCode">Filter Auftrag</param>
        public UserProtocolFactory(int type, DateTime? logVon, DateTime? logBis, DateTime? datVon, DateTime? datBis,
                                   String user, String auCode)
        {
            
            StringBuilder sql = new StringBuilder();
            
            sql.Append(            

              @"SELECT * FROM (
                SELECT stamp, clientaddress,
                COALESCE(adept.name, psnachname || COALESCE(', ' || psvorname) || COALESCE(' [' || psnr || ']', ''), username) AS name,
                CASE logtype WHEN 1 THEN 'Anmeldung' WHEN 2 THEN 'Planerstellung' WHEN 3 THEN 'Terminplanung' WHEN 4 THEN 'Realisierung' 
                WHEN 5 THEN 'Planungskonfiguration' WHEN 6 THEN 'Benutzerkonfiguration' ELSE '' END AS type,
                logtype,
                CASE WHEN logtype=4 AND atstatus=1 THEN arg1 || ' Sonderleistung' ELSE arg1 END AS arg1,
                arg2,ataucode,aukurzb,atdate,atmenge,atwert,atstunden

                FROM oasm.protocol
                LEFT JOIN oasm.adept ON keyclientuser=idadept
                LEFT JOIN tblps ON keyserviceuser=psid
                LEFT JOIN tbl_auftrag ON aucode=ataucode
                )v WHERE true");
            
            
            switch (type) {
            
            case 1:
            
                sql.Append(" AND logtype=1");
                break; 
            
            case 2:
            
                sql.Append(" AND logtype IN (2,3,4)");
                break; 
            
            case 3:
            
                sql.Append(" AND logtype IN (3,4)");
                break; 
            
            case 4:
            
                sql.Append(" AND logtype IN (5,6)");
                break; 
            }
            
            
            if (logVon.HasValue) sql.Append(String.Format(" AND date(stamp) >='{0}'", logVon.Value.ToShortDateString())); 
            if (logBis.HasValue) sql.Append(String.Format(" AND date(stamp) <='{0}'", logBis.Value.ToShortDateString())); 
            if (datVon.HasValue) sql.Append(String.Format(" AND (atdate IS NULL OR atdate >='{0}')", datVon.Value.ToShortDateString())); 
            if (datBis.HasValue) sql.Append(String.Format(" AND (atdate IS NULL OR atdate <='{0}')", datBis.Value.ToShortDateString())); 
             
            if (user != "")   sql.Append(String.Format(" AND UPPER(name) LIKE '%{0}%'", user.ToUpper().EscapeText()));              
            if (auCode != "") sql.Append(String.Format(" AND ataucode LIKE '%{0}%'", auCode.EscapeText()));              

            sql.Append(" ORDER BY stamp");
            
            mySql = sql.ToString();

            
            myPeriod = "";
            if (logVon.HasValue && logBis.HasValue) myPeriod = logVon.Value.ToShortDateString() + " - " + logBis.Value.ToShortDateString();
            else if (logVon.HasValue)               myPeriod = "ab " + logVon.Value.ToShortDateString();
            else if (logBis.HasValue)               myPeriod = "bis " + logBis.Value.ToShortDateString();           
            
            
            StringBuilder bld = new StringBuilder();
            
            if (type != 0) bld.Append("Ausgewählte Ereignisse, ");
            
            if (datVon.HasValue) {
            
                bld.Append("Termine von ");
                bld.Append(datVon.Value.ToShortDateString());
                bld.Append(", ");
            }
            
            if (datBis.HasValue) {
            
                bld.Append("Termine bis ");
                bld.Append(datBis.Value.ToShortDateString());
                bld.Append(", ");
            }
            
            if (user.Length > 0) {
            
                bld.Append("Benutzer ");
                bld.Append(user);
                bld.Append(", ");
            }
            
            if (auCode.Length > 0) {
            
                bld.Append("Auftrag ");
                bld.Append(auCode);
                bld.Append(", ");
            }
            
            if (bld.Length > 0) bld.Remove(bld.Length - 2, 2);
            myFilter = bld.ToString();
        }
        
        
        public Protocol GetProtocol(String client)
        {

            Protocol data = new Protocol();
            data.RemotingFormat = System.Data.SerializationFormat.Binary;
            
            data.Description.Rows.Add(client, myFilter, myPeriod);
            
            using (DbConnection cnc = DbFactory.CreateConnection(client)) 
            using (DbDataAdapter adp = DbFactory.CreateAdapter(mySql, cnc)) {
                
                adp.Fill(data.Log);            
            }
            
            return data;
        }
        
        
        public static int DeleteEntries(String client, DateTime von, DateTime bis) 
        {
        
            String sql = String.Format("DELETE FROM oasm.protocol WHERE CAST(stamp AS date) BETWEEN '{0}' AND '{1}'",
                                       von.ToString("d"), bis.ToString("d"));
        
            using (DbConnection cnc = DbFactory.CreateConnection(client))
            using (DbCommand cmd = DbFactory.CreateCommand(sql, cnc)) {

                long count = (long)cmd.ExecuteNonQuery(); 
                return (int)count;
            }
        }
    }
}

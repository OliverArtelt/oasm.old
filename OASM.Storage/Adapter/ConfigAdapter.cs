﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Text;
using comain.Calendar;
using comain.Connect;
using comain.Log;
using comain.User;


namespace comain.Adapter
{

    /// <summary>
    /// DTO der Konfigurationen bestücken/aktualisieren
    /// </summary>
    public class ConfigAdapter : IDisposable
    {

        DbConnection myConnection;
        
        DbCommand SelectGlobalCommand;
        DbCommand UpdateGlobalCommand;
        DbCommand InsertGlobalCommand;


        public ConfigAdapter(String client)
        {
        
            myConnection = DbFactory.CreateConnection(client);
            
            SelectGlobalCommand = DbFactory.CreateCommand("SELECT wert FROM tblglobal WHERE entitaet=:name", myConnection);
            SelectGlobalCommand.Parameters.Add(DbFactory.CreateParameter("name", DbType.String));
          
            UpdateGlobalCommand = DbFactory.CreateCommand("UPDATE tblglobal SET wert=:wert WHERE entitaet=:name", myConnection);
            UpdateGlobalCommand.Parameters.Add(DbFactory.CreateParameter("name", DbType.String));
            UpdateGlobalCommand.Parameters.Add(DbFactory.CreateParameter("wert", DbType.String));
          
            InsertGlobalCommand = DbFactory.CreateCommand("INSERT INTO tblglobal (entitaet,wert) VALUES (:name,:wert)", myConnection);
            InsertGlobalCommand.Parameters.Add(DbFactory.CreateParameter("name", DbType.String));
            InsertGlobalCommand.Parameters.Add(DbFactory.CreateParameter("wert", DbType.String));
        }
        

#region D I S P O S I N G

        private bool disposed = false;

        /// <summary>
        /// Archive schließen
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {

            if (!this.disposed) {

                if (disposing) {

                    InsertGlobalCommand.Dispose();
                    UpdateGlobalCommand.Dispose();
                    SelectGlobalCommand.Dispose();
 	                myConnection.Dispose();
                }

                disposed = true;
            }
        }

        ~ConfigAdapter()
        {
            Dispose(false);
        }
        
        
#endregion


        public BaseConfiguration ReadBase()
        {
        
            String s;
            int i;
            DateTime t;
            BaseConfiguration cfg = new BaseConfiguration();
            
            //sonstiges
            if (Int32.TryParse(ReadGlobalValue("GrauPlanJahr"), out i)) cfg.PlanJahr = i;
            else cfg.PlanJahr = null;    
            cfg.GreySumType = (ReadGlobalValue("GrauPlanReal") == "1")? 1: 0;

            //Operative Planung
            cfg.OperDate = null;
            cfg.OperWeeks = null;
            s = ReadGlobalValue("GrauPlanOperativ");
            if (DateTime.TryParse(s, out t)) cfg.OperDate = t;
            else if (Int32.TryParse(s, out i)) cfg.OperWeeks = i;

            //Grauzonenplanung
            cfg.GreyDate = null;
            cfg.GreyWeeks = null;
            s = ReadGlobalValue("GrauPlanGrauzone");
            if (DateTime.TryParse(s, out t)) cfg.GreyDate = t;
            else if (Int32.TryParse(s, out i)) cfg.GreyWeeks = i;   
            
            return cfg;
        }

        
        public void WriteBase(BaseConfiguration cfg, GrantedUser user)
        {
        
            //sonstiges
            WriteGlobalValue("GrauPlanJahr", (cfg.PlanJahr.HasValue)? cfg.PlanJahr.ToString(): "");
            WriteGlobalValue("GrauPlanReal", cfg.GreySumType.ToString());
            
            //Operative Planung
            String value = String.Empty;
            if (cfg.OperDate.HasValue) value = cfg.OperDate.Value.Date.ToString("d");
            else if (cfg.OperWeeks.HasValue) value = cfg.OperWeeks.Value.ToString("d");
            WriteGlobalValue("GrauPlanOperativ", value);
                        
            //Grauzonenplanung
            value = String.Empty;
            if (cfg.GreyDate.HasValue) value = cfg.GreyDate.Value.Date.ToString("d");
            else if (cfg.GreyWeeks.HasValue) value = cfg.GreyWeeks.Value.ToString("d");
            WriteGlobalValue("GrauPlanGrauzone", value);
            
            //Manipulation protokollieren
            using (DbCommand log = LogEntryFactory.GetPlanConfigEntry(user.Name, user.Id, user.IsClientUser)) {
            
                log.Connection = myConnection;
                log.ExecuteNonQuery();
            }
        }


        public BaseExportConfiguration ReadExport()
        {
        
            String s;
            int i;
            BaseExportConfiguration cfg = new BaseExportConfiguration();
            
            cfg.WIYearTemplatePath = ReadGlobalValue("XLSX Path OASM WIJahr");
            cfg.WIWeekTemplatePath = ReadGlobalValue("XLSX Path OASM WIWeek");
            
            s = ReadGlobalValue("XLSX Row OASM WIJahr");
            Int32.TryParse(s, out i);
            cfg.WIYearTemplateRow = (i > 1)? i : 1;
            
            s = ReadGlobalValue("XLSX Row OASM WIWeek");
            Int32.TryParse(s, out i);
            cfg.WIWeekTemplateRow = (i > 1)? i : 1;
            
            return cfg;
        }

        
        public void WriteExport(BaseExportConfiguration cfg, GrantedUser user)
        {
        
            WriteGlobalValue("XLSX Path OASM WIJahr", cfg.WIYearTemplatePath);
            WriteGlobalValue("XLSX Path OASM WIWeek", cfg.WIWeekTemplatePath);
            WriteGlobalValue("XLSX Row OASM WIJahr",  cfg.WIYearTemplateRow.ToString());
            WriteGlobalValue("XLSX Row OASM WIWeek",  cfg.WIWeekTemplateRow.ToString());
            
            //Manipulation protokollieren
            using (DbCommand log = LogEntryFactory.GetPlanConfigEntry(user.Name, user.Id, user.IsClientUser)) {
            
                log.Connection = myConnection;
                log.ExecuteNonQuery();
            }
        }


        /// <summary>
        /// Konfiguration auslesen
        /// </summary>
        /// <returns></returns>
        public Configuration Read()
        {
    
            Configuration cfg = new Configuration();
            
            String s;
            int i;
            DateTime t;
            
            cfg.UseGreyArea = false;
            CultureInfo myCI = new CultureInfo("de-DE");
            
            //Jahr strategische Planung ermitteln (im Regelfall nächstes Jahr)
            s = ReadGlobalValue("GrauPlanJahr");
            if (s != "") cfg.PlanYear = Int32.Parse(s);
            else         cfg.PlanYear = DateTime.Now.Year + 1;
            
            //kleinstes planbares Jahr ermitteln (aus Sollwerten oder Planjahr - 1 (== aktuelles Jahr))
            s = ReadValue("SELECT MIN(CAST(swjahr AS int4)) FROM tblsw");
            if (s != "") cfg.MinDate = new DateTime(Int32.Parse(s), 1, 1);
            else         cfg.MinDate = new DateTime(cfg.PlanYear - 1, 1, 1);
            
            //größtes planbares Jahr ermitteln (aus Sollwerten oder Planjahr (== nächstes Jahr))
            s = ReadValue("SELECT MAX(CAST(swjahr AS int4)) FROM tblsw");
            if (s != "") cfg.MaxDate = new DateTime(Int32.Parse(s), 12, 31);
            else         cfg.MaxDate = new DateTime(cfg.PlanYear, 12, 31);
            if (cfg.PlanYear > cfg.MaxDate.Year) cfg.MaxDate = new DateTime(cfg.PlanYear, 12, 31);
            
            //Summenberechnungsmodus
            cfg.GreySumType = (ReadGlobalValue("GrauPlanReal") == "1")? 1: 0;
            
            //Operative Planung
            bool hasOp = false;
            s = ReadGlobalValue("GrauPlanOperativ");
            
            if (DateTime.TryParse(s, out t)) {
            
                cfg.OperDate = t;
                hasOp = true;
            }
            else if (Int32.TryParse(s, out i)) {
            
                cfg.OperDate = DateTime.Now.AddDays(7 * i).MondayOf(myCI).Date;
                hasOp = true;
            }
            else cfg.OperDate = cfg.MinDate;
            
            //Grauzonenplanung
            bool hasGrey = false;
            s = ReadGlobalValue("GrauPlanGrauzone");
            
            if (DateTime.TryParse(s, out t)) {
            
                cfg.GreyDate = t;
                hasGrey = true;
            }
            else if (Int32.TryParse(s, out i)) {
            
                cfg.GreyDate = DateTime.Now.AddDays(-7 * i).MondayOf(myCI).Date;
                hasGrey = true;
            }
            else cfg.GreyDate = cfg.MinDate;                

            cfg.UseGreyArea = hasGrey && hasOp;
            if (!cfg.UseGreyArea) {
            
                cfg.AsText = String.Empty;
            
            } else {
            
                StringBuilder bld = new StringBuilder(); 
                bld.Append("Grauzonenplanung, ");
                bld.Append("Grauzone ");
                bld.Append(cfg.GreyDate.ToShortDateString());
                bld.Append(", Operative Planung ");
                bld.Append(cfg.OperDate.ToShortDateString());
                bld.Append(", ");
                if (cfg.GreySumType == 1) bld.Append("Summen geplanter und realisierter Termine");
                else                      bld.Append("Summen geplanter Termine");
                
                cfg.AsText = bld.ToString(); 
            }
            
            
            cfg.WIYearTemplatePath = ReadGlobalValue("XLSX Path OASM WIJahr");
            cfg.WIWeekTemplatePath = ReadGlobalValue("XLSX Path OASM WIWeek");
            
            s = ReadGlobalValue("XLSX Row OASM WIJahr");
            Int32.TryParse(s, out i);
            cfg.WIYearTemplateRow = (i > 1)? i : 0;
            
            s = ReadGlobalValue("XLSX Row OASM WIWeek");
            Int32.TryParse(s, out i);
            cfg.WIWeekTemplateRow = (i > 1)? i : 0;

            return cfg;
        }
        
        
#region P R I V A T E


        private String ReadGlobalValue(String name)
        {
        
            SelectGlobalCommand.Parameters[0].Value = name;
            
            object o = SelectGlobalCommand.ExecuteScalar();
            if (o is DBNull || o == null) return String.Empty;
            return o.ToString();
        }


        private void WriteGlobalValue(String name, String wert)
        {
            
            UpdateGlobalCommand.Parameters[0].Value = name;
            UpdateGlobalCommand.Parameters[1].Value = wert;
            
            if (UpdateGlobalCommand.ExecuteNonQuery() > 0) return;
            
            InsertGlobalCommand.Parameters[0].Value = name;
            InsertGlobalCommand.Parameters[1].Value = wert;
            
            if (InsertGlobalCommand.ExecuteNonQuery() == 0) throw new ApplicationException("Konfiguration kann nicht geschrieben werden.");
        }


        private String ReadValue(String sql)
        {
        
            using (DbCommand cmd = DbFactory.CreateCommand(sql, myConnection)) {
                       
                object o = cmd.ExecuteScalar();
                if (o is DBNull || o == null) return String.Empty;
            
                return o.ToString();
            }
        }
        

#endregion

    }
}

﻿using System;
using System.Collections.Generic;
using comain.User;
using comain.Schedule;
using System.Data.Common;
using comain.Connect;


namespace comain.Adapter
{
    
    /// <summary>
    /// Filterobjekte geben
    /// </summary>
    public class FilterAdapter
    {
    
        GrantedUser myUser;
        String myClient;
        
    
        public FilterAdapter(GrantedUser user, String client)
        {
        
            myUser = user;
            myClient = client;
        }
        
        
        public List<PicklistItem> AuList()
        {            
        
            if (!myUser.IsClientUser && !myUser.IsAdmin) 
                return GetItems(String.Format("SELECT auid,aucode || ' - ' || CAST(aukurzb as varchar(128)),CAST(la as varchar(10)) FROM tbl_auftrag WHERE aukeyauftnehmer={0} ORDER BY aucode", myUser.Id));
            return GetItems("SELECT auid,aucode || ' - ' || CAST(aukurzb as varchar(128)),CAST(la as varchar(10)) FROM tbl_auftrag ORDER BY aucode");
        }

        
        public List<PicklistItem> BereichList()
        {
        
            return GetItems("SELECT id_auftgb,auauftragg,'' FROM tbl_auftgb ORDER BY auauftragg");
        }


        public List<PicklistItem> IHList()
        {
        
            return GetItems("SELECT ihid,ihnr || ' - ' || ihname,COALESCE(ihinventnr, '') FROM tblih ORDER BY ihnr");
        }


        public List<PicklistItem> KstList()
        {
        
            if (String.IsNullOrEmpty(myUser.KstFilter)) 
                return GetItems("SELECT CAST(id_kst AS int),kstnr || ' - ' || kstname, '' FROM tblkst ORDER BY kstsort");
            return GetItems("SELECT CAST(id_kst AS int),kstnr || ' - ' || kstname, '' FROM tblkst WHERE true " + myUser.KstFilter + " ORDER BY kstsort");
        }


        public List<PicklistItem> LaList()
        {
        
            return GetItems("SELECT id_la,lanr || ' - ' || laname,'' FROM tblla ORDER BY lanr");
        }
        

        public List<PicklistItem> ProfilList()
        {
        
            String sql;
            if (myUser.IsClientUser) sql = String.Format("SELECT idprofile,name,'' FROM oasm.adprofile WHERE keyad={0} ORDER BY name", myUser.Id);
            else                     sql = String.Format("SELECT idprofile,name,'' FROM oasm.psprofile WHERE keyps={0} ORDER BY name", myUser.Id);
                                                  
            return GetItems(sql);
        }
        

        public List<PicklistItem> PzList()
        {
        
            return GetItems("SELECT id_pz,pzkey || ' - ' || pznr,pzkey FROM tblpz ORDER BY pzkey");
        }
        
        
        private List<PicklistItem> GetItems(String sql)
        {
    
            List<PicklistItem> list = new List<PicklistItem>();
        
            using (DbConnection cnc = DbFactory.CreateConnection(myClient))
            using (DbCommand cmd = DbFactory.CreateCommand(sql, cnc))
            using (DbDataReader rdr = cmd.ExecuteReader()) {

                while (rdr.Read()) list.Add(new PicklistItem { Id = rdr.GetInt32(0), Value1 = rdr.GetString(1), Value2 = rdr.GetString(2) }); 
            }

            return list;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using comain.Connect;
using comain.Schedule;
using comain.User;


namespace comain.Adapter
{
    
    /// <summary>
    /// Planung erstellen
    /// </summary>
    public class ScheduleBuilder
    {
  
  
#region D E F I N I T I O N S
    

        class Auftrag
        {
        
            public int         ihidbg;   
            public String      ihnrbg;   
            public String      ihnamebg;   
            public String      ihinventnrbg;   
            public int         ihidmo;   
            public String      ihnrmo;   
            public String      ihnamemo;   
            public String      ihinventnrmo;   
            public int         id_pz;   
            public String      pzkey;   
            public String      pznr;   
            public String      pzname;   
            public int         id_kst;   
            public String      kstnr;   
            public int         id_la;   
            public int         auid;   
            public String      aucode;   
            public String      aucodeextern;   
            public String      aukurzb;   
            public short       auzyklus;   
            public float       auprowoche;   
            public String      zyklus;   
            public short       aumenge;   
            public Decimal     auwert;   
            public float       stundenvorgabe;   
            public int         psid;   
            public String      psnr;   
            public String      psname;   
            public int         id_auftgb;   
            public String      bereich;   
        }
        
        
        enum AuColumn
        {
        
            ihidbg, ihnrbg, ihnamebg, ihinventnrbg, 
            ihidmo, ihnrmo, ihnamemo, ihinventnrmo,   
            id_pz, pzkey, pznr, pzname,   
            id_kst, kstnr, id_la,   
            auid, aucode, aucodeextern, aukurzb, auzyklus, auprowoche, zyklus, aumenge, auwert, stundenvorgabe,   
            psid, psnr, psname,   
            id_auftgb, bereich
        }
        
        
        enum PlanColumn
        {
            idautermin, atkeyauftrag, attermin, atfixed, atstatus, atmenge, atwert, atstunden
        }
        
        
        enum ImplColumn
        {
            id_realzyklus, key_au, rztyp, rzmenge, rzwert, rzstunden, aurealzyklus
        }
        
        
#endregion

    
        public ScheduleDTO Schedule { get; private set; }
    
    
        /// <summary>
        /// Planung erstellen
        /// </summary>
        /// <param name="client">zu verwendende DB/Mandant</param>
        /// <param name="user">Benutzertyp für Profil-,Filter-,Rechteinformationen</param>
        /// <param name="filter">Filterkriterien der Planung</param>
        public ScheduleBuilder(String client, GrantedUser user, Dictionary<String, List<String>> filter)
        {
        
            SQLBuilder sql = new SQLBuilder(user, filter);
            Schedule = new ScheduleDTO();
            
            using (DbConnection cnc = DbFactory.CreateConnection(client)) {
            
                var aufträge = new List<Auftrag>();

                using (DbCommand cmd = DbFactory.CreateCommand(sql.AuftragSQL, cnc)) 
                using (DbDataReader rdr = cmd.ExecuteReader()) {
                   
                    while (rdr.Read()) {
                    
                        Auftrag au = new Auftrag(); 
                        
                        au.ihidbg           = rdr.GetInt32((int)AuColumn.ihidbg);    
                        au.ihnrbg           = rdr.GetString((int)AuColumn.ihnrbg); 
                        au.ihnamebg         = rdr.GetString((int)AuColumn.ihnamebg); 
                        au.ihinventnrbg     = (rdr.IsDBNull((int)AuColumn.ihinventnrbg))? String.Empty: rdr.GetString((int)AuColumn.ihinventnrbg); 
                        au.ihidmo           = rdr.GetInt32((int)AuColumn.ihidmo);    
                        au.ihnrmo           = rdr.GetString((int)AuColumn.ihnrmo); 
                        au.ihnamemo         = rdr.GetString((int)AuColumn.ihnamemo); 
                        au.ihinventnrmo     = (rdr.IsDBNull((int)AuColumn.ihinventnrmo))? String.Empty: rdr.GetString((int)AuColumn.ihinventnrmo);
                    
                        au.id_pz            = rdr.GetInt32((int)AuColumn.id_pz);    
                        au.pzkey            = rdr.GetString((int)AuColumn.pzkey);    
                        au.pznr             = rdr.GetString((int)AuColumn.pznr);    
                        au.pzname           = rdr.GetString((int)AuColumn.pzname);    
                        au.id_kst           = (int)rdr.GetDecimal((int)AuColumn.id_kst);
                        au.kstnr            = rdr.GetString((int)AuColumn.kstnr);
                        au.id_la            = rdr.GetInt32((int)AuColumn.id_la); 
                           
                        au.auid             = rdr.GetInt32((int)AuColumn.auid);
                        au.aucode           = rdr.GetString((int)AuColumn.aucode);
                        au.aucodeextern     = (rdr.IsDBNull((int)AuColumn.aucodeextern))? String.Empty: rdr.GetString((int)AuColumn.aucodeextern);
                        au.aukurzb          = rdr.GetString((int)AuColumn.aukurzb);
                        au.auzyklus         = (rdr.IsDBNull((int)AuColumn.auzyklus))?    (short)0: rdr.GetInt16((int)AuColumn.auzyklus);
                        au.auprowoche       = (rdr.IsDBNull((int)AuColumn.auprowoche))?  0: (float)rdr.GetDecimal((int)AuColumn.auprowoche);
                        au.zyklus           = (rdr.IsDBNull((int)AuColumn.zyklus))?      String.Empty: rdr.GetString((int)AuColumn.zyklus);
                        au.aumenge          = (rdr.IsDBNull((int)AuColumn.aumenge))?     (short)1: rdr.GetInt16((int)AuColumn.aumenge);
                        au.auwert           = (rdr.IsDBNull((int)AuColumn.auwert))?      0: rdr.GetDecimal((int)AuColumn.auwert);
                        au.stundenvorgabe   = (rdr.IsDBNull((int)AuColumn.stundenvorgabe))? 0: (float)rdr.GetDecimal((int)AuColumn.stundenvorgabe);
                        
                        au.psid             = (rdr.IsDBNull((int)AuColumn.psid))?        0: rdr.GetInt32((int)AuColumn.psid);
                        au.psnr             = (rdr.IsDBNull((int)AuColumn.psnr))?        String.Empty: rdr.GetString((int)AuColumn.psnr);
                        au.psname           = (rdr.IsDBNull((int)AuColumn.psname))?      String.Empty: rdr.GetString((int)AuColumn.psname);
                        au.id_auftgb        = (rdr.IsDBNull((int)AuColumn.id_auftgb))?   0: rdr.GetInt32((int)AuColumn.id_auftgb);
                        au.bereich          = (rdr.IsDBNull((int)AuColumn.bereich))?     String.Empty: rdr.GetString((int)AuColumn.bereich);
                     
                        aufträge.Add(au);    
                    }            
                }
          
                //Fliegengewichte    
                var audic = from row in aufträge
                            select new AUDetail { Code = row.aucode, CodeExtern = row.aucodeextern, Id = row.auid, Kurzbeschreibung = row.aukurzb,
                                               Menge = row.aumenge, ProWoche = row.auprowoche, Stunden = row.stundenvorgabe, Wert = row.auwert,
                                               Zyklus = row.auzyklus, ZyklusAsString = row.zyklus }; 
                Schedule.AuftragInfo = audic.Distinct().ToDictionary(p => p.Id);
            
                var br = from row in aufträge
                         select new BereichDetail { Id = row.id_auftgb, Name = row.bereich };
                Schedule.BereichInfo = br.Distinct().ToDictionary(p => p.Id);                     
        
                var ihbg = from row in aufträge
                           select new IHDetail { Id = row.ihidbg, Nummer = row.ihnrbg, Name = row.ihnamebg, InventarNummer = row.ihinventnrbg };
                var ihmo = from row in aufträge
                           select new IHDetail { Id = row.ihidmo, Nummer = row.ihnrmo, Name = row.ihnamemo, InventarNummer = row.ihinventnrmo };
                Schedule.IHObjektInfo = ihbg.Union(ihmo).Distinct().ToDictionary(p => p.Id);
                     
                var kst = from row in aufträge
                          select new KSTDetail { Id = row.id_kst, Nummer = row.kstnr };
                Schedule.KostenstellenInfo = kst.Distinct().ToDictionary(p => p.Id);
        
                var ps = from row in aufträge
                         select new PSDetail { Id = row.psid, Nummer = row.psnr, Name = row.psname };
                Schedule.PersonalInfo = ps.Distinct().ToDictionary(p => p.Id);
            
                var pz = from row in aufträge
                         select new PZDetail { Id = row.id_pz, Key = row.pzkey, Name = row.pzname, Nummer = row.pznr };
                Schedule.StandortInfo = pz.Distinct().ToDictionary(p => p.Id);
               
                var le = (from row in aufträge
                          select new Leistung { IhIdBgr = row.ihidbg, IhIdMo = row.ihidmo, PzId = row.id_pz, KstId = row.id_kst,
                                                AuId = row.auid, PsId = row.psid, BereichId = row.id_auftgb,
                                                Planungen = new List<Planung>(), Realisierungen = new List<Realisierung>() }).ToList();
                var ledict = le.ToDictionary(p => p.AuId);


                using (DbCommand cmd = DbFactory.CreateCommand(sql.PlannedSQL, cnc)) 
                using (DbDataReader rdr = cmd.ExecuteReader()) {
                   
                    while (rdr.Read()) {              
                      
                        var row = new Planung();
                        
                        row.Id              = rdr.GetInt32((int)PlanColumn.idautermin);
                        row.Auftrag         = rdr.GetInt32((int)PlanColumn.atkeyauftrag);
                        row.Date            = (rdr.IsDBNull((int)PlanColumn.attermin))?     DateTime.MinValue: rdr.GetDateTime((int)PlanColumn.attermin);
                        row.Fixed           = (rdr.IsDBNull((int)PlanColumn.atfixed))?      false: rdr.GetBoolean((int)PlanColumn.atfixed);
                        row.Status          = (rdr.IsDBNull((int)PlanColumn.atstatus))?     (short)0: rdr.GetInt16((int)PlanColumn.atstatus);
                        row.Menge           = (rdr.IsDBNull((int)PlanColumn.atmenge))?      (short)1: (short)rdr.GetInt32((int)PlanColumn.atmenge);
                        row.Wert            = (rdr.IsDBNull((int)PlanColumn.atwert))?       0: rdr.GetDecimal((int)PlanColumn.atwert);
                        row.Stunden         = (rdr.IsDBNull((int)PlanColumn.atstunden))?    0: (float)rdr.GetDecimal((int)PlanColumn.atstunden);
                    
                        if (ledict.ContainsKey(row.Auftrag)) ledict[row.Auftrag].Planungen.Add(row); 
                    }            
                }

                
                using (DbCommand cmd = DbFactory.CreateCommand(sql.ImplementedSQL, cnc)) 
                using (DbDataReader rdr = cmd.ExecuteReader()) {
                   
                    while (rdr.Read()) {
                      
                        Realisierung row = new Realisierung();
                        
                        row.Id              = rdr.GetInt32((int)ImplColumn.id_realzyklus); 
                        row.Auftrag         = rdr.GetInt32((int)ImplColumn.key_au); 
                        row.Sonder          = (rdr.IsDBNull((int)ImplColumn.rztyp))?        false: rdr.GetInt32((int)ImplColumn.rztyp) != 0;
                        row.Menge           = (rdr.IsDBNull((int)ImplColumn.rzmenge))?      (short)1: (short)rdr.GetInt32((int)ImplColumn.rzmenge);
                        row.Wert            = (rdr.IsDBNull((int)ImplColumn.rzwert))?       0: rdr.GetDecimal((int)ImplColumn.rzwert);
                        row.Stunden         = (rdr.IsDBNull((int)ImplColumn.rzstunden))?    0: (float)rdr.GetDecimal((int)ImplColumn.rzstunden);
                        row.Date            = rdr.GetDateTime((int)ImplColumn.aurealzyklus);
                    
                        if (ledict.ContainsKey(row.Auftrag)) ledict[row.Auftrag].Realisierungen.Add(row); 
                    }            
                }

                if (filter["Proj"].Contains("ShowRelevant")) Schedule.Leistungen = le.Where(p => p.IstRelevant).ToList();
                else                                         Schedule.Leistungen = le.ToList();
            }            
        }    
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using comain.User;


namespace comain.Adapter
{


    /// <summary>
    /// SQL-String für Planung zusammenstellen
    /// </summary>
    internal class SQLBuilder 
    {
        
        Dictionary<String, List<String>> filter;
        String period;
        GrantedUser user;
        
        
        /// <summary>
        /// SQL-String für Aufträge
        /// </summary>
        public String AuftragSQL { get; private set; }
        /// <summary>
        /// SQL-String für geplante Termine
        /// </summary>
        public String PlannedSQL { get; private set; }
        /// <summary>
        /// SQL-String für realisierte Termine
        /// </summary>
        public String ImplementedSQL { get; private set; }
        
        
        /// <summary>
        /// SQL-Erbauer für Schedule
        /// </summary>
        /// <param name="user">Benutzertyp für Profil-,Filter-,Rechteinformationen</param>
        /// <param name="filter">Filterkriterien der Planung</param>
        public SQLBuilder(GrantedUser user, Dictionary<String, List<String>> filter)
        {

            this.filter = filter;
            this.user = user;
            
            DateTime von = DateTime.Parse(filter["Period"][0]);
            DateTime bis = DateTime.Parse(filter["Period"][1]);
            period = String.Format(" BETWEEN '{0}' AND '{1}'", von.ToShortDateString(), bis.ToShortDateString());

            CreateAuftragSQL();
            CreatePlanSQL();
            CreateImplSQL();
        }

        
        private void CreateAuftragSQL()
        {
        
            StringBuilder bld = new StringBuilder();
            
            bld.AppendLine(@"SELECT i1.ihid AS ihidbg,i1.ihnr AS ihnrbg,i1.ihname AS ihnamebg,i1.ihinventnr AS ihinventnrbg,
                         i2.ihid AS ihidmo,i2.ihnr AS ihnrmo,i2.ihname AS ihnamemo,i2.ihinventnr AS ihinventnrmo,
                         id_pz,pzkey,pznr,pzname,id_kst,kstnr,id_la,
                         auid,aucode,aucodeextern,aukurzb,auzyklus,auprowoche,
                         formatcycle(auzyklus,auprowoche,COALESCE((SELECT CAST(wert AS int4) FROM tblglobal WHERE entitaet='FormatDailyCycle'), 0)) AS zyklus,
                         aumenge,auwert,stundenvorgabe,
                         psid,psnr,psnachname || COALESCE(', ' || psvorname, '') AS psname,                         
                         id_auftgb,auauftragg AS bereich");
            BuildJoin(bld);
            bld.Append(@" LEFT JOIN tblps ON aukeyauftnehmer=psid");             
            BuildWhere(bld);

            if (filter["Proj"].Contains("ShowWithHoursOnly")) 
                bld.Append(@" AND (SELECT SUM(COALESCE(atstunden, 0)) FROM tblautermin WHERE CAST(attermin AS date)").Append(period).Append(@" AND atkeyauftrag=auid) > 0");

            BuildOrderBy(bld);
               
            AuftragSQL = bld.ToString();        
        }

        
        private void CreatePlanSQL()
        {
        
            StringBuilder bld = new StringBuilder();
            
            bld.AppendLine(@"SELECT idautermin,atkeyauftrag,attermin,atfixed,atstatus,atmenge,atwert,atstunden");
            BuildJoin(bld);
            bld.Append(@" JOIN (SELECT * FROM tblautermin WHERE CAST(attermin AS date)").Append(period).Append(@") tblautermin ON atkeyauftrag=auid");             
            BuildWhere(bld);
            bld.Append(@" ORDER BY attermin,atkeyauftrag");
                                     
            PlannedSQL = bld.ToString();
        }

        
        private void CreateImplSQL()
        {
        
            StringBuilder bld = new StringBuilder();
            
            bld.AppendLine(@"SELECT id_realzyklus,key_au,rztyp,rzmenge,rzwert,rzstunden,aurealzyklus");
            BuildJoin(bld);
            bld.Append(@" JOIN (SELECT * FROM tblrealzyklus WHERE CAST(aurealzyklus AS date)").Append(period).Append(@") tblrealzyklus ON key_au=auid");
            BuildWhere(bld);
            bld.Append(@" ORDER BY aurealzyklus,key_au");
               
            ImplementedSQL = bld.ToString();        
        }
        
        
        private void BuildJoin(StringBuilder bld)
        {

            bld.Append(@" 
                         FROM tbl_auftrag
                         JOIN tbl_ih_kst ON auihks=id_ih_kst
                         JOIN tblkst ON id_kst=kst
                         JOIN tblih i1 ON ih=i1.ihid
                         JOIN tblih i2 ON getmainih(i1.ihnr) = i2.ihnr
                         JOIN tblpz ON id_pz=i1.ihpzid
                         JOIN tblla ON id_la=la
                         LEFT JOIN tbl_auftgb ON id_auftgb=auftgb");
        }
        
        
        private void BuildWhere(StringBuilder bld)
        {
            
            bld.Append(@" WHERE iswimarked AND au_aktiv = 0");

            if (!user.IsClientUser && !user.IsAdmin) bld.Append(" AND aukeyauftnehmer=" + user.Id);
            if (!String.IsNullOrEmpty(user.KstFilter)) bld.Append(user.KstFilter);
            
            if (!filter["Proj"].Contains("ShowBedarf")) {
            
                bld.Append(@" AND (AUZyklus Is NOT NULL AND AUStartKW Is NOT NULL OR AUProWoche > 0)");
            }

            if (filter["PZ"].Count == 2) bld.Append(" AND pzkey BETWEEN '").Append(filter["PZ"][0]).Append("' AND '").Append(filter["PZ"][1]).Append("'");

            if (filter["IH"].Count > 0) {
            
                String s = filter["IH"][0];
                if (s.StartsWith("MAIN ")) bld.Append(" AND i2.ihid=").Append(s.Substring(5));
                else                       bld.Append(" AND i1.ihid=").Append(s);
            }
            
            if (filter["Profil"].Count > 0) {
            
                if (user.IsClientUser) bld.Append(" AND auid IN (SELECT keyau FROM oasm.auadprofile WHERE keyprofile=").Append(filter["Profil"][0]).Append(")");
                else                     bld.Append(" AND auid IN (SELECT keyau FROM oasm.aupsprofile WHERE keyprofile=").Append(filter["Profil"][0]).Append(")");
            }
            
            if (filter["Bereich"].Count > 0) bld.Append(" AND auftgb=").Append(filter["Bereich"][0]);
            
            if (filter["LA"].Count > 0) {
            
                bld.Append(" AND id_la IN (").Append(String.Join(",", filter["LA"].ToArray())).Append(")");
            }

            if (filter["KST"].Count > 0) {
            
                if (filter["KST"][0].StartsWith("RANGE:")) bld.Append(filter["KST"][0].Substring(6));
                else                                         bld.Append(" AND id_kst=").Append(filter["KST"][0]);
            }
            
            if (filter["AU"].Count > 0) bld.Append(" AND auid=").Append(filter["AU"][0]);
        }

        
        private void BuildOrderBy(StringBuilder bld)
        {
        
            if (filter["Sort"].Count == 0) return;
        
            Dictionary<String, String> map = new Dictionary<string,string>(9);
            map.Add("KST",          "kstsort");
            map.Add("PZ",           "pzkey");
            map.Add("IH",           "i1.ihnr");
            map.Add("BEREICH",      "bereich");
            map.Add("AUCODEEXTERN", "aucodeextern");
            map.Add("AUCODE",       "aucode");
            map.Add("IHINVENT",     "i1.ihinventnr");
            map.Add("IH DESC",      "i1.ihnr DESC");
            map.Add("AUCODE DESC",  "aucode DESC");
        
            bld.Append(" ORDER BY ");
            foreach (var entry in filter["Sort"]) bld.Append(map[entry]).Append(",");
            bld.Length--;
        }
    }
}

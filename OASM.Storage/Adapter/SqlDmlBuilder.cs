﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using comain.Connect;
using comain.Log;


namespace comain.Adapter
{
    
    /// <summary>
    /// DML-SQL-Befehle für die Datenbankaktualisierung erzeugen
    /// </summary>
    /// <remarks>
    /// Bei inkpompatiblen Datenbanksystemen in AbstractFactoryPattern wandeln
    /// </remarks>
    public class SqlDmlBuilder
    {


#region D E F I N I T I O N S 

        /// <summary>
        /// Terminstatus
        /// </summary>
        public enum PlannedState 
        { 
            /// <summary>
            /// Termin ist noch zu realisieren
            /// </summary>
            ToDo = 0, 
            /// <summary>
            /// Termin ist bereits realisiert
            /// </summary>
            Implemented = 2 
        }
         
        /// <summary>
        /// Realisierungstyp
        /// </summary>
        public enum ImplementedType 
        { 
            /// <summary>
            /// normal (ein bestimmter Termin wurde realisert)
            /// </summary>
            Common = 0,
            /// <summary>
            /// Sonderleistung ohne Termin
            /// </summary>
            Special = 1 
        } 

#endregion


        List<DbCommand> cmdlist;
        String username;
        int userid;
        bool isClientUser;
        
        
        /// <summary>
        /// DML-SQL-Befehle für die Datenbankaktualisierung bereitstellen
        /// </summary>
        public SqlDmlBuilder(String username, int userid, bool isClientUser)
        {
            
            this.username = username;
            this.userid = userid;
            this.isClientUser = isClientUser;
            
            cmdlist = new List<DbCommand>(0);
        }


        public void InsertBegin()
        {
            
            cmdlist.Add(DbFactory.CreateCommand("BEGIN"));
        }


        public void InsertCommit()
        {
            
            cmdlist.Add(DbFactory.CreateCommand("COMMIT"));
        }
        
        /// <summary>
        /// Geplanten Termin löschen
        /// </summary>
        /// <param name="id">tblautermin.idautermin</param>
        public void DeletePlanned(int id)
        {
    
            cmdlist.Add(LogEntryFactory.GetUploadDateEntry(username, userid, isClientUser, UploadType.DateDelete, id));

            DbCommand cmd = DbFactory.CreateCommand("DELETE FROM tblautermin WHERE idautermin=:id");
            cmd.Parameters.Add(DbFactory.CreateParameter("id", DbType.Int32));
            cmd.Parameters[0].Value = id;
            cmdlist.Add(cmd);            
        }
        
        /// <summary>
        /// Terminrealisierung löschen
        /// </summary>
        /// <param name="id">tblrealzyklus.id_realzyklus</param>
        public void DeleteImplemented(int id)
        {

            cmdlist.Add(LogEntryFactory.GetUploadImplEntry(username, userid, isClientUser, UploadType.ImplDelete, id));

            DbCommand cmd = DbFactory.CreateCommand("DELETE FROM tblrealzyklus WHERE id_realzyklus=:id");
            cmd.Parameters.Add(DbFactory.CreateParameter("id", DbType.Int32));
            cmd.Parameters[0].Value = id;
            cmdlist.Add(cmd);            
        }
        
        /// <summary>
        /// Geplanten Termin aktualisieren
        /// </summary>
        /// <param name="id">tblautermin.idautermin</param>
        /// <param name="menge">Neue tblautermin.atmenge</param>
        public void UpdatePlanned(int id, short menge)
        {
        
            DbCommand cmd = DbFactory.CreateCommand("UPDATE tblautermin SET atmenge=:menge WHERE idautermin=:id");
            cmd.Parameters.Add(DbFactory.CreateParameter("id", DbType.Int32));
            cmd.Parameters.Add(DbFactory.CreateParameter("menge", DbType.Int16));
            cmd.Parameters[0].Value = id;
            cmd.Parameters[1].Value = menge;
            cmdlist.Add(cmd);            

            cmdlist.Add(LogEntryFactory.GetUploadDateEntry(username, userid, isClientUser, UploadType.DateUpdate, id));
        }       

        /// <summary>
        /// Terminstatus setzen => alte Online-Replikation. z.Z. nicht genutzt 
        /// </summary>
        /// <param name="id">tblautermin.idautermin</param>
        /// <param name="state">tblautermin.atstatus</param>
        public void SetPlannedState(int id, PlannedState state)
        {
        
            DbCommand cmd = DbFactory.CreateCommand("UPDATE tblautermin SET atstatus=:status WHERE idautermin=:id");
            cmd.Parameters.Add(DbFactory.CreateParameter("id", DbType.Int32));
            cmd.Parameters.Add(DbFactory.CreateParameter("status", DbType.Int32));
            cmd.Parameters[0].Value = id;
            cmd.Parameters[1].Value = state;
            cmdlist.Add(cmd);            
        }
        
        /// <summary>
        /// Neuen geplanten Termin eintragen
        /// </summary>
        /// <param name="auid">tblautermin.atkeyauftrag</param>
        /// <param name="psid">tblautermin.atkeybearbeiter</param>
        /// <param name="menge">tblautermin.atmenge</param>
        /// <param name="date">tblautermin.attermin</param>
        public void InsertPlanned(int auid, int psid, short menge, DateTime date)
        {
            String sql = @"INSERT INTO tblautermin 
                           (atkeyauftrag, atkeybearbeiter, atfixed, atstatus, atmenge, atwert, atstunden, attermin)
                           SELECT auid, :psid, true, 1, :menge, auwert, stundenvorgabe, :termin FROM tbl_auftrag WHERE auid=:auid";
            DbCommand cmd = DbFactory.CreateCommand(sql);
            cmd.Parameters.Add(DbFactory.CreateParameter("auid", DbType.Int32));
            cmd.Parameters.Add(DbFactory.CreateParameter("psid", DbType.Int32));
            cmd.Parameters.Add(DbFactory.CreateParameter("menge", DbType.Int16));
            cmd.Parameters.Add(DbFactory.CreateParameter("termin", DbType.Date));
            cmd.Parameters[0].Value = auid;
            cmd.Parameters[1].Value = psid;
            cmd.Parameters[2].Value = menge;
            cmd.Parameters[3].Value = date;
            cmdlist.Add(cmd); 
            
            cmdlist.Add(LogEntryFactory.GetUploadDateInsertEntry(username, userid, isClientUser));           
        }

        /// <summary>
        /// Terminrealisierung aktualisieren
        /// </summary>
        /// <param name="id">tblrealzyklus.id_realzyklus</param>
        /// <param name="type">tblrealzyklus.rztyp (1: Sonderleistung)</param>
        /// <param name="menge">tblrealzyklus.rzmenge</param>
        public void UpdateImplemented(int id, ImplementedType type, int menge)
        {
            DbCommand cmd = DbFactory.CreateCommand("UPDATE tblrealzyklus SET rzmenge=:menge,rztyp=:typ WHERE id_realzyklus=:id");
            cmd.Parameters.Add(DbFactory.CreateParameter("id", DbType.Int32));
            cmd.Parameters.Add(DbFactory.CreateParameter("menge", DbType.Int32));
            cmd.Parameters.Add(DbFactory.CreateParameter("typ", DbType.Int32));
            cmd.Parameters[0].Value = id;
            cmd.Parameters[1].Value = menge;
            cmd.Parameters[2].Value = type;
            cmdlist.Add(cmd);            

            cmdlist.Add(LogEntryFactory.GetUploadImplEntry(username, userid, isClientUser, UploadType.ImplUpdate, id));
        }

        /// <summary>
        /// Termin realisieren
        /// </summary>
        /// <param name="auid">tblrealzyklus.key_au</param>
        /// <param name="date">tblrealzyklus.aurealzyklus</param>
        /// <param name="type">tblrealzyklus.rztyp (1: Sonderleistung)</param>
        /// <param name="menge">tblrealzyklus.rzmenge</param>
        public void InsertImplemented(int auid, DateTime date, ImplementedType type, int menge)
        {
            
            String sql = @"INSERT INTO tblrealzyklus(
                           key_au, rzkeykst, rzkstnr, rztyp, rzmenge, rzwert, rzstunden, aurealzyklus)
                           SELECT auid, id_kst, kstnr, :typ, :menge, auwert, stundenvorgabe, :termin  
                           FROM tbl_auftrag 
                           JOIN tbl_ih_kst ON auihks=id_ih_kst
                           JOIN tblkst ON kst=id_kst 
                           WHERE auid=:auid";                        
            DbCommand cmd = DbFactory.CreateCommand(sql);
            cmd.Parameters.Add(DbFactory.CreateParameter("auid", DbType.Int32));
            cmd.Parameters.Add(DbFactory.CreateParameter("typ", DbType.Int32));
            cmd.Parameters.Add(DbFactory.CreateParameter("menge", DbType.Int32));
            cmd.Parameters.Add(DbFactory.CreateParameter("termin", DbType.Date));
            cmd.Parameters[0].Value = auid;
            cmd.Parameters[1].Value = type;
            cmd.Parameters[2].Value = menge;
            cmd.Parameters[3].Value = date;
            cmdlist.Add(cmd);            
            
            cmdlist.Add(LogEntryFactory.GetUploadImplInsertEntry(username, userid, isClientUser));           
        }
        
        /// <summary>
        /// fertige SQL-Befehlsliste übergeben
        /// </summary>
        public List<DbCommand> CommandList { get { return cmdlist; } }
    
    } 
}

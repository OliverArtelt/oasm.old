﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.IO;
using System.Security.Authentication;
using CO.Crypt;
using comain.Connect;
using comain.Log;
using comain.User;


namespace comain.Adapter
{
    
    public static class GrantedUserFactory
    {
    
        public static GrantedUser CreateUser(Credentials authinfo)
        {
        
            if (authinfo.IsClientUser) return CreateClientUser(authinfo, true);
            else                       return CreateServiceUser(authinfo, true); 
        }
    
        public static GrantedUser InitializeUser(Credentials authinfo)
        {
        
            if (authinfo.IsClientUser) return CreateClientUser(authinfo, false);
            else                       return CreateServiceUser(authinfo, false); 
        }

        
        /// <summary>
        /// Dienstleisterzugang: Zugangsdaten verifizieren und Administrator einrichten
        /// </summary>
        /// <param name="authinfo">Login-Informationen</param>
        /// <param name="logEvent">soll erfolgreicher Login protokolliert werden?</param>
        private static GrantedUser CreateServiceUser(Credentials authinfo, bool logEvent)
        {
        

            if (authinfo.Username == "") throw new AuthenticationException("Geben Sie einen Benutzernamen an.");
                        
            
            using (DbConnection cnc = DbFactory.CreateConnection(authinfo.Client)) {
           

                //Test ob Datenbank für OASM eingerichtet ist == Schema oasm existiert
                DbCommand cmd = DbFactory.CreateCommand("SELECT COUNT(*) FROM oasm.adept", cnc);
                cmd.ExecuteScalar();
                
                String sql = "SELECT psid,psnachname || COALESCE(', ' || psvorname, '') AS name,pspasswort AS pass,psadmin " +
                             "FROM tblps WHERE pslogin=:user";
                cmd = DbFactory.CreateCommand(sql, cnc);
                cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String));
                cmd.Parameters[0].Value = authinfo.Username.Trim();
                
                DbDataReader rdr = cmd.ExecuteReader();
                if (!rdr.Read()) {
                
                    //Ablehnung protokollieren
                    DbCommand log = LogEntryFactory.GetLoginEntry(authinfo.Username, "Unbekannt");
                    log.Connection = cnc;
                    log.ExecuteNonQuery();

                    throw new AuthenticationException("Ungültiger Benutzer oder Kennwort.");
                }
                
                String hash = String.Empty;
                if (!rdr.IsDBNull(rdr.GetOrdinal("pass"))) hash = rdr.GetString(rdr.GetOrdinal("pass"));
                if (SimpleHash.VerifyHash(authinfo.Password, hash) == false) {
                
                    //Ablehnung protokollieren
                    DbCommand log = LogEntryFactory.GetLoginEntry(authinfo.Username, "Abgelehnt");
                    log.Connection = cnc;
                    log.ExecuteNonQuery();

                    throw new AuthenticationException("Ungültiger Benutzer oder Kennwort.");
                }
                
                bool admin = rdr.GetBoolean(rdr.GetOrdinal("psadmin"));
                GrantedUser user = new GrantedUser { Id = rdr.GetInt32(rdr.GetOrdinal("psid")), 
                                                     Name = rdr.GetString(rdr.GetOrdinal("name")),
                                                     IsAdmin = admin,
                                                     IsClientUser = false,
                                                     KstFilter = String.Empty,
                                                     MayPlan = admin,
                                                     SeeHours = true,
                                                     SeePrices = admin };
                                                     
                //Login protokollieren
                if (logEvent) {

                    DbCommand log1 = LogEntryFactory.GetLoginEntry(authinfo.Username, "Erfolgreich");
                    log1.Connection = cnc;
                    log1.ExecuteNonQuery();
                }
                
                return user;
            }
        }

        
        /// <summary>
        /// Meisterzugang: Zugangsdaten verifizieren und Meister einrichten
        /// </summary>
        /// <param name="authinfo">Login-Informationen</param>
        /// <param name="logEvent">soll erfolgreicher Login protokolliert werden?</param>
        private static GrantedUser CreateClientUser(Credentials authinfo, bool logEvent)
        {
        
        
            if (authinfo.Username == "") throw new AuthenticationException("Geben Sie einen Benutzernamen an.");
            String kstFilter = "";
            
            
            using (DbConnection cnc = DbFactory.CreateConnection(authinfo.Client)) {
            

                //Test ob Datenbank für OASM eingerichtet ist == Schema oasm existiert
                DbCommand cmd = DbFactory.CreateCommand("SELECT COUNT(*) FROM oasm.adept", cnc);
                cmd.ExecuteScalar();

                //Authentifizierung
                String sql = "SELECT idadept,name,pass,plannable,showhours,showprices FROM oasm.adept WHERE login=:user";
                
                cmd = DbFactory.CreateCommand(sql, cnc);
                cmd.Parameters.Add(DbFactory.CreateParameter("user", DbType.String));
                cmd.Parameters[0].Value = authinfo.Username.Trim();
                
                DbDataReader rdr = cmd.ExecuteReader();
                if (!rdr.Read()) {
                
                    //Ablehnung protokollieren
                    DbCommand log = LogEntryFactory.GetLoginEntry(authinfo.Username, "Unbekannt");
                    log.Connection = cnc;
                    log.ExecuteNonQuery();

                    throw new AuthenticationException("Ungültiger Benutzer oder Kennwort.");
                }
                
                if (rdr.IsDBNull(rdr.GetOrdinal("pass"))) {
                
                    //Ablehnung protokollieren
                    DbCommand log = LogEntryFactory.GetLoginEntry(authinfo.Username, "Abgelehnt");
                    log.Connection = cnc;
                    log.ExecuteNonQuery();

                    throw new AuthenticationException("Der Benutzer ist nicht für diese Anwendung vorgesehen.");
                }
                
                String hash = rdr.GetString(rdr.GetOrdinal("pass"));
                if (SimpleHash.VerifyHash(authinfo.Password, hash) == false) {
                
                    //Ablehnung protokollieren
                    DbCommand log = LogEntryFactory.GetLoginEntry(authinfo.Username, "Abgelehnt");
                    log.Connection = cnc;
                    log.ExecuteNonQuery();

                    throw new AuthenticationException("Ungültiger Benutzer oder Kennwort.");
                }
                
                int id           = rdr.GetInt32(rdr.GetOrdinal("idadept"));
                String name      = rdr.GetString(rdr.GetOrdinal("name"));
                bool mayPlan     = rdr.GetBoolean(rdr.GetOrdinal("plannable"));
                bool seeHours    = rdr.GetBoolean(rdr.GetOrdinal("showhours"));
                bool seePrices   = rdr.GetBoolean(rdr.GetOrdinal("showprices"));
                
                //Kostenstellenlimits
                sql = "SELECT kstvon,kstbis FROM oasm.kstrole WHERE keyadept=" + id.ToString();
                cmd = DbFactory.CreateCommand(sql, cnc);
                rdr = cmd.ExecuteReader();

                //invariante Kultur (SQL-Decimals) verwenden
                using (StringWriter wr = new StringWriter(new CultureInfo(""))) {
                    
                    while (rdr.Read()) {
                    
                        if (rdr.IsDBNull(1)) {
                        
                            wr.Write(" OR kstsort="); 
                            wr.Write(rdr.GetDecimal(0)); 
                    
                        } else {
                        
                            wr.Write(" OR kstsort BETWEEN "); 
                            wr.Write(rdr.GetDecimal(0)); 
                            wr.Write(" AND "); 
                            wr.Write(rdr.GetDecimal(1)); 
                        }
                    }
                    
                    kstFilter = wr.ToString();
                    if (kstFilter != "") kstFilter = " AND (" + kstFilter.Substring(4) + ")";
                }

                GrantedUser user = new GrantedUser { Id = id, IsClientUser = true, IsAdmin = false,
                                                     Name = name,
                                                     KstFilter = kstFilter, MayPlan = mayPlan, 
                                                     SeeHours = seeHours, SeePrices = seePrices };

                //Login protokollieren
                if (logEvent) {

                    DbCommand log1 = LogEntryFactory.GetLoginEntry(authinfo.Username, "Erfolgreich");
                    log1.Connection = cnc;
                    log1.ExecuteNonQuery();
                }
                
                return user;
            }
        }        
    }
}

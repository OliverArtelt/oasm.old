﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using comain.Schedule;
using comain.Connect;


namespace comain.Adapter
{
    
    /// <summary>
    /// Geänderte Termine in SQL-Anweisungen umwandeln, 
    /// Builder-Director verwendet Besucherschnittstelle des Schedules
    /// </summary>
    public class SqlDmlDirector
    {
  
        SqlDmlBuilder myBuilder;       
        
        /// <summary>
        /// fertigen Batch als DbCommand-Liste
        /// </summary>
        public List<DbCommand> CommandList { get { return myBuilder.CommandList; } }
        
        
        /// <summary>
        /// Geänderte Termine in DB-Befehlsobjekte wandeln
        /// </summary>
        /// <param name="obj">zu betrachtendes Terminobjekt</param>
        public SqlDmlDirector(SqlDmlBuilder bld)
        {
        
            myBuilder = bld;
        }
        
             
        public void Create(IEnumerable<ModifiedDate> list) 
        {
        
            foreach (var entry in list) {
            
                //keine Bearbeitung dieses Termines
                if (entry.EditedType == EditedType.Empty) continue; 
        
                //Termin und evtl. bereits erfolgte Realisierung wird gelöscht
                if (entry.EditedType == EditedType.Deleted) {
                
                    if (entry.ImplementedId > 0) myBuilder.DeleteImplemented(entry.ImplementedId);
                    if (entry.PlannedId > 0)     myBuilder.DeletePlanned(entry.PlannedId);
                }

                //Realisierung des Termines wurde gelöscht
                // => wurde wieder auf Planstatus zurückgesetzt
                //oder Termin wird neu geplant
                if (entry.EditedType == EditedType.Planned) {
                
                    //existiert eine Realisierung?
                    if (entry.ImplementedId > 0) myBuilder.DeleteImplemented(entry.ImplementedId);
                    //existiert eine Planung? => Status rücksetzen
                    if (entry.PlannedId > 0) {
                    
                        myBuilder.UpdatePlanned(entry.PlannedId, entry.EditedMenge);
                        myBuilder.SetPlannedState(entry.PlannedId, SqlDmlBuilder.PlannedState.ToDo);
                    
                    } else {
                    
                        myBuilder.InsertPlanned(entry.AuftragId, entry.PersonalId, entry.EditedMenge, entry.FirstDate);
                    }    
                }

                //Termin wird realisiert
                if (entry.EditedType == EditedType.Common || entry.EditedType == EditedType.Daily) {
                
                    //bereits realisiert => aktualisieren
                    if (entry.ImplementedId > 0) myBuilder.UpdateImplemented(entry.ImplementedId, SqlDmlBuilder.ImplementedType.Common, entry.EditedMenge);
                    else {
                    
                        DateTime date = (entry.PlannedId > 0)? entry.PlannedDate: entry.FirstDate;
                        myBuilder.InsertImplemented(entry.AuftragId, date, SqlDmlBuilder.ImplementedType.Common, entry.EditedMenge);
                    }
                    
                    //zugehöriger Termin auf realisiert setzen
                    if (entry.PlannedId > 0) myBuilder.SetPlannedState(entry.PlannedId, SqlDmlBuilder.PlannedState.Implemented);
                }

                //Termin wird als Sonderleistung realisiert
                if (entry.EditedType == EditedType.Special) {
                
                    //bereits realisiert => aktualiseren
                    if (entry.ImplementedId > 0) myBuilder.UpdateImplemented(entry.ImplementedId, SqlDmlBuilder.ImplementedType.Special, entry.EditedMenge);
                    else {
                    
                        myBuilder.InsertImplemented(entry.AuftragId, entry.FirstDate, SqlDmlBuilder.ImplementedType.Special, entry.EditedMenge);
                    }

                    //zugehöriger Termin auf ToDo rücksetzen
                    if (entry.PlannedId > 0) myBuilder.SetPlannedState(entry.PlannedId, SqlDmlBuilder.PlannedState.ToDo);
                }
            }
        }
    }
}

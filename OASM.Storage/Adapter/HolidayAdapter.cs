﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using comain.Calendar;
using comain.Connect;
using System.Data;


namespace comain.Adapter
{
    
    public class HolidayAdapter
    {
    
        String myClient;
    
    
        public HolidayAdapter(String client)
        {
        
            myClient = client;
        }
        
        
        public List<Vacation> GetVacationCloseDown(DateTime von, DateTime bis)
        {
                   
            List<Vacation> list = new List<Vacation>();
                        
            
            using (DbConnection cnc = DbFactory.CreateConnection(myClient)) {
            
                String sql = @"SELECT periodename,periodevon,periodebis 
                               FROM tblauzyklusfrei
                               WHERE periodebis >= :v AND periodevon <= :b";
                DbCommand cmd = DbFactory.CreateCommand(sql, cnc);
                cmd.Parameters.Add(DbFactory.CreateParameter("v", DbType.DateTime));
                cmd.Parameters.Add(DbFactory.CreateParameter("b", DbType.DateTime));
                cmd.Parameters[0].Value = von;
                cmd.Parameters[1].Value = bis;
                
                using (DbDataReader rdr = cmd.ExecuteReader()) {
                    
                    while (rdr.Read()) list.Add(new Vacation { Name = rdr.GetString(0), StartDate = rdr.GetDateTime(1), 
                                                               EndDate = rdr.GetDateTime(2) });
                }
            }
            
            return list;
        }

        
        public List<Holiday> GetHoliday(DateTime von, DateTime bis)
        {
               
            List<Holiday> list = new List<Holiday>();


            using (DbConnection cnc = DbFactory.CreateConnection(myClient)) {
            
                String sql = @"SELECT fttext,ftdatum  
                               FROM tblfeiertag
                               WHERE ftdatum BETWEEN :v AND :b";
                DbCommand cmd = DbFactory.CreateCommand(sql, cnc);
                cmd.Parameters.Add(DbFactory.CreateParameter("v", DbType.DateTime));
                cmd.Parameters.Add(DbFactory.CreateParameter("b", DbType.DateTime));
                cmd.Parameters[0].Value = von;
                cmd.Parameters[1].Value = bis;
                
                using (DbDataReader rdr = cmd.ExecuteReader()) {
                    
                    while (rdr.Read()) list.Add(new Holiday { Name = rdr.GetString(0), Date = rdr.GetDateTime(1) });
                }
            }
            
            return list;
        }
    }
}

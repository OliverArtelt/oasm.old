﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Security.Authentication;
using comain.Connect;
using comain.User;


namespace comain.Adapter
{

    /// <summary>
    /// Profilverwaltung des Benutzers
    /// </summary>
    public class ProfileAdapter
    {
    
        String myClient;
        GrantedUser myUser;
        
        readonly String profileTable;
        readonly String profilePsKey;
        readonly String profileMapTable;
        readonly String profileSequence;
        
        
        public ProfileAdapter(String client, GrantedUser user)
        {
        
            myUser = user;
            myClient = client;
            
            if (myUser.IsClientUser) {
            
                profileTable = "oasm.adprofile";
                profilePsKey = "keyad";
                profileMapTable = "oasm.auadprofile";
                profileSequence = "'oasm.seq_adprofile'";
            
            } else {
            
                profileTable = "oasm.psprofile";
                profilePsKey = "keyps";
                profileMapTable = "oasm.aupsprofile";
                profileSequence = "'oasm.seq_psprofile'";
            }
        } 
        
        
        /// <summary>
        /// Neues Profil anlegen
        /// </summary>
        public int Create(String profilName, IList<int> auList)
        {

            if (profilName == "") throw new ApplicationException("Das neue Profil besitzt keinen Namen.");
            
            using (DbConnection cnc = DbFactory.CreateConnection(myClient)) {

                String sql = String.Format("INSERT INTO {0} (idprofile,{1},name) VALUES (NEXTVAL({2}),{3},:name)", 
                                           profileTable, profilePsKey, profileSequence, myUser.Id);
                DbCommand cmd = DbFactory.CreateCommand(sql, cnc);
                cmd.Parameters.Add(DbFactory.CreateParameter("name", DbType.String));
                cmd.Parameters[0].Value = profilName;                           
                cmd.ExecuteNonQuery();
                    
                sql = String.Format("INSERT INTO {0} (keyprofile,keyau) VALUES (CURRVAL({1}),:id)", profileMapTable, profileSequence);
            
                cmd = DbFactory.CreateCommand(sql, cnc);
                cmd.Parameters.Add(DbFactory.CreateParameter("id", DbType.Int32));
                cmd.Prepare();
                
                foreach (int id in auList) {
                
                    cmd.Parameters[0].Value = id;
                    cmd.ExecuteNonQuery();
                }  
                
                sql = String.Format("SELECT CURRVAL({0})", profileSequence);
                
                cmd = DbFactory.CreateCommand(sql, cnc);
                long did = (long)cmd.ExecuteScalar();
                return (int)did;
            }
        }
        

        /// <summary>
        /// Aufträge des Profiles um neue ergänzen
        /// </summary>
        public void Add(int profilId, IList<int> auList)
        {

            if (profilId == 0) throw new ApplicationException("Kein Profil gewählt.");    
            
            using (DbConnection cnc = DbFactory.CreateConnection(myClient)) {
            
                CheckProfile(profilId, cnc);

                String sql = String.Format("SELECT keyau FROM {0} WHERE keyprofile={1}", profileMapTable, profilId);
                DbCommand cmd = DbFactory.CreateCommand(sql, cnc);
                DbDataReader rdr = cmd.ExecuteReader();
                
                List<int> old = new List<int>();
                while (rdr.Read()) old.Add(rdr.GetInt32(0));
                
                sql = String.Format("INSERT INTO {0} (keyprofile,keyau) VALUES ({1},:id)", profileMapTable, profilId);
                cmd = DbFactory.CreateCommand(sql, cnc);
                cmd.Parameters.Add(DbFactory.CreateParameter("id", DbType.Int32));
                cmd.Prepare();
                
                foreach (int id in auList.Except(old)) {
                
                    cmd.Parameters[0].Value = id;
                    cmd.ExecuteNonQuery();
                }                                     
            } 
        }


        /// <summary>
        /// Profil löschen
        /// </summary>
        public void Delete(int profilId)
        {

            if (profilId == 0) throw new ApplicationException("Kein Profil gewählt.");
            
            String sql = String.Format("DELETE FROM {0} WHERE idprofile={1}", profileTable, profilId);
            
            using (DbConnection cnc = DbFactory.CreateConnection(myClient)) {
             
                CheckProfile(profilId, cnc);

                DbCommand cmd = DbFactory.CreateCommand(sql, cnc);             
                cmd.ExecuteNonQuery();
            }
        }


        /// <summary>
        /// Aufträge des Profiles durch neue ersetzen
        /// </summary>
        public void Replace(int profilId, IList<int> auList)
        {

            if (profilId == 0) throw new ApplicationException("Kein Profil gewählt.");
            
            using (DbConnection cnc = DbFactory.CreateConnection(myClient)) {
            
                CheckProfile(profilId, cnc);
                String sql = String.Format("DELETE FROM {0} WHERE keyprofile={1}", profileMapTable, profilId);
            
                DbCommand cmd = DbFactory.CreateCommand(sql, cnc);
                cmd.ExecuteNonQuery();
                
                sql = String.Format("INSERT INTO {0} (keyprofile,keyau) VALUES ({1},:id)", profileMapTable, profilId);
            
                cmd = DbFactory.CreateCommand(sql, cnc);
                cmd.Parameters.Add(DbFactory.CreateParameter("id", DbType.Int32));
                cmd.Prepare();
                
                foreach (int id in auList) {
                
                    cmd.Parameters[0].Value = id;
                    cmd.ExecuteNonQuery();
                }               
            }
        }
        
        /// <summary>
        /// Prüfen ob fremde Profile manipuliert werden
        /// </summary>
        /// <param name="profilId"></param>
        /// <param name="cnc"></param>
        private void CheckProfile(int profilId, DbConnection cnc)
        {
        
            String sql = String.Format("SELECT COUNT(*) FROM {0} WHERE idprofile={1} AND {2}={3}", profileTable, profilId, profilePsKey, myUser.Id);
            DbCommand cmd = DbFactory.CreateCommand(sql, cnc);
            
            long count = (long)cmd.ExecuteScalar();
            if (count == 0) throw new AuthenticationException("Benutzer hat keinen Zugriff auf das Profil.");
        }
    }
}

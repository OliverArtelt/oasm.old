﻿using System;
using System.Collections.Generic;
using System.Data.Common;


namespace comain.Connect
{
    
    /// <summary>
    /// Sql-Kommandoliste abarbeiten
    /// </summary>
    /// <remarks>
    /// kein Transaktionssupport - Befehlsfolge muß in diesem Fall durch BEGIN + COMMIT-Kommandos umschlagen werden 
    /// </remarks>
    public class BatchExecuter
    {
        
        String client;
        
        
        public BatchExecuter(String db)
        {
        
            client = db;
        }
        
        /// <summary>
        /// Sql-Kommandoliste abarbeiten
        /// </summary>
        /// <param name="cmdList"></param>
        public void Execute(IEnumerable<DbCommand> cmdList)
        {
        
            using (DbConnection cnc = DbFactory.CreateConnection(client)) {
            
                foreach (DbCommand cmd in cmdList) {
                
                    cmd.Connection = cnc;
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}

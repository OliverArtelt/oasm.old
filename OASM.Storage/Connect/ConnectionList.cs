﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Xml;


namespace comain.Connect
{
      
    /// <summary>
    /// verfügbare DB-Verbindungen auslesen
    /// </summary>
    public class ConnectionList
    {
    
        /// <summary>
        /// Einzelne DB-Verbindung
        /// </summary>
        public class DBConnectInfo : IComparable<DBConnectInfo>
        {

            readonly String name;
            readonly String server;
            readonly String db;
            readonly String user;
            readonly String pass;
            readonly String port;
            readonly String ssl;


            public DBConnectInfo(String name_, String server_, String db_, String user_, String pass_, String port_, String ssl_)
            {
            
                if (name_ == "") throw new ApplicationException("Name der Datenbankverbindung muß angegeben werden.");
            
                name = name_;
                server = server_;
                db = db_.ToLower();
                user = user_;
                pass = pass_;            
                port = port_;            
                ssl = ssl_;            
            }
            
            
            /// <summary>
            /// Name der Verbindung geben
            /// </summary>
            public String Name { get { return name; } }
            
            
            /// <summary>
            /// Verbindungsangaben sortieren
            /// </summary>
            /// <param name="other">zu vergeleichende DB-Connection</param>
            /// <returns>übliches größer/kleiner/gleich</returns>
            public int CompareTo(DBConnectInfo other)
            {
                return String.Compare(this.name, other.name);
            }

            
            /// <summary>
            /// Verbindungsstring geben
            /// </summary>
            public override String ToString() 
            {

                DbConnectionStringBuilder bld = DbFactory.CreateConnectionStringBuilder();
                
                if (server != "") bld.Add("Server", server);
                if (db != "")     bld.Add("Database", db);
                if (user != "")   bld.Add("User Id", user);
                if (pass != "")   bld.Add("Password", pass);
                if (port != "")   bld.Add("Port", port);

                if (ssl.ToLower() == "true") {
                
                    //SSL-Support
                    bld.Add("Ssl", true);
                    bld.Add("Sslmode", "Require");      //Debug: Prefer, Require, Allow, Disable 
                }
                
                bld.Add("Timeout", 60);  //connection timeout
                bld.Add("CommandTimeout", 600);
                   
                return bld.ConnectionString;
            }
        }
        
        /// <summary>
        /// verfügbare DB-Verbindungen
        /// </summary>                  
        public IDictionary<String, DBConnectInfo> List { get; private set; }  

        
        /// <summary>
        /// verfügbare DB-Verbindungen auslesen
        /// </summary>
        public ConnectionList(XmlReader rdr)
        {
            
            String name = ""; 
            String server = "";
            String db = "";
            String user = "";
            String pass = "";
            String port = "";
            String ssl = "";
            
            
            //Elementtyp, dessen Textknoten gelesen wird
            String currentNodeType = null;
            //Verbindungsliste aufsetzen
            List = new Dictionary<String, DBConnectInfo>();

            
            while (rdr.Read()) {
            
                switch (rdr.NodeType) {
                
                case XmlNodeType.Element:
                
                    //neue Verbindung erstellen
                    if (rdr.Name == "dbconnection") {
                    
                        name = ""; 
                        server = "";
                        db = "";
                        user = "";
                        pass = "";
                        port = "";
                        ssl = "";
                        
                        while (rdr.MoveToNextAttribute()) 
                            if (rdr.Name == "name") name = rdr.Value;
                    }
                    
                    currentNodeType = rdr.Name;
                    break;
                    
                case XmlNodeType.Text:
                
                    switch (currentNodeType) {
                    
                    case "server":
                    
                        server = rdr.Value;
                        break;
                    
                    case "db":
                    
                        db = rdr.Value;
                        break;
                    
                    case "user":
                    
                        user = rdr.Value;
                        break;
                    
                    case "pass":
                    
                        pass = rdr.Value;
                        break;
                    
                    case "port":
                    
                        port = rdr.Value;
                        break;
                    
                    case "ssl":
                    
                        ssl = rdr.Value;
                        break;
                    }
                    break;
                    
                case XmlNodeType.EndElement:
                
                    //Verbindung vollständig eingelesen => Speichern
                    if (rdr.Name == "dbconnection") List.Add(name, new DBConnectInfo(name, server, db, user, pass, port, ssl));
                    break;
                }
            }
        }
        
        /// <summary>
        /// Verbindungsnamen für Listbox etc. auslesen 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<String> Names
        {
            get {
             
                return List.Keys.OrderBy(p => p);
            }    
        }
        
        /// <summary>
        /// DB-Verbindungsobjekt geben
        /// </summary>
        /// <param name="name">zu suchender DB-Verbindungsname</param>
        /// <returns>DB-Verbindungsobjekt, danach mit ToString() Verbindungsstring auslesen</returns>
        public DBConnectInfo this[String name]
        {
        
            get {
                
                return List[name];
            }
        }
    }
}

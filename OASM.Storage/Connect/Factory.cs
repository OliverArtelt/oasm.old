﻿using System;
using System.Data.Common;
using System.Xml;


namespace comain.Connect
{
    
    /// <summary>
    /// DB-Manipulationsobjekte
    /// </summary>
    public static class DbFactory
    {
    
        /// <summary>
        /// providerspezifisches Factory-Objekt als Singleton
        /// </summary>
        static DbProviderFactory factory = Npgsql.NpgsqlFactory.Instance;
        
        static String configPath;
        static ConnectionList myConnList;
        
        
        /// <summary>
        /// Factory aufsetzen: Typ ermitteln und Singleton instanziieren
        /// </summary>
        static DbFactory()
        {
        
            configPath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
            configPath += "db.config";
        
            using (XmlReader rdr = XmlReader.Create(configPath)) {
            
                myConnList = new ConnectionList(rdr);
            }
        }
        
        /// <summary>
        /// DbConnectionStringBuilder übergeben, um Verbindungsstring zusammenbauen zu können
        /// </summary>
        /// <returns>DbConnectionStringBuilder</returns>
        public static DbConnectionStringBuilder CreateConnectionStringBuilder() 
        { 
            return factory.CreateConnectionStringBuilder();
        } 

        /// <summary>
        /// Verbindung aus Einstellungen erzeugen, konfigurieren und öffnen
        /// </summary>
        /// <returns>offene Datenbankverbindung</returns>
        public static DbConnection CreateConnection(String client) 
        { 
        
            DbConnection conn = factory.CreateConnection();
            conn.ConnectionString = myConnList[client].ToString();
            conn.Open();
            
            return conn;
        } 

        /// <summary>
        /// Befehl für ExecuteScalar() usw. erzeugen
        /// </summary>
        /// <param name="sql">SQL-Anfragestring (je nach Anwendung Select oder DML)</param>
        /// <returns>Command-Objekt</returns>
        public static DbCommand CreateCommand(String sql) 
        { 
            DbCommand cmd = factory.CreateCommand();
            cmd.CommandText = sql;
            
            return cmd;
        }
        
        /// <summary>
        /// Befehl für ExecuteScalar() usw. erzeugen
        /// </summary>
        /// <param name="sql">SQL-Anfragestring (je nach Anwendung Select oder DML)</param>
        /// <param name="cnc">Datenbankverbindung</param>
        /// <returns>Command-Objekt</returns>
        public static DbCommand CreateCommand(String sql, DbConnection cnc) 
        { 
            DbCommand cmd = DbFactory.CreateCommand(sql);
            cmd.Connection = cnc;
            
            return cmd;
        }
        
        /// <summary>
        /// Adapter zum Befüllen einer DataTable erzeugen
        /// </summary>
        /// <param name="sql">SQL-Anfragestring (Select)</param>
        /// <returns>Füll-fähiger Adapter</returns>
        public static DbDataAdapter CreateAdapter(String sql) 
        {
        
            DbDataAdapter adp = factory.CreateDataAdapter();
            adp.SelectCommand = factory.CreateCommand();
            adp.SelectCommand.CommandText = sql;
            
            return adp;
        }
            
        /// <summary>
        /// Adapter zum Befüllen einer DataTable erzeugen
        /// </summary>
        /// <param name="sql">SQL-Anfragestring (Select)</param>
        /// <param name="cnc">Datenbankverbindung</param>
        /// <returns>Füll-fähiger Adapter</returns>
        public static DbDataAdapter CreateAdapter(String sql, DbConnection cnc) 
        {
        
            DbDataAdapter adp = DbFactory.CreateAdapter(sql);
            adp.SelectCommand.Connection = cnc;
            
            return adp;
        }
            
        /// <summary>
        /// Adapter zum Rückschreiben der DataTable-Änderungen in die Datenbank erzeugen
        /// </summary>
        /// <param name="sql">SQL-Anfragestring (Select)</param>
        /// <returns>Update-fähiger Adapter</returns>
        public static DbDataAdapter CreateDMLAdapter(String sql) 
        { 
            DbDataAdapter adp = DbFactory.CreateAdapter(sql);
            DbCommandBuilder bld = factory.CreateCommandBuilder();
            bld.DataAdapter = adp;
            
            return adp;
        }
        
        /// <summary>
        /// Adapter zum Rückschreiben der DataTable-Änderungen in die Datenbank erzeugen
        /// </summary>
        /// <param name="sql">SQL-Anfragestring (Select)</param>
        /// <param name="cnc">Datenbankverbindung</param>
        /// <returns>Update-fähiger Adapter</returns>
        public static DbDataAdapter CreateDMLAdapter(String sql, DbConnection cnc) 
        { 
            DbDataAdapter adp = DbFactory.CreateAdapter(sql);
            adp.SelectCommand.Connection = cnc;
            DbCommandBuilder bld = factory.CreateCommandBuilder();
            bld.DataAdapter = adp;
            
            return adp;
        }
        
        /// <summary>
        /// Leeres Parameterobjekt erzeugen
        /// </summary>
        /// <returns>Datenbankparameter</returns>
        public static DbParameter CreateParameter()
        {
        
            return factory.CreateParameter();
        }

        /// <summary>
        /// Parameterobjekt mit Typ und Namen erzeugen
        /// </summary>
        /// <param name="name">Parametername</param>
        /// <param name="dbType">Datenbanktyp</param>
        /// <returns>Datenbankparameter</returns>
        public static object CreateParameter(string name, System.Data.DbType dbType)
        {
            DbParameter p = CreateParameter();

            p.ParameterName = name;
            p.DbType = dbType;

            return p;
        }
    }
}

﻿namespace comain.Forms
{
    partial class FilterPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnlFilter = new System.Windows.Forms.TableLayoutPanel();
            this.chkOutput = new System.Windows.Forms.CheckedListBox();
            this.pnlFilter1 = new System.Windows.Forms.Panel();
            this.cboPZBis = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cboPS = new System.Windows.Forms.ComboBox();
            this.cboKST = new System.Windows.Forms.ComboBox();
            this.cboPZVon = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.pnlProj = new System.Windows.Forms.Panel();
            this.pnlSort = new System.Windows.Forms.Panel();
            this.datBis = new CO.Forms.DTPWithCalWeek();
            this.datVon = new CO.Forms.DTPWithCalWeek();
            this.lblBis = new System.Windows.Forms.Label();
            this.lblVon = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.cboSort = new System.Windows.Forms.ComboBox();
            this.cboPr = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.chkLA = new System.Windows.Forms.CheckedListBox();
            this.pnlFilter2 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cboInv = new System.Windows.Forms.ComboBox();
            this.cboBG = new System.Windows.Forms.ComboBox();
            this.cboAU = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cboIH = new System.Windows.Forms.ComboBox();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pnlFilter.SuspendLayout();
            this.pnlFilter1.SuspendLayout();
            this.pnlProj.SuspendLayout();
            this.pnlSort.SuspendLayout();
            this.pnlFilter2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlFilter
            // 
            this.pnlFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlFilter.ColumnCount = 5;
            this.pnlFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.pnlFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.pnlFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.pnlFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.pnlFilter.Controls.Add(this.chkOutput, 4, 0);
            this.pnlFilter.Controls.Add(this.pnlFilter1, 0, 0);
            this.pnlFilter.Controls.Add(this.pnlProj, 3, 0);
            this.pnlFilter.Controls.Add(this.chkLA, 1, 0);
            this.pnlFilter.Controls.Add(this.pnlFilter2, 2, 0);
            this.pnlFilter.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter.Margin = new System.Windows.Forms.Padding(0);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.RowCount = 1;
            this.pnlFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.pnlFilter.Size = new System.Drawing.Size(1067, 120);
            this.pnlFilter.TabIndex = 4;
            // 
            // chkOutput
            // 
            this.chkOutput.BackColor = System.Drawing.Color.WhiteSmoke;
            this.chkOutput.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chkOutput.CheckOnClick = true;
            this.chkOutput.ColumnWidth = 95;
            this.chkOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkOutput.FormattingEnabled = true;
            this.chkOutput.Items.AddRange(new object[] {
            "Preis",
            "Stunden",
            "Standort",
            "Maschine",
            "Baugruppe",
            "Inventar-Nr.",
            "Kostenstelle",
            "Position",
            "KW 1 - 52/53",
            "Bedarfspos.",
            "Nur relevante",
            "Nur mit Stunden"});
            this.chkOutput.Location = new System.Drawing.Point(857, 5);
            this.chkOutput.Margin = new System.Windows.Forms.Padding(5);
            this.chkOutput.MultiColumn = true;
            this.chkOutput.Name = "chkOutput";
            this.chkOutput.Size = new System.Drawing.Size(205, 110);
            this.chkOutput.TabIndex = 1;
            this.toolTip1.SetToolTip(this.chkOutput, "Anzuzeigende Elemente definieren");
            // 
            // pnlFilter1
            // 
            this.pnlFilter1.Controls.Add(this.cboPZBis);
            this.pnlFilter1.Controls.Add(this.label1);
            this.pnlFilter1.Controls.Add(this.label14);
            this.pnlFilter1.Controls.Add(this.cboPS);
            this.pnlFilter1.Controls.Add(this.cboKST);
            this.pnlFilter1.Controls.Add(this.cboPZVon);
            this.pnlFilter1.Controls.Add(this.label10);
            this.pnlFilter1.Controls.Add(this.label15);
            this.pnlFilter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFilter1.Location = new System.Drawing.Point(0, 0);
            this.pnlFilter1.Margin = new System.Windows.Forms.Padding(0);
            this.pnlFilter1.Name = "pnlFilter1";
            this.pnlFilter1.Size = new System.Drawing.Size(226, 120);
            this.pnlFilter1.TabIndex = 0;
            // 
            // cboPZBis
            // 
            this.cboPZBis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPZBis.BackColor = System.Drawing.Color.LemonChiffon;
            this.cboPZBis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPZBis.DropDownWidth = 300;
            this.cboPZBis.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboPZBis.FormattingEnabled = true;
            this.cboPZBis.Location = new System.Drawing.Point(82, 35);
            this.cboPZBis.MaxDropDownItems = 30;
            this.cboPZBis.Name = "cboPZBis";
            this.cboPZBis.Size = new System.Drawing.Size(141, 21);
            this.cboPZBis.TabIndex = 12;
            this.cboPZBis.DropDown += new System.EventHandler(this.cboPZBis_DropDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Standort bis";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(5, 92);
            this.label14.Name = "label14";
            this.label14.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label14.Size = new System.Drawing.Size(42, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Bereich";
            // 
            // cboPS
            // 
            this.cboPS.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPS.BackColor = System.Drawing.Color.AntiqueWhite;
            this.cboPS.DisplayMember = "id";
            this.cboPS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPS.DropDownWidth = 300;
            this.cboPS.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboPS.FormattingEnabled = true;
            this.cboPS.Location = new System.Drawing.Point(82, 89);
            this.cboPS.MaxDropDownItems = 30;
            this.cboPS.Name = "cboPS";
            this.cboPS.Size = new System.Drawing.Size(141, 21);
            this.cboPS.TabIndex = 4;
            this.toolTip1.SetToolTip(this.cboPS, "Nur Aufträge eines Auftraggebers/Bereich suchen");
            this.cboPS.ValueMember = "id";
            this.cboPS.DropDown += new System.EventHandler(this.cboPS_DropDown);
            this.cboPS.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxKeyDown);
            // 
            // cboKST
            // 
            this.cboKST.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboKST.BackColor = System.Drawing.Color.AliceBlue;
            this.cboKST.DisplayMember = "id";
            this.cboKST.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKST.DropDownWidth = 300;
            this.cboKST.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboKST.FormattingEnabled = true;
            this.cboKST.Location = new System.Drawing.Point(81, 63);
            this.cboKST.MaxDropDownItems = 30;
            this.cboKST.Name = "cboKST";
            this.cboKST.Size = new System.Drawing.Size(142, 21);
            this.cboKST.TabIndex = 3;
            this.toolTip1.SetToolTip(this.cboKST, "Nur Aufträge einer Kostenstelle suchen");
            this.cboKST.ValueMember = "id";
            this.cboKST.DropDown += new System.EventHandler(this.cboKST_DropDown);
            this.cboKST.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxKeyDown);
            // 
            // cboPZVon
            // 
            this.cboPZVon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPZVon.BackColor = System.Drawing.Color.LemonChiffon;
            this.cboPZVon.DisplayMember = "id";
            this.cboPZVon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPZVon.DropDownWidth = 300;
            this.cboPZVon.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboPZVon.FormattingEnabled = true;
            this.cboPZVon.Location = new System.Drawing.Point(81, 8);
            this.cboPZVon.MaxDropDownItems = 30;
            this.cboPZVon.Name = "cboPZVon";
            this.cboPZVon.Size = new System.Drawing.Size(142, 21);
            this.cboPZVon.TabIndex = 1;
            this.cboPZVon.ValueMember = "id";
            this.cboPZVon.DropDown += new System.EventHandler(this.cboPZVon_DropDown);
            this.cboPZVon.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxKeyDown);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Standort von";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 66);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(25, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "KST";
            // 
            // pnlProj
            // 
            this.pnlProj.Controls.Add(this.pnlSort);
            this.pnlProj.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlProj.Location = new System.Drawing.Point(652, 0);
            this.pnlProj.Margin = new System.Windows.Forms.Padding(0);
            this.pnlProj.Name = "pnlProj";
            this.pnlProj.Size = new System.Drawing.Size(200, 120);
            this.pnlProj.TabIndex = 3;
            // 
            // pnlSort
            // 
            this.pnlSort.Controls.Add(this.datBis);
            this.pnlSort.Controls.Add(this.datVon);
            this.pnlSort.Controls.Add(this.lblBis);
            this.pnlSort.Controls.Add(this.lblVon);
            this.pnlSort.Controls.Add(this.label19);
            this.pnlSort.Controls.Add(this.cboSort);
            this.pnlSort.Controls.Add(this.cboPr);
            this.pnlSort.Controls.Add(this.label18);
            this.pnlSort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSort.Location = new System.Drawing.Point(0, 0);
            this.pnlSort.Margin = new System.Windows.Forms.Padding(0);
            this.pnlSort.Name = "pnlSort";
            this.pnlSort.Size = new System.Drawing.Size(200, 120);
            this.pnlSort.TabIndex = 3;
            // 
            // datBis
            // 
            this.datBis.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datBis.CalendarForeColor = System.Drawing.SystemColors.ControlText;
            this.datBis.CalendarMonthBackground = System.Drawing.SystemColors.Window;
            this.datBis.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datBis.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.datBis.CalendarTrailingForeColor = System.Drawing.SystemColors.GrayText;
            this.datBis.CustomFormat = "dd.MM.yyyy";
            this.datBis.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datBis.Location = new System.Drawing.Point(62, 89);
            this.datBis.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.datBis.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.datBis.Name = "datBis";
            this.datBis.NullableValue = new System.DateTime(2008, 8, 25, 16, 44, 55, 515);
            this.datBis.NullValue = " ";
            this.datBis.Size = new System.Drawing.Size(147, 21);
            this.datBis.TabIndex = 4;
            this.toolTip1.SetToolTip(this.datBis, "Wochansicht Endetermin definieren");
            this.datBis.Value = new System.DateTime(2008, 8, 25, 16, 44, 55, 515);
            // 
            // datVon
            // 
            this.datVon.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datVon.CalendarForeColor = System.Drawing.SystemColors.ControlText;
            this.datVon.CalendarMonthBackground = System.Drawing.SystemColors.Window;
            this.datVon.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datVon.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.datVon.CalendarTrailingForeColor = System.Drawing.SystemColors.GrayText;
            this.datVon.CustomFormat = "dd.MM.yyyy";
            this.datVon.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datVon.Location = new System.Drawing.Point(62, 62);
            this.datVon.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.datVon.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.datVon.Name = "datVon";
            this.datVon.NullableValue = new System.DateTime(2008, 8, 25, 16, 44, 55, 515);
            this.datVon.NullValue = " ";
            this.datVon.Size = new System.Drawing.Size(147, 21);
            this.datVon.TabIndex = 3;
            this.toolTip1.SetToolTip(this.datVon, "Wochansicht Starttermin definieren");
            this.datVon.Value = new System.DateTime(2008, 8, 25, 16, 44, 55, 515);
            // 
            // lblBis
            // 
            this.lblBis.AutoSize = true;
            this.lblBis.Location = new System.Drawing.Point(5, 92);
            this.lblBis.Name = "lblBis";
            this.lblBis.Size = new System.Drawing.Size(20, 13);
            this.lblBis.TabIndex = 19;
            this.lblBis.Text = "bis";
            // 
            // lblVon
            // 
            this.lblVon.AutoSize = true;
            this.lblVon.Location = new System.Drawing.Point(5, 66);
            this.lblVon.Name = "lblVon";
            this.lblVon.Size = new System.Drawing.Size(25, 13);
            this.lblVon.TabIndex = 17;
            this.lblVon.Text = "von";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 39);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "Sortierung";
            // 
            // cboSort
            // 
            this.cboSort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboSort.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cboSort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSort.DropDownWidth = 200;
            this.cboSort.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboSort.FormattingEnabled = true;
            this.cboSort.Items.AddRange(new object[] {
            "KST, Standort, IHO",
            "KST, Standort, Inventar-Nr.",
            "Standort, IHO, Auftrag",
            "Inventar-Nr., Auftrag",
            "IHO, Auftrag",
            "Absteigend: IHO, Auftrag"});
            this.cboSort.Location = new System.Drawing.Point(62, 36);
            this.cboSort.MaxDropDownItems = 30;
            this.cboSort.Name = "cboSort";
            this.cboSort.Size = new System.Drawing.Size(114, 21);
            this.cboSort.TabIndex = 2;
            this.toolTip1.SetToolTip(this.cboSort, "Gefundene Daten sortieren");
            // 
            // cboPr
            // 
            this.cboPr.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboPr.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cboPr.DisplayMember = "idprofile";
            this.cboPr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPr.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboPr.FormattingEnabled = true;
            this.cboPr.Location = new System.Drawing.Point(62, 8);
            this.cboPr.MaxDropDownItems = 30;
            this.cboPr.Name = "cboPr";
            this.cboPr.Size = new System.Drawing.Size(114, 21);
            this.cboPr.TabIndex = 1;
            this.toolTip1.SetToolTip(this.cboPr, "Nur Aufträge eines Profiles suchen");
            this.cboPr.ValueMember = "idprofile";
            this.cboPr.DropDown += new System.EventHandler(this.cboPr_DropDown);
            this.cboPr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxKeyDown);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 11);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(31, 13);
            this.label18.TabIndex = 12;
            this.label18.Text = "Profil";
            // 
            // chkLA
            // 
            this.chkLA.BackColor = System.Drawing.Color.White;
            this.chkLA.CheckOnClick = true;
            this.chkLA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkLA.FormattingEnabled = true;
            this.chkLA.Items.AddRange(new object[] {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.chkLA.Location = new System.Drawing.Point(231, 5);
            this.chkLA.Margin = new System.Windows.Forms.Padding(5);
            this.chkLA.Name = "chkLA";
            this.chkLA.Size = new System.Drawing.Size(190, 110);
            this.chkLA.TabIndex = 1;
            // 
            // pnlFilter2
            // 
            this.pnlFilter2.Controls.Add(this.label12);
            this.pnlFilter2.Controls.Add(this.label13);
            this.pnlFilter2.Controls.Add(this.label11);
            this.pnlFilter2.Controls.Add(this.cboInv);
            this.pnlFilter2.Controls.Add(this.cboBG);
            this.pnlFilter2.Controls.Add(this.cboAU);
            this.pnlFilter2.Controls.Add(this.label16);
            this.pnlFilter2.Controls.Add(this.cboIH);
            this.pnlFilter2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFilter2.Location = new System.Drawing.Point(426, 0);
            this.pnlFilter2.Margin = new System.Windows.Forms.Padding(0);
            this.pnlFilter2.Name = "pnlFilter2";
            this.pnlFilter2.Size = new System.Drawing.Size(226, 120);
            this.pnlFilter2.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(5, 39);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "Baugruppe";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 66);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Inventar-Nr.";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 11);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Maschine";
            // 
            // cboInv
            // 
            this.cboInv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboInv.BackColor = System.Drawing.Color.Honeydew;
            this.cboInv.DisplayMember = "id";
            this.cboInv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboInv.DropDownWidth = 300;
            this.cboInv.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboInv.FormattingEnabled = true;
            this.cboInv.Location = new System.Drawing.Point(73, 63);
            this.cboInv.MaxDropDownItems = 30;
            this.cboInv.Name = "cboInv";
            this.cboInv.Size = new System.Drawing.Size(150, 21);
            this.cboInv.TabIndex = 3;
            this.toolTip1.SetToolTip(this.cboInv, "Nur Aufträge einer Maschine nach Inventarnummer suchen");
            this.cboInv.ValueMember = "id";
            this.cboInv.DropDown += new System.EventHandler(this.cboInv_DropDown);
            this.cboInv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxKeyDown);
            // 
            // cboBG
            // 
            this.cboBG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboBG.BackColor = System.Drawing.Color.Honeydew;
            this.cboBG.DisplayMember = "id";
            this.cboBG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBG.DropDownWidth = 400;
            this.cboBG.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboBG.FormattingEnabled = true;
            this.cboBG.Location = new System.Drawing.Point(73, 36);
            this.cboBG.MaxDropDownItems = 30;
            this.cboBG.Name = "cboBG";
            this.cboBG.Size = new System.Drawing.Size(150, 21);
            this.cboBG.TabIndex = 2;
            this.toolTip1.SetToolTip(this.cboBG, "Nur Aufträge einer Baugruppe suchen");
            this.cboBG.ValueMember = "id";
            this.cboBG.DropDown += new System.EventHandler(this.cboBG_DropDown);
            this.cboBG.DropDownClosed += new System.EventHandler(this.cboBG_DropDownClosed);
            this.cboBG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxKeyDown);
            // 
            // cboAU
            // 
            this.cboAU.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboAU.BackColor = System.Drawing.Color.LavenderBlush;
            this.cboAU.DisplayMember = "id";
            this.cboAU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAU.DropDownWidth = 400;
            this.cboAU.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAU.FormattingEnabled = true;
            this.cboAU.Location = new System.Drawing.Point(73, 89);
            this.cboAU.MaxDropDownItems = 30;
            this.cboAU.Name = "cboAU";
            this.cboAU.Size = new System.Drawing.Size(150, 21);
            this.cboAU.TabIndex = 4;
            this.toolTip1.SetToolTip(this.cboAU, "Nur bestimmten Auftrag suchen");
            this.cboAU.ValueMember = "id";
            this.cboAU.DropDown += new System.EventHandler(this.cboAU_DropDown);
            this.cboAU.DropDownClosed += new System.EventHandler(this.cboAU_DropDownClosed);
            this.cboAU.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxKeyDown);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(5, 92);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Auftrag";
            // 
            // cboIH
            // 
            this.cboIH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboIH.BackColor = System.Drawing.Color.Honeydew;
            this.cboIH.DisplayMember = "id";
            this.cboIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIH.DropDownWidth = 400;
            this.cboIH.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboIH.FormattingEnabled = true;
            this.cboIH.Location = new System.Drawing.Point(73, 8);
            this.cboIH.MaxDropDownItems = 30;
            this.cboIH.Name = "cboIH";
            this.cboIH.Size = new System.Drawing.Size(150, 21);
            this.cboIH.TabIndex = 1;
            this.toolTip1.SetToolTip(this.cboIH, "Nur Aufträge einer Maschine suchen");
            this.cboIH.ValueMember = "id";
            this.cboIH.DropDown += new System.EventHandler(this.cboIH_DropDown);
            this.cboIH.DropDownClosed += new System.EventHandler(this.cboIH_DropDownClosed);
            this.cboIH.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ComboBoxKeyDown);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSubmit.BackColor = System.Drawing.Color.Orange;
            this.btnSubmit.FlatAppearance.BorderSize = 0;
            this.btnSubmit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnSubmit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSubmit.Location = new System.Drawing.Point(1070, 87);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 6;
            this.btnSubmit.Text = "Suchen";
            this.toolTip1.SetToolTip(this.btnSubmit, "Suche starten");
            this.btnSubmit.UseVisualStyleBackColor = false;
            this.btnSubmit.Click += new System.EventHandler(this.SubmitClick);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.Color.Orange;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnReset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.Location = new System.Drawing.Point(1070, 58);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "Alles";
            this.toolTip1.SetToolTip(this.btnReset, "Suchkriterien zurücksetzen");
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.ResetClick);
            // 
            // FilterPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.pnlFilter);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "FilterPanel";
            this.Size = new System.Drawing.Size(1157, 120);
            this.Resize += new System.EventHandler(this.FilterPanel_Resize);
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter1.ResumeLayout(false);
            this.pnlFilter1.PerformLayout();
            this.pnlProj.ResumeLayout(false);
            this.pnlSort.ResumeLayout(false);
            this.pnlSort.PerformLayout();
            this.pnlFilter2.ResumeLayout(false);
            this.pnlFilter2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel pnlFilter;
        private System.Windows.Forms.Panel pnlSort;
        private System.Windows.Forms.Label lblBis;
        private System.Windows.Forms.Label lblVon;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel pnlFilter1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel pnlFilter2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel pnlProj;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ToolTip toolTip1;
        internal CO.Forms.DTPWithCalWeek datBis;
        internal CO.Forms.DTPWithCalWeek datVon;
        internal System.Windows.Forms.ComboBox cboSort;
        internal System.Windows.Forms.ComboBox cboPr;
        internal System.Windows.Forms.ComboBox cboInv;
        internal System.Windows.Forms.ComboBox cboBG;
        internal System.Windows.Forms.ComboBox cboIH;
        internal System.Windows.Forms.ComboBox cboPZVon;
        internal System.Windows.Forms.ComboBox cboPS;
        internal System.Windows.Forms.ComboBox cboAU;
        internal System.Windows.Forms.ComboBox cboKST;
        internal System.Windows.Forms.CheckedListBox chkOutput;
        internal System.Windows.Forms.CheckedListBox chkLA;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.ComboBox cboPZBis;
    }
}

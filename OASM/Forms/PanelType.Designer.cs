﻿namespace comain.Forms
{
    partial class TypePanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpReport = new System.Windows.Forms.GroupBox();
            this.StundenLAReport = new System.Windows.Forms.LinkLabel();
            this.StundenReport = new System.Windows.Forms.LinkLabel();
            this.ReportSum = new System.Windows.Forms.LinkLabel();
            this.PlanReport = new System.Windows.Forms.LinkLabel();
            this.grpPlan = new System.Windows.Forms.GroupBox();
            this.YearOld5 = new System.Windows.Forms.LinkLabel();
            this.YearOld4 = new System.Windows.Forms.LinkLabel();
            this.YearOld3 = new System.Windows.Forms.LinkLabel();
            this.YearOld2 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.YearNew = new System.Windows.Forms.LinkLabel();
            this.YearCurr = new System.Windows.Forms.LinkLabel();
            this.YearOld1 = new System.Windows.Forms.LinkLabel();
            this.Week = new System.Windows.Forms.LinkLabel();
            this.dlgExport = new System.Windows.Forms.SaveFileDialog();
            this.grpReport.SuspendLayout();
            this.grpPlan.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpReport
            // 
            this.grpReport.Controls.Add(this.StundenLAReport);
            this.grpReport.Controls.Add(this.StundenReport);
            this.grpReport.Controls.Add(this.ReportSum);
            this.grpReport.Controls.Add(this.PlanReport);
            this.grpReport.Location = new System.Drawing.Point(270, 6);
            this.grpReport.Name = "grpReport";
            this.grpReport.Size = new System.Drawing.Size(198, 111);
            this.grpReport.TabIndex = 3;
            this.grpReport.TabStop = false;
            this.grpReport.Text = "Berichte und Exporte";
            // 
            // StundenLAReport
            // 
            this.StundenLAReport.ActiveLinkColor = System.Drawing.Color.Coral;
            this.StundenLAReport.AutoSize = true;
            this.StundenLAReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StundenLAReport.LinkColor = System.Drawing.Color.Navy;
            this.StundenLAReport.Location = new System.Drawing.Point(9, 78);
            this.StundenLAReport.Name = "StundenLAReport";
            this.StundenLAReport.Size = new System.Drawing.Size(171, 13);
            this.StundenLAReport.TabIndex = 4;
            this.StundenLAReport.TabStop = true;
            this.StundenLAReport.Text = "Stundenbericht mit Leistungsarten";
            this.StundenLAReport.VisitedLinkColor = System.Drawing.Color.Navy;
            this.StundenLAReport.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.StundenReport_LinkClicked);
            // 
            // StundenReport
            // 
            this.StundenReport.ActiveLinkColor = System.Drawing.Color.Coral;
            this.StundenReport.AutoSize = true;
            this.StundenReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StundenReport.LinkColor = System.Drawing.Color.Navy;
            this.StundenReport.Location = new System.Drawing.Point(9, 60);
            this.StundenReport.Name = "StundenReport";
            this.StundenReport.Size = new System.Drawing.Size(80, 13);
            this.StundenReport.TabIndex = 3;
            this.StundenReport.TabStop = true;
            this.StundenReport.Text = "Stundenbericht";
            this.StundenReport.VisitedLinkColor = System.Drawing.Color.Navy;
            this.StundenReport.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.StundenReport_LinkClicked);
            // 
            // ReportSum
            // 
            this.ReportSum.ActiveLinkColor = System.Drawing.Color.Coral;
            this.ReportSum.AutoSize = true;
            this.ReportSum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReportSum.LinkColor = System.Drawing.Color.Navy;
            this.ReportSum.Location = new System.Drawing.Point(9, 42);
            this.ReportSum.Name = "ReportSum";
            this.ReportSum.Size = new System.Drawing.Size(125, 13);
            this.ReportSum.TabIndex = 2;
            this.ReportSum.TabStop = true;
            this.ReportSum.Text = "Kalenderwochensummen";
            this.ReportSum.VisitedLinkColor = System.Drawing.Color.Navy;
            this.ReportSum.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ReportSum_LinkClicked);
            // 
            // PlanReport
            // 
            this.PlanReport.ActiveLinkColor = System.Drawing.Color.Coral;
            this.PlanReport.AutoSize = true;
            this.PlanReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PlanReport.LinkColor = System.Drawing.Color.Navy;
            this.PlanReport.Location = new System.Drawing.Point(9, 24);
            this.PlanReport.Name = "PlanReport";
            this.PlanReport.Size = new System.Drawing.Size(45, 13);
            this.PlanReport.TabIndex = 1;
            this.PlanReport.TabStop = true;
            this.PlanReport.Text = "Planung";
            this.PlanReport.VisitedLinkColor = System.Drawing.Color.Navy;
            this.PlanReport.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.PlanReport_LinkClicked);
            // 
            // grpPlan
            // 
            this.grpPlan.Controls.Add(this.YearOld5);
            this.grpPlan.Controls.Add(this.YearOld4);
            this.grpPlan.Controls.Add(this.YearOld3);
            this.grpPlan.Controls.Add(this.YearOld2);
            this.grpPlan.Controls.Add(this.label1);
            this.grpPlan.Controls.Add(this.YearNew);
            this.grpPlan.Controls.Add(this.YearCurr);
            this.grpPlan.Controls.Add(this.YearOld1);
            this.grpPlan.Controls.Add(this.Week);
            this.grpPlan.Location = new System.Drawing.Point(3, 6);
            this.grpPlan.Name = "grpPlan";
            this.grpPlan.Size = new System.Drawing.Size(261, 111);
            this.grpPlan.TabIndex = 2;
            this.grpPlan.TabStop = false;
            this.grpPlan.Text = "Planung";
            // 
            // YearOld5
            // 
            this.YearOld5.ActiveLinkColor = System.Drawing.Color.Coral;
            this.YearOld5.AutoSize = true;
            this.YearOld5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.YearOld5.LinkColor = System.Drawing.Color.Navy;
            this.YearOld5.Location = new System.Drawing.Point(213, 85);
            this.YearOld5.Name = "YearOld5";
            this.YearOld5.Size = new System.Drawing.Size(31, 13);
            this.YearOld5.TabIndex = 9;
            this.YearOld5.TabStop = true;
            this.YearOld5.Text = "2003";
            this.YearOld5.VisitedLinkColor = System.Drawing.Color.Navy;
            this.YearOld5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.YearOld_LinkClicked);
            // 
            // YearOld4
            // 
            this.YearOld4.ActiveLinkColor = System.Drawing.Color.Coral;
            this.YearOld4.AutoSize = true;
            this.YearOld4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.YearOld4.LinkColor = System.Drawing.Color.Navy;
            this.YearOld4.Location = new System.Drawing.Point(184, 85);
            this.YearOld4.Name = "YearOld4";
            this.YearOld4.Size = new System.Drawing.Size(31, 13);
            this.YearOld4.TabIndex = 8;
            this.YearOld4.TabStop = true;
            this.YearOld4.Text = "2004";
            this.YearOld4.VisitedLinkColor = System.Drawing.Color.Navy;
            this.YearOld4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.YearOld_LinkClicked);
            // 
            // YearOld3
            // 
            this.YearOld3.ActiveLinkColor = System.Drawing.Color.Coral;
            this.YearOld3.AutoSize = true;
            this.YearOld3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.YearOld3.LinkColor = System.Drawing.Color.Navy;
            this.YearOld3.Location = new System.Drawing.Point(155, 85);
            this.YearOld3.Name = "YearOld3";
            this.YearOld3.Size = new System.Drawing.Size(31, 13);
            this.YearOld3.TabIndex = 7;
            this.YearOld3.TabStop = true;
            this.YearOld3.Text = "2005";
            this.YearOld3.VisitedLinkColor = System.Drawing.Color.Navy;
            this.YearOld3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.YearOld_LinkClicked);
            // 
            // YearOld2
            // 
            this.YearOld2.ActiveLinkColor = System.Drawing.Color.Coral;
            this.YearOld2.AutoSize = true;
            this.YearOld2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.YearOld2.LinkColor = System.Drawing.Color.Navy;
            this.YearOld2.Location = new System.Drawing.Point(126, 85);
            this.YearOld2.Name = "YearOld2";
            this.YearOld2.Size = new System.Drawing.Size(31, 13);
            this.YearOld2.TabIndex = 6;
            this.YearOld2.TabStop = true;
            this.YearOld2.Text = "2006";
            this.YearOld2.VisitedLinkColor = System.Drawing.Color.Navy;
            this.YearOld2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.YearOld_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Alte Planungen:";
            // 
            // YearNew
            // 
            this.YearNew.ActiveLinkColor = System.Drawing.Color.Coral;
            this.YearNew.AutoSize = true;
            this.YearNew.Cursor = System.Windows.Forms.Cursors.Hand;
            this.YearNew.LinkColor = System.Drawing.Color.Navy;
            this.YearNew.Location = new System.Drawing.Point(8, 42);
            this.YearNew.Name = "YearNew";
            this.YearNew.Size = new System.Drawing.Size(135, 13);
            this.YearNew.TabIndex = 3;
            this.YearNew.TabStop = true;
            this.YearNew.Text = "Strategische Planung 2009";
            this.YearNew.VisitedLinkColor = System.Drawing.Color.Navy;
            this.YearNew.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.YearNew_LinkClicked);
            // 
            // YearCurr
            // 
            this.YearCurr.ActiveLinkColor = System.Drawing.Color.Coral;
            this.YearCurr.AutoSize = true;
            this.YearCurr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.YearCurr.LinkColor = System.Drawing.Color.Navy;
            this.YearCurr.Location = new System.Drawing.Point(8, 24);
            this.YearCurr.Name = "YearCurr";
            this.YearCurr.Size = new System.Drawing.Size(123, 13);
            this.YearCurr.TabIndex = 2;
            this.YearCurr.TabStop = true;
            this.YearCurr.Text = "Operative Planung 2008";
            this.YearCurr.VisitedLinkColor = System.Drawing.Color.Navy;
            this.YearCurr.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.YearCurr_LinkClicked);
            // 
            // YearOld1
            // 
            this.YearOld1.ActiveLinkColor = System.Drawing.Color.Coral;
            this.YearOld1.AutoSize = true;
            this.YearOld1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.YearOld1.LinkColor = System.Drawing.Color.Navy;
            this.YearOld1.Location = new System.Drawing.Point(97, 85);
            this.YearOld1.Name = "YearOld1";
            this.YearOld1.Size = new System.Drawing.Size(31, 13);
            this.YearOld1.TabIndex = 0;
            this.YearOld1.TabStop = true;
            this.YearOld1.Text = "2007";
            this.YearOld1.VisitedLinkColor = System.Drawing.Color.Navy;
            this.YearOld1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.YearOld_LinkClicked);
            // 
            // Week
            // 
            this.Week.ActiveLinkColor = System.Drawing.Color.Coral;
            this.Week.AutoSize = true;
            this.Week.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Week.LinkColor = System.Drawing.Color.Navy;
            this.Week.Location = new System.Drawing.Point(8, 60);
            this.Week.Name = "Week";
            this.Week.Size = new System.Drawing.Size(84, 13);
            this.Week.TabIndex = 4;
            this.Week.TabStop = true;
            this.Week.Text = "Wochenplanung";
            this.Week.VisitedLinkColor = System.Drawing.Color.Navy;
            this.Week.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Week_LinkClicked);
            // 
            // dlgExport
            // 
            this.dlgExport.DefaultExt = "xlsx";
            this.dlgExport.Filter = "Microsoft Office Excel 2007|*.xlsx";
            this.dlgExport.Title = "Planung exportieren";
            // 
            // TypePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.Controls.Add(this.grpReport);
            this.Controls.Add(this.grpPlan);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "TypePanel";
            this.Size = new System.Drawing.Size(941, 120);
            this.grpReport.ResumeLayout(false);
            this.grpReport.PerformLayout();
            this.grpPlan.ResumeLayout(false);
            this.grpPlan.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpReport;
        private System.Windows.Forms.LinkLabel ReportSum;
        private System.Windows.Forms.LinkLabel PlanReport;
        private System.Windows.Forms.GroupBox grpPlan;
        private System.Windows.Forms.LinkLabel YearNew;
        private System.Windows.Forms.LinkLabel YearCurr;
        private System.Windows.Forms.LinkLabel YearOld1;
        private System.Windows.Forms.LinkLabel Week;
        private System.Windows.Forms.LinkLabel YearOld5;
        private System.Windows.Forms.LinkLabel YearOld4;
        private System.Windows.Forms.LinkLabel YearOld3;
        private System.Windows.Forms.LinkLabel YearOld2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SaveFileDialog dlgExport;
        private System.Windows.Forms.LinkLabel StundenReport;
        private System.Windows.Forms.LinkLabel StundenLAReport;
    }
}

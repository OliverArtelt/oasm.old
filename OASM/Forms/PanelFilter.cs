﻿using System;
using System.Text;
using System.Windows.Forms;
using comain.Filter;
using comain.Schedule;
using System.Collections.Generic;
using System.Drawing;


namespace comain.Forms
{

    internal partial class FilterPanel : UserControl
    {


        IMainFormUpdater myForm;


        internal FilterPanel(IMainFormUpdater frm)
        {
            
            InitializeComponent();

            myForm = frm;
            BackColor = Color.White;
        }
        
        /// <summary>
        /// Neuer Benutzer gewählt, Panel einrichten
        /// </summary>
        /// <remarks>
        /// Daten der Filter werden verzögert (beim Herunterklappen der Comboboxlisten) 
        /// geladen um Ladegeschwindigkeit zu erhöhen. Die Checkboxliste der
        /// Projektion muß in jedem Fall sofort geladen werden.
        /// </remarks>
        internal void Init()
        {
        
            myForm.Filter.BindValues(PicklistType.LA);
            cboAU.SelectedItem = null;
            cboPZVon.SelectedItem = null;
            cboPZBis.SelectedItem = null;
            cboBG.SelectedItem = null;
            cboIH.SelectedItem = null;
            cboInv.SelectedItem = null;
            cboKST.SelectedItem = null;
            cboPr.SelectedItem = null;
            cboPS.SelectedItem = null;
            cboSort.SelectedItem = null;
        }

        /// <summary>
        /// Controls abh. von der Benutzerinteraktion einstellen (z.B. Undo-/Redobuttons de./aktivieren)
        /// </summary>
        internal void UpdatePanel(ScheduleType currType)
        {
        
            datVon.Visible = currType == ScheduleType.Week;
            datBis.Visible = currType == ScheduleType.Week;
            lblVon.Visible = currType == ScheduleType.Week;
            lblBis.Visible = currType == ScheduleType.Week;
        }

        /// <summary>
        /// Filterkriterien zurücksetzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResetClick(object sender, EventArgs e)
        {
            
            comain.Properties.Settings.Default.Reload();
            myForm.Filter.ResetFilter();
        }

        /// <summary>
        /// Plan erstellen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitClick(object sender, EventArgs e)
        {
            
            myForm.CreateSchedule();
            myForm.Filter.SaveFilter();
        }
        
   
#region F I L T E R   D E P E N D E N C I E S
 

        /// <summary>
        /// nach Standorten filtern, wenn gewählt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboIH_DropDown(object sender, EventArgs e)
        {
            
            myForm.Filter.BindValues(PicklistType.IH);
        
            if (cboPZVon.SelectedValue == null) return;
            String s = cboPZVon.Text.Substring(0, cboPZVon.Text.IndexOf(' '));
        }

        /// <summary>
        /// IHO-Filter freigeben
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboIH_DropDownClosed(object sender, EventArgs e)
        {
        
            Object o = cboIH.SelectedItem;
            cboIH.SelectedItem = o;
        }

        /// <summary>
        /// IHO-Baugruppen: nach IHO/Standorte filtern, wenn gewählt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboBG_DropDown(object sender, EventArgs e)
        {
       
            myForm.Filter.BindValues(PicklistType.IHSub);

            //Haupt-IHO gewählt
            if (cboIH.SelectedValue != null) {
            
                String s = cboIH.Text.Substring(0, cboIH.Text.IndexOf(' '));
                return;
            }
                               
            //oder Standort gewählt
            if (cboPZVon.SelectedValue != null) {
            
                String s = cboPZVon.Text.Substring(0, cboPZVon.Text.IndexOf(' '));
                return;
            }
        }

        /// <summary>
        /// IHO-Baugruppenliste geschlossen: Filter aufheben
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboBG_DropDownClosed(object sender, EventArgs e)
        {
        
            Object o = cboBG.SelectedItem;
            cboBG.SelectedItem = o;
        }

        /// <summary>
        /// Aufträge: nach Leistungsarten filtern, wenn gewählt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboAU_DropDown(object sender, EventArgs e)
        {
        
            myForm.Filter.BindValues(PicklistType.AU);
        }

        /// <summary>
        /// Auftragsliste geschlossen: Filter aufheben
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboAU_DropDownClosed(object sender, EventArgs e)
        {
        
            Object o = cboAU.SelectedItem;
            cboAU.SelectedItem = o;
        }
        
        private void ComboBoxKeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode != Keys.Delete) return;
            if (!(sender is ComboBox)) return;

            (sender as ComboBox).SelectedIndex = -1;
        }                      

        private void cboPZVon_DropDown(object sender, EventArgs e)
        {
            myForm.Filter.BindValues(PicklistType.PZVon);
        }

        private void cboPZBis_DropDown(object sender, EventArgs e)
        {
            myForm.Filter.BindValues(PicklistType.PZBis);
        }

        private void cboInv_DropDown(object sender, EventArgs e)
        {
            myForm.Filter.BindValues(PicklistType.IHInvent);
        }

        private void cboKST_DropDown(object sender, EventArgs e)
        {
            myForm.Filter.BindValues(PicklistType.KST);
        }

        private void cboPS_DropDown(object sender, EventArgs e)
        {
            myForm.Filter.BindValues(PicklistType.Bereich);
        }

        private void cboPr_DropDown(object sender, EventArgs e)
        {
            myForm.Filter.BindValues(PicklistType.Profil);
        }
        
#endregion

        
        private void FilterPanel_Resize(object sender, EventArgs e)
        {

            cboAU.Invalidate();
            cboPZVon.Invalidate();
            cboBG.Invalidate();
            cboIH.Invalidate();
            cboInv.Invalidate();
            cboKST.Invalidate();
            cboPr.Invalidate();
            cboPS.Invalidate();
            cboSort.Invalidate();
        }
    }
}

﻿using System;
using System.Windows.Forms;
using comain.User;
using System.Data;
using CO.Crypt;


namespace comain.Forms
{
    
    
    /// <summary>
    /// Meister bearbeiten
    /// </summary>
    public partial class AdeptForm : Form
    {
        
        MasterAdapter myData;
        
        
        /// <summary>
        /// Meister bearbeiten
        /// </summary>
        public AdeptForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Form erstellen: Stammdaten ins Dataset laden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AdeptLoad(object sender, EventArgs e)
        {

            try {
            
                myData = new MasterAdapter();
                myData.Load();
                dsMaster = myData.Dataset;
                bndAdept.DataSource = dsMaster.Adept;   
            }
            catch (Exception x) { new comain.Error(x);}         
        }

        /// <summary>
        /// Passwort-Hash eintragen/überschreiben
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetPassword(object sender, EventArgs e)
        {

            try {
            
                //Eingaben prüfen
                if (txtPass1.Text != txtPass2.Text) {
                
                    MessageBox.Show("Die beiden Passwörter stimmen nicht überein.");
                    txtPass1.Text = "";
                    txtPass2.Text = "";
                    return;
                }
                
                if (txtPass1.Text.Length < 2) {
                
                    MessageBox.Show("Verwenden Sie mindestens zwei Zeichen für das Passwort.");
                    txtPass1.Text = "";
                    txtPass2.Text = "";
                    return;
                }
                
                //Hashwert bilden
                String hashedValue = SimpleHash.ComputeHash(txtPass1.Text, "MD5", null);
                txtPass1.Text = "";
                txtPass2.Text = "";
                
                //in Dataset eintragen
                DataRowView rv = bndAdept.Current as DataRowView;
                if (rv == null) return;
                Master.AdeptRow row = rv.Row as Master.AdeptRow;
                if (row == null) return;
                row.pass = hashedValue;
                
            }
            catch (Exception x) { new comain.Error(x);}         
        }

        /// <summary>
        /// Neuen Meister anlegen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Add(object sender, EventArgs e)
        {

            try {
            
                bndAdept.AddNew();
                txtName.Focus();
            }
            catch (Exception x) { new comain.Error(x);}         
        }

        /// <summary>
        /// Meister löschen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Delete(object sender, EventArgs e)
        {
           
            try {
            
                //in Dataset eintragen
                DataRowView rv = bndAdept.Current as DataRowView;
                if (rv == null) return;
                
                //Sonderfall: Löschen eines frisch angefügten Datensatzes
                if (rv.IsNew) {
                
                    rv.Delete();
                    return;
                }
                
                Master.AdeptRow row = rv.Row as Master.AdeptRow;
                if (row == null) return;

                if (MessageBox.Show("Meister " + row.name + " wirklich entfernen?", ProductName, MessageBoxButtons.YesNo)
                                    == DialogResult.No) return;
                
                //KST-Rollen werden automatisch durch Relation-Objekt gelöscht.
                row.Delete();
            }
            catch (Exception x) { new comain.Error(x);}      
        }

        /// <summary>
        /// Änderungen in DB zurückschreiben
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Save(object sender, EventArgs e)
        {

            try {
            
                //geänderte Zeilen mit Dataset abstimmen
                bndAdeptKstRole.EndEdit();
                bndAdept.EndEdit();
          
                myData.Save();
            }
            catch (Exception x) { new comain.Error(x); }         
        }

        /// <summary>
        /// Allgemeiner Fehler Datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            new comain.Error(e.Exception);
        }

        private void DataError(object sender, BindingManagerDataErrorEventArgs e)
        {
            new comain.Error(e.Exception);
        }

        /// <summary>
        /// Namensangaben überprüfen, Errorhandler setzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtName.Text == "") errHandler.SetError(txtName, "Geben Sie den Namen des Meisters an.");
            else                    errHandler.SetError(txtName, "");
        }

        /// <summary>
        /// Loginangaben überprüfen, Errorhandler setzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUser_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (txtUser.Text == "") errHandler.SetError(txtUser, "Geben Sie den Anmeldenamen des Meisters an.");
            else                    errHandler.SetError(txtUser, "");
        }
    }
}

﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using comain.Command;
using comain.Schedule;
using comain.Statistics;
using comain.User;
using comain.Workspace;


namespace comain.Forms
{

    internal partial class EditPanel : UserControl
    {

        IMainFormUpdater myForm;
        

        public EditPanel(IMainFormUpdater frm)
        {
            
            InitializeComponent();

            myForm = frm;
            BackColor = Color.White;
        }

        /// <summary>
        /// Bearbeitungswerkzeuge bereitstellen
        /// </summary>
        internal void Init()
        {
        
            cboProfil.SelectedItem = null;
            cboAU.SelectedItem = null;
            cboIH.SelectedItem = null;
            cboAmount.SelectedIndex = 0;
            
            btnTypeX.Enabled = Session.Current.UserActions.PlanType;
            btnTypeO.Enabled = Session.Current.UserActions.ImplementType;
            btnTypeT.Enabled = Session.Current.UserActions.DailyType;
            btnTypeS.Enabled = Session.Current.UserActions.SpecialType;
            btnTypeD.Enabled = Session.Current.UserActions.DeleteType;
            cboAmount.Enabled = Session.Current.UserActions.AmountType;
            
            btnUpdate.Enabled = cboAmount.Enabled;
            btnUpdate.BackColor = (btnUpdate.Enabled)? Color.LightGreen: Color.LightGray;
            
            UpdatePanel();
            TypeDClick(this, null);
        }

        /// <summary>
        /// Controls abh. von der Benutzerinteraktion einstellen (z.B. Undo-/Redobuttons de./aktivieren)
        /// </summary>
        internal void UpdatePanel()
        {       

            btnUndo.Enabled = Session.Current.Schedule != null && Session.Current.CommandHistory != null && Session.Current.CommandHistory.HasPrev;
            btnRedo.Enabled = Session.Current.Schedule != null && Session.Current.CommandHistory != null && Session.Current.CommandHistory.HasNext;
            btnSelect.Text = (Session.Current.Schedule != null && Session.Current.OrdersSelectedState)? "Alle AUS": "Alle EIN";
        }

#region C O M M A N D


        /// <summary>
        /// letzten Befehl widerrufen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UndoClick(object sender, EventArgs e)
        {
        
            try {
            
                if (Session.Current.Schedule == null) return;
                Session.Current.CommandHistory.Prev();
                
                UpdatePanel();
                FillStatistics();
            }
            catch (Exception x) { new comain.Error(x); }
        }

        /// <summary>
        /// letzten widerrufenen Befehl erneut ausführen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RedoClick(object sender, EventArgs e)
        {
            
            try {
            
                if (Session.Current.Schedule == null) return;
                Session.Current.CommandHistory.Next();
            
                UpdatePanel();
                FillStatistics();
            }
            catch (Exception x) { new comain.Error(x); }
        }
        
        /// <summary>
        /// Alle Aufträge an-/abwählen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectClick(object sender, EventArgs e)
        {

            if (Session.Current.Schedule == null) return;

            try {
            
                ICommand cmd = new SelectAllOrdersCommand(Session.Current.Schedule, myForm.Grid);
                cmd.Execute(Session.Current.CommandHistory);
            
                UpdatePanel();
            }
            catch (Exception x) { new comain.Error(x); }
        }

        /// <summary>
        /// Auftragszeilenhöhe individuell so verändern, dass Auftragskurzbeschreibung gelesen werden kann
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RowsClick(object sender, EventArgs e)
        {

            try {

                Session.Current.Schedule.Accept(new AutoResizer(myForm.Grid));
            }
            catch (Exception x) { new comain.Error(x); }
        }

        /// <summary>
        /// Planänderungen in DB rückschreiben
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdateClick(object sender, EventArgs e)
        {
            myForm.UpdateSchedule();
        }


#endregion

#region P R O F I L E S


        /// <summary>
        /// Profile laden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboProfil_DropDown(object sender, EventArgs e)
        {
        
            myForm.Filter.BindValues(comain.Filter.PicklistType.Profil);
        }

        /// <summary>
        /// Neues Profil erstellen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CreateProfile(object sender, EventArgs e)
        {

            try {
            
                ProfileMapper m = new ProfileMapper();
                m.Create(cboProfil.Text);
                
                myForm.Filter.ClearProfile();
            }
            catch (Exception x) { new comain.Error(x); }
        }

        /// <summary>
        /// Profil löschen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteProfile(object sender, EventArgs e)
        {

            try {
            
                ProfileMapper m = new ProfileMapper();
                m.Delete(cboProfil.SelectedItem as PicklistItem);
                
                myForm.Filter.ClearProfile();
            }
            catch (Exception x) { new comain.Error(x); }
        }

        /// <summary>
        /// Aufträge des Profiles durch neue ersetzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReplaceProfile(object sender, EventArgs e)
        {

            try {
            
                ProfileMapper m = new ProfileMapper();
                m.Replace(cboProfil.SelectedItem as PicklistItem);
            }
            catch (Exception x) { new comain.Error(x); }
        }

        /// <summary>
        /// Aufträge des Profiles um neue ergänzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddToProfile(object sender, EventArgs e)
        {

            try {
            
                ProfileMapper m = new ProfileMapper();
                m.Add(cboProfil.SelectedItem as PicklistItem);
            }
            catch (Exception x) { new comain.Error(x); }
        }


#endregion

#region  S T A T I S T I C S

        /// <summary>
        /// Statistiktabelle aktualisieren
        /// </summary>
        internal void FillStatistics()
        {

            try {

                Session.Current.DeltaVon = DeltaVon;
                Session.Current.DeltaBis = DeltaBis;
                StatsCollector coll = new StatsCollector(Session.Current.Schedule.Traits, DeltaVon, DeltaBis);
                Session.Current.Schedule.Accept(coll);
            
                var list = grdStats.Controls.OfType<StatsEntryView>();
                grdStats.Controls.Clear();
                foreach (var ctrl in list) ctrl.Dispose();
                
                foreach (TotalEntry entry in coll.GetTotalStats()) grdStats.Controls.Add(new StatsEntryView(entry));                 
            }
            catch (Exception x) { new comain.Error(x); }
        }

#endregion

#region G O T O


        /// <summary>
        /// Liste aller angezeigten Aufträge generieren
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListOrders(object sender, EventArgs e)
        {

            CollectOrders clc = new CollectOrders();

            Session.Current.Schedule.Accept(clc);
            cboAU.Items.Clear();
            cboAU.Items.AddRange(clc.Items.ToArray());
        }     

        /// <summary>
        /// Liste aller angezeigten IH-Objekte generieren
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListObjects(object sender, EventArgs e)
        {

            CollectIHObjects clc = new CollectIHObjects();

            Session.Current.Schedule.Accept(clc);
            cboIH.Items.Clear();
            cboIH.Items.AddRange(clc.Items.ToArray());
        }

        /// <summary>
        /// zum gewählten Auftrag scrollen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickOrders(object sender, EventArgs e)
        {

            ComboBox cb = sender as ComboBox;
            if (cb == null || cb.SelectedItem == null) return;

            FindOrder fnd = new FindOrder(cb.SelectedItem.ToString());
            Session.Current.Schedule.Accept(fnd);
            myForm.ScrollTo(fnd.Header);
        }

        /// <summary>
        /// zum gewählten IHObjekt scrollen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClickObjects(object sender, EventArgs e)
        {

            ComboBox cb = sender as ComboBox;
            if (cb == null || cb.SelectedItem == null) return;

            FindIHObject fnd = new FindIHObject(cb.SelectedItem.ToString());
            Session.Current.Schedule.Accept(fnd);
            myForm.ScrollTo(fnd.Position);
        }

        /// <summary>
        /// zu aktiver Zeile scrollen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGoToActive_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            if (Session.Current.CurrentPosition < 0) return;
            
            NodeAtPosition nap = new NodeAtPosition();
            Session.Current.Schedule.AcceptOnce(nap, Session.Current.CurrentPosition);
            if (nap.Node is Auftrag) myForm.ScrollTo(nap.Node.Parent.Position + 1); 
            else                     myForm.ScrollTo(Session.Current.CurrentPosition);
        }

                 
#endregion 

#region E D I T   M O D E S 

        private void TypeXClick(object sender, EventArgs e)
        {
            Session.Current.SelectedEditType = EditedType.Planned;
            SetColor();
        }

        private void TypeOClick(object sender, EventArgs e)
        {
            Session.Current.SelectedEditType = EditedType.Common;
            SetColor();
        }

        private void TypeTClick(object sender, EventArgs e)
        {
            Session.Current.SelectedEditType = EditedType.Daily;
            SetColor();
        }

        private void TypeSClick(object sender, EventArgs e)
        {
            Session.Current.SelectedEditType = EditedType.Special;
            SetColor();
        }

        private void TypeDClick(object sender, EventArgs e)
        {
            Session.Current.SelectedEditType = EditedType.Deleted;
            SetColor();
        }

        private void AmountChanged(object sender, EventArgs e)
        {
            Session.Current.SelectedEditAmount = (short)(cboAmount.SelectedIndex + 1);
        }      
        
        private void cboAmount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {

            short dummy;

            if (!Int16.TryParse(cboAmount.Text, out dummy)) {

                MessageBox.Show("Geben Sie als Anzahl einen ganzen Wert zwischen 1 und 99 an.");
                cboAmount.Focus();
            }

            Session.Current.SelectedEditAmount = dummy;   
        }

        void SetColor()
        {
        
            btnTypeX.BackColor = (btnTypeX.Enabled)? 
                ((Session.Current.SelectedEditType == EditedType.Planned)? Color.OrangeRed: Color.Orange) : Color.LightGray;
            btnTypeO.BackColor = (btnTypeO.Enabled)? 
                ((Session.Current.SelectedEditType == EditedType.Common)?  Color.OrangeRed: Color.Orange) : Color.LightGray;
            btnTypeT.BackColor = (btnTypeT.Enabled)? 
                ((Session.Current.SelectedEditType == EditedType.Daily)?   Color.OrangeRed: Color.Orange) : Color.LightGray;
            btnTypeS.BackColor = (btnTypeS.Enabled)? 
                ((Session.Current.SelectedEditType == EditedType.Special)? Color.OrangeRed: Color.Orange) : Color.LightGray;
            btnTypeD.BackColor = (btnTypeD.Enabled)? 
                ((Session.Current.SelectedEditType == EditedType.Deleted)? Color.OrangeRed: Color.Orange) : Color.LightGray;
        }

#endregion

#region R E A L I S I E R U N G S S T A T U S

        
        private PicklistItem   currentPeriod;
        private PicklistItem[] periodList;


        internal void SetPeriodLists()
        {
            
            var cal = Session.Current.Schedule.Traits.Calendar;
            periodList = cal.CalendarKeys.Select(p => new PicklistItem { Id = p, Value1 = cal.FormatKeyLong(p)}).ToArray();
            currentPeriod = null;
            if (cal.CurrentKey.HasValue) currentPeriod = periodList.FirstOrDefault(p => p.Id == cal.CurrentKey.Value);

            cboRSVon.Items.Clear();
            cboRSVon.Items.AddRange(periodList);
            cboRSBis.Items.Clear();
            cboRSBis.Items.AddRange(periodList);
        }


        internal int DeltaVon 
        { 
        
            get { 
            
                var cal = Session.Current.Schedule.Traits.Calendar;
                return cboRSVon.SelectedItem == null ? cal.CalculateCalendarKey(cal.DateVon) : ((PicklistItem)cboRSVon.SelectedItem).Id; 
            } 

            set {

                var si = periodList.FirstOrDefault(p => p.Id == value);
                if (si != null) cboRSVon.SelectedItem = si;
            }
        }


        internal int DeltaBis 
        { 
        
            get { 
            
                var cal = Session.Current.Schedule.Traits.Calendar;
                return cboRSBis.SelectedItem == null ? cal.CalculateCalendarKey(cal.DateBis) : ((PicklistItem)cboRSBis.SelectedItem).Id; 
            } 

            set {

                var si = periodList.FirstOrDefault(p => p.Id == value);
                if (si != null) cboRSBis.SelectedItem = si;
            }
        }


        public void ResetImplPeriod(bool asCurrent)
        {
            
            if (asCurrent) {

                cboRSVon.SelectedItem = periodList.FirstOrDefault();
                if (currentPeriod != null) cboRSBis.SelectedItem = currentPeriod;
                else  cboRSBis.SelectedItem = periodList.LastOrDefault();
            }

            FillStatistics();
            myForm.Grid.Invalidate();
        }


        private void btnImplState_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ResetImplPeriod(false);
        }


        private void btnImplStateCurrent_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ResetImplPeriod(true);
        }


#endregion

    }
}

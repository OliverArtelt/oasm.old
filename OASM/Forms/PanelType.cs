﻿using System;
using System.Drawing;
using System.Windows.Forms;
using comain.Excel;
using comain.Reports;
using comain.Schedule;
using comain.Workspace;
using System.IO;


namespace comain.Forms
{
    
    internal partial class TypePanel : UserControl
    {
        
        IMainFormUpdater myForm;
        
        
        public TypePanel(IMainFormUpdater frm)
        {
            
            InitializeComponent();
            
            myForm = frm;
            BackColor = Color.White;
        }
        
        /// <summary>
        /// Neuer Benutzer / Datenbank gewählt, verfügbare Planungstypen aushandeln
        /// </summary>
        internal void Init()
        {
            
            int y = Session.Current.OASMSettings.PlanYear;
            
            YearNew.Text = "Strategische Planung " + y;
            YearNew.Tag  = y;
            
            YearCurr.Text = "Operative Planung " + (y - 1).ToString();
            YearCurr.Tag = y - 1;

            SetOldLink(YearOld1, y - 2, Session.Current.OASMSettings.MinDate.Year);
            SetOldLink(YearOld2, y - 3, Session.Current.OASMSettings.MinDate.Year);
            SetOldLink(YearOld3, y - 4, Session.Current.OASMSettings.MinDate.Year);
            SetOldLink(YearOld4, y - 5, Session.Current.OASMSettings.MinDate.Year);
            SetOldLink(YearOld5, y - 6, Session.Current.OASMSettings.MinDate.Year);
            
            ReportSum.Enabled = Session.Current.User.SeePrices;
        }
        
        /// <summary>
        /// Alte Planungen aufsetzen
        /// </summary>
        /// <param name="lbl">Verwendetes Label</param>
        /// <param name="year">Jahr des Labels</param>
        /// <param name="border">Ältester verfügbarer Plan</param>
        private void SetOldLink(LinkLabel lbl, int year, int border)
        {
        
            if (year < border || border == 0) {
            
                lbl.Visible = false;
                return;
            }
           
            lbl.Visible = true;
            lbl.Tag = year;
            lbl.Text = year.ToString();
        }        

        private void YearOld_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            LinkLabel lbl = sender as LinkLabel;
            if (lbl == null) return;
            
            myForm.SetSchedule(ScheduleType.YearOld, "Alte Planung " + lbl.Text, (int)lbl.Tag); 
        }

        private void YearCurr_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            LinkLabel lbl = sender as LinkLabel;
            if (lbl == null) return;
            
            myForm.SetSchedule(ScheduleType.YearCurrent, YearCurr.Text, (int)lbl.Tag); 
        }

        private void YearNew_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            LinkLabel lbl = sender as LinkLabel;
            if (lbl == null) return;
            
            myForm.SetSchedule(ScheduleType.YearNew, YearNew.Text, (int)lbl.Tag); 
        }

        private void Week_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            LinkLabel lbl = sender as LinkLabel;
            if (lbl == null) return;
            
            myForm.SetSchedule(ScheduleType.Week, Week.Text, 0); 
        }

        /// <summary>
        /// Bericht Kalenderwochensummen ausgeben
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReportSum_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            if (Session.Current.Schedule == null) {
            
                MessageBox.Show("Erzeugen Sie zunächst eine Planung.");
                return;
            }
            
            try {
            
                KWSumCarrier frm = new KWSumCarrier();
                frm.Show();
            }
            catch (Exception x) { new Error(x); }
        }


        private void StundenReport_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            if (Session.Current.Schedule == null) {
            
                MessageBox.Show("Erzeugen Sie zuerst eine Planung.");
                return;
            }
            
            try {
            
                var frm = new StundenCarrier(sender == StundenLAReport);
                frm.Show();
            }
            catch (Exception x) { new Error(x); }
        }

        /// <summary>
        /// Planung ausdrucken
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PlanReport_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            if (Session.Current.Schedule == null) {
            
                MessageBox.Show("Erzeugen Sie zuerst eine Planung.");
                return;
            }
            
            try {
            
                //Excel-Export zur Weiterverarbeitung
                dlgExport.Title = Session.Current.Schedule.Traits.ShortName + " exportieren";
                
                dlgExport.FileName = Session.Current.Schedule.Traits.ShortName;
                if (dlgExport.ShowDialog() == DialogResult.Cancel) return;
                
                String s = dlgExport.FileName;
                //manche Systeme haben Probleme mit Dots im Dateinamen
                if (!s.ToLower().EndsWith(".xlsx")) s += ".xlsx";
                Exporter exp = new Exporter(s, Session.Current.Schedule);
                exp.Write();                
            }
            catch (Exception x) { new Error(x); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using comain.Workspace;


namespace comain.Forms
{
    
    internal partial class LoginPanel : UserControl
    {
        
        IMainFormUpdater myForm;
        
        
        internal LoginPanel(IMainFormUpdater frm)
        {
            
            InitializeComponent();
            
            BackColor = Color.White;
            SetState(false);
            myForm = frm; 

#if DEBUG

            txtPass.Text = "pih";

#endif

        }
    
        /// <summary>
        /// Verbindungsnamen für Listbox etc. auslesen 
        /// </summary>
        internal void Init(IEnumerable<String> items)
        {
                
            cboDB.DataSource = items; 
            
            //Meisterzugang: einzige zugewiesene DB gleich eintragen
            cboDB.SelectedIndex = (items.Count() == 1)? 0: -1;
            
            if (!String.IsNullOrEmpty(comain.Properties.Settings.Default.AccessDatabase))
                cboDB.SelectedItem = comain.Properties.Settings.Default.AccessDatabase;
            chkAdept.Checked = comain.Properties.Settings.Default.AccessRole;            
            txtUser.Text = comain.Properties.Settings.Default.AccessUser;
            
            txtPass.Select();
        }
        
        /// <summary>
        /// Eingabedaten entfernen (Paßwort)
        /// </summary>
        internal void ClearLoginData()
        {
            txtPass.Text = "";
        }
        
        /// <summary>
        /// Anmeldestatus grafisch repräsentieren
        /// </summary>
        /// <param name="loggedIn">An/Abmeldung darstellen</param>
        internal void SetState(bool loggedIn)
        {
        
            if (loggedIn) {
            
                grpLogin.BackColor = Color.Honeydew;
                btnLogin.Text = "Abmelden";
            
            } else {
            
                grpLogin.BackColor = Color.Snow;
                btnLogin.Text = "Anmelden";
            }        
        }

        /// <summary>
        /// Anwender hat auf Login-Button geklickt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginClick(object sender, EventArgs e)
        {
            
            try {
               
                if (Session.Current.User == null) {
                
                    myForm.Login(cboDB.SelectedItem.ToString(), txtUser.Text, txtPass.Text, chkAdept.Checked);            
                    comain.Properties.Settings.Default.Save();
                
                } else {
                
                    myForm.Logout();
                }
            }
            catch (Exception x) { new comain.Error(x); }
        }        

        /// <summary>
        /// Impressum anzeigen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenImpressum(object sender, LinkLabelLinkClickedEventArgs e)
        {

            try {
            
                Impressum frm = new Impressum();
                frm.Show();
            }
            catch (Exception x) { new comain.Error(x); }
        }

        /// <summary>
        /// Returntaste im Usertextfeld springt zum Passwortfeld
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPress(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return) txtPass.Focus();
        }

        /// <summary>
        /// Returntaste im Passworttextfeld probiert Login
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PassPress(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return) LoginClick(sender, null);
        }

        /// <summary>
        /// Hilfe anzeigen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReportTip_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            try {
            
                ProcessStartInfo doc = new ProcessStartInfo("Manual.pdf");
                Process.Start(doc);
            }
            catch (Exception x) { new Error(x); }
        }
    }    
}

﻿using System;
using System.Windows.Forms;
using comain.Workspace;
using comain.Calendar;


namespace comain.Forms
{
    
    public partial class ExportSettings : Form
    {
        
        public ExportSettings()
        {
            InitializeComponent();
        }

        private void ExportSettingsLoad(object sender, EventArgs e)
        {
            
            BaseExportConfiguration cfg = BaseConfigAdapter.LoadExport();
             
            txtYear.Text = cfg.WIYearTemplatePath;
            txtWeek.Text = cfg.WIWeekTemplatePath;   
            txtYearFirst.Value = cfg.WIYearTemplateRow;  
            txtWeekFirst.Value = cfg.WIWeekTemplateRow;  
        }

        private void SaveClick(object sender, EventArgs e)
        {

            BaseExportConfiguration cfg = new BaseExportConfiguration { WIWeekTemplatePath = txtWeek.Text,
                                                                        WIYearTemplatePath = txtYear.Text,
                                                                        WIYearTemplateRow = (int)txtYearFirst.Value,
                                                                        WIWeekTemplateRow = (int)txtWeekFirst.Value };
            BaseConfigAdapter.SaveExport(cfg);
        }

        private void SelectYearClick(object sender, EventArgs e)
        {

            if (dlgOpen.ShowDialog() == DialogResult.Cancel) return;
            txtYear.Text = dlgOpen.FileName;
        }

        private void SelectWeekClick(object sender, EventArgs e)
        {

            if (dlgOpen.ShowDialog() == DialogResult.Cancel) return;
            txtWeek.Text = dlgOpen.FileName;
        }
    }
}

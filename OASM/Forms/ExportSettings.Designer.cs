﻿namespace comain.Forms
{
    partial class ExportSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
        if (disposing && (components != null)) {
        components.Dispose();
        }
        base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportSettings));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.grpTempl = new System.Windows.Forms.GroupBox();
            this.btnSelectWeek = new System.Windows.Forms.Button();
            this.btnSelectYear = new System.Windows.Forms.Button();
            this.txtWeek = new System.Windows.Forms.TextBox();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.txtWeekFirst = new System.Windows.Forms.NumericUpDown();
            this.txtYearFirst = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.dlgOpen = new System.Windows.Forms.OpenFileDialog();
            this.grpTempl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeekFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearFirst)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 343);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(572, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // grpTempl
            // 
            this.grpTempl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpTempl.Controls.Add(this.btnSelectWeek);
            this.grpTempl.Controls.Add(this.btnSelectYear);
            this.grpTempl.Controls.Add(this.txtWeek);
            this.grpTempl.Controls.Add(this.txtYear);
            this.grpTempl.Controls.Add(this.txtWeekFirst);
            this.grpTempl.Controls.Add(this.txtYearFirst);
            this.grpTempl.Controls.Add(this.label3);
            this.grpTempl.Controls.Add(this.label4);
            this.grpTempl.Controls.Add(this.label2);
            this.grpTempl.Controls.Add(this.label1);
            this.grpTempl.Location = new System.Drawing.Point(12, 41);
            this.grpTempl.Name = "grpTempl";
            this.grpTempl.Size = new System.Drawing.Size(548, 292);
            this.grpTempl.TabIndex = 1;
            this.grpTempl.TabStop = false;
            this.grpTempl.Text = "Exportschablonen";
            // 
            // btnSelectWeek
            // 
            this.btnSelectWeek.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectWeek.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectWeek.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectWeek.Image")));
            this.btnSelectWeek.Location = new System.Drawing.Point(510, 97);
            this.btnSelectWeek.Name = "btnSelectWeek";
            this.btnSelectWeek.Size = new System.Drawing.Size(26, 26);
            this.btnSelectWeek.TabIndex = 9;
            this.btnSelectWeek.UseVisualStyleBackColor = true;
            this.btnSelectWeek.Click += new System.EventHandler(this.SelectWeekClick);
            // 
            // btnSelectYear
            // 
            this.btnSelectYear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectYear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectYear.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectYear.Image")));
            this.btnSelectYear.Location = new System.Drawing.Point(510, 29);
            this.btnSelectYear.Name = "btnSelectYear";
            this.btnSelectYear.Size = new System.Drawing.Size(26, 26);
            this.btnSelectYear.TabIndex = 8;
            this.btnSelectYear.UseVisualStyleBackColor = true;
            this.btnSelectYear.Click += new System.EventHandler(this.SelectYearClick);
            // 
            // txtWeek
            // 
            this.txtWeek.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWeek.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.txtWeek.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWeek.Location = new System.Drawing.Point(160, 100);
            this.txtWeek.Name = "txtWeek";
            this.txtWeek.Size = new System.Drawing.Size(344, 21);
            this.txtWeek.TabIndex = 7;
            // 
            // txtYear
            // 
            this.txtYear.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtYear.BackColor = System.Drawing.Color.LightCyan;
            this.txtYear.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtYear.Location = new System.Drawing.Point(160, 32);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(344, 21);
            this.txtYear.TabIndex = 6;
            // 
            // txtWeekFirst
            // 
            this.txtWeekFirst.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.txtWeekFirst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtWeekFirst.Location = new System.Drawing.Point(160, 127);
            this.txtWeekFirst.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtWeekFirst.Name = "txtWeekFirst";
            this.txtWeekFirst.Size = new System.Drawing.Size(71, 21);
            this.txtWeekFirst.TabIndex = 5;
            this.txtWeekFirst.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txtYearFirst
            // 
            this.txtYearFirst.BackColor = System.Drawing.Color.LightCyan;
            this.txtYearFirst.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtYearFirst.Location = new System.Drawing.Point(160, 60);
            this.txtYearFirst.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtYearFirst.Name = "txtYearFirst";
            this.txtYearFirst.Size = new System.Drawing.Size(71, 21);
            this.txtYearFirst.TabIndex = 4;
            this.txtYearFirst.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Erste Ausgabezeile";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Erste Ausgabezeile";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Wochenplanung";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jahresplanung";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.Orange;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(485, 12);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Speichern";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.SaveClick);
            // 
            // dlgOpen
            // 
            this.dlgOpen.Filter = "Excel-2007-Arbeitsmappe (*.xlsx)|*.xlsx|Excel-2007-Vorlage (*.xltx)|*.xltx|Alle D" +
                "ateien|*.*";
            this.dlgOpen.Title = "Excelvorlage wählen";
            // 
            // ExportSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(572, 365);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.grpTempl);
            this.Controls.Add(this.statusStrip1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ExportSettings";
            this.Text = "ExportSettings";
            this.Load += new System.EventHandler(this.ExportSettingsLoad);
            this.grpTempl.ResumeLayout(false);
            this.grpTempl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtWeekFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtYearFirst)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.GroupBox grpTempl;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWeek;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.NumericUpDown txtWeekFirst;
        private System.Windows.Forms.NumericUpDown txtYearFirst;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSelectWeek;
        private System.Windows.Forms.Button btnSelectYear;
        private System.Windows.Forms.OpenFileDialog dlgOpen;
    }
}
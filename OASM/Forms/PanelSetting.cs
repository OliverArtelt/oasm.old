﻿using System;
using System.Drawing;
using System.Windows.Forms;
using comain.Workspace;
using comain.Calendar;


namespace comain.Forms
{
    
    internal partial class SettingPanel : UserControl
    {
         
        IMainFormUpdater myForm; 
         
             
        public SettingPanel(IMainFormUpdater frm)
        {
            
            InitializeComponent();
            BackColor = Color.White;
            myForm = frm;
        }
        
        /// <summary>
        /// Konfiguration laden
        /// </summary>
        internal void Init()
        {

            LoadData();
        }

        /// <summary>
        /// Einstellungen aus DB laden
        /// </summary>
        void LoadData()
        {
        
            try {
           
                BaseConfiguration cfg = BaseConfigAdapter.LoadBase();
               
                cboYear.Items.Clear();           
                for (int i = Session.Current.OASMSettings.MinDate.Year; i <= Session.Current.OASMSettings.MaxDate.Year; i++)
                    cboYear.Items.Add(i);
                cboYear.SelectedItem = cfg.PlanJahr;

                datPlan.Value = cfg.OperDate;
                datGrey.Value = cfg.GreyDate;
                txtPlan.Text = (cfg.OperWeeks.HasValue)? cfg.OperWeeks.Value.ToString(): "";
                txtGrey.Text = (cfg.GreyWeeks.HasValue)? cfg.GreyWeeks.Value.ToString(): "";
                
                radPlan0.Checked = true;
                if (cfg.OperType == BaseConfiguration.UpdateMode.FixDate) radPlan1.Checked = true;
                else if (cfg.OperType == BaseConfiguration.UpdateMode.Dynamic) radPlan2.Checked = true;
                
                radGrey0.Checked = true;
                if (cfg.GreyType == BaseConfiguration.UpdateMode.FixDate) radGrey1.Checked = true;
                else if (cfg.GreyType == BaseConfiguration.UpdateMode.Dynamic) radGrey2.Checked = true;
                
                if (cfg.GreySumType == 1) radGreyPR.Checked = true;
                else radGreyP.Checked = true;
            }
            catch (Exception x) { new comain.Error(x); } 
        }
        
        /// <summary>
        /// Einstellungen in DB sichern
        /// </summary>
        void SaveData()
        {
           
            try {   
                
                BaseConfiguration cfg = new BaseConfiguration();

                int year;
                if (Int32.TryParse(cboYear.Text, out year)) cfg.PlanJahr = year;
                else cfg.PlanJahr = null;
                
                cfg.OperDate = null;   
                cfg.OperWeeks = null;

                if (radPlan1.Checked) cfg.OperDate = datPlan.NullableValue; 
                if (radPlan2.Checked) cfg.OperWeeks = Int32.Parse(txtPlan.Text);
                
                cfg.GreyDate = null;   
                cfg.GreyWeeks = null;   

                if (radGrey1.Checked) cfg.GreyDate = datGrey.NullableValue;
                if (radGrey2.Checked) cfg.GreyWeeks = Int32.Parse(txtGrey.Text);
                
                cfg.GreySumType = radGreyPR.Checked? 1: 0;

                BaseConfigAdapter.SaveBase(cfg);
            }
            catch (Exception x) { new comain.Error(x); } 
        }

        /// <summary>
        /// Administrationsform der Meister öffnen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenAdeptForm(object sender, EventArgs e)
        {

            try {
            
                AdeptForm frm = new AdeptForm();
                frm.ShowDialog(this);
            }
            catch (Exception x) { new comain.Error(x); }
        }

        private void OpenExportForm(object sender, EventArgs e)
        {

            try {
            
                ExportSettings frm = new ExportSettings();
                frm.ShowDialog();
            }
            catch (Exception x) { new comain.Error(x); }
        }


        private void cboYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {

            if (String.IsNullOrEmpty(cboYear.Text)) return;
            short dummy;

            if (!Int16.TryParse(cboYear.Text, out dummy)) {

                MessageBox.Show("Geben Sie das Jahr korrekt an oder entfernen Sie den Wert.");
                cboYear.Focus();
            }
        }
        
        
#region J U M P E R S

        private void SaveClick(object sender, EventArgs e)
        {

            try {

                SaveData();
                myForm.UpdateConfiguration();
            }
            catch (Exception x) { new comain.Error(x); }
        }
        
#endregion
        
    }
}

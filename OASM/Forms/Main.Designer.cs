﻿namespace comain.Forms
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.Images = new System.Windows.Forms.ImageList(this.components);
            this.strState = new System.Windows.Forms.StatusStrip();
            this.strProgress = new System.Windows.Forms.ToolStripProgressBar();
            this.txtFound = new System.Windows.Forms.ToolStripStatusLabel();
            this.ControlTips = new System.Windows.Forms.ToolTip(this.components);
            this.grdMain = new System.Windows.Forms.DataGridView();
            this.KMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuPlan = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuImpl = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuSpecial = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuDay = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuSep1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuAmount = new System.Windows.Forms.ToolStripComboBox();
            this.btnPanelLogin = new System.Windows.Forms.Button();
            this.pnlCmd = new System.Windows.Forms.Panel();
            this.btnPanelProtocol = new System.Windows.Forms.Button();
            this.btnPanelSetting = new System.Windows.Forms.Button();
            this.btnPanelEdit = new System.Windows.Forms.Button();
            this.btnPanelFilter = new System.Windows.Forms.Button();
            this.btnPanelType = new System.Windows.Forms.Button();
            this.PanelHost = new System.Windows.Forms.Panel();
            this.SplitMain = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tmrRedraw = new System.Windows.Forms.Timer(this.components);
            this.strState.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMain)).BeginInit();
            this.KMenu.SuspendLayout();
            this.pnlCmd.SuspendLayout();
            this.SplitMain.Panel1.SuspendLayout();
            this.SplitMain.Panel2.SuspendLayout();
            this.SplitMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Images
            // 
            this.Images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Images.ImageStream")));
            this.Images.TransparentColor = System.Drawing.Color.Magenta;
            this.Images.Images.SetKeyName(0, "Edit_Undo.bmp");
            this.Images.Images.SetKeyName(1, "Edit_Redo.bmp");
            this.Images.Images.SetKeyName(2, "reload.png");
            this.Images.Images.SetKeyName(3, "impl.PNG");
            this.Images.Images.SetKeyName(4, "implS.PNG");
            this.Images.Images.SetKeyName(5, "implT.PNG");
            this.Images.Images.SetKeyName(6, "delete.png");
            // 
            // strState
            // 
            this.strState.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.strProgress,
            this.txtFound});
            this.strState.Location = new System.Drawing.Point(0, 585);
            this.strState.Name = "strState";
            this.strState.Size = new System.Drawing.Size(947, 22);
            this.strState.TabIndex = 2;
            // 
            // strProgress
            // 
            this.strProgress.Name = "strProgress";
            this.strProgress.Size = new System.Drawing.Size(100, 16);
            // 
            // txtFound
            // 
            this.txtFound.AutoSize = false;
            this.txtFound.BackColor = System.Drawing.Color.Transparent;
            this.txtFound.Name = "txtFound";
            this.txtFound.Size = new System.Drawing.Size(150, 17);
            this.txtFound.Text = "4 Aufträge gefunden";
            this.txtFound.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ControlTips
            // 
            this.ControlTips.ShowAlways = true;
            this.ControlTips.Popup += new System.Windows.Forms.PopupEventHandler(this.ControlTips_Popup);
            // 
            // grdMain
            // 
            this.grdMain.AllowUserToAddRows = false;
            this.grdMain.AllowUserToDeleteRows = false;
            this.grdMain.BackgroundColor = System.Drawing.Color.White;
            this.grdMain.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.grdMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMain.ColumnHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdMain.DefaultCellStyle = dataGridViewCellStyle1;
            this.grdMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdMain.GridColor = System.Drawing.Color.Gray;
            this.grdMain.Location = new System.Drawing.Point(0, 0);
            this.grdMain.Name = "grdMain";
            this.grdMain.ReadOnly = true;
            this.grdMain.RowHeadersVisible = false;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gold;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            this.grdMain.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grdMain.RowTemplate.ReadOnly = true;
            this.grdMain.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.grdMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdMain.Size = new System.Drawing.Size(941, 422);
            this.grdMain.TabIndex = 3;
            this.grdMain.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.GridFormatData);
            this.grdMain.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.CellMouseClick);
            this.grdMain.CellMouseLeave += new System.Windows.Forms.DataGridViewCellEventHandler(this.CellMouseLeave);
            this.grdMain.CellMouseMove += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.CellMouseMove);
            this.grdMain.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.GridPaint);
            this.grdMain.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.GridColumnWidthChanged);
            this.grdMain.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridRowEnter);
            this.grdMain.Scroll += new System.Windows.Forms.ScrollEventHandler(this.GridScroll);
            // 
            // KMenu
            // 
            this.KMenu.AllowMerge = false;
            this.KMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuPlan,
            this.MenuImpl,
            this.MenuSpecial,
            this.MenuDay,
            this.MenuDelete,
            this.MenuSep1,
            this.MenuAmount});
            this.KMenu.Name = "contextMenuStrip1";
            this.KMenu.Size = new System.Drawing.Size(157, 145);
            // 
            // MenuPlan
            // 
            this.MenuPlan.ForeColor = System.Drawing.Color.Black;
            this.MenuPlan.Image = ((System.Drawing.Image)(resources.GetObject("MenuPlan.Image")));
            this.MenuPlan.ImageTransparentColor = System.Drawing.Color.Black;
            this.MenuPlan.Name = "MenuPlan";
            this.MenuPlan.Size = new System.Drawing.Size(156, 22);
            this.MenuPlan.Text = "Planen";
            this.MenuPlan.ToolTipText = "Neuen Termin an diesen Zeitpunkt eintragen";
            this.MenuPlan.Click += new System.EventHandler(this.MenuPlanClick);
            // 
            // MenuImpl
            // 
            this.MenuImpl.Image = ((System.Drawing.Image)(resources.GetObject("MenuImpl.Image")));
            this.MenuImpl.Name = "MenuImpl";
            this.MenuImpl.Size = new System.Drawing.Size(156, 22);
            this.MenuImpl.Text = "Realisieren";
            this.MenuImpl.ToolTipText = "Termin realisieren";
            this.MenuImpl.Click += new System.EventHandler(this.MenuImplClick);
            // 
            // MenuSpecial
            // 
            this.MenuSpecial.Image = ((System.Drawing.Image)(resources.GetObject("MenuSpecial.Image")));
            this.MenuSpecial.Name = "MenuSpecial";
            this.MenuSpecial.Size = new System.Drawing.Size(156, 22);
            this.MenuSpecial.Text = "Sonderleistung";
            this.MenuSpecial.ToolTipText = "Termin als Sonderleistung realisieren";
            this.MenuSpecial.Click += new System.EventHandler(this.MenuSpecialClick);
            // 
            // MenuDay
            // 
            this.MenuDay.Image = ((System.Drawing.Image)(resources.GetObject("MenuDay.Image")));
            this.MenuDay.Name = "MenuDay";
            this.MenuDay.Size = new System.Drawing.Size(156, 22);
            this.MenuDay.Text = "Tagesauftrag";
            this.MenuDay.ToolTipText = "Termin als Tagesauftrag realisieren";
            this.MenuDay.Click += new System.EventHandler(this.MenuDayClick);
            // 
            // MenuDelete
            // 
            this.MenuDelete.Image = ((System.Drawing.Image)(resources.GetObject("MenuDelete.Image")));
            this.MenuDelete.Name = "MenuDelete";
            this.MenuDelete.Size = new System.Drawing.Size(156, 22);
            this.MenuDelete.Text = "Löschen";
            this.MenuDelete.ToolTipText = "Termin oder Realisierung entfernen";
            this.MenuDelete.Click += new System.EventHandler(this.MenuDeleteClick);
            // 
            // MenuSep1
            // 
            this.MenuSep1.Name = "MenuSep1";
            this.MenuSep1.Size = new System.Drawing.Size(153, 6);
            // 
            // MenuAmount
            // 
            this.MenuAmount.DropDownHeight = 500;
            this.MenuAmount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MenuAmount.DropDownWidth = 50;
            this.MenuAmount.IntegralHeight = false;
            this.MenuAmount.Items.AddRange(new object[] {
            "1x",
            "2x",
            "3x",
            "4x",
            "5x",
            "6x",
            "7x",
            "8x",
            "9x"});
            this.MenuAmount.MaxDropDownItems = 10;
            this.MenuAmount.MergeIndex = 0;
            this.MenuAmount.Name = "MenuAmount";
            this.MenuAmount.Size = new System.Drawing.Size(75, 21);
            this.MenuAmount.Tag = "";
            this.MenuAmount.ToolTipText = "Geplante oder realisierte Anzahl";
            // 
            // btnPanelLogin
            // 
            this.btnPanelLogin.BackColor = System.Drawing.Color.PowderBlue;
            this.btnPanelLogin.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btnPanelLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnPanelLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.btnPanelLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPanelLogin.Location = new System.Drawing.Point(4, 3);
            this.btnPanelLogin.Name = "btnPanelLogin";
            this.btnPanelLogin.Size = new System.Drawing.Size(83, 23);
            this.btnPanelLogin.TabIndex = 4;
            this.btnPanelLogin.Text = "Login";
            this.btnPanelLogin.UseVisualStyleBackColor = false;
            this.btnPanelLogin.Click += new System.EventHandler(this.SelectLoginPanel);
            // 
            // pnlCmd
            // 
            this.pnlCmd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlCmd.BackColor = System.Drawing.Color.White;
            this.pnlCmd.Controls.Add(this.btnPanelProtocol);
            this.pnlCmd.Controls.Add(this.btnPanelSetting);
            this.pnlCmd.Controls.Add(this.btnPanelEdit);
            this.pnlCmd.Controls.Add(this.btnPanelFilter);
            this.pnlCmd.Controls.Add(this.btnPanelType);
            this.pnlCmd.Controls.Add(this.btnPanelLogin);
            this.pnlCmd.Location = new System.Drawing.Point(3, 1);
            this.pnlCmd.Name = "pnlCmd";
            this.pnlCmd.Size = new System.Drawing.Size(941, 29);
            this.pnlCmd.TabIndex = 1;
            // 
            // btnPanelProtocol
            // 
            this.btnPanelProtocol.BackColor = System.Drawing.Color.PowderBlue;
            this.btnPanelProtocol.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btnPanelProtocol.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnPanelProtocol.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.btnPanelProtocol.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPanelProtocol.Location = new System.Drawing.Point(449, 3);
            this.btnPanelProtocol.Name = "btnPanelProtocol";
            this.btnPanelProtocol.Size = new System.Drawing.Size(83, 23);
            this.btnPanelProtocol.TabIndex = 9;
            this.btnPanelProtocol.Text = "Protokoll";
            this.btnPanelProtocol.UseVisualStyleBackColor = false;
            this.btnPanelProtocol.Click += new System.EventHandler(this.SelectProtocolPanel);
            // 
            // btnPanelSetting
            // 
            this.btnPanelSetting.BackColor = System.Drawing.Color.PowderBlue;
            this.btnPanelSetting.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btnPanelSetting.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnPanelSetting.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.btnPanelSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPanelSetting.Location = new System.Drawing.Point(360, 3);
            this.btnPanelSetting.Name = "btnPanelSetting";
            this.btnPanelSetting.Size = new System.Drawing.Size(83, 23);
            this.btnPanelSetting.TabIndex = 8;
            this.btnPanelSetting.Text = "Konfiguration";
            this.btnPanelSetting.UseVisualStyleBackColor = false;
            this.btnPanelSetting.Click += new System.EventHandler(this.SelectSettingPanel);
            // 
            // btnPanelEdit
            // 
            this.btnPanelEdit.BackColor = System.Drawing.Color.PowderBlue;
            this.btnPanelEdit.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btnPanelEdit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnPanelEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.btnPanelEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPanelEdit.Location = new System.Drawing.Point(271, 3);
            this.btnPanelEdit.Name = "btnPanelEdit";
            this.btnPanelEdit.Size = new System.Drawing.Size(83, 23);
            this.btnPanelEdit.TabIndex = 7;
            this.btnPanelEdit.Text = "Bearbeiten";
            this.btnPanelEdit.UseVisualStyleBackColor = false;
            this.btnPanelEdit.Click += new System.EventHandler(this.SelectEditPanel);
            // 
            // btnPanelFilter
            // 
            this.btnPanelFilter.BackColor = System.Drawing.Color.PowderBlue;
            this.btnPanelFilter.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btnPanelFilter.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnPanelFilter.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.btnPanelFilter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPanelFilter.Location = new System.Drawing.Point(182, 3);
            this.btnPanelFilter.Name = "btnPanelFilter";
            this.btnPanelFilter.Size = new System.Drawing.Size(83, 23);
            this.btnPanelFilter.TabIndex = 6;
            this.btnPanelFilter.Text = "Filter";
            this.btnPanelFilter.UseVisualStyleBackColor = false;
            this.btnPanelFilter.Click += new System.EventHandler(this.SelectFilterPanel);
            // 
            // btnPanelType
            // 
            this.btnPanelType.BackColor = System.Drawing.Color.PaleGreen;
            this.btnPanelType.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue;
            this.btnPanelType.FlatAppearance.MouseDownBackColor = System.Drawing.Color.SteelBlue;
            this.btnPanelType.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightCyan;
            this.btnPanelType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPanelType.Location = new System.Drawing.Point(93, 3);
            this.btnPanelType.Name = "btnPanelType";
            this.btnPanelType.Size = new System.Drawing.Size(83, 23);
            this.btnPanelType.TabIndex = 5;
            this.btnPanelType.Text = "Planungstyp";
            this.btnPanelType.UseVisualStyleBackColor = false;
            this.btnPanelType.Click += new System.EventHandler(this.SelectTypePanel);
            // 
            // PanelHost
            // 
            this.PanelHost.BackColor = System.Drawing.Color.White;
            this.PanelHost.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PanelHost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelHost.Location = new System.Drawing.Point(0, 0);
            this.PanelHost.Name = "PanelHost";
            this.PanelHost.Size = new System.Drawing.Size(941, 123);
            this.PanelHost.TabIndex = 0;
            this.PanelHost.TabStop = true;
            // 
            // SplitMain
            // 
            this.SplitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitMain.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SplitMain.Location = new System.Drawing.Point(0, 0);
            this.SplitMain.Name = "SplitMain";
            this.SplitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitMain.Panel1
            // 
            this.SplitMain.Panel1.Controls.Add(this.PanelHost);
            // 
            // SplitMain.Panel2
            // 
            this.SplitMain.Panel2.Controls.Add(this.grdMain);
            this.SplitMain.Size = new System.Drawing.Size(941, 549);
            this.SplitMain.SplitterDistance = 123;
            this.SplitMain.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.SplitMain);
            this.panel1.Location = new System.Drawing.Point(3, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(941, 549);
            this.panel1.TabIndex = 8;
            // 
            // tmrRedraw
            // 
            this.tmrRedraw.Interval = 250;
            this.tmrRedraw.Tick += new System.EventHandler(this.TimerTick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(947, 607);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.strState);
            this.Controls.Add(this.pnlCmd);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(695, 299);
            this.Name = "Main";
            this.Text = "Online-Auftragsschlussmeldung";
            this.Load += new System.EventHandler(this.MainLoad);
            this.strState.ResumeLayout(false);
            this.strState.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMain)).EndInit();
            this.KMenu.ResumeLayout(false);
            this.pnlCmd.ResumeLayout(false);
            this.SplitMain.Panel1.ResumeLayout(false);
            this.SplitMain.Panel2.ResumeLayout(false);
            this.SplitMain.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList Images;
        private System.Windows.Forms.StatusStrip strState;
        private System.Windows.Forms.ToolStripProgressBar strProgress;
        private System.Windows.Forms.ToolTip ControlTips;
        private System.Windows.Forms.DataGridView grdMain;
        private System.Windows.Forms.ContextMenuStrip KMenu;
        private System.Windows.Forms.ToolStripSeparator MenuSep1;
        private System.Windows.Forms.ToolStripComboBox MenuAmount;
        private System.Windows.Forms.Button btnPanelLogin;
        private System.Windows.Forms.Panel pnlCmd;
        private System.Windows.Forms.Button btnPanelSetting;
        private System.Windows.Forms.Button btnPanelEdit;
        private System.Windows.Forms.Button btnPanelFilter;
        private System.Windows.Forms.Button btnPanelType;
        private System.Windows.Forms.Panel PanelHost;
        internal System.Windows.Forms.ToolStripStatusLabel txtFound;
        internal System.Windows.Forms.ToolStripMenuItem MenuPlan;
        internal System.Windows.Forms.ToolStripMenuItem MenuImpl;
        internal System.Windows.Forms.ToolStripMenuItem MenuSpecial;
        internal System.Windows.Forms.ToolStripMenuItem MenuDay;
        internal System.Windows.Forms.ToolStripMenuItem MenuDelete;
        private System.Windows.Forms.Button btnPanelProtocol;
        private System.Windows.Forms.SplitContainer SplitMain;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer tmrRedraw;



    }
}
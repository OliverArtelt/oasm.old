﻿using System.Windows.Forms;
using comain.User;
using comain.Workspace;


namespace comain.Forms
{
    
    /// <summary>
    /// Zustände des Hauptformulars
    /// </summary>
    interface IMainFormState
    {
    
        /// <summary>
        /// Zustandsübergang Benutzer ist abgemeldet
        /// </summary>
        void LoggedOut();
        /// <summary>
        /// Zustandsübergang Benutzer ist angemeldet / hat Planungstyp gewählt
        /// </summary>
        void LoggedIn();
        /// <summary>
        /// Zustandsübergang Benutzer hat Plan erstellt und kann ihn bearbeiten
        /// </summary>
        void Schedule();
    }


    /// <summary>
    /// Zustand Benutzer ist abgemeldet
    /// </summary>
    class LoggedOff : IMainFormState
    {
    
        /// <summary>
        /// einzustellendes Hauptformular
        /// </summary>
        Main myForm;
    
        /// <summary>
        /// Benutzer abmelden
        /// </summary>
        /// <param name="frm">einzustellendes Hauptformular</param>
        public LoggedOff(Main frm)
        {
        
            myForm = frm;

            myForm.EnablePanels(false, false, false, false);
            myForm.SetLoginState(false);
            myForm.Text = Application.ProductName;
            myForm.txtFound.Text = "";
            Session.Current.UserActions = null;
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist abgemeldet
        /// </summary>
        public void LoggedOut()
        {
            return;
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist angemeldet / hat Planungstyp gewählt
        /// </summary>
        public void LoggedIn()
        {
            if (Session.Current.User is ClientUser) myForm.State = new ClientUserLoggedIn(myForm);
            else                                    myForm.State = new ServiceUserLoggedIn(myForm);
        }

        /// <summary>
        /// Zustandsübergang Benutzer hat Plan erstellt und kann ihn bearbeiten
        /// </summary>
        public void Schedule()
        {
            return;
        }
    }

    /// <summary>
    /// Zustand Administrator ist angemeldet / hat Planungstyp gewählt
    /// </summary>
    class ServiceUserLoggedIn : IMainFormState
    {
    
        /// <summary>
        /// einzustellendes Hauptformular
        /// </summary>
        Main myForm;
    
        /// <summary>
        /// Administrator anmelden / anderen Planungstyp wählen
        /// </summary>
        /// <param name="frm">einzustellendes Hauptformular</param>
        public ServiceUserLoggedIn(Main frm)
        {
            myForm = frm;

            myForm.EnablePanels(true, true, false, Session.Current.User.MayAdminister);
            myForm.SetLoginState(true);
            myForm.Text = Application.ProductName + " " + Session.Current.Database + ", " + Session.Current.User.Name;
            myForm.txtFound.Text = "";
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist abgemeldet
        /// </summary>
        public void LoggedOut()
        {
            myForm.State = new LoggedOff(myForm);
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist angemeldet / hat Planungstyp gewählt
        /// </summary>
        public void LoggedIn()
        {
            myForm.Text = Application.ProductName + " " + Session.Current.Database + ", " + Session.Current.User.Name + ", " + Session.Current.SelectedScheduleName;
        }

        /// <summary>
        /// Zustandsübergang Benutzer hat Plan erstellt und kann ihn bearbeiten
        /// </summary>
        public void Schedule()
        {
            myForm.State = new ServiceUserSchedule(myForm);
        }
    }

    /// <summary>
    /// Zustand Administrator hat Plan erstellt und kann ihn bearbeiten
    /// </summary>
    class ServiceUserSchedule : IMainFormState
    {
    
        /// <summary>
        /// einzustellendes Hauptformular
        /// </summary>
        Main myForm;
    
        /// <summary>
        /// Planbearbeitungswerkzeuge für Administrator bereitstellen
        /// </summary>
        /// <param name="frm">einzustellendes Hauptformular</param>
        public ServiceUserSchedule(Main frm)
        {
            myForm = frm;

            myForm.SetLoginState(true);
            myForm.EnablePanels(true, true, true, Session.Current.User.MayAdminister);
            myForm.Text = Application.ProductName + " " + Session.Current.Database + ", " + Session.Current.User.Name + ", " + Session.Current.SelectedScheduleName;
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist abgemeldet
        /// </summary>
        public void LoggedOut()
        {
            myForm.State = new LoggedOff(myForm);
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist angemeldet / hat Planungstyp gewählt
        /// </summary>
        public void LoggedIn()
        {
            myForm.State = new ServiceUserLoggedIn(myForm);
        }

        /// <summary>
        /// Zustandsübergang Benutzer hat Plan erstellt und kann ihn bearbeiten
        /// </summary>
        public void Schedule()
        {
            return;
        }
    }

    /// <summary>
    /// Zustand Meister ist angemeldet / hat Planungstyp gewählt
    /// </summary>
    class ClientUserLoggedIn : IMainFormState
    {
    
        /// <summary>
        /// einzustellendes Hauptformular
        /// </summary>
        Main myForm;
    
        /// <summary>
        /// Meister anmelden / anderen Planungstyp wählen
        /// </summary>
        /// <param name="frm">einzustellendes Hauptformular</param>
        public ClientUserLoggedIn(Main frm)
        {
            myForm = frm;

            myForm.SetLoginState(true);
            myForm.EnablePanels(true, true, false, false);
            myForm.Text = Application.ProductName + " " + Session.Current.Database + ", " + Session.Current.User.Name;
            myForm.txtFound.Text = "";
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist abgemeldet
        /// </summary>
        public void LoggedOut()
        {
            myForm.State = new LoggedOff(myForm);
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist angemeldet / hat Planungstyp gewählt
        /// </summary>
        public void LoggedIn()
        {
            myForm.Text = Application.ProductName + " " + Session.Current.Database + ", " + Session.Current.User.Name + ", " + Session.Current.SelectedScheduleName;
        }

        /// <summary>
        /// Zustandsübergang Benutzer hat Plan erstellt und kann ihn bearbeiten
        /// </summary>
        public void Schedule()
        {
            myForm.State = new ClientUserSchedule(myForm);
        }
    }

    /// <summary>
    /// Zustand Meister hat Plan erstellt und kann ihn bearbeiten
    /// </summary>
    class ClientUserSchedule : IMainFormState
    {
    
        /// <summary>
        /// einzustellendes Hauptformular
        /// </summary>
        Main myForm;
    
        /// <summary>
        /// Planbearbeitungswerkzeuge für Meister bereitstellen
        /// </summary>
        /// <param name="frm">einzustellendes Hauptformular</param>
        public ClientUserSchedule(Main frm)
        {
            myForm = frm;

            myForm.SetLoginState(true);
            myForm.EnablePanels(true, true, true, false);
            myForm.SelectEditPanel(null, null);
            myForm.Text = Application.ProductName + " " + Session.Current.Database + ", " + Session.Current.User.Name + ", " + Session.Current.SelectedScheduleName;
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist abgemeldet
        /// </summary>
        public void LoggedOut()
        {
            myForm.State = new LoggedOff(myForm);
        }

        /// <summary>
        /// Zustandsübergang Benutzer ist angemeldet / hat Planungstyp gewählt
        /// </summary>
        public void LoggedIn()
        {
            myForm.State = new ClientUserLoggedIn(myForm);
        }

        /// <summary>
        /// Zustandsübergang Benutzer hat Plan erstellt und kann ihn bearbeiten
        /// </summary>
        public void Schedule()
        {
            return;
        }
    }
}

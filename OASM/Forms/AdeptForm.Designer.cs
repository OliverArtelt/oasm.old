﻿namespace comain.Forms
{
    partial class AdeptForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdeptForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splMain = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grdList = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bndAdept = new System.Windows.Forms.BindingSource(this.components);
            this.dsMaster = new comain.User.Master();
            this.pnlKST = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.navKstRole = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bndAdeptKstRole = new System.Windows.Forms.BindingSource(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.grdKST = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlCmd = new System.Windows.Forms.Panel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.pnlMask = new System.Windows.Forms.Panel();
            this.chkHour = new System.Windows.Forms.CheckBox();
            this.chkPrice = new System.Windows.Forms.CheckBox();
            this.chkPlan = new System.Windows.Forms.CheckBox();
            this.grpPass = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPass = new System.Windows.Forms.Button();
            this.txtPass1 = new System.Windows.Forms.TextBox();
            this.txtPass2 = new System.Windows.Forms.TextBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.strMain = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.errHandler = new System.Windows.Forms.ErrorProvider(this.components);
            this.splMain.Panel1.SuspendLayout();
            this.splMain.Panel2.SuspendLayout();
            this.splMain.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndAdept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsMaster)).BeginInit();
            this.pnlKST.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navKstRole)).BeginInit();
            this.navKstRole.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bndAdeptKstRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdKST)).BeginInit();
            this.pnlCmd.SuspendLayout();
            this.pnlMask.SuspendLayout();
            this.grpPass.SuspendLayout();
            this.strMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errHandler)).BeginInit();
            this.SuspendLayout();
            // 
            // splMain
            // 
            this.splMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splMain.BackColor = System.Drawing.Color.Coral;
            this.splMain.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splMain.Location = new System.Drawing.Point(0, 0);
            this.splMain.Name = "splMain";
            // 
            // splMain.Panel1
            // 
            this.splMain.Panel1.Controls.Add(this.panel1);
            // 
            // splMain.Panel2
            // 
            this.splMain.Panel2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.splMain.Panel2.Controls.Add(this.pnlKST);
            this.splMain.Panel2.Controls.Add(this.pnlCmd);
            this.splMain.Panel2.Controls.Add(this.pnlMask);
            this.splMain.Size = new System.Drawing.Size(805, 529);
            this.splMain.SplitterDistance = 238;
            this.splMain.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.grdList);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(236, 527);
            this.panel1.TabIndex = 0;
            // 
            // grdList
            // 
            this.grdList.AllowUserToAddRows = false;
            this.grdList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AntiqueWhite;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.grdList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.grdList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdList.AutoGenerateColumns = false;
            this.grdList.BackgroundColor = System.Drawing.Color.White;
            this.grdList.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdList.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdList.ColumnHeadersVisible = false;
            this.grdList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.grdList.DataSource = this.bndAdept;
            this.grdList.Location = new System.Drawing.Point(4, 5);
            this.grdList.MultiSelect = false;
            this.grdList.Name = "grdList";
            this.grdList.ReadOnly = true;
            this.grdList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.grdList.RowHeadersVisible = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Moccasin;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.grdList.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.grdList.RowTemplate.Height = 20;
            this.grdList.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdList.Size = new System.Drawing.Size(228, 518);
            this.grdList.TabIndex = 0;
            this.grdList.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataError);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn1.DataPropertyName = "name";
            this.dataGridViewTextBoxColumn1.HeaderText = "name";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // bndAdept
            // 
            this.bndAdept.DataMember = "Adept";
            this.bndAdept.DataSource = this.dsMaster;
            this.bndAdept.Sort = "name";
            this.bndAdept.DataError += new System.Windows.Forms.BindingManagerDataErrorEventHandler(this.DataError);
            // 
            // dsMaster
            // 
            this.dsMaster.DataSetName = "Master";
            this.dsMaster.Prefix = "comain.User";
            this.dsMaster.RemotingFormat = System.Data.SerializationFormat.Binary;
            this.dsMaster.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pnlKST
            // 
            this.pnlKST.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlKST.BackColor = System.Drawing.Color.White;
            this.pnlKST.Controls.Add(this.textBox1);
            this.pnlKST.Controls.Add(this.navKstRole);
            this.pnlKST.Controls.Add(this.grdKST);
            this.pnlKST.Location = new System.Drawing.Point(4, 204);
            this.pnlKST.Name = "pnlKST";
            this.pnlKST.Size = new System.Drawing.Size(554, 321);
            this.pnlKST.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Location = new System.Drawing.Point(3, 28);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(523, 48);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // navKstRole
            // 
            this.navKstRole.AddNewItem = this.bindingNavigatorAddNewItem;
            this.navKstRole.BindingSource = this.bndAdeptKstRole;
            this.navKstRole.CountItem = this.bindingNavigatorCountItem;
            this.navKstRole.CountItemFormat = "von {0}";
            this.navKstRole.DeleteItem = this.bindingNavigatorDeleteItem;
            this.navKstRole.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.navKstRole.Location = new System.Drawing.Point(0, 0);
            this.navKstRole.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.navKstRole.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.navKstRole.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.navKstRole.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.navKstRole.Name = "navKstRole";
            this.navKstRole.PositionItem = this.bindingNavigatorPositionItem;
            this.navKstRole.Size = new System.Drawing.Size(554, 25);
            this.navKstRole.TabIndex = 1;
            this.navKstRole.Text = "navKst";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Zum neuen Datensatz gehen";
            // 
            // bndAdeptKstRole
            // 
            this.bndAdeptKstRole.DataMember = "AdeptKstRole";
            this.bndAdeptKstRole.DataSource = this.bndAdept;
            this.bndAdeptKstRole.DataError += new System.Windows.Forms.BindingManagerDataErrorEventHandler(this.DataError);
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(44, 22);
            this.bindingNavigatorCountItem.Text = "von {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Anzahl Datensätze";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Datensatz entfernen";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Zum ersten Datensatz gehen";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Zum vorigen Datensatz gehen";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Aktueller Datensatz";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Zum nächsten Datensatz gehen";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Zum letzten Datensatz gehen";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // grdKST
            // 
            this.grdKST.AllowUserToAddRows = false;
            this.grdKST.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.grdKST.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.grdKST.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdKST.AutoGenerateColumns = false;
            this.grdKST.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdKST.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.grdKST.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdKST.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.grdKST.DataSource = this.bndAdeptKstRole;
            this.grdKST.Location = new System.Drawing.Point(3, 79);
            this.grdKST.Name = "grdKST";
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.grdKST.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.grdKST.RowTemplate.Height = 20;
            this.grdKST.Size = new System.Drawing.Size(548, 239);
            this.grdKST.TabIndex = 2;
            this.grdKST.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.DataError);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "kstvon";
            this.dataGridViewTextBoxColumn2.HeaderText = "KST-Bereich von";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 150;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "kstbis";
            this.dataGridViewTextBoxColumn3.HeaderText = "KST-Bereich bis";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 150;
            // 
            // pnlCmd
            // 
            this.pnlCmd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlCmd.BackColor = System.Drawing.Color.White;
            this.pnlCmd.Controls.Add(this.btnSave);
            this.pnlCmd.Controls.Add(this.btnDelete);
            this.pnlCmd.Controls.Add(this.btnAdd);
            this.pnlCmd.Location = new System.Drawing.Point(4, 3);
            this.pnlCmd.Name = "pnlCmd";
            this.pnlCmd.Size = new System.Drawing.Size(554, 48);
            this.pnlCmd.TabIndex = 0;
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Orange;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(169, 8);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 5;
            this.btnSave.Text = "Speichern";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.Save);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Orange;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Location = new System.Drawing.Point(88, 8);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Entfernen";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.Delete);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Orange;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Location = new System.Drawing.Point(7, 8);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Anlegen";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.Add);
            // 
            // pnlMask
            // 
            this.pnlMask.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlMask.BackColor = System.Drawing.Color.White;
            this.pnlMask.Controls.Add(this.chkHour);
            this.pnlMask.Controls.Add(this.chkPrice);
            this.pnlMask.Controls.Add(this.chkPlan);
            this.pnlMask.Controls.Add(this.grpPass);
            this.pnlMask.Controls.Add(this.txtUser);
            this.pnlMask.Controls.Add(this.txtName);
            this.pnlMask.Controls.Add(this.label2);
            this.pnlMask.Controls.Add(this.label1);
            this.pnlMask.Location = new System.Drawing.Point(4, 57);
            this.pnlMask.Name = "pnlMask";
            this.pnlMask.Size = new System.Drawing.Size(554, 141);
            this.pnlMask.TabIndex = 1;
            // 
            // chkHour
            // 
            this.chkHour.AutoSize = true;
            this.chkHour.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bndAdept, "showhours", true));
            this.chkHour.Location = new System.Drawing.Point(105, 114);
            this.chkHour.Name = "chkHour";
            this.chkHour.Size = new System.Drawing.Size(121, 17);
            this.chkHour.TabIndex = 12;
            this.chkHour.Text = "darf Stunden sehen";
            this.chkHour.UseVisualStyleBackColor = true;
            // 
            // chkPrice
            // 
            this.chkPrice.AutoSize = true;
            this.chkPrice.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bndAdept, "showprices", true));
            this.chkPrice.Location = new System.Drawing.Point(105, 91);
            this.chkPrice.Name = "chkPrice";
            this.chkPrice.Size = new System.Drawing.Size(110, 17);
            this.chkPrice.TabIndex = 11;
            this.chkPrice.Text = "darf Preise sehen";
            this.chkPrice.UseVisualStyleBackColor = true;
            // 
            // chkPlan
            // 
            this.chkPlan.AutoSize = true;
            this.chkPlan.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.bndAdept, "plannable", true));
            this.chkPlan.Location = new System.Drawing.Point(105, 68);
            this.chkPlan.Name = "chkPlan";
            this.chkPlan.Size = new System.Drawing.Size(186, 17);
            this.chkPlan.TabIndex = 10;
            this.chkPlan.Text = "darf Termine planen oder löschen";
            this.chkPlan.UseVisualStyleBackColor = true;
            // 
            // grpPass
            // 
            this.grpPass.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpPass.BackColor = System.Drawing.Color.AliceBlue;
            this.grpPass.Controls.Add(this.label5);
            this.grpPass.Controls.Add(this.label3);
            this.grpPass.Controls.Add(this.btnPass);
            this.grpPass.Controls.Add(this.txtPass1);
            this.grpPass.Controls.Add(this.txtPass2);
            this.grpPass.Location = new System.Drawing.Point(376, 3);
            this.grpPass.Name = "grpPass";
            this.grpPass.Size = new System.Drawing.Size(175, 105);
            this.grpPass.TabIndex = 9;
            this.grpPass.TabStop = false;
            this.grpPass.Text = "Passwort setzen";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Probe 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Probe 1";
            // 
            // btnPass
            // 
            this.btnPass.BackColor = System.Drawing.Color.Orange;
            this.btnPass.FlatAppearance.BorderSize = 0;
            this.btnPass.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnPass.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPass.Location = new System.Drawing.Point(94, 73);
            this.btnPass.Name = "btnPass";
            this.btnPass.Size = new System.Drawing.Size(75, 23);
            this.btnPass.TabIndex = 5;
            this.btnPass.Text = "Setzen";
            this.btnPass.UseVisualStyleBackColor = false;
            this.btnPass.Click += new System.EventHandler(this.SetPassword);
            // 
            // txtPass1
            // 
            this.txtPass1.Font = new System.Drawing.Font("Wingdings", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txtPass1.Location = new System.Drawing.Point(68, 19);
            this.txtPass1.Name = "txtPass1";
            this.txtPass1.PasswordChar = 'l';
            this.txtPass1.Size = new System.Drawing.Size(101, 20);
            this.txtPass1.TabIndex = 3;
            // 
            // txtPass2
            // 
            this.txtPass2.Font = new System.Drawing.Font("Wingdings", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txtPass2.Location = new System.Drawing.Point(68, 47);
            this.txtPass2.Name = "txtPass2";
            this.txtPass2.PasswordChar = 'l';
            this.txtPass2.Size = new System.Drawing.Size(101, 20);
            this.txtPass2.TabIndex = 4;
            // 
            // txtUser
            // 
            this.txtUser.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUser.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bndAdept, "login", true));
            this.txtUser.Location = new System.Drawing.Point(105, 39);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(235, 21);
            this.txtUser.TabIndex = 1;
            this.txtUser.Validating += new System.ComponentModel.CancelEventHandler(this.txtUser_Validating);
            // 
            // txtName
            // 
            this.txtName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bndAdept, "name", true));
            this.txtName.Location = new System.Drawing.Point(105, 12);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(235, 21);
            this.txtName.TabIndex = 0;
            this.txtName.Validating += new System.ComponentModel.CancelEventHandler(this.txtName_Validating);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Anmeldename";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bezeichnung";
            // 
            // strMain
            // 
            this.strMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1});
            this.strMain.Location = new System.Drawing.Point(0, 530);
            this.strMain.Name = "strMain";
            this.strMain.Size = new System.Drawing.Size(805, 22);
            this.strMain.TabIndex = 1;
            this.strMain.Text = "statusStrip1";
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 16);
            // 
            // errHandler
            // 
            this.errHandler.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errHandler.ContainerControl = this;
            this.errHandler.DataSource = this.bndAdept;
            // 
            // AdeptForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(805, 552);
            this.Controls.Add(this.strMain);
            this.Controls.Add(this.splMain);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AdeptForm";
            this.Text = "Meister einrichten";
            this.Load += new System.EventHandler(this.AdeptLoad);
            this.splMain.Panel1.ResumeLayout(false);
            this.splMain.Panel2.ResumeLayout(false);
            this.splMain.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndAdept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsMaster)).EndInit();
            this.pnlKST.ResumeLayout(false);
            this.pnlKST.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navKstRole)).EndInit();
            this.navKstRole.ResumeLayout(false);
            this.navKstRole.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bndAdeptKstRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdKST)).EndInit();
            this.pnlCmd.ResumeLayout(false);
            this.pnlMask.ResumeLayout(false);
            this.pnlMask.PerformLayout();
            this.grpPass.ResumeLayout(false);
            this.grpPass.PerformLayout();
            this.strMain.ResumeLayout(false);
            this.strMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errHandler)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splMain;
        private System.Windows.Forms.DataGridView grdList;
        private System.Windows.Forms.BindingSource bndAdept;
        private System.Windows.Forms.StatusStrip strMain;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.Panel pnlMask;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPass2;
        private System.Windows.Forms.TextBox txtPass1;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Panel pnlCmd;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView grdKST;
        private System.Windows.Forms.BindingSource bndAdeptKstRole;
        private System.Windows.Forms.Panel pnlKST;
        private System.Windows.Forms.BindingNavigator navKstRole;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.GroupBox grpPass;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnPass;
        private System.Windows.Forms.ErrorProvider errHandler;
        private System.Windows.Forms.CheckBox chkPlan;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.CheckBox chkHour;
        private System.Windows.Forms.CheckBox chkPrice;
        private comain.User.Master dsMaster;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}
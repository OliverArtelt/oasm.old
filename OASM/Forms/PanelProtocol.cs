﻿using System;
using System.Drawing;
using System.Windows.Forms;
using comain.Reports;
using comain.Workspace;


namespace comain.Forms
{
    
    public partial class ProtocolPanel : UserControl
    {
        
        IMainFormUpdater myForm;
        

        public ProtocolPanel(IMainFormUpdater frm)
        {
            
            InitializeComponent();
            
            myForm = frm;
            BackColor = Color.White;
        }

 
        internal void Init()
        {
            FilterReset(this, null);
        }
        

        private void ProtocolDelete(object sender, EventArgs e)
        {

            try {
            
                CheckPeriod();
                
                String msg = String.Format("Wollen Sie die Protokolleinträge zwischen {0} und {1} löschen?", 
                                           datVon.NullableValue.Value.ToString("d"), datBis.NullableValue.Value.ToString("d"));
                if (MessageBox.Show(msg, Application.ProductName, MessageBoxButtons.YesNo) == DialogResult.No) return;
                
                int i = UserProtocol.DeleteEntries(datVon.NullableValue.Value, datBis.NullableValue.Value);
                myForm.SetWorkState(String.Format("{0} Einträge gelöscht.", i));
            }
            catch (Exception x) { new Error(x); }
        }

        private void FilterReset(object sender, EventArgs e)
        {

            cboType.SelectedIndex = 0;
            
            datVon.NullableValue = DateTime.Today.AddDays(-14);
            datBis.NullableValue = DateTime.Today;
            datAtVon.NullableValue = null;
            datAtBis.NullableValue = null;
            txtUser.Text = "";
            txtAuCode.Text = "";
        }

        private void ProtocolShow(object sender, EventArgs e)
        {

            try {
            
                CheckPeriod();
                
                UserProtocol p = new UserProtocol(cboType.SelectedIndex, 
                                                  datVon.NullableValue, datBis.NullableValue,
                                                  datAtVon.NullableValue, datAtBis.NullableValue,
                                                  txtUser.Text, txtAuCode.Text);
                ProtocolCarrier frm = new ProtocolCarrier(p.DataSet);
                frm.Show();
            }
            catch (Exception x) { new Error(x); }
        }
        
#region I N T E R N A L S
        
        private void CheckPeriod()
        {
        
            if (!datVon.NullableValue.HasValue) throw new ApplicationException("Geben Sie das Startdatum an.");
            if (!datBis.NullableValue.HasValue) throw new ApplicationException("Geben Sie das Enddatum an.");
            if (datVon.NullableValue > datBis.NullableValue) throw new ApplicationException("Das Enddatum darf nicht vor dem Startdatum liegen.");
        }
        
#endregion

    }
}

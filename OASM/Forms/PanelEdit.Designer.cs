﻿namespace comain.Forms
{
    partial class EditPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditPanel));
            this.grpProfil = new System.Windows.Forms.GroupBox();
            this.btnPAdd = new System.Windows.Forms.Button();
            this.btnPEdit = new System.Windows.Forms.Button();
            this.btnPDel = new System.Windows.Forms.Button();
            this.btnPNew = new System.Windows.Forms.Button();
            this.cboProfil = new System.Windows.Forms.ComboBox();
            this.bndProfil = new System.Windows.Forms.BindingSource(this.components);
            this.grpSelect = new System.Windows.Forms.GroupBox();
            this.btnRows = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.grpHistory = new System.Windows.Forms.GroupBox();
            this.btnRedo = new System.Windows.Forms.Button();
            this.Images = new System.Windows.Forms.ImageList(this.components);
            this.btnUndo = new System.Windows.Forms.Button();
            this.grpGoto = new System.Windows.Forms.GroupBox();
            this.btnGoToActive = new System.Windows.Forms.LinkLabel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboIH = new System.Windows.Forms.ComboBox();
            this.cboAU = new System.Windows.Forms.ComboBox();
            this.grpTools = new System.Windows.Forms.GroupBox();
            this.btnTypeD = new System.Windows.Forms.Button();
            this.cboAmount = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnTypeS = new System.Windows.Forms.Button();
            this.btnTypeT = new System.Windows.Forms.Button();
            this.btnTypeO = new System.Windows.Forms.Button();
            this.btnTypeX = new System.Windows.Forms.Button();
            this.ToolTips = new System.Windows.Forms.ToolTip(this.components);
            this.cboRSBis = new System.Windows.Forms.ComboBox();
            this.cboRSVon = new System.Windows.Forms.ComboBox();
            this.master1 = new comain.User.Master();
            this.grpImplState = new System.Windows.Forms.GroupBox();
            this.btnImplState = new System.Windows.Forms.LinkLabel();
            this.btnImplStateComplete = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.grdStats = new System.Windows.Forms.FlowLayoutPanel();
            this.grpProfil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bndProfil)).BeginInit();
            this.grpSelect.SuspendLayout();
            this.grpHistory.SuspendLayout();
            this.grpGoto.SuspendLayout();
            this.grpTools.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.master1)).BeginInit();
            this.grpImplState.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpProfil
            // 
            this.grpProfil.Controls.Add(this.btnPAdd);
            this.grpProfil.Controls.Add(this.btnPEdit);
            this.grpProfil.Controls.Add(this.btnPDel);
            this.grpProfil.Controls.Add(this.btnPNew);
            this.grpProfil.Controls.Add(this.cboProfil);
            this.grpProfil.Location = new System.Drawing.Point(308, 5);
            this.grpProfil.Name = "grpProfil";
            this.grpProfil.Size = new System.Drawing.Size(169, 111);
            this.grpProfil.TabIndex = 7;
            this.grpProfil.TabStop = false;
            this.grpProfil.Text = "Profil";
            // 
            // btnPAdd
            // 
            this.btnPAdd.BackColor = System.Drawing.Color.Orange;
            this.btnPAdd.FlatAppearance.BorderSize = 0;
            this.btnPAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnPAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnPAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPAdd.Location = new System.Drawing.Point(87, 79);
            this.btnPAdd.Name = "btnPAdd";
            this.btnPAdd.Size = new System.Drawing.Size(75, 23);
            this.btnPAdd.TabIndex = 4;
            this.btnPAdd.Text = "Anfügen";
            this.ToolTips.SetToolTip(this.btnPAdd, "Gewählte Aufträge ergänzen diese im Auftragsprofil");
            this.btnPAdd.UseVisualStyleBackColor = false;
            this.btnPAdd.Click += new System.EventHandler(this.AddToProfile);
            // 
            // btnPEdit
            // 
            this.btnPEdit.BackColor = System.Drawing.Color.Orange;
            this.btnPEdit.FlatAppearance.BorderSize = 0;
            this.btnPEdit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnPEdit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnPEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPEdit.Location = new System.Drawing.Point(6, 79);
            this.btnPEdit.Name = "btnPEdit";
            this.btnPEdit.Size = new System.Drawing.Size(75, 23);
            this.btnPEdit.TabIndex = 3;
            this.btnPEdit.Text = "Ersetzen";
            this.ToolTips.SetToolTip(this.btnPEdit, "Gewählte Aufträge ersetzen diese im Auftragsprofil");
            this.btnPEdit.UseVisualStyleBackColor = false;
            this.btnPEdit.Click += new System.EventHandler(this.ReplaceProfile);
            // 
            // btnPDel
            // 
            this.btnPDel.BackColor = System.Drawing.Color.Orange;
            this.btnPDel.FlatAppearance.BorderSize = 0;
            this.btnPDel.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnPDel.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnPDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPDel.Location = new System.Drawing.Point(87, 50);
            this.btnPDel.Name = "btnPDel";
            this.btnPDel.Size = new System.Drawing.Size(75, 23);
            this.btnPDel.TabIndex = 2;
            this.btnPDel.Text = "Löschen";
            this.ToolTips.SetToolTip(this.btnPDel, "Auftragsprofil löschen");
            this.btnPDel.UseVisualStyleBackColor = false;
            this.btnPDel.Click += new System.EventHandler(this.DeleteProfile);
            // 
            // btnPNew
            // 
            this.btnPNew.BackColor = System.Drawing.Color.Orange;
            this.btnPNew.FlatAppearance.BorderSize = 0;
            this.btnPNew.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnPNew.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnPNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPNew.Location = new System.Drawing.Point(6, 50);
            this.btnPNew.Name = "btnPNew";
            this.btnPNew.Size = new System.Drawing.Size(75, 23);
            this.btnPNew.TabIndex = 1;
            this.btnPNew.Text = "Anlegen";
            this.ToolTips.SetToolTip(this.btnPNew, "Neues Auftragsprofil erstellen");
            this.btnPNew.UseVisualStyleBackColor = false;
            this.btnPNew.Click += new System.EventHandler(this.CreateProfile);
            // 
            // cboProfil
            // 
            this.cboProfil.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboProfil.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cboProfil.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboProfil.DataSource = this.bndProfil;
            this.cboProfil.DisplayMember = "name";
            this.cboProfil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboProfil.FormattingEnabled = true;
            this.cboProfil.Location = new System.Drawing.Point(6, 23);
            this.cboProfil.MaxDropDownItems = 30;
            this.cboProfil.Name = "cboProfil";
            this.cboProfil.Size = new System.Drawing.Size(156, 21);
            this.cboProfil.TabIndex = 0;
            this.ToolTips.SetToolTip(this.cboProfil, "Auftragsprofil wählen");
            this.cboProfil.ValueMember = "idprofile";
            this.cboProfil.DropDown += new System.EventHandler(this.cboProfil_DropDown);
            // 
            // grpSelect
            // 
            this.grpSelect.Controls.Add(this.btnRows);
            this.grpSelect.Controls.Add(this.btnUpdate);
            this.grpSelect.Controls.Add(this.btnSelect);
            this.grpSelect.Location = new System.Drawing.Point(76, 5);
            this.grpSelect.Name = "grpSelect";
            this.grpSelect.Size = new System.Drawing.Size(91, 111);
            this.grpSelect.TabIndex = 6;
            this.grpSelect.TabStop = false;
            this.grpSelect.Text = "Aufträge";
            // 
            // btnRows
            // 
            this.btnRows.BackColor = System.Drawing.Color.Orange;
            this.btnRows.FlatAppearance.BorderSize = 0;
            this.btnRows.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnRows.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnRows.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRows.Location = new System.Drawing.Point(10, 50);
            this.btnRows.Name = "btnRows";
            this.btnRows.Size = new System.Drawing.Size(75, 23);
            this.btnRows.TabIndex = 1;
            this.btnRows.Text = "Zeilenhöhen";
            this.ToolTips.SetToolTip(this.btnRows, "Zeilenhöhen an Auftragskurzbeschreibung anpassen (Spaltenbreiten können vorher mi" +
        "t der Maus angepaßt werden)");
            this.btnRows.UseVisualStyleBackColor = false;
            this.btnRows.Click += new System.EventHandler(this.RowsClick);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.LightGreen;
            this.btnUpdate.FlatAppearance.BorderSize = 0;
            this.btnUpdate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.MediumTurquoise;
            this.btnUpdate.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUpdate.Location = new System.Drawing.Point(10, 79);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Eintragen";
            this.ToolTips.SetToolTip(this.btnUpdate, "Geänderte Termine eintragen");
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.UpdateClick);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.Orange;
            this.btnSelect.FlatAppearance.BorderSize = 0;
            this.btnSelect.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnSelect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelect.Location = new System.Drawing.Point(10, 21);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(75, 23);
            this.btnSelect.TabIndex = 0;
            this.btnSelect.Text = "Alle EIN";
            this.ToolTips.SetToolTip(this.btnSelect, "Alle Aufträge für ein neues Profil auswählen");
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.SelectClick);
            // 
            // grpHistory
            // 
            this.grpHistory.Controls.Add(this.btnRedo);
            this.grpHistory.Controls.Add(this.btnUndo);
            this.grpHistory.Location = new System.Drawing.Point(5, 5);
            this.grpHistory.Name = "grpHistory";
            this.grpHistory.Size = new System.Drawing.Size(65, 111);
            this.grpHistory.TabIndex = 5;
            this.grpHistory.TabStop = false;
            this.grpHistory.Text = "Historie";
            // 
            // btnRedo
            // 
            this.btnRedo.AutoEllipsis = true;
            this.btnRedo.FlatAppearance.BorderSize = 0;
            this.btnRedo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnRedo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnRedo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRedo.ImageIndex = 1;
            this.btnRedo.ImageList = this.Images;
            this.btnRedo.Location = new System.Drawing.Point(34, 20);
            this.btnRedo.Name = "btnRedo";
            this.btnRedo.Size = new System.Drawing.Size(22, 23);
            this.btnRedo.TabIndex = 1;
            this.ToolTips.SetToolTip(this.btnRedo, "Letzte Änderung wiederherstellen");
            this.btnRedo.UseVisualStyleBackColor = false;
            this.btnRedo.Click += new System.EventHandler(this.RedoClick);
            // 
            // Images
            // 
            this.Images.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Images.ImageStream")));
            this.Images.TransparentColor = System.Drawing.Color.Magenta;
            this.Images.Images.SetKeyName(0, "Edit_Undo.bmp");
            this.Images.Images.SetKeyName(1, "Edit_Redo.bmp");
            this.Images.Images.SetKeyName(2, "reload.png");
            this.Images.Images.SetKeyName(3, "impl.PNG");
            this.Images.Images.SetKeyName(4, "implS.PNG");
            this.Images.Images.SetKeyName(5, "implT.PNG");
            this.Images.Images.SetKeyName(6, "delete.png");
            this.Images.Images.SetKeyName(7, "Cut.bmp");
            // 
            // btnUndo
            // 
            this.btnUndo.AutoEllipsis = true;
            this.btnUndo.FlatAppearance.BorderSize = 0;
            this.btnUndo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnUndo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnUndo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUndo.ImageIndex = 0;
            this.btnUndo.ImageList = this.Images;
            this.btnUndo.Location = new System.Drawing.Point(6, 20);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(22, 23);
            this.btnUndo.TabIndex = 0;
            this.ToolTips.SetToolTip(this.btnUndo, "Letzte Änderung rückgängig");
            this.btnUndo.UseVisualStyleBackColor = false;
            this.btnUndo.Click += new System.EventHandler(this.UndoClick);
            // 
            // grpGoto
            // 
            this.grpGoto.Controls.Add(this.btnGoToActive);
            this.grpGoto.Controls.Add(this.label2);
            this.grpGoto.Controls.Add(this.label1);
            this.grpGoto.Controls.Add(this.cboIH);
            this.grpGoto.Controls.Add(this.cboAU);
            this.grpGoto.Location = new System.Drawing.Point(483, 5);
            this.grpGoto.Name = "grpGoto";
            this.grpGoto.Size = new System.Drawing.Size(196, 111);
            this.grpGoto.TabIndex = 9;
            this.grpGoto.TabStop = false;
            this.grpGoto.Text = "Gehe zu";
            // 
            // btnGoToActive
            // 
            this.btnGoToActive.ActiveLinkColor = System.Drawing.Color.Coral;
            this.btnGoToActive.AutoSize = true;
            this.btnGoToActive.LinkColor = System.Drawing.Color.Navy;
            this.btnGoToActive.Location = new System.Drawing.Point(63, 84);
            this.btnGoToActive.Name = "btnGoToActive";
            this.btnGoToActive.Size = new System.Drawing.Size(124, 13);
            this.btnGoToActive.TabIndex = 4;
            this.btnGoToActive.TabStop = true;
            this.btnGoToActive.Text = "Gehe zu aktiven Auftrag";
            this.btnGoToActive.VisitedLinkColor = System.Drawing.Color.Navy;
            this.btnGoToActive.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnGoToActive_LinkClicked);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "IH-Objekt";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Auftrag";
            // 
            // cboIH
            // 
            this.cboIH.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboIH.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cboIH.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboIH.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboIH.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboIH.FormattingEnabled = true;
            this.cboIH.Location = new System.Drawing.Point(66, 50);
            this.cboIH.MaxDropDownItems = 30;
            this.cboIH.Name = "cboIH";
            this.cboIH.Size = new System.Drawing.Size(124, 21);
            this.cboIH.TabIndex = 1;
            this.ToolTips.SetToolTip(this.cboIH, "Springe zum IH-Objekt");
            this.cboIH.DropDown += new System.EventHandler(this.ListObjects);
            this.cboIH.SelectedIndexChanged += new System.EventHandler(this.ClickObjects);
            // 
            // cboAU
            // 
            this.cboAU.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboAU.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cboAU.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboAU.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboAU.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboAU.FormattingEnabled = true;
            this.cboAU.Location = new System.Drawing.Point(66, 23);
            this.cboAU.MaxDropDownItems = 30;
            this.cboAU.Name = "cboAU";
            this.cboAU.Size = new System.Drawing.Size(124, 21);
            this.cboAU.TabIndex = 0;
            this.ToolTips.SetToolTip(this.cboAU, "Springe zum Auftrag");
            this.cboAU.DropDown += new System.EventHandler(this.ListOrders);
            this.cboAU.SelectedIndexChanged += new System.EventHandler(this.ClickOrders);
            // 
            // grpTools
            // 
            this.grpTools.Controls.Add(this.btnTypeD);
            this.grpTools.Controls.Add(this.cboAmount);
            this.grpTools.Controls.Add(this.label3);
            this.grpTools.Controls.Add(this.btnTypeS);
            this.grpTools.Controls.Add(this.btnTypeT);
            this.grpTools.Controls.Add(this.btnTypeO);
            this.grpTools.Controls.Add(this.btnTypeX);
            this.grpTools.Location = new System.Drawing.Point(173, 5);
            this.grpTools.Name = "grpTools";
            this.grpTools.Size = new System.Drawing.Size(129, 111);
            this.grpTools.TabIndex = 10;
            this.grpTools.TabStop = false;
            this.grpTools.Text = "Werkzeuge";
            // 
            // btnTypeD
            // 
            this.btnTypeD.BackColor = System.Drawing.Color.Orange;
            this.btnTypeD.FlatAppearance.BorderSize = 0;
            this.btnTypeD.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnTypeD.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnTypeD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTypeD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTypeD.ImageKey = "Cut.bmp";
            this.btnTypeD.ImageList = this.Images;
            this.btnTypeD.Location = new System.Drawing.Point(12, 50);
            this.btnTypeD.Name = "btnTypeD";
            this.btnTypeD.Size = new System.Drawing.Size(22, 22);
            this.btnTypeD.TabIndex = 6;
            this.ToolTips.SetToolTip(this.btnTypeD, "Termine oder Realisierungen löschen");
            this.btnTypeD.UseVisualStyleBackColor = false;
            this.btnTypeD.Click += new System.EventHandler(this.TypeDClick);
            // 
            // cboAmount
            // 
            this.cboAmount.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cboAmount.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboAmount.FormattingEnabled = true;
            this.cboAmount.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cboAmount.Location = new System.Drawing.Point(54, 81);
            this.cboAmount.MaxDropDownItems = 11;
            this.cboAmount.Name = "cboAmount";
            this.cboAmount.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cboAmount.Size = new System.Drawing.Size(64, 21);
            this.cboAmount.TabIndex = 5;
            this.ToolTips.SetToolTip(this.cboAmount, "Anzahl erfolgter / anzuwendender Realisierungen");
            this.cboAmount.SelectedIndexChanged += new System.EventHandler(this.AmountChanged);
            this.cboAmount.Validating += new System.ComponentModel.CancelEventHandler(this.cboAmount_Validating);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Anzahl";
            // 
            // btnTypeS
            // 
            this.btnTypeS.BackColor = System.Drawing.Color.Orange;
            this.btnTypeS.FlatAppearance.BorderSize = 0;
            this.btnTypeS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnTypeS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnTypeS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTypeS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTypeS.Location = new System.Drawing.Point(96, 21);
            this.btnTypeS.Name = "btnTypeS";
            this.btnTypeS.Size = new System.Drawing.Size(22, 22);
            this.btnTypeS.TabIndex = 3;
            this.btnTypeS.Text = "S";
            this.ToolTips.SetToolTip(this.btnTypeS, "Sonderleistung eintragen");
            this.btnTypeS.UseVisualStyleBackColor = false;
            this.btnTypeS.Click += new System.EventHandler(this.TypeSClick);
            // 
            // btnTypeT
            // 
            this.btnTypeT.BackColor = System.Drawing.Color.Orange;
            this.btnTypeT.FlatAppearance.BorderSize = 0;
            this.btnTypeT.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnTypeT.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnTypeT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTypeT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTypeT.Location = new System.Drawing.Point(68, 21);
            this.btnTypeT.Name = "btnTypeT";
            this.btnTypeT.Size = new System.Drawing.Size(22, 22);
            this.btnTypeT.TabIndex = 2;
            this.btnTypeT.Text = "T";
            this.ToolTips.SetToolTip(this.btnTypeT, "Tagestermin planen");
            this.btnTypeT.UseVisualStyleBackColor = false;
            this.btnTypeT.Click += new System.EventHandler(this.TypeTClick);
            // 
            // btnTypeO
            // 
            this.btnTypeO.BackColor = System.Drawing.Color.Orange;
            this.btnTypeO.FlatAppearance.BorderSize = 0;
            this.btnTypeO.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnTypeO.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnTypeO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTypeO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTypeO.Location = new System.Drawing.Point(40, 21);
            this.btnTypeO.Name = "btnTypeO";
            this.btnTypeO.Size = new System.Drawing.Size(22, 22);
            this.btnTypeO.TabIndex = 1;
            this.btnTypeO.Text = "O";
            this.ToolTips.SetToolTip(this.btnTypeO, "Termin realisieren");
            this.btnTypeO.UseVisualStyleBackColor = false;
            this.btnTypeO.Click += new System.EventHandler(this.TypeOClick);
            // 
            // btnTypeX
            // 
            this.btnTypeX.BackColor = System.Drawing.Color.Orange;
            this.btnTypeX.FlatAppearance.BorderSize = 0;
            this.btnTypeX.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnTypeX.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnTypeX.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTypeX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTypeX.Location = new System.Drawing.Point(12, 21);
            this.btnTypeX.Name = "btnTypeX";
            this.btnTypeX.Size = new System.Drawing.Size(22, 22);
            this.btnTypeX.TabIndex = 0;
            this.btnTypeX.Text = "X";
            this.ToolTips.SetToolTip(this.btnTypeX, "Termin planen");
            this.btnTypeX.UseVisualStyleBackColor = false;
            this.btnTypeX.Click += new System.EventHandler(this.TypeXClick);
            // 
            // cboRSBis
            // 
            this.cboRSBis.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboRSBis.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cboRSBis.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboRSBis.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRSBis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboRSBis.FormattingEnabled = true;
            this.cboRSBis.Location = new System.Drawing.Point(37, 50);
            this.cboRSBis.MaxDropDownItems = 30;
            this.cboRSBis.Name = "cboRSBis";
            this.cboRSBis.Size = new System.Drawing.Size(140, 21);
            this.cboRSBis.TabIndex = 1;
            this.ToolTips.SetToolTip(this.cboRSBis, "Springe zum IH-Objekt");
            // 
            // cboRSVon
            // 
            this.cboRSVon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboRSVon.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cboRSVon.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboRSVon.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRSVon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboRSVon.FormattingEnabled = true;
            this.cboRSVon.Location = new System.Drawing.Point(37, 23);
            this.cboRSVon.MaxDropDownItems = 30;
            this.cboRSVon.Name = "cboRSVon";
            this.cboRSVon.Size = new System.Drawing.Size(140, 21);
            this.cboRSVon.TabIndex = 0;
            this.ToolTips.SetToolTip(this.cboRSVon, "Springe zum Auftrag");
            // 
            // master1
            // 
            this.master1.DataSetName = "Master";
            this.master1.Prefix = "comain.User";
            this.master1.SchemaSerializationMode = System.Data.SchemaSerializationMode.ExcludeSchema;
            // 
            // grpImplState
            // 
            this.grpImplState.Controls.Add(this.btnImplState);
            this.grpImplState.Controls.Add(this.btnImplStateComplete);
            this.grpImplState.Controls.Add(this.label4);
            this.grpImplState.Controls.Add(this.label5);
            this.grpImplState.Controls.Add(this.cboRSBis);
            this.grpImplState.Controls.Add(this.cboRSVon);
            this.grpImplState.Location = new System.Drawing.Point(685, 5);
            this.grpImplState.Name = "grpImplState";
            this.grpImplState.Size = new System.Drawing.Size(183, 111);
            this.grpImplState.TabIndex = 11;
            this.grpImplState.TabStop = false;
            this.grpImplState.Text = "Realisierungsstatus";
            // 
            // btnImplState
            // 
            this.btnImplState.ActiveLinkColor = System.Drawing.Color.Coral;
            this.btnImplState.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnImplState.AutoSize = true;
            this.btnImplState.LinkColor = System.Drawing.Color.Navy;
            this.btnImplState.Location = new System.Drawing.Point(119, 84);
            this.btnImplState.Name = "btnImplState";
            this.btnImplState.Size = new System.Drawing.Size(58, 13);
            this.btnImplState.TabIndex = 5;
            this.btnImplState.TabStop = true;
            this.btnImplState.Text = "Berechnen";
            this.btnImplState.VisitedLinkColor = System.Drawing.Color.Navy;
            this.btnImplState.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnImplState_LinkClicked);
            // 
            // btnImplStateComplete
            // 
            this.btnImplStateComplete.ActiveLinkColor = System.Drawing.Color.Coral;
            this.btnImplStateComplete.AutoSize = true;
            this.btnImplStateComplete.LinkColor = System.Drawing.Color.Navy;
            this.btnImplStateComplete.Location = new System.Drawing.Point(6, 84);
            this.btnImplStateComplete.Name = "btnImplStateComplete";
            this.btnImplStateComplete.Size = new System.Drawing.Size(39, 13);
            this.btnImplStateComplete.TabIndex = 4;
            this.btnImplStateComplete.TabStop = true;
            this.btnImplStateComplete.Text = "Aktuell";
            this.btnImplStateComplete.VisitedLinkColor = System.Drawing.Color.Navy;
            this.btnImplStateComplete.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnImplStateCurrent_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "bis";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "von";
            // 
            // grdStats
            // 
            this.grdStats.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdStats.AutoScroll = true;
            this.grdStats.BackColor = System.Drawing.Color.AliceBlue;
            this.grdStats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.grdStats.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.grdStats.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.grdStats.Location = new System.Drawing.Point(874, 10);
            this.grdStats.Margin = new System.Windows.Forms.Padding(0);
            this.grdStats.Name = "grdStats";
            this.grdStats.Size = new System.Drawing.Size(236, 104);
            this.grdStats.TabIndex = 12;
            // 
            // EditPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.Controls.Add(this.grdStats);
            this.Controls.Add(this.grpImplState);
            this.Controls.Add(this.grpTools);
            this.Controls.Add(this.grpGoto);
            this.Controls.Add(this.grpProfil);
            this.Controls.Add(this.grpSelect);
            this.Controls.Add(this.grpHistory);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "EditPanel";
            this.Size = new System.Drawing.Size(1113, 120);
            this.grpProfil.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bndProfil)).EndInit();
            this.grpSelect.ResumeLayout(false);
            this.grpHistory.ResumeLayout(false);
            this.grpGoto.ResumeLayout(false);
            this.grpGoto.PerformLayout();
            this.grpTools.ResumeLayout(false);
            this.grpTools.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.master1)).EndInit();
            this.grpImplState.ResumeLayout(false);
            this.grpImplState.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpProfil;
        private System.Windows.Forms.Button btnPAdd;
        private System.Windows.Forms.Button btnPEdit;
        private System.Windows.Forms.Button btnPDel;
        private System.Windows.Forms.Button btnPNew;
        private System.Windows.Forms.GroupBox grpSelect;
        private System.Windows.Forms.Button btnRows;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.GroupBox grpHistory;
        private System.Windows.Forms.Button btnRedo;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.BindingSource bndProfil;
        private System.Windows.Forms.ImageList Images;
        private System.Windows.Forms.GroupBox grpGoto;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboIH;
        private System.Windows.Forms.ComboBox cboAU;
        private System.Windows.Forms.GroupBox grpTools;
        private System.Windows.Forms.Button btnTypeS;
        private System.Windows.Forms.Button btnTypeT;
        private System.Windows.Forms.Button btnTypeO;
        private System.Windows.Forms.ComboBox cboAmount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnTypeD;
        private System.Windows.Forms.ToolTip ToolTips;
        private System.Windows.Forms.Button btnTypeX;
        internal System.Windows.Forms.ComboBox cboProfil;
        private comain.User.Master master1;
        private System.Windows.Forms.LinkLabel btnGoToActive;
        private System.Windows.Forms.GroupBox grpImplState;
        private System.Windows.Forms.LinkLabel btnImplState;
        private System.Windows.Forms.LinkLabel btnImplStateComplete;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cboRSBis;
        private System.Windows.Forms.ComboBox cboRSVon;
        private System.Windows.Forms.FlowLayoutPanel grdStats;
    }
}

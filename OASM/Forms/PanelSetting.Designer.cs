﻿namespace comain.Forms
{
    partial class SettingPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingPanel));
            this.grpTerminPlan = new System.Windows.Forms.GroupBox();
            this.cboYear = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.grpOpPlan = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPlan = new System.Windows.Forms.TextBox();
            this.radPlan2 = new System.Windows.Forms.RadioButton();
            this.radPlan1 = new System.Windows.Forms.RadioButton();
            this.radPlan0 = new System.Windows.Forms.RadioButton();
            this.datPlan = new CO.Forms.DTPWithCalWeek();
            this.grpGrey = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radGreyP = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.radGreyPR = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGrey = new System.Windows.Forms.TextBox();
            this.radGrey2 = new System.Windows.Forms.RadioButton();
            this.radGrey1 = new System.Windows.Forms.RadioButton();
            this.radGrey0 = new System.Windows.Forms.RadioButton();
            this.datGrey = new CO.Forms.DTPWithCalWeek();
            this.btnMaster = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.grpTerminPlan.SuspendLayout();
            this.grpOpPlan.SuspendLayout();
            this.grpGrey.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpTerminPlan
            // 
            this.grpTerminPlan.Controls.Add(this.cboYear);
            this.grpTerminPlan.Controls.Add(this.label4);
            this.grpTerminPlan.Location = new System.Drawing.Point(3, 6);
            this.grpTerminPlan.Name = "grpTerminPlan";
            this.grpTerminPlan.Size = new System.Drawing.Size(141, 111);
            this.grpTerminPlan.TabIndex = 3;
            this.grpTerminPlan.TabStop = false;
            this.grpTerminPlan.Text = "Strategische Planung";
            // 
            // cboYear
            // 
            this.cboYear.FormattingEnabled = true;
            this.cboYear.Location = new System.Drawing.Point(43, 23);
            this.cboYear.MaxDropDownItems = 30;
            this.cboYear.Name = "cboYear";
            this.cboYear.Size = new System.Drawing.Size(84, 21);
            this.cboYear.TabIndex = 0;
            this.cboYear.Validating += new System.ComponentModel.CancelEventHandler(this.cboYear_Validating);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Jahr";
            // 
            // grpOpPlan
            // 
            this.grpOpPlan.Controls.Add(this.label1);
            this.grpOpPlan.Controls.Add(this.txtPlan);
            this.grpOpPlan.Controls.Add(this.radPlan2);
            this.grpOpPlan.Controls.Add(this.radPlan1);
            this.grpOpPlan.Controls.Add(this.radPlan0);
            this.grpOpPlan.Controls.Add(this.datPlan);
            this.grpOpPlan.Location = new System.Drawing.Point(150, 6);
            this.grpOpPlan.Name = "grpOpPlan";
            this.grpOpPlan.Size = new System.Drawing.Size(252, 111);
            this.grpOpPlan.TabIndex = 4;
            this.grpOpPlan.TabStop = false;
            this.grpOpPlan.Text = "Operative Planung (Zugriff durch Meister)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(141, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Wochen voraus";
            // 
            // txtPlan
            // 
            this.txtPlan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPlan.Location = new System.Drawing.Point(104, 76);
            this.txtPlan.Name = "txtPlan";
            this.txtPlan.Size = new System.Drawing.Size(31, 21);
            this.txtPlan.TabIndex = 17;
            this.txtPlan.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radPlan2
            // 
            this.radPlan2.AutoSize = true;
            this.radPlan2.Location = new System.Drawing.Point(6, 78);
            this.radPlan2.Name = "radPlan2";
            this.radPlan2.Size = new System.Drawing.Size(75, 17);
            this.radPlan2.TabIndex = 16;
            this.radPlan2.Text = "dynamisch";
            this.radPlan2.UseVisualStyleBackColor = true;
            // 
            // radPlan1
            // 
            this.radPlan1.AutoSize = true;
            this.radPlan1.Location = new System.Drawing.Point(6, 52);
            this.radPlan1.Name = "radPlan1";
            this.radPlan1.Size = new System.Drawing.Size(89, 17);
            this.radPlan1.TabIndex = 15;
            this.radPlan1.Text = "festes Datum";
            this.radPlan1.UseVisualStyleBackColor = true;
            // 
            // radPlan0
            // 
            this.radPlan0.AutoSize = true;
            this.radPlan0.Checked = true;
            this.radPlan0.Location = new System.Drawing.Point(6, 24);
            this.radPlan0.Name = "radPlan0";
            this.radPlan0.Size = new System.Drawing.Size(56, 17);
            this.radPlan0.TabIndex = 14;
            this.radPlan0.TabStop = true;
            this.radPlan0.Text = "inaktiv";
            this.radPlan0.UseVisualStyleBackColor = true;
            // 
            // datPlan
            // 
            this.datPlan.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datPlan.CalendarForeColor = System.Drawing.SystemColors.ControlText;
            this.datPlan.CalendarMonthBackground = System.Drawing.SystemColors.Window;
            this.datPlan.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datPlan.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.datPlan.CalendarTrailingForeColor = System.Drawing.SystemColors.GrayText;
            this.datPlan.CustomFormat = "\'Inaktiv\'";
            this.datPlan.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datPlan.Location = new System.Drawing.Point(104, 50);
            this.datPlan.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.datPlan.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.datPlan.Name = "datPlan";
            this.datPlan.NullableValue = null;
            this.datPlan.NullValue = "Inaktiv";
            this.datPlan.ResetToMonday = true;
            this.datPlan.Size = new System.Drawing.Size(139, 21);
            this.datPlan.TabIndex = 13;
            this.datPlan.Value = ((object)(resources.GetObject("datPlan.Value")));
            // 
            // grpGrey
            // 
            this.grpGrey.BackColor = System.Drawing.Color.WhiteSmoke;
            this.grpGrey.Controls.Add(this.panel1);
            this.grpGrey.Controls.Add(this.label2);
            this.grpGrey.Controls.Add(this.txtGrey);
            this.grpGrey.Controls.Add(this.radGrey2);
            this.grpGrey.Controls.Add(this.radGrey1);
            this.grpGrey.Controls.Add(this.radGrey0);
            this.grpGrey.Controls.Add(this.datGrey);
            this.grpGrey.Location = new System.Drawing.Point(408, 6);
            this.grpGrey.Name = "grpGrey";
            this.grpGrey.Size = new System.Drawing.Size(407, 111);
            this.grpGrey.TabIndex = 5;
            this.grpGrey.TabStop = false;
            this.grpGrey.Text = "Grauzonenplanung (Zugriff durch Auftragnehmer)";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radGreyP);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.radGreyPR);
            this.panel1.Location = new System.Drawing.Point(240, 18);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(161, 82);
            this.panel1.TabIndex = 24;
            // 
            // radGreyP
            // 
            this.radGreyP.AutoSize = true;
            this.radGreyP.Location = new System.Drawing.Point(19, 33);
            this.radGreyP.Name = "radGreyP";
            this.radGreyP.Size = new System.Drawing.Size(127, 17);
            this.radGreyP.TabIndex = 1;
            this.radGreyP.Text = "nur geplante Termine";
            this.radGreyP.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(16, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(124, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "Summenberechnung";
            // 
            // radGreyPR
            // 
            this.radGreyPR.AutoSize = true;
            this.radGreyPR.Location = new System.Drawing.Point(19, 59);
            this.radGreyPR.Name = "radGreyPR";
            this.radGreyPR.Size = new System.Drawing.Size(138, 17);
            this.radGreyPR.TabIndex = 2;
            this.radGreyPR.Text = "geplante und realisierte";
            this.radGreyPR.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(141, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Wochen zurück";
            // 
            // txtGrey
            // 
            this.txtGrey.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGrey.Location = new System.Drawing.Point(101, 76);
            this.txtGrey.Name = "txtGrey";
            this.txtGrey.Size = new System.Drawing.Size(31, 21);
            this.txtGrey.TabIndex = 22;
            this.txtGrey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // radGrey2
            // 
            this.radGrey2.AutoSize = true;
            this.radGrey2.Location = new System.Drawing.Point(6, 78);
            this.radGrey2.Name = "radGrey2";
            this.radGrey2.Size = new System.Drawing.Size(75, 17);
            this.radGrey2.TabIndex = 21;
            this.radGrey2.Text = "dynamisch";
            this.radGrey2.UseVisualStyleBackColor = true;
            // 
            // radGrey1
            // 
            this.radGrey1.AutoSize = true;
            this.radGrey1.Location = new System.Drawing.Point(6, 52);
            this.radGrey1.Name = "radGrey1";
            this.radGrey1.Size = new System.Drawing.Size(89, 17);
            this.radGrey1.TabIndex = 20;
            this.radGrey1.Text = "festes Datum";
            this.radGrey1.UseVisualStyleBackColor = true;
            // 
            // radGrey0
            // 
            this.radGrey0.AutoSize = true;
            this.radGrey0.Checked = true;
            this.radGrey0.Location = new System.Drawing.Point(6, 24);
            this.radGrey0.Name = "radGrey0";
            this.radGrey0.Size = new System.Drawing.Size(56, 17);
            this.radGrey0.TabIndex = 19;
            this.radGrey0.TabStop = true;
            this.radGrey0.Text = "inaktiv";
            this.radGrey0.UseVisualStyleBackColor = true;
            // 
            // datGrey
            // 
            this.datGrey.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datGrey.CalendarForeColor = System.Drawing.SystemColors.ControlText;
            this.datGrey.CalendarMonthBackground = System.Drawing.SystemColors.Window;
            this.datGrey.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datGrey.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.datGrey.CalendarTrailingForeColor = System.Drawing.SystemColors.GrayText;
            this.datGrey.CustomFormat = "\'Inaktiv\'";
            this.datGrey.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datGrey.Location = new System.Drawing.Point(101, 50);
            this.datGrey.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.datGrey.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.datGrey.Name = "datGrey";
            this.datGrey.NullableValue = null;
            this.datGrey.NullValue = "Inaktiv";
            this.datGrey.ResetToMonday = true;
            this.datGrey.Size = new System.Drawing.Size(139, 21);
            this.datGrey.TabIndex = 11;
            this.datGrey.Value = ((object)(resources.GetObject("datGrey.Value")));
            // 
            // btnMaster
            // 
            this.btnMaster.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMaster.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnMaster.BackColor = System.Drawing.Color.Orange;
            this.btnMaster.FlatAppearance.BorderSize = 0;
            this.btnMaster.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnMaster.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnMaster.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMaster.Location = new System.Drawing.Point(854, 58);
            this.btnMaster.Name = "btnMaster";
            this.btnMaster.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnMaster.Size = new System.Drawing.Size(75, 23);
            this.btnMaster.TabIndex = 7;
            this.btnMaster.Text = "Meister";
            this.btnMaster.UseVisualStyleBackColor = false;
            this.btnMaster.Click += new System.EventHandler(this.OpenAdeptForm);
            // 
            // btnSettings
            // 
            this.btnSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSettings.BackColor = System.Drawing.Color.Orange;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnSettings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSettings.Location = new System.Drawing.Point(854, 87);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(75, 23);
            this.btnSettings.TabIndex = 6;
            this.btnSettings.Text = "Eintragen";
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.Click += new System.EventHandler(this.SaveClick);
            // 
            // btnExport
            // 
            this.btnExport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExport.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnExport.BackColor = System.Drawing.Color.Orange;
            this.btnExport.FlatAppearance.BorderSize = 0;
            this.btnExport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnExport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExport.Location = new System.Drawing.Point(854, 29);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 23);
            this.btnExport.TabIndex = 8;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.OpenExportForm);
            // 
            // SettingPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnMaster);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.grpTerminPlan);
            this.Controls.Add(this.grpOpPlan);
            this.Controls.Add(this.grpGrey);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "SettingPanel";
            this.Size = new System.Drawing.Size(941, 120);
            this.grpTerminPlan.ResumeLayout(false);
            this.grpTerminPlan.PerformLayout();
            this.grpOpPlan.ResumeLayout(false);
            this.grpOpPlan.PerformLayout();
            this.grpGrey.ResumeLayout(false);
            this.grpGrey.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpTerminPlan;
        private System.Windows.Forms.ComboBox cboYear;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox grpOpPlan;
        private System.Windows.Forms.GroupBox grpGrey;
        private CO.Forms.DTPWithCalWeek datGrey;
        private System.Windows.Forms.RadioButton radGreyPR;
        private System.Windows.Forms.RadioButton radGreyP;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnMaster;
        private System.Windows.Forms.Button btnSettings;
        private CO.Forms.DTPWithCalWeek datPlan;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPlan;
        private System.Windows.Forms.RadioButton radPlan2;
        private System.Windows.Forms.RadioButton radPlan1;
        private System.Windows.Forms.RadioButton radPlan0;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtGrey;
        private System.Windows.Forms.RadioButton radGrey2;
        private System.Windows.Forms.RadioButton radGrey1;
        private System.Windows.Forms.RadioButton radGrey0;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnExport;
    }
}

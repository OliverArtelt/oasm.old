﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using comain.Command;
using comain.Drawing;
using comain.Schedule;
using comain.User;
using comain.Workspace;
using System.Diagnostics;


namespace comain.Forms
{
   
    
    /// <summary>
    /// Hauptformular
    /// </summary>
    public partial class Main : Form, IMainFormUpdater
    {   

      
#region P R O P E R T I E S

        
        /// <summary>
        /// Aktueller Status des Formulars (angemeldet, abgemeldet...) 
        /// ist internal, damit Statusübergänge von des Zustandsobjekten eingetragen werden kann
        /// </summary>
        internal IMainFormState State;
        /// <summary>
        /// Filterobjekte der Panels zentral verwalten
        /// </summary>
        public FilterManager Filter { get { return myFilter; } }       

        LoginPanel    myLoginPanel;
        TypePanel     myTypePanel;
        SettingPanel  mySettingPanel;
        FilterPanel   myFilterPanel; 
        EditPanel     myEditPanel;
        ProtocolPanel myProtocolPanel;
        FilterManager myFilter;

        int lastTooltipXPos = 0;
        int lastTooltipYPos = 0;
                      
        
#endregion
        
#region C O N S T R U C T I O N 

        
        /// <summary>
        /// Hauptformular, Formularkomponenten initialisieren
        /// </summary>
        public Main()
        {
        
            InitializeComponent();
            
            try {
            
                myLoginPanel = new LoginPanel(this);
                myTypePanel = new TypePanel(this);
                myFilterPanel = new FilterPanel(this);
                myEditPanel = new EditPanel(this);
                mySettingPanel = new SettingPanel(this);
                myProtocolPanel = new ProtocolPanel(this);
            }
            catch (Exception x) { 
            
                new comain.Error(x);
                Application.Exit(); 
            }        
        }

        /// <summary>
        /// Notwendige Ressourcen aufsetzen, im Fehlerfall Abort
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainLoad(object sender, EventArgs e)
        {
        
            try {
            
                //Bearbeitermenü: Realisierungsmenge auf 1 
                MenuAmount.SelectedIndex = 0;
                SelectLoginPanel(null, null);
                txtFound.Text = String.Empty;
                Session.Current.CurrentYear = DateTime.Today.Year;  //ToDo: aus Voreinstellungen lesen
                
                //DB-Verbindungseinstellungen
                State = new LoggedOff(this);
                
                //DB-Verbindungseinstellungen
                DBListFactory dbl = new DBListFactory();
                myLoginPanel.Init(dbl.List);
            
            }
            catch (Exception x) { 
            
                new comain.Error(x);
                Application.Exit(); 
            }        
        }


#endregion

#region G R I D


        /// <summary>
        /// Plangrid scrollen: Neuzeichnen veranlassen, Kontextmenü schließen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridScroll(object sender, ScrollEventArgs e)
        {   
            
            if (KMenu.Visible) KMenu.Hide();
            if (e.ScrollOrientation == ScrollOrientation.VerticalScroll) return;
            
            Session.Current.ScrollXPosition = e.NewValue;
            
            Session.Current.IsScrolledHorizontally = true;
            tmrRedraw.Start();
            grdMain.Invalidate();
        }

        /// <summary>
        /// Überschriften erst nach horizontalem Scrollen zeichnen (Flackern unterdrücken)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerTick(object sender,EventArgs e)
        {

            tmrRedraw.Stop();
            Session.Current.IsScrolledHorizontally = false;
            grdMain.Invalidate();
        }

        /// <summary>
        /// Zeichen des Plangrids: Prüfen, ob Zelle von Anwendung selber gezeichnet werden muss und dieses vornehmen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPaint(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (Session.Current.Schedule == null) return;
            Session.Current.Schedule.AcceptOnce(new DataGridViewDrawer(sender, e), e.RowIndex);
        }

        /// <summary>
        /// Setzen der Checkbox / Kontextmenüevent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFormatData(object sender, DataGridViewCellFormattingEventArgs e)
        {
            
            if (Session.Current.Schedule == null) return;
            Session.Current.Schedule.AcceptOnce(new DataGridViewFormatter(sender, e), e.RowIndex);
        }

        /// <summary>
        /// Klick in Plangrids: Verwerfen, Kontextmenü öffnen oder Au-Checkbox setzen, aktive Zeile färben
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridRowEnter(object sender,DataGridViewCellEventArgs e)
        {
            
            if (Session.Current.Schedule == null) return;

            //aktive Zeile markieren
            if (Session.Current.CurrentPosition != e.RowIndex) {
            
                int oldRow = Session.Current.CurrentPosition;
                Session.Current.CurrentPosition = e.RowIndex;

                if (oldRow >= 0) grdMain.Rows[oldRow].Selected = false;
                grdMain.Rows[e.RowIndex].Selected = true;
                
                if (oldRow >= 0) grdMain.InvalidateRow(oldRow);    
            }
     
            if (e.RowIndex >= 0) grdMain.InvalidateRow(e.RowIndex);    
        }

        /// <summary>
        /// Klick in Plangrids: Verwerfen, Kontextmenü öffnen oder Au-Checkbox setzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
            if (Session.Current.Schedule == null) return;


            try {
            
                //Auftrags-Checkbox wird geklickt
                if (e.ColumnIndex == 0 && e.Button == MouseButtons.Left) {
                
                    Session.Current.Schedule.AcceptOnce(
                            new OrderClickProcessor(sender, Session.Current.CommandHistory), 
                            e.RowIndex);
                    UpdateCommandPanel();
                }
                
                //Termin wird bearbeitet
                if (e.ColumnIndex == Session.Current.Schedule.Traits.DateColumn) {
                
                    //Termin suchen
                    FindTermin myDate = new FindTermin(sender, e);
                    Session.Current.Schedule.AcceptOnce(myDate, e.RowIndex);
                    
                    //irgendwo in die Landschaft geklickt
                    if (myDate.Termin == null) return;
                                        
                    switch (e.Button) {
                    
                    case MouseButtons.Left:
                    
                        //mit gewähltem Werkzeug aus dem Werkzeugkasten arbeiten
                        //nochmaliger Klick auf eine Bearbeitung soll diese entfernen
                        if (myDate.Termin.EditedType == Session.Current.SelectedEditType)
                             EditDate(myDate.Termin, EditedType.Deleted, myDate.Termin.EditedMenge);
                        else EditDate(myDate.Termin, Session.Current.SelectedEditType, Session.Current.SelectedEditAmount);
                        break;
                    
                    case MouseButtons.Right:
                    
                        //Kontextmenü: Einwegwerkzeug nutzen
                        ShowMenu(myDate.Termin);
                        break;
                    }
                }
            }
            catch (Exception x) { new comain.Error(x);}
        }
        
        /// <summary>
        /// Grid zur Zeile scrollen
        /// </summary>
        /// <param name="position">erste darzustellende Zeile</param>
        public void ScrollTo(int position)
        {
        
            if (position == -1) return;
            grdMain.FirstDisplayedScrollingRowIndex = position;
        }

        /// <summary>
        /// Cellspanning: Überschriften neuzeichnen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridColumnWidthChanged(object sender,DataGridViewColumnEventArgs e)
        {
            grdMain.Invalidate();    
        }       
        
        public Control Grid
        {
            get { return grdMain; }
        }


#endregion

#region N A V I G A T I O N   A N D   G U I   F E E D B A C K


        /// <summary>
        /// Controls abh. von der Benutzerinteraktion einstellen (z.B. Undo-/Redobuttons de./aktivieren)
        /// </summary>
        private void UpdateCommandPanel()
        {
            try {
            
                myEditPanel.UpdatePanel();
            }
            catch (Exception x) { new comain.Error(x); }
        }
            
        /// <summary>
        /// Erfolg der Plangenerierung in Statusleiste darstellen
        /// </summary>
        /// <param name="amnt"></param>
        void UpdateFoundState(int amnt)
        {
        
            switch (amnt) {
            
            case -1:
            
                txtFound.Text = "";
                break;
            
            case 0:
            
                txtFound.Text = "Keine Aufträge gefunden.";
                break;
            
            case 1:
            
                txtFound.Text = "Ein Auftrag gefunden.";
                break;
            
            default:
            
                txtFound.Text = amnt + " Aufträge gefunden.";
                break;
            }
        }


        public void SetWorkProgress(int percent)
        {
        
            strProgress.ProgressBar.Value = percent;
        }
        
        
        public void SetWorkState(String message)
        {
        
            txtFound.Text = message; 
        }
        

        internal void EnablePanels(bool hasType, bool hasFilter, bool hasEdit, bool hasSetting)
        {
        
            btnPanelType.Enabled = hasType;    
            btnPanelType.BackColor = (hasType)? Color.PowderBlue: Color.Gainsboro;    
        
            btnPanelFilter.Enabled = hasFilter;    
            btnPanelFilter.BackColor = (hasFilter)? Color.PowderBlue: Color.Gainsboro;    
        
            btnPanelEdit.Enabled = hasEdit;    
            btnPanelEdit.BackColor = (hasEdit)? Color.PowderBlue: Color.Gainsboro;    
        
            btnPanelSetting.Enabled = hasSetting;    
            btnPanelSetting.BackColor = (hasSetting)? Color.PowderBlue: Color.Gainsboro;    
        
            btnPanelProtocol.Enabled = hasSetting;    
            btnPanelProtocol.BackColor = (hasSetting)? Color.PowderBlue: Color.Gainsboro;    
        }
        
        internal void SelectLoginPanel(object sender, EventArgs e)
        {
            SetPanel(myLoginPanel);
            SetButton(btnPanelLogin);
        }

        internal void SelectTypePanel(object sender, EventArgs e)
        {
            SetPanel(myTypePanel);
            SetButton(btnPanelType);
        }

        internal void SelectFilterPanel(object sender, EventArgs e)
        {
            SetPanel(myFilterPanel);
            SetButton(btnPanelFilter);
            myFilterPanel.Refresh();
        }

        internal void SelectEditPanel(object sender, EventArgs e)
        {
            SetPanel(myEditPanel);
            SetButton(btnPanelEdit);
        }

        internal void SelectSettingPanel(object sender, EventArgs e)
        {
            SetPanel(mySettingPanel);
            SetButton(btnPanelSetting);
        }

        internal void SelectProtocolPanel(object sender, EventArgs e)
        {
            SetPanel(myProtocolPanel);
            SetButton(btnPanelProtocol);
        }
        
        private void SetPanel(UserControl myPanel)
        {

            try {
            
                if (PanelHost.HasChildren) {
                
                    PanelHost.Controls[0].Visible = false;
                    PanelHost.Controls[0].Parent = null;
                }    
                    
                myPanel.Parent = PanelHost;
                myPanel.Dock = DockStyle.Fill;
                myPanel.Visible = true;
                
            }
            catch (Exception x) { new comain.Error(x); }
        }
        
        private void SetButton(Button activeButton)
        {
         
            btnPanelEdit.BackColor = (btnPanelEdit.Enabled)? Color.PowderBlue: Color.Gainsboro; 
            btnPanelSetting.BackColor = (btnPanelSetting.Enabled)? Color.PowderBlue: Color.Gainsboro; 
            btnPanelFilter.BackColor = (btnPanelFilter.Enabled)? Color.PowderBlue: Color.Gainsboro; 
            btnPanelType.BackColor = (btnPanelType.Enabled)? Color.PowderBlue: Color.Gainsboro; 
            btnPanelLogin.BackColor = (btnPanelLogin.Enabled)? Color.PowderBlue: Color.Gainsboro; 
            btnPanelProtocol.BackColor = (btnPanelSetting.Enabled)? Color.PowderBlue: Color.Gainsboro; 
         
            btnPanelEdit.FlatAppearance.MouseOverBackColor = Color.LightCyan; 
            btnPanelSetting.FlatAppearance.MouseOverBackColor = Color.LightCyan;
            btnPanelFilter.FlatAppearance.MouseOverBackColor = Color.LightCyan;
            btnPanelType.FlatAppearance.MouseOverBackColor = Color.LightCyan; 
            btnPanelLogin.FlatAppearance.MouseOverBackColor = Color.LightCyan;
            btnPanelProtocol.FlatAppearance.MouseOverBackColor = Color.LightCyan;

            activeButton.BackColor = Color.PaleGreen;
            activeButton.FlatAppearance.MouseOverBackColor = Color.Honeydew;
        }

        /// <summary>
        /// Statistiktabelle aktualisieren
        /// </summary>
        public void UpdateStatistics()
        {

            myEditPanel.FillStatistics();
        }
                
        void DisplayProgress(int percCompleted)
        {
        
            strProgress.ProgressBar.Value = percCompleted;        
        }

        public void Invalidate(int position)
        {
            grdMain.InvalidateRow(position);
        }
        
#endregion 
              
#region L O G I N


        /// <summary>
        /// Anmelden, Fehlerhandler erstellt Caller
        /// </summary>
        public void Login(String client, String user, String pass, bool adept)
        {
        
            Session.ResetNewUser();
            
            if (String.IsNullOrEmpty(client)) throw new ApplicationException("Geben Sie eine Datenbankverbindung an.");
            if (String.IsNullOrEmpty(user)) throw new ApplicationException("Geben Sie einen Benutzer an.");
            

            Session.Current.ReadUser(new Credentials { Client = client, IsClientUser = adept, Password = pass, Username = user });
            Session.Current.ReadSettings();

            //Statusübergang Mainform
            State.LoggedIn();
            SelectTypePanel(null, null);
            
            //Benutzereinstellungen aktualisieren
            comain.Properties.Settings.Default.AccessRole = adept;
            comain.Properties.Settings.Default.AccessDatabase = client;
            comain.Properties.Settings.Default.AccessUser = user;
            
            //Filterdaten laden
            FilterControls ctrls = new FilterControls { AuftragsListe = myFilterPanel.cboAU,
                                                        BaugruppenListe = myFilterPanel.cboBG,
                                                        IHObjektListe = myFilterPanel.cboIH,
                                                        Projektion = myFilterPanel.chkOutput,
                                                        SortierListe = myFilterPanel.cboSort,
                                                        ProfilBearbeitenListe = myEditPanel.cboProfil,
                                                        ProfilListe = myFilterPanel.cboPr,
                                                        PersonalListe = myFilterPanel.cboPS,
                                                        LeistungsartenListe = myFilterPanel.chkLA,
                                                        KostenstellenListe = myFilterPanel.cboKST,
                                                        InventarListe = myFilterPanel.cboInv,
                                                        PeriodeBis = myFilterPanel.datBis,
                                                        PeriodeVon = myFilterPanel.datVon,
                                                        StandortVonListe = myFilterPanel.cboPZVon,
                                                        StandortBisListe = myFilterPanel.cboPZBis };         
            myFilter = new FilterManager(ctrls);
            strProgress.ProgressBar.Maximum = 100;
            mySettingPanel.Init();
            myFilterPanel.Init();
            myTypePanel.Init();
            myProtocolPanel.Init();
            strProgress.ProgressBar.Value = 0;
            
            myLoginPanel.ClearLoginData();           
        }
        
        /// <summary>
        /// Abmelden, Fehlerhandler erstellt Caller
        /// </summary>
        public void Logout()
        {
        
            if (!ConfirmChanges()) return;
        
            //Statusübergang Mainform
            State.LoggedOut();

            //aktuullen Bearbeitungsstand zurücksetzen
            Session.Current.User = null;            
            Session.Current.Schedule = null;
            Session.Current.OASMSettings = null;
            Session.Current.ScheduleType = ScheduleType.None;
            grdMain.Rows.Clear();
            grdMain.Columns.Clear();
        }
        
        internal void SetLoginState(bool state) 
        { 
            myLoginPanel.SetState(state); 
        }
        
        public void UpdateConfiguration()
        {
            myTypePanel.Init();
        }
        

#endregion        

#region S C H E D U L E

        
        /// <summary>
        /// Schedule generieren
        /// </summary>
        public void CreateSchedule()
        {
            
            
            if (Session.Current.ScheduleType == ScheduleType.None) {
            
                MessageBox.Show("Wählen Sie zuerst ein Planungstyp aus.");
                SelectTypePanel(null, null);
                return;            
            }
            
                    
            try {  
           
           
                if (!ConfirmChanges()) return;
                
                //Bestehender Schedule entfernen
                grdMain.Rows.Clear();
                grdMain.Columns.Clear();

                //Planeigenschaften
                Session.Current.CreateSchedule(Filter.FilterValues, Filter.FilterDescription);    
                int countOrders = Session.Current.Schedule.CountOrders();
                
                //Formcontrols auf neuen Schedule einstellen
                Session.ResetNewSchedule();
                UpdateFoundState(countOrders);
                UpdateCommandPanel();
                Session.Current.CommandHistory = new CommandHistory();

                var cal = Session.Current.Schedule.Traits.Calendar;

                if (countOrders > 0) {
                
                    //Daten mit Widget verknüpfen
                    using (WidgetBuilder bld = new WidgetBuilder(grdMain, strProgress.ProgressBar)) {
                        
                        Session.Current.Schedule.Accept(bld);
                    }
                    
                    Filter.SaveFilter();
                    
                    //Status Schedule gewählt => Schedule bearbeiten
                    State.Schedule();
                    myEditPanel.SetPeriodLists();
                    myEditPanel.ResetImplPeriod(true);
                    SelectEditPanel(null, null);
                    Filter.ClearProfile();

                    grdMain.Focus();
                }   
            } 
            catch (Exception x) { new comain.Error(x); }
        }       
        
        /// <summary>
        /// Scheduletyp einstellen, damit Filterkriterien gewählt werden können
        /// </summary>
        /// <param name="type">zu verwendender Scheduletyp</param>
        /// <param name="name">Name des Schedules</param>
        public void SetSchedule(ScheduleType type, String name, int year)
        {
        
            if (!ConfirmChanges()) return;

            try {

                Session.Current.ScheduleType = type;
                Session.Current.CurrentYear = year;
                Session.Current.SelectedScheduleName = name;
                
                Session.Current.UserActions = new CommonAction(type);
                myFilterPanel.UpdatePanel(type);
                myEditPanel.Init();

                //aktuellen Schedule entfernen
                grdMain.Rows.Clear();
                grdMain.Columns.Clear();
                Session.Current.Schedule = null;
                UpdateCommandPanel();
                SelectFilterPanel(null, null);
                
                //Formstatus => Scheduletyp gewählt 
                State.LoggedIn();
            }
            catch (Exception x) { new comain.Error(x); }
        }                 
        
        /// <summary>
        /// Planänderungen in DB rückschreiben 
        /// </summary>
        public void UpdateSchedule()
        {

            try {
                       
                
                int changesCount = Session.Current.CountModifications();
                if (changesCount == 0) return;
                
                if (MessageBox.Show("Es werden " + changesCount + " Änderungen in die Datenbank zurückgeschrieben.\n" +
                                    "Wollen Sie fortfahren?", Application.ProductName, MessageBoxButtons.YesNo) == DialogResult.No) return; 

                //in DB eintragen
                Session.Current.SaveModifications();
                    
                //Plan neu aufbauen (Kontrolle des Datenbestandes in DB)
                Session.Current.CommandHistory = null;

                int v = myEditPanel.DeltaVon;
                int b = myEditPanel.DeltaBis;

                CreateSchedule();  
                
                myEditPanel.DeltaVon = v;
                myEditPanel.DeltaBis = b;

                myEditPanel.ResetImplPeriod(false);        
            }
            catch (Exception x) { new comain.Error(x); }
        }
        
        /// <summary>
        /// Wurde Plan bearbeitet? => Verwerfen bestätigen lassen
        /// </summary>
        /// <returns>true => Hau wech</returns>
        private bool ConfirmChanges()
        {
            
            if (Session.Current.Schedule == null) return true;
            if (Session.Current.CommandHistory == null) return true;
            if (!Session.Current.CommandHistory.HasModifications()) return true;
            
            if (MessageBox.Show("Es befinden sich noch ungespeicherte Änderungen im Plan. Fortfahren?", 
                                Application.ProductName, MessageBoxButtons.YesNo) == DialogResult.Yes) return true;
            return false;
        }
                
#endregion        
        
#region C O N T E X T   M E N U
        
        /// <summary>
        /// darzustellende Menüpunkte aushandeln
        /// </summary>
        /// <param name="obj">Auftrag</param>
        /// <param name="pos">Terminindex des Auftrages</param>
        private void ShowMenu(Termin currTermin)
        {
        
            //Terminobjekt im Menü speichern, damit später bei Auswahl eines Menüeintrages dieser nicht verloren geht 
            KMenu.Tag = currTermin;

            //mögliche Aktionen aushandeln
            IUserAction act = new SpecificAction(Session.Current.Schedule.Traits, currTermin);
            KMenu.Items["MenuPlan"].Enabled     = act.PlanType;
            KMenu.Items["MenuImpl"].Enabled     = act.ImplementType;
            KMenu.Items["MenuSpecial"].Enabled  = act.SpecialType;
            KMenu.Items["MenuDay"].Enabled      = act.DailyType;
            KMenu.Items["MenuDelete"].Enabled   = act.DeleteType;
            KMenu.Items["MenuAmount"].Enabled   = act.AmountType;

            //Menü am Cursor anzeigen, nach links rausgeschobene Spaltenteile berücksichtigen
            int titleBarHeight = Size.Height - ClientSize.Height;
            Point pt = Cursor.Position;            
            pt.X = pt.X - Left - grdMain.Left - 7;
            pt.Y = pt.Y - Top - SplitMain.SplitterDistance - titleBarHeight - 33;

            KMenu.Show(grdMain, pt);
        }

        /// <summary>
        /// Termin auf "geplant" setzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuPlanClick(object sender, EventArgs e)
        {
            EditDate(KMenu.Tag, EditedType.Planned, (short)(MenuAmount.SelectedIndex + 1));
        }
        /// <summary>
        /// Termin auf "realisiert" setzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuImplClick(object sender, EventArgs e)
        {
            EditDate(KMenu.Tag, EditedType.Common, (short)(MenuAmount.SelectedIndex + 1));
        }
        /// <summary>
        /// Termin auf "Sonderleistung" setzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuSpecialClick(object sender, EventArgs e)
        {
            EditDate(KMenu.Tag, EditedType.Special, (short)(MenuAmount.SelectedIndex + 1));
        }
        /// <summary>
        /// Termin auf "Tagestermin" setzen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuDayClick(object sender, EventArgs e)
        {
            EditDate(KMenu.Tag, EditedType.Daily, (short)(MenuAmount.SelectedIndex + 1));
        }
        /// <summary>
        /// Termineintrag löschen 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuDeleteClick(object sender, EventArgs e)
        {
            EditDate(KMenu.Tag, EditedType.Deleted, (short)(MenuAmount.SelectedIndex + 1));
        }        
        /// <summary>
        /// Terminänderung vornehmen
        /// </summary>
        /// <param name="obj">Termin, dessen Kontextmenü aufgerufen wurde</param>
        /// <param name="type">Termintyp</param>
        /// <param name="amnt">geplante/realisierte Anzahl</param>
        private void EditDate(object obj, EditedType type, short amnt)
        {
        
            Termin date = obj as Termin;   
            if (date == null) return;

            //Aktion auf Gültigkeit überprüfen
            IUserAction act = new SpecificAction(Session.Current.Schedule.Traits, date);
            if (type == EditedType.Planned && !act.PlanType) return;
            if (type == EditedType.Common && !act.ImplementType) return;
            if (type == EditedType.Daily && !act.DailyType) return;
            if (type == EditedType.Special && !act.SpecialType) return;
            if (type == EditedType.Deleted && !act.DeleteType) return;

            try {
                        
                comain.Command.ICommand cmd = new comain.Command.EditDateCommand(this, date, type, amnt);
                cmd.Execute(Session.Current.CommandHistory);
                UpdateCommandPanel();
            }
            catch (Exception x) { new comain.Error(x); }
        } 
               
        
#endregion

#region T O O L T I P   S U P P O R T
        
        
        private int currentToolTipWidth = 150;
        

        private void CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {

            if (e.ColumnIndex == Session.Current.Schedule.Traits.DateColumn) {
        
                Point rp = grdMain.PointToClient(Control.MousePosition);
        
                //Termin suchen
                MouseOverDate myDate = new MouseOverDate(sender, e.RowIndex, e.ColumnIndex, e.X, true);
                Session.Current.Schedule.AcceptOnce(myDate, e.RowIndex);
            
                if (myDate.Termin != null) {
            
                    String toolTipText = myDate.Termin.Explain();
                    int rows = toolTipText.Count(n => n == '\n') + 1;
                
                    //damit das Teil nicht flackert, wenn es rechts hinausgeschoben wird
                    int xOffset = rp.X + grdMain.Left - 10;
                    int xabs = xOffset + this.Left;
                    int xmax = Screen.FromControl(grdMain).WorkingArea.Right;
                    int xpos = (xabs >= xmax - currentToolTipWidth && xabs <= xmax)?    
                                xOffset - (currentToolTipWidth - xmax + xabs): xOffset;
                
                    int yOffset = rp.Y + SplitMain.SplitterDistance - 13 * rows + 30;

                    //Windows 7 entprellen: CellMouseMove wird auch gefeuert wenn keine Mausbewegung erfolgt
                    //http://stackoverflow.com/questions/3795659/tooltip-on-datagridview
                    if (xpos == lastTooltipXPos && yOffset == lastTooltipYPos) return;

                    lastTooltipXPos = xpos;
                    lastTooltipYPos = yOffset;

                    ControlTips.Show(toolTipText, this, xpos, yOffset);
            
                } else {
            
                    ControlTips.Hide(this);
                }

            } else if (e.ColumnIndex == Session.Current.Schedule.Traits.Columns.DAnzahlIndex) {

                ControlTips.Show(String.Empty, this, 0, 0);
            }
        }


        private void CellMouseLeave(object sender, DataGridViewCellEventArgs e)
        {
            ControlTips.Hide(this);
        }

        /// <summary>
        /// ToolTip-Breite auslesen, damit das Teil nicht flackert, wenn es rechts hinausgeschoben wird
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControlTips_Popup(object sender, PopupEventArgs e)
        {

            currentToolTipWidth = e.ToolTipSize.Width;
        }
        
#endregion

    }    
}

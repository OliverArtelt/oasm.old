﻿namespace comain.Forms
{
    partial class LoginPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpLogin = new System.Windows.Forms.GroupBox();
            this.chkAdept = new System.Windows.Forms.CheckBox();
            this.cboDB = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lnkImpress = new System.Windows.Forms.LinkLabel();
            this.ReportTip = new System.Windows.Forms.LinkLabel();
            this.grpLogin.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpLogin
            // 
            this.grpLogin.BackColor = System.Drawing.Color.SeaShell;
            this.grpLogin.Controls.Add(this.chkAdept);
            this.grpLogin.Controls.Add(this.cboDB);
            this.grpLogin.Controls.Add(this.label1);
            this.grpLogin.Controls.Add(this.txtPass);
            this.grpLogin.Controls.Add(this.label2);
            this.grpLogin.Controls.Add(this.txtUser);
            this.grpLogin.Controls.Add(this.label3);
            this.grpLogin.Controls.Add(this.btnLogin);
            this.grpLogin.Location = new System.Drawing.Point(3, 6);
            this.grpLogin.Name = "grpLogin";
            this.grpLogin.Size = new System.Drawing.Size(306, 111);
            this.grpLogin.TabIndex = 0;
            this.grpLogin.TabStop = false;
            // 
            // chkAdept
            // 
            this.chkAdept.AutoSize = true;
            this.chkAdept.Checked = global::comain.Properties.Settings.Default.AccessRole;
            this.chkAdept.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::comain.Properties.Settings.Default, "AccessRole", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.chkAdept.Location = new System.Drawing.Point(215, 49);
            this.chkAdept.Name = "chkAdept";
            this.chkAdept.Size = new System.Drawing.Size(78, 17);
            this.chkAdept.TabIndex = 2;
            this.chkAdept.Text = "Als Meister";
            this.chkAdept.UseVisualStyleBackColor = true;
            // 
            // cboDB
            // 
            this.cboDB.BackColor = System.Drawing.Color.PeachPuff;
            this.cboDB.Cursor = System.Windows.Forms.Cursors.Default;
            this.cboDB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDB.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cboDB.FormattingEnabled = true;
            this.cboDB.Location = new System.Drawing.Point(72, 20);
            this.cboDB.MaxDropDownItems = 20;
            this.cboDB.Name = "cboDB";
            this.cboDB.Size = new System.Drawing.Size(218, 21);
            this.cboDB.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Datenbank";
            // 
            // txtPass
            // 
            this.txtPass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPass.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtPass.Font = new System.Drawing.Font("Wingdings", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.txtPass.Location = new System.Drawing.Point(72, 76);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = 'l';
            this.txtPass.Size = new System.Drawing.Size(112, 20);
            this.txtPass.TabIndex = 3;
            this.txtPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PassPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Bearbeiter";
            // 
            // txtUser
            // 
            this.txtUser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUser.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtUser.Location = new System.Drawing.Point(72, 48);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(112, 21);
            this.txtUser.TabIndex = 1;
            this.txtUser.KeyDown += new System.Windows.Forms.KeyEventHandler(this.UserPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(5, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Passwort";
            // 
            // btnLogin
            // 
            this.btnLogin.BackColor = System.Drawing.Color.Orange;
            this.btnLogin.FlatAppearance.BorderSize = 0;
            this.btnLogin.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnLogin.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.Location = new System.Drawing.Point(215, 72);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(75, 23);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Anmelden";
            this.btnLogin.UseVisualStyleBackColor = false;
            this.btnLogin.Click += new System.EventHandler(this.LoginClick);
            // 
            // lnkImpress
            // 
            this.lnkImpress.ActiveLinkColor = System.Drawing.Color.Coral;
            this.lnkImpress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lnkImpress.AutoSize = true;
            this.lnkImpress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lnkImpress.LinkColor = System.Drawing.Color.Navy;
            this.lnkImpress.Location = new System.Drawing.Point(868, 61);
            this.lnkImpress.Name = "lnkImpress";
            this.lnkImpress.Size = new System.Drawing.Size(59, 13);
            this.lnkImpress.TabIndex = 0;
            this.lnkImpress.TabStop = true;
            this.lnkImpress.Text = "Impressum";
            this.lnkImpress.VisitedLinkColor = System.Drawing.Color.Navy;
            this.lnkImpress.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.OpenImpressum);
            // 
            // ReportTip
            // 
            this.ReportTip.ActiveLinkColor = System.Drawing.Color.Coral;
            this.ReportTip.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ReportTip.AutoSize = true;
            this.ReportTip.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ReportTip.LinkColor = System.Drawing.Color.Navy;
            this.ReportTip.Location = new System.Drawing.Point(816, 83);
            this.ReportTip.Name = "ReportTip";
            this.ReportTip.Size = new System.Drawing.Size(111, 13);
            this.ReportTip.TabIndex = 0;
            this.ReportTip.TabStop = true;
            this.ReportTip.Text = "Bearbeitungshinweise";
            this.ReportTip.VisitedLinkColor = System.Drawing.Color.Navy;
            this.ReportTip.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.ReportTip_LinkClicked);
            // 
            // LoginPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.Controls.Add(this.ReportTip);
            this.Controls.Add(this.lnkImpress);
            this.Controls.Add(this.grpLogin);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "LoginPanel";
            this.Size = new System.Drawing.Size(941, 120);
            this.grpLogin.ResumeLayout(false);
            this.grpLogin.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel lnkImpress;
        private System.Windows.Forms.CheckBox chkAdept;
        private System.Windows.Forms.GroupBox grpLogin;
        private System.Windows.Forms.Button btnLogin;
        internal System.Windows.Forms.ComboBox cboDB;
        private System.Windows.Forms.LinkLabel ReportTip;
    }
}

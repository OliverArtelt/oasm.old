﻿using System.Windows.Forms;


namespace comain.Forms
{

    /// <summary>
    /// Info About...-Box anzeigen
    /// </summary>
    public partial class Impressum : Form
    {

        /// <summary>
        /// Info About...-Box anzeigen
        /// </summary>
        public Impressum()
        {
            
            InitializeComponent();
            
            lblVersion.Text = Application.ProductVersion;
        }
    }
}

﻿namespace comain.Forms
{
    partial class ProtocolPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProtocolPanel));
            this.btnShow = new System.Windows.Forms.Button();
            this.grpPeriod = new System.Windows.Forms.GroupBox();
            this.datBis = new CO.Forms.DTPWithCalWeek();
            this.lblBis = new System.Windows.Forms.Label();
            this.lblVon = new System.Windows.Forms.Label();
            this.datVon = new CO.Forms.DTPWithCalWeek();
            this.btnDelete = new System.Windows.Forms.Button();
            this.grpFilter = new System.Windows.Forms.GroupBox();
            this.txtUser = new System.Windows.Forms.TextBox();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grpDates = new System.Windows.Forms.GroupBox();
            this.datAtBis = new CO.Forms.DTPWithCalWeek();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.datAtVon = new CO.Forms.DTPWithCalWeek();
            this.txtAuCode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.grpPeriod.SuspendLayout();
            this.grpFilter.SuspendLayout();
            this.grpDates.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnShow
            // 
            this.btnShow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnShow.BackColor = System.Drawing.Color.Orange;
            this.btnShow.FlatAppearance.BorderSize = 0;
            this.btnShow.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnShow.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnShow.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnShow.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnShow.Location = new System.Drawing.Point(854, 87);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 23);
            this.btnShow.TabIndex = 4;
            this.btnShow.Text = "Anzeigen";
            this.btnShow.UseVisualStyleBackColor = false;
            this.btnShow.Click += new System.EventHandler(this.ProtocolShow);
            // 
            // grpPeriod
            // 
            this.grpPeriod.Controls.Add(this.datBis);
            this.grpPeriod.Controls.Add(this.lblBis);
            this.grpPeriod.Controls.Add(this.lblVon);
            this.grpPeriod.Controls.Add(this.datVon);
            this.grpPeriod.Location = new System.Drawing.Point(3, 6);
            this.grpPeriod.Name = "grpPeriod";
            this.grpPeriod.Size = new System.Drawing.Size(195, 111);
            this.grpPeriod.TabIndex = 0;
            this.grpPeriod.TabStop = false;
            this.grpPeriod.Text = "Zeitraum";
            // 
            // datBis
            // 
            this.datBis.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datBis.CalendarForeColor = System.Drawing.SystemColors.ControlText;
            this.datBis.CalendarMonthBackground = System.Drawing.SystemColors.Window;
            this.datBis.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datBis.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.datBis.CalendarTrailingForeColor = System.Drawing.SystemColors.GrayText;
            this.datBis.CustomFormat = " ";
            this.datBis.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datBis.Location = new System.Drawing.Point(44, 53);
            this.datBis.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.datBis.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.datBis.Name = "datBis";
            this.datBis.NullableValue = null;
            this.datBis.NullValue = "";
            this.datBis.Size = new System.Drawing.Size(139, 21);
            this.datBis.TabIndex = 16;
            this.datBis.Value = ((object)(resources.GetObject("datBis.Value")));
            // 
            // lblBis
            // 
            this.lblBis.AutoSize = true;
            this.lblBis.Location = new System.Drawing.Point(13, 56);
            this.lblBis.Name = "lblBis";
            this.lblBis.Size = new System.Drawing.Size(20, 13);
            this.lblBis.TabIndex = 15;
            this.lblBis.Text = "bis";
            // 
            // lblVon
            // 
            this.lblVon.AutoSize = true;
            this.lblVon.Location = new System.Drawing.Point(13, 29);
            this.lblVon.Name = "lblVon";
            this.lblVon.Size = new System.Drawing.Size(25, 13);
            this.lblVon.TabIndex = 14;
            this.lblVon.Text = "von";
            // 
            // datVon
            // 
            this.datVon.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datVon.CalendarForeColor = System.Drawing.SystemColors.ControlText;
            this.datVon.CalendarMonthBackground = System.Drawing.SystemColors.Window;
            this.datVon.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datVon.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.datVon.CalendarTrailingForeColor = System.Drawing.SystemColors.GrayText;
            this.datVon.CustomFormat = " ";
            this.datVon.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datVon.Location = new System.Drawing.Point(44, 26);
            this.datVon.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.datVon.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.datVon.Name = "datVon";
            this.datVon.NullableValue = null;
            this.datVon.NullValue = "";
            this.datVon.Size = new System.Drawing.Size(139, 21);
            this.datVon.TabIndex = 13;
            this.datVon.Value = ((object)(resources.GetObject("datVon.Value")));
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.BackColor = System.Drawing.Color.Orange;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDelete.Location = new System.Drawing.Point(854, 29);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 3;
            this.btnDelete.Text = "Löschen";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.ProtocolDelete);
            // 
            // grpFilter
            // 
            this.grpFilter.Controls.Add(this.txtUser);
            this.grpFilter.Controls.Add(this.cboType);
            this.grpFilter.Controls.Add(this.label1);
            this.grpFilter.Controls.Add(this.label2);
            this.grpFilter.Location = new System.Drawing.Point(204, 6);
            this.grpFilter.Name = "grpFilter";
            this.grpFilter.Size = new System.Drawing.Size(196, 111);
            this.grpFilter.TabIndex = 1;
            this.grpFilter.TabStop = false;
            this.grpFilter.Text = "Filter";
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(62, 26);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(122, 21);
            this.txtUser.TabIndex = 16;
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "<alles>",
            "Anmelden",
            "Planung alles",
            "Planungsänderung",
            "Konfiguration"});
            this.cboType.Location = new System.Drawing.Point(62, 53);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(121, 21);
            this.cboType.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Benutzer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Typ";
            // 
            // grpDates
            // 
            this.grpDates.Controls.Add(this.datAtBis);
            this.grpDates.Controls.Add(this.label4);
            this.grpDates.Controls.Add(this.label5);
            this.grpDates.Controls.Add(this.datAtVon);
            this.grpDates.Controls.Add(this.txtAuCode);
            this.grpDates.Controls.Add(this.label3);
            this.grpDates.Location = new System.Drawing.Point(406, 6);
            this.grpDates.Name = "grpDates";
            this.grpDates.Size = new System.Drawing.Size(232, 111);
            this.grpDates.TabIndex = 2;
            this.grpDates.TabStop = false;
            this.grpDates.Text = "Filter Termine / Realisierungen";
            // 
            // datAtBis
            // 
            this.datAtBis.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datAtBis.CalendarForeColor = System.Drawing.SystemColors.ControlText;
            this.datAtBis.CalendarMonthBackground = System.Drawing.SystemColors.Window;
            this.datAtBis.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datAtBis.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.datAtBis.CalendarTrailingForeColor = System.Drawing.SystemColors.GrayText;
            this.datAtBis.CustomFormat = "\'<alles>\'";
            this.datAtBis.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datAtBis.Location = new System.Drawing.Point(84, 80);
            this.datAtBis.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.datAtBis.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.datAtBis.Name = "datAtBis";
            this.datAtBis.NullableValue = null;
            this.datAtBis.NullValue = "<alles>";
            this.datAtBis.Size = new System.Drawing.Size(139, 21);
            this.datAtBis.TabIndex = 21;
            this.datAtBis.Value = ((object)(resources.GetObject("datAtBis.Value")));
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Termine bis";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Termine von";
            // 
            // datAtVon
            // 
            this.datAtVon.CalendarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datAtVon.CalendarForeColor = System.Drawing.SystemColors.ControlText;
            this.datAtVon.CalendarMonthBackground = System.Drawing.SystemColors.Window;
            this.datAtVon.CalendarTitleBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.datAtVon.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.datAtVon.CalendarTrailingForeColor = System.Drawing.SystemColors.GrayText;
            this.datAtVon.CustomFormat = "\'<alles>\'";
            this.datAtVon.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datAtVon.Location = new System.Drawing.Point(84, 53);
            this.datAtVon.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.datAtVon.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.datAtVon.Name = "datAtVon";
            this.datAtVon.NullableValue = null;
            this.datAtVon.NullValue = "<alles>";
            this.datAtVon.Size = new System.Drawing.Size(139, 21);
            this.datAtVon.TabIndex = 18;
            this.datAtVon.Value = ((object)(resources.GetObject("datAtVon.Value")));
            // 
            // txtAuCode
            // 
            this.txtAuCode.Location = new System.Drawing.Point(84, 26);
            this.txtAuCode.Name = "txtAuCode";
            this.txtAuCode.Size = new System.Drawing.Size(139, 21);
            this.txtAuCode.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Auftragscode";
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.Color.Orange;
            this.btnReset.FlatAppearance.BorderSize = 0;
            this.btnReset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Coral;
            this.btnReset.FlatAppearance.MouseOverBackColor = System.Drawing.Color.PeachPuff;
            this.btnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReset.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnReset.Location = new System.Drawing.Point(854, 58);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 5;
            this.btnReset.Text = "Alles";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.FilterReset);
            // 
            // ProtocolPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LemonChiffon;
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.grpDates);
            this.Controls.Add(this.grpFilter);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.grpPeriod);
            this.Controls.Add(this.btnShow);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "ProtocolPanel";
            this.Size = new System.Drawing.Size(941, 120);
            this.grpPeriod.ResumeLayout(false);
            this.grpPeriod.PerformLayout();
            this.grpFilter.ResumeLayout(false);
            this.grpFilter.PerformLayout();
            this.grpDates.ResumeLayout(false);
            this.grpDates.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.GroupBox grpPeriod;
        private CO.Forms.DTPWithCalWeek datVon;
        private System.Windows.Forms.Label lblBis;
        private System.Windows.Forms.Label lblVon;
        private CO.Forms.DTPWithCalWeek datBis;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.GroupBox grpFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUser;
        private System.Windows.Forms.ComboBox cboType;
        private System.Windows.Forms.GroupBox grpDates;
        private CO.Forms.DTPWithCalWeek datAtBis;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private CO.Forms.DTPWithCalWeek datAtVon;
        private System.Windows.Forms.TextBox txtAuCode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnReset;
    }
}

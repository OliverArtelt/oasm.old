﻿using System;
using System.Drawing;
using System.Windows.Forms;
using comain.Statistics;


namespace comain.Forms
{
    
    public partial class StatsEntryView : UserControl
    {
        
        public StatsEntryView(TotalEntry entry)
        {
            
            InitializeComponent();

            lblDate.Text  = entry.Period;
            lblName.Text  = entry.Name;
            lblValue.Text = entry.Value;

            switch (entry.Type) {

                case TotalEntry.EntryType.DeltaPositive:

                    lblValue.BackColor = Color.PaleGreen;
                    lblValue.ForeColor = Color.Black;
                    break; 

                case TotalEntry.EntryType.DeltaNegative:

                    lblValue.BackColor = Color.MistyRose;
                    lblValue.ForeColor = Color.Black;
                    break; 

                case TotalEntry.EntryType.DeltaUnspecified:

                    lblValue.BackColor = Color.LightYellow;
                    lblValue.ForeColor = Color.Black;
                    break; 

                default:

                    lblValue.BackColor = Color.White;
                    lblValue.ForeColor = Color.Black;
                    break;
            }
        }
    }
}

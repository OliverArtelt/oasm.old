﻿namespace comain.Reports
{
    partial class ProtocolCarrier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProtocolCarrier));
            this.bndLog = new System.Windows.Forms.BindingSource(this.components);
            this.strMain = new System.Windows.Forms.StatusStrip();
            this.Viewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dsProtocol = new comain.User.Protocol();
            ((System.ComponentModel.ISupportInitialize)(this.bndLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsProtocol)).BeginInit();
            this.SuspendLayout();
            // 
            // bndLog
            // 
            this.bndLog.DataMember = "Log";
            this.bndLog.DataSource = this.dsProtocol;
            // 
            // strMain
            // 
            this.strMain.Location = new System.Drawing.Point(0, 483);
            this.strMain.Name = "strMain";
            this.strMain.Size = new System.Drawing.Size(700, 22);
            this.strMain.TabIndex = 1;
            this.strMain.Text = "statusStrip1";
            // 
            // Viewer
            // 
            this.Viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "Log";
            reportDataSource1.Value = this.bndLog;
            this.Viewer.LocalReport.DataSources.Add(reportDataSource1);
            this.Viewer.LocalReport.ReportEmbeddedResource = "comain.Reports.Protocol.rdlc";
            this.Viewer.Location = new System.Drawing.Point(0, 0);
            this.Viewer.Name = "Viewer";
            this.Viewer.Size = new System.Drawing.Size(700, 483);
            this.Viewer.TabIndex = 2;
            // 
            // dsProtocol
            // 
            this.dsProtocol.DataSetName = "Protocol";
            this.dsProtocol.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ProtocolCarrier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 505);
            this.Controls.Add(this.Viewer);
            this.Controls.Add(this.strMain);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ProtocolCarrier";
            this.Text = "OASM Bearbeitungsprotokoll";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ProtocolReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bndLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsProtocol)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip strMain;
        private System.Windows.Forms.BindingSource bndLog;
        private Microsoft.Reporting.WinForms.ReportViewer Viewer;
        private comain.User.Protocol dsProtocol;
    }
}
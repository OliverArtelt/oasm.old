﻿namespace comain.Reports
{
    partial class KWSumCarrier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KWSumCarrier));
            this.bndKWSum = new System.Windows.Forms.BindingSource(this.components);
            this.strMain = new System.Windows.Forms.StatusStrip();
            this.Viewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.dsKWSum = new comain.Reports.KWSum();
            ((System.ComponentModel.ISupportInitialize)(this.bndKWSum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsKWSum)).BeginInit();
            this.SuspendLayout();
            // 
            // bndKWSum
            // 
            this.bndKWSum.DataMember = "Weeks";
            this.bndKWSum.DataSource = this.dsKWSum;
            // 
            // strMain
            // 
            this.strMain.Location = new System.Drawing.Point(0, 468);
            this.strMain.Name = "strMain";
            this.strMain.Size = new System.Drawing.Size(671, 22);
            this.strMain.TabIndex = 2;
            this.strMain.Text = "statusStrip1";
            // 
            // Viewer
            // 
            this.Viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "KWSum_Weeks";
            reportDataSource1.Value = this.bndKWSum;
            this.Viewer.LocalReport.DataSources.Add(reportDataSource1);
            this.Viewer.LocalReport.ReportEmbeddedResource = "comain.Reports.KWSum.rdlc";
            this.Viewer.Location = new System.Drawing.Point(0, 0);
            this.Viewer.Name = "Viewer";
            this.Viewer.Size = new System.Drawing.Size(671, 468);
            this.Viewer.TabIndex = 3;
            // 
            // dsKWSum
            // 
            this.dsKWSum.DataSetName = "KWSum";
            this.dsKWSum.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // KWSumCarrier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(671, 490);
            this.Controls.Add(this.Viewer);
            this.Controls.Add(this.strMain);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "KWSumCarrier";
            this.Text = "OASM Kalenderwochensummen";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.KWSumReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bndKWSum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsKWSum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip strMain;
        private Microsoft.Reporting.WinForms.ReportViewer Viewer;
        private System.Windows.Forms.BindingSource bndKWSum;
        private KWSum dsKWSum;
    }
}
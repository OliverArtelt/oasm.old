using Microsoft.Reporting.WinForms;


namespace comain.Reports
{


    /// <summary>
    /// Deutsche Meldungen des Berichtsbetrachters
    /// </summary>
    public class GermanLocalizedMessages : IReportViewerMessages
    {

        #region IReportViewerMessages Members


        public string BackButtonToolTip
        {
            get { return "Zur�ck"; }
        }

        public string BackMenuItemText
        {
            get { return "Zur�ck"; }
        }

        public string ChangeCredentialsText
        {
            get { return ""; }
        }

        public string CurrentPageTextBoxToolTip
        {
            get { return "Aktuelle Seite"; }
        }

        public string DocumentMapButtonToolTip
        {
            get { return "Navigation ein-/ausblenden"; }
        }

        public string DocumentMapMenuItemText
        {
            get { return "Navigation"; }
        }

        public string ExportButtonToolTip
        {
            get { return "Bericht exportieren nach..."; }
        }

        public string ExportMenuItemText
        {
            get { return "Exportieren"; }
        }

        public string FalseValueText
        {
            get { return "falsch"; }
        }

        public string FindButtonText
        {
            get { return "Text suchen"; }
        }

        public string FindButtonToolTip
        {
            get { return "Text im Bericht suchen"; }
        }

        public string FindNextButtonText
        {
            get { return "Weiter"; }
        }

        public string FindNextButtonToolTip
        {
            get { return "N�chstes Vorkommen suchen"; }
        }

        public string FirstPageButtonToolTip
        {
            get { return "Erste Seite"; }
        }

        public string LastPageButtonToolTip
        {
            get { return "Letzte Seite"; }
        }

        public string NextPageButtonToolTip
        {
            get { return "N�chste Seite"; }
        }

        public string NoMoreMatches
        {
            get { return "Keine weiteren Vorkommen gefunden"; }
        }

        public string NullCheckBoxText
        {
            get { return "Leer"; }
        }

        public string NullCheckBoxToolTip
        {
            get { return "Dieser Wert ist leer"; }
        }

        public string NullValueText
        {
            get { return "Leer"; }
        }

        public string PageOf
        {
            get { return "von"; }
        }

        public string PageSetupButtonToolTip
        {
            get { return "Seite einrichten"; }
        }

        public string PageSetupMenuItemText
        {
            get { return "Einrichten"; }
        }

        public string ParameterAreaButtonToolTip
        {
            get { return "Suchkriterien eingeben"; }
        }

        public string PasswordPrompt
        {
            get { return "Passwort"; }
        }

        public string PreviousPageButtonToolTip
        {
            get { return "Vorige Seite"; }
        }

        public string PrintButtonToolTip
        {
            get { return "Bericht drucken"; }
        }

        public string PrintLayoutButtonToolTip
        {
            get { return "Seitenansicht"; }
        }

        public string PrintLayoutMenuItemText
        {
            get { return "Seitenansicht"; }
        }

        public string PrintMenuItemText
        {
            get { return "Drucken"; }
        }

        public string ProgressText
        {
            get { return "Bitte warten"; }
        }

        public string RefreshButtonToolTip
        {
            get { return "Daten neu abrufen und Bericht neu formattieren."; }
        }

        public string RefreshMenuItemText
        {
            get { return "Neu laden"; }
        }

        public string SearchTextBoxToolTip
        {
            get { return "Suchen"; }
        }

        public string SelectAValue
        {
            get { return "Wert ausw�hlen"; }
        }

        public string SelectAll
        {
            get { return "Alles ausw�hlen"; }
        }

        public string StopButtonToolTip
        {
            get { return "Berichtserstellung abbrechen"; }
        }

        public string StopMenuItemText
        {
            get { return "Abbrechen"; }
        }

        public string TextNotFound
        {
            get { return "Text nicht gefunden."; }
        }

        public string TotalPagesToolTip
        {
            get { return "Gesamtanzahl Seiten"; }
        }

        public string TrueValueText
        {
            get { return "wahr"; }
        }

        public string UserNamePrompt
        {
            get { return "Benutzer"; }
        }

        public string ViewReportButtonText
        {
            get { return "Berichtsvorschau"; }
        }

        public string ViewReportButtonToolTip
        {
            get { return "Bericht auf dem Bildschirm anzeigen"; }
        }

        public string ZoomControlToolTip
        {
            get { return "Berichtsansicht vergr��ern"; }
        }

        public string ZoomMenuItemText
        {
            get { return "Vergr��ern"; }
        }

        public string ZoomToPageWidth
        {
            get { return "Seitenbreite"; }
        }

        public string ZoomToWholePage
        {
            get { return "Ganze Seite"; }
        }

        #endregion
    }
}

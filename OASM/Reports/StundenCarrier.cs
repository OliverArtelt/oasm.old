﻿using System;
using System.Linq;
using System.Windows.Forms;
using comain.Schedule;
using comain.Workspace;
using Microsoft.Reporting.WinForms;


namespace comain.Reports
{

    public partial class StundenCarrier:Form
    {
        
        public StundenCarrier(bool mitLA)
        {
            
            InitializeComponent();
            this.Viewer.LocalReport.ReportEmbeddedResource = mitLA? "comain.Reports.StundenLAReport.rdlc": "comain.Reports.StundenReport.rdlc";
        }


        private void StundenCarrier_Load(object sender, System.EventArgs e)
        {
            
            var coll = new StundenCollector(Session.Current.Schedule.Traits);
            Session.Current.Schedule.Accept(coll);
            var result = coll.GetResult();

            Viewer.Messages = new GermanLocalizedMessages();
            var par = new ReportParameter("parFilter", Session.Current.Schedule.Traits.Name);
            Viewer.LocalReport.SetParameters(new ReportParameter[] { par });
            Viewer.ProcessingMode = ProcessingMode.Local;
            Viewer.LocalReport.DataSources.Clear();
            Viewer.LocalReport.DataSources.Add(new ReportDataSource("MainData", result));
            Viewer.LocalReport.DisplayName = "Wochenstunden " + Session.Current.Schedule.Traits.Name;

            Viewer.RefreshReport();
        }
    }
}

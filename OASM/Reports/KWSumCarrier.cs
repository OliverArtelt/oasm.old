﻿using System;
using System.Windows.Forms;
using comain.Workspace;
using Microsoft.Reporting.WinForms;


namespace comain.Reports
{
    
    public partial class KWSumCarrier : Form
    {
        
        /// <summary>
        /// Träger für Bericht Kalenderwochensummen
        /// </summary>
        public KWSumCarrier()
        {
            
            InitializeComponent();
        }

        private void KWSumReport_Load(object sender, EventArgs e)
        {

            Viewer.Messages = new GermanLocalizedMessages();
            dsKWSum = new KWSum();
            dsKWSum.Fill();
            bndKWSum.DataSource = dsKWSum;
            
            ReportParameter p1 = new ReportParameter("parUser",   Session.Current.User.Name);
            ReportParameter p2 = new ReportParameter("parFilter", Session.Current.Schedule.Traits.FilterDescription);
            ReportParameter p3 = new ReportParameter("parHeader", Application.ProductName + " Kalenderwochensummen " + Session.Current.Database);
            ReportParameter p4 = new ReportParameter("parPlan",   Session.Current.Schedule.Traits.Name);
            ReportParameter p5 = new ReportParameter("parGrey",   Session.Current.OASMSettings.AsText);
            
            Viewer.LocalReport.SetParameters(new ReportParameter[] {p1, p2, p3, p4, p5});
            Viewer.RefreshReport();
        }
    }
}

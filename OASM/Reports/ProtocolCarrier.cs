﻿using System;
using System.Windows.Forms;
using comain.User;
using comain.Workspace;
using Microsoft.Reporting.WinForms;


namespace comain.Reports
{
    
    public partial class ProtocolCarrier : Form
    {

        String   myHeader;
        Protocol myData;
        
        /// <summary>
        /// Träger für Protokollbericht
        /// </summary>
        public ProtocolCarrier(Protocol data)
        {
            
            InitializeComponent();
            
            myHeader = String.Format("{0} Ereignisprotokoll {1}", Application.ProductName, Session.Current.Database);
            myData = data;
        }

        /// <summary>
        /// Bericht rendern
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ProtocolReport_Load(object sender, EventArgs e)
        {

            Viewer.Messages = new GermanLocalizedMessages();
            bndLog.DataSource = myData;
            object[] o = myData.Description.Rows[0].ItemArray;
            
            ReportParameter p1 = new ReportParameter("parPeriod", o[2].ToString());
            ReportParameter p2 = new ReportParameter("parFilter", o[1].ToString());
            ReportParameter p3 = new ReportParameter("parHeader", myHeader);
            Viewer.LocalReport.SetParameters(new ReportParameter[] {p1, p2, p3});
            
            Viewer.RefreshReport();
        }
    }
}
